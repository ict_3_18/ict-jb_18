<?php
/*$currentCookieParams = session_get_cookie_params();  
session_set_cookie_params(    
    0,//expires at end of session  
    $currentCookieParams['path'],//path  
    $currentCookieParams['domain'],//domain  
    $secure=true, // secure
    $httponly=true // httponly
); */
session_start();
require("./config/config.inc.php");
require_once(WAY . "/includes/autoload.inc.php");
require_once(WAY . "/includes/head.inc.php");

//print_r($_SESSION);
?>


   <div class="row">
      <div class="header">
         <h3>Connexion</h3>
      </div>
   </div>
   
   <div class="panel panel-primary">
      <div class="panel-heading">
         Connexion au portail ICT-JB pour experts, maîtres d'apprentissage et enseignants
      </div>
      <div class="panel-body">
         <form id="connexion_form">
            <div class="form-group row">
               <label for="email_per" class="col-sm-2 col-form-label">E-mail</label>
               <div class="col-sm-10">
                  <input type="email" class="form-control" id="email_per"  name="email_per" placeholder="votre adresse e-mail">
               </div>
            </div>
            <div class="form-group row">
               <label for="password_per" class="col-sm-2 col-form-label">Mot de passe</label>
               <div class="col-sm-10">
                  <input type="password" class="form-control" id="password_per"  name="password_per" placeholder="votre mot de passe">
               </div>
            </div>
            <div class="form-group row">
               <div class="col-sm-offset-4 col-sm-2">
                  <input type="submit" class="form-control btn btn-primary submit" id="submit_conf" value="Se connecter">
               </div>
               <div class="col-sm-2">
                  <input type="reset" class="form-control btn btn-warning" id="reset_conf" value="Annuler">
               </div>

            </div>
         </form>
          <a href="./lost_password.php">Mot de passe perdu</a>

      </div>
       <div class="panel-footer">

       </div>
   </div>
       <div class="panel panel-primary">
           <div class="panel-heading">
               Pour les candidats
           </div>
           <div class="panel-body">
               <p> L'accès pour le candidat-e-s se fait via le lien ci-contre -> <a href="https://new.ict-jb.net/Public/login.php">new.ict-jb.net/Public</a></p>
               <table class="table table-bordered">
                   <tr>
                       <th>Classe avec accès au site en ce moment</th>
                       <th>Visibilité des notes</th>
                   </tr>
                   <?php
                   $cla = new Classe();
                   $tab_cla = $cla->get_all_actifs();

                   //print_r($tab_cla);
                   foreach($tab_cla AS $cla){
                       $grp = new Groupe($cla['ref_grp']);
                       ?>
                       <tr>
                           <td><?=$grp->get_nom_grp()." - ".$cla['nom_cla']?></td>
                           <td>
                               <?php
                               if($cla['notes_open']){
                                   echo '<button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-eye-open"></span>';
                               }else{
                                   echo '<button type="button" class="btn btn-danger"><span class="glyphicon glyphicon-eye-close"></span>';
                               }

                               ?>
                               </button>
                           </td>
                       </tr>
                       <?php
                   }
                   ?>
               </table>
           </div>
           <div class="panel-footer">

           </div>
       </div>

</div>
<script src="./js/functions.js"></script>
<script src="./js/login.js"></script>
</body>
</html>
<?php
/**
 * @author : CP-15CHT
 * Date :  09.01.2020
 * Ajout d'un événement
 */

/** Autotisation, Header, Config, Autoload, Sécurité **/
session_start();
$aut = "USR_EXA";
header('Content-type: text/json');
header('Content-type: application/json; charset=utf-8');
require_once substr(__dir__, 0, strpos(__dir__, "files")+strlen("files")) . "/config/config.inc.php";
require_once(WAY . "/includes/autoload.inc.php");
require_once(WAY . "/includes/secure.inc.php");

$vnt = new Evenement();
$sle = new Salle();

$tab_vnt['id_exa'] = $_POST['id_exa'];
$tab_vnt['id_sle'] = $_POST['id_sle'];
$tab_vnt['text_vnt'] = $_POST['text_vnt'];
$tab_vnt['hrs_vnt'] = $_POST['hrs_vnt'];

$tab_vnt['nom_sle'] = $sle->get_name_salle_by_id($_POST['id_sle']);

$id_can = $_POST['id_can'];
if ($id_can != 0){
    if($_POST['fin_exa'] != 0){
        $tab_vnt['id_vnt'] = $vnt->add_evenement($tab_vnt['id_exa'],$tab_vnt['hrs_vnt'],$tab_vnt['text_vnt'],$id_can,$tab_vnt['id_sle']);
    }else{
        $tab_vnt['id_vnt'] = $vnt->add_evenement($tab_vnt['id_exa'],$tab_vnt['hrs_vnt'],$tab_vnt['text_vnt'],0,$tab_vnt['id_sle']);
    }
    $vnt->add_candidat_event($id_can, $tab_vnt['id_vnt']);
}else{
    $tab_vnt['id_vnt'] = $vnt->add_evenement($tab_vnt['id_exa'],$tab_vnt['hrs_vnt'],$tab_vnt['text_vnt'],0,$tab_vnt['id_sle']);
}

echo json_encode($tab_vnt);

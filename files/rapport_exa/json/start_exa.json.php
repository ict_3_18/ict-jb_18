<?php
/**
 * @author : CP-15CHT
 * Date :  09.01.2020
 * Débute l'examen
 */

/** Autotisation, Header, Config, Autoload, Sécurité **/
session_start();
$aut = "USR_EXA";
header('Content-type: text/json');
header('Content-type: application/json; charset=utf-8');
require_once substr(__dir__, 0, strpos(__dir__, "files")+strlen("files")) . "/config/config.inc.php";
require_once(WAY . "/includes/autoload.inc.php");
require_once(WAY . "/includes/secure.inc.php");

$exa = new Examen($_POST['id_exa']);

//Création des variables avec les posts reçus
$debut_exa = $_POST['debut_exa'];
$duree_exa = $_POST['duree_exa'];
$duree_dys = $_POST['duree_dys'];
$id_exa = $_POST['id_exa'];
$id_sle = $_POST['id_sle'];

//Séparation de l'heure en heure et minutes
$heure_exa = explode(":", $debut_exa);

//Si le nombre du tableau heure_exa et inférieur à 3
/*if(sizeof($heure_exa) < 3) {
    $debut_exa .= ":00";
}*/

$jour_exa = date('Y-m-d')." ".$debut_exa;

//Modification des durées de l'examen
$exa->start_exa($id_sle,$duree_exa, $duree_dys, $debut_exa);

//$exa->modif_examen($jour_exa, $duree_exa, $duree_dys, $id_exa);
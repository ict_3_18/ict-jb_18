<?php
/**
 * @author : CP-15CHT
 * Date :  09.01.2020
 * Modification d'un événement
 */

/** Autotisation, Header, Config, Autoload, Sécurité **/
session_start();
$aut = "USR_EXA";
header('Content-type: text/json');
header('Content-type: application/json; charset=utf-8');
require_once substr(__dir__, 0, strpos(__dir__, "files")+strlen("files")) . "/config/config.inc.php";
require_once(WAY . "/includes/autoload.inc.php");
require_once(WAY . "/includes/secure.inc.php");

$evnt = new Evenement();

//Création des variables avec les posts reçus
$texte = $_POST['text_vnt'];
$id_vnt = $_POST['id_vnt'];
//Modification de la remarque
$evnt->modif_evenement($id_vnt, $texte);

echo json_encode($_POST);
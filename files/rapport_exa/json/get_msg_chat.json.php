<?php
/**
 * @author : CP-15CHT
 * Date :  16.01.2020
 * Récupère les messages chats
 */

/** Autotisation, Header, Config, Autoload, Sécurité **/
session_start();
$aut = "USR_EXA";
header('Content-type: text/json');
header('Content-type: application/json; charset=utf-8');
require_once substr(__dir__, 0, strpos(__dir__, "files")+strlen("files")) . "/config/config.inc.php";
require_once(WAY . "/includes/autoload.inc.php");
require_once(WAY . "/includes/secure.inc.php");

$chat = new Chat();

$id_exa = $_POST['id_exa'];

$messages = $chat->get_all_messages($id_exa);

echo json_encode($messages);

/*//Création des variables
$id_exa = $_POST['id_exa'];
$nbr_msg = $_POST['nbr_msg'];

//Récupère les messages chats
$messages = $chat->get_all_messages($id_exa);

//Si le nombre de message de la fonction et différents de celui du post ..??
if (sizeof($messages) != $nbr_msg) {
    //Boucle sur le nombre de différences
    for ($i = 0; $i < (sizeof($messages)-$nbr_msg); $i++) {
        //Création du tableau de message
        $tab_msg = [];
        //Ajout de l'événement différents dans le tableau
        array_push($tab_msg, $messages[$i]);
    }
    echo json_encode($tab_msg);
}*/


<?php
/**
 * @author : CP-15CHT
 * Date : 10.01.2020
 * Suppression d'un événement
 */

/** Autotisation, Header, Config, Autoload, Sécurité **/
session_start();
$aut = "USR_EXA";
header('Content-type: text/json');
header('Content-type: application/json; charset=utf-8');
require_once substr(__dir__, 0, strpos(__dir__, "files")+strlen("files")) . "/config/config.inc.php";
require_once(WAY . "/includes/autoload.inc.php");
require_once(WAY . "/includes/secure.inc.php");

$evnt = new Evenement();

$id_vnt_exa = $_POST['id_vnt'];

//Supression du/des candidats
$evnt->suppr_candidat($id_vnt_exa);
//Suppression de l'événement
$evnt->suppr_evenement($id_vnt_exa);

echo json_encode($_POST);
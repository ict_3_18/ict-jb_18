<?php
/**
 * @author : CP-15CHT
 * Date :  09.01.2020
 * Fini l'examen pour tous les candidats dans la salle
 */

/** Autotisation, Header, Config, Autoload, Sécurité **/
session_start();
$aut = "USR_EXA";
header('Content-type: text/json');
header('Content-type: application/json; charset=utf-8');
require_once substr(__dir__, 0, strpos(__dir__, "files")+strlen("files")) . "/config/config.inc.php";
require_once(WAY . "/includes/autoload.inc.php");
require_once(WAY . "/includes/secure.inc.php");

$vnt = new Evenement();
$sle = new Salle();

$tab_rlst['id_exa'] = $_POST['id_exa'];
$tab_rlst['id_sle'] = $_POST['id_sle'];
$tab_rlst['text_vnt'] = $_POST['text_vnt'];
$tab_rlst['hrs_vnt'] = $_POST['hrs_vnt'];

$tab_rlst['nom_sle'] = $sle->get_name_salle_by_id($_POST['id_sle']);

$exa = new Examen($tab_rlst['id_exa']);
$pls = new Plan($exa->get_id_pls());
$all_can = $pls->get_all_can_by_sle($tab_rlst['id_sle'], $tab_rlst['id_exa']);

$can_fin = $exa->get_can_fin($tab_rlst['id_exa'], $tab_rlst['id_sle']);

$tab_can = array();
$nbr = 0;
foreach ($all_can as $can){
    $id_can = $can['id_can'];
    $num_can = substr($can['no_candidat_can'],1);
    if(!in_array( $id_can, $can_fin)){
        $tab_can[$nbr]['num_can']= $num_can;
        $tab_can[$nbr]['id_can']= $id_can;
        $nbr++;
    }
}

$nbr = 0;
foreach ($tab_can as $can){

    $txt = $tab_rlst['text_vnt'] . $can['num_can'];

    $tab_rlst["can"][$nbr]['id_vnt'] = $vnt->add_evenement($tab_rlst['id_exa'],$tab_rlst['hrs_vnt'],$txt,$can['id_can'],$tab_rlst['id_sle']);
    $vnt->add_candidat_event($can['id_can'], $tab_rlst["can"][$nbr]['id_vnt']);
    $tab_rlst["can"][$nbr]['text_vnt'] = $txt;
    $tab_rlst["can"][$nbr]['num_can'] = $can['num_can'];

    $nbr++;
}

echo json_encode($tab_rlst);
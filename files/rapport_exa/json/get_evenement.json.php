<?php
/**
 * @author : CP-15CHT
 * Date :  14.01.2020
 * Récupère les événements
 */

/** Autotisation, Header, Config, Autoload, Sécurité **/
session_start();
$aut = "USR_EXA";
header('Content-type: text/json');
header('Content-type: application/json; charset=utf-8');
require_once substr(__dir__, 0, strpos(__dir__, "files")+strlen("files")) . "/config/config.inc.php";
require_once(WAY . "/includes/autoload.inc.php");
require_once(WAY . "/includes/secure.inc.php");

$vnt = new Evenement();
$sle = new Salle();

$tab_vnt = $vnt->get_vnt_exa($_POST['id_exa']);

foreach ($tab_vnt as $keys=>$event){
    $tab_vnt[$keys]['nom_sle'] = $sle->get_name_salle_by_id($event['ref_sle']);
}

echo json_encode($tab_vnt);
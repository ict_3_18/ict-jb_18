<?php
/**
 * @author : CP-16RDM
 * Date :  14.01.2020
 */

/** Autotisation, Header, Config, Autoload, Sécurité **/
session_start();
$aut = "USR_EXA";
header('Content-type: text/json');
header('Content-type: application/json; charset=utf-8');
require_once substr(__dir__, 0, strpos(__dir__, "files")+strlen("files")) . "/config/config.inc.php";
require_once(WAY . "/includes/autoload.inc.php");
require_once(WAY . "/includes/secure.inc.php");

$exa = new Examen($_POST['id_exa']);
$exa->add_com_rapport_exa($_POST['com_rapport']);


echo json_encode($_POST);

<?php
/**
 * @author : CP-15CHT
 * Date :  16.01.2020
 * Ajout d'un message chat
 */

/** Autotisation, Header, Config, Autoload, Sécurité **/
session_start();
$aut = "USR_EXA";
header('Content-type: text/json');
header('Content-type: application/json; charset=utf-8');
require_once substr(__dir__, 0, strpos(__dir__, "files")+strlen("files")) . "/config/config.inc.php";
require_once(WAY . "/includes/autoload.inc.php");
require_once(WAY . "/includes/secure.inc.php");

$chat = new Chat();

//Création des variables
$msg = $_POST['msg'];
$hrs = $_POST['heure'];
$id_exa = $_POST['id_exa'];
$id_sle = $_POST['id_sle'];

//Ajout du message
$chat->add_chat($msg, $hrs, $id_exa, $id_sle);

echo json_encode($_POST);
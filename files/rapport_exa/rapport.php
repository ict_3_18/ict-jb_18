<?php
session_start();
$aut = "USR_EXA";
require("./../config/config.inc.php");
require(WAY . "/includes/secure.inc.php");
require_once(WAY . "/includes/autoload.inc.php");
require_once(WAY . "/includes/head.inc.php");

$id_exa = $_POST['id_exa'];
$exa = New Examen($id_exa);
?>

<div class="panel panel-primary">
    <div class="container-full">
        <ul class="nav nav-tabs">
            <?php
            //Affichage des salles de l'examen
            $tab_exa = $exa->get_salles_and_exa();
            //print_r($tab_exa);
            foreach ($tab_exa AS $exam) {
                echo "<li>"
                . "<a class=\"menu_exa\" "
                . "id_exa=\"" . $id_exa . "\" "
                . "id_sle=\"" . $exam['id_sle'] . "\" "
                . "date_heure_exa=\"" . $exam['date_heure_exa'] . "\" "
                . "duree_exa=\"" . $exam['duree_exa'] . "\" "
                . "duree_exa_dys=\"" . $exam['temps_supp_dys_exa'] . "\" "
                . "href=\"#" . $exam['nom_sle'] . "\">" . $exam['nom_sle'] . "</a>"
                . "</li>";
            }
            ?>
        </ul>
    </div>
    <div id="tab">

    </div>
</div>
</body>
<script>
    //Fonction click sur une des salles
    $(".menu_exa").click(function () {
        //Load de la page examen.load.php
        $("#tab").load(
                "./load/examen.load.php",
                {
                    id_exa: $(this).attr('id_exa'),
                    id_sle: $(this).attr('id_sle'),
                    date_heure_exa: $(this).attr('date_heure_exa'),
                    duree_exa: $(this).attr('duree_exa'),
                    duree_exa_dys: $(this).attr('duree_exa_dys')
                }
        );
    });
</script>
</html>
$(function () {
    /**
     * Utiliser
     * Rappor exa
     * Débuter l'examen (btn démarrer)
     */
    $("#btn_debut_exa").click(function () {
        //Appel des fonctions de calcul du temps
        temps_normal();
        temps_dys();

        //Changement du type de l'input en hidden pour les cacher
        $('#debut_exa').attr('type', 'hidden');
        $('#duree_normal').attr('type', 'hidden');
        $('#duree_dys').attr('type', 'hidden');

        //Création de label avec les valeurs des inputs cacher
        $('#debut_exa').after('<label>' + $('#debut_exa').val() + '</label>');
        $('#duree_normal').after('<label>' + $('#duree_normal').val() + '</label>');
        $('#duree_dys').after('<label>' + $('#duree_dys').val() + '</label>');

        //Désactiver le bouton démarrer
        $(this).prop('disabled', true);
        $(this).attr('start', 1);

        $(this).css("display","none");
        $("#btn_terminer").prop('disabled', false);


        //Modification des valeurs des labels du chrono
        // console.log($("#duree_normal").val());
        $('#temps_restant').innerHTML = $("#duree_normal").val();
        $('#temps_restant_dys').innerHTML = "("+(parseInt($("#duree_normal").val())+parseInt($("#duree_dys").val()))+"' -> dys)";


        /**
         * Ajout de l'event "Début de l'examen"
         */
        var date = new Date();
        var hrs_vnt = date.getHours();
        hrs_vnt += ':' + (date.getMinutes() < 10 ? '0' : '') + date.getMinutes() + ':00';
        var text_vnt = "Début de l'examen";
        var id_sle = $("#Plan").attr("id_sle");
        var id_exa = $("#Plan").attr("id_exa");
        add_event(hrs_vnt, text_vnt, id_sle, id_exa);


        /**
         * Défine que l'examen a commencer dans t_ref_examens_salles
         */
        $.post(
            './json/start_exa.json.php',
            {
                //L'heure de début de l'examen
                debut_exa: $("#debut_exa").val(),
                //La durée de l'examen
                duree_exa: $("#duree_normal").val(),
                //Le temps dys en plus
                duree_dys: $("#duree_dys").val(),
                //id de l'examen
                id_exa: $(this).attr("id_exa"),
                //id de la salle
                id_sle: $(this).attr("id_sle"),
            });
    });

    /**
     * Date : 10.01.2020
     * Modale Candidat
     * Ouverture de la modale
     */
    $(".candidat").click(function () {
        $("#modal_rapport").modal("show");
        $("#rem_text_modal").val("");

        var num_can = $(this).attr('num_can');
        var id_can = $(this).attr('id_can');
        $("#num_can").html(num_can);
        $("#id_can").html(id_can);
    });

    /**
     * Date : 10.01.2020
     * Modale Candidat
     * Btn fin candidat
     */
    $("#candidat_fin").click(function () {
        //Création de l'heure
        var date = new Date();
        var hrs_vnt = date.getHours();
        hrs_vnt += ':' + (date.getMinutes() < 10 ? '0' : '') + date.getMinutes() + ':00';

        //Reprise de données
        var id_can = $('#id_can').text();
        var num_can = $('#num_can').text();
        var text_vnt = "Fin de l'examen pour le candidat " + num_can;
        var id_sle = $("#Plan").attr("id_sle");
        var id_exa = $("#Plan").attr("id_exa");

        //Ajout
        add_event(hrs_vnt, text_vnt, id_sle, id_exa, 1, id_can);

        //Fermeture de la modal
        $(".candidat[id_can='"+id_can+"']").css('background-color', 'grey');
        $("#modal_rapport").modal("hide");
    });

    /**
     * Date : 10.01.2020
     * Modale Candidat
     * Btn valider
     */
    $("#btn_add_rem_modal").click(function () {
        //Création de l'heure
        var date = new Date();
        var hrs_vnt = date.getHours();
        hrs_vnt += ':' + (date.getMinutes() < 10 ? '0' : '') + date.getMinutes() + ':00';

        //Reprise de données
        var num_can = $('#num_can').text();
        var text_vnt = "(Candidat " + num_can + ") " + $("#modal_rapport #rem_text_modal").val();
        var id_sle = $("#Plan").attr("id_sle");
        var id_exa = $("#Plan").attr("id_exa");
        var id_can = $('#id_can').text();

        //Ajout
        add_event(hrs_vnt, text_vnt, id_sle, id_exa, 0, id_can);

        //Fermeture de la modal
        $("#modal_rapport").modal("hide");
    });

    /**
     * Date : 14.01.2020
     * Btn Terminer
     * Termine l'examen pour tout les candidats de la salle
     */
    $("#btn_terminer").click(function () {
        if(confirm("Voulez-vous vraiment mettre fin à l'examen ?")){

            var date = new Date();
            var hrs_vnt = date.getHours();
            hrs_vnt += ':' + (date.getMinutes() < 10 ? '0' : '') + date.getMinutes() + ':00';
            var text_vnt = "Fin de l'examen pour le candidat ";
            var id_sle = $("#Plan").attr("id_sle");
            var id_exa = $("#Plan").attr("id_exa");

            $.post(
                './json/fin_exa.json.php?_=' + Date.now(),
                {
                    hrs_vnt: hrs_vnt,
                    text_vnt: text_vnt,
                    id_sle: id_sle,
                    id_exa: id_exa
                },
                function (data) {
                    $.each(data.can, function (key, val) {
                        $(".candidat[num_can='"+val.num_can+"']").css('background-color', 'grey');
                        $("#form_add_event").after(
                            "<tr class='appended' id_event='" + val.id_vnt + "'>" +
                            "<td class='text-center' id='hrs_" + val.id_vnt + "'>" + data.hrs_vnt + "</td>" +
                            "<td class='text-center'>" + data.nom_sle + "</td>" +
                            "<td id='texte_" + val.id_vnt + "'>" + val.text_vnt + "</td>" +
                            "<td id='btn_area_" + val.id_vnt + "'>" +
                            "<button class='btn btn-warning btn-xs' onclick='edit_event(" + val.id_vnt + ")'> <span class='glyphicon glyphicon-pencil' ></span>" + "</button>" +
                            "<button class='btn btn-danger btn-xs' onclick='del_event(" + val.id_vnt + ")'> <span class='glyphicon glyphicon-trash' ></span>" + "</button>" +
                            "</td>" +
                            "</tr>"
                        );
                    })
                }
            );
        }
    });

    /**
     * Date : 16.01.2020
     * Chat
     * Btn d'envoie des messages
     */
    $("#btn_whatceff").click(function () {
        //Current time
        var date = new Date();
        var str = date.getHours();
        str += ':' + (date.getMinutes() < 10 ? '0' : '') + date.getMinutes() + ":" + date.getSeconds();

        $.post(
            './json/add_msg_chat.json.php?_=' + Date.now(),
            {
                msg: $('#msg').val(),
                heure: str,
                id_sle: $("#Plan").attr("id_sle"),
                id_exa: $("#Plan").attr("id_exa")
            },
            function (data) {
                //Clean input
                $("#msg").val("");
            }
        );
    });

    /**
     * Date : 09.01.2020
     * Remarques / Evénements
     * Ajout d'un événement (Gestion du clic sur btn Ajout)
     */
    $('#btn_add_event').click(function (e) {
        var hrs_vnt = $("#event_time_form").val();
        var text_vnt = $("#event_text_form").val();
        var id_sle = $("#event_sle_form").attr("id_sle");
        var id_exa = $("#Plan").attr("id_exa");

        add_event(hrs_vnt,text_vnt,id_sle,id_exa);
        $("#event_text_form").val("");
    });

    /**
     * Date : 14.01.2020
     * Rapport PDF
     * ...
     */
    $(".com_rapport").click(function () {
        $("#com_raport_modal").modal("show");
    });

    /**
     * Date : 14.01.2020
     * Rapport PDF
     * ...
     */
    $(".send_com_rapport").click(function () {

        $.ajax({
            type: "POST",
            url: "./json/add_com_rapport.json.php?_=" + Date.now(),
            data: {
                com_rapport: $(".commentaire_rapport").val(),
                id_exa: $(this).attr("id_exa")
            },
            dataType: 'json',
            async: 'false',
        })
    });

});

/**
 * Date : 14.01.2020
 * Remarques / Evénements
 * Fonction de mise à jour des remarques / événements
 */
function refresh_vnt() {
    // console.log("auidhwa");
    if ($("#event_text_form").val() === ""){
        var hrs = new Date().getHours();
        if (hrs < 10) {
            hrs = '0' + hrs;
        }
        var min = new Date().getMinutes();
        if (min < 10) {
            min = '0' + min;
        }
        var sec = new Date().getSeconds();
        if (sec < 10) {
            sec = '0' + sec;
        }
        $("#event_time_form").val(hrs + ":" + min + ":" + sec);
    }


    if($(".event_heading").attr("edit_mode") === "0") {
        $.post(
            './json/get_evenement.json.php',
            {
                id_exa: $("#Plan").attr("id_exa")
            },
            function (data) {
                $(".appended").remove();
                $.each(data.reverse(), function (key, val) {
                    $("#form_add_event").after(
                        "<tr class='appended' id_event='" + val.id_vnt_exa + "'>" +
                        "<td class='text-center' id='hrs_" + val.id_vnt_exa + "'>" + val.heure_vnt_exa + "</td>" +
                        "<td class='text-center'>" + val.nom_sle + "</td>" +
                        "<td id='texte_" + val.id_vnt_exa + "'>" + val.event_exa + "</td>" +
                        "<td id='btn_area_" + val.id_vnt_exa + "'>" +
                        "<button class='btn btn-warning btn-xs' onclick='edit_event(" + val.id_vnt_exa + ")'> <span class='glyphicon glyphicon-pencil' ></span>" + "</button>" +
                        "<button class='btn btn-danger btn-xs' onclick='del_event(" + val.id_vnt_exa + ")'> <span class='glyphicon glyphicon-trash' ></span>" + "</button>" +
                        "</td>" +
                        "</tr>"
                    );
                })
            }
        );
    }
}

/**
 * 16.01.2020
 * Chat
 * Fonction de mise à jour du chat (Upgraded)
 */
function refresh_chat() {

    var id_exa = $("#Plan").attr("id_exa");
    var id_sle = $("#Plan").attr("id_sle");

    $.post(
        './json/get_msg_chat.json.php?_=' + Date.now(),
        {
            id_exa: id_exa
        },
        function (data) {
            $("#chatbox").children().remove();
            $.each(data, function (key, val) {
                if(id_sle == val.id_sle){
                    $("#chatbox").append(
                    '<div id="salle">\n' +
                    '<div class="col-sm-2"></div>\n' +
                    '<div class="col-sm-8" id="msg1">\n' +
                    '<label id="msg_text">' + val.message_chat + '</label>\n' +
                    '</div>\n' +
                    '<div class="col-sm-2" id="info_salle">\n' +
                    '<label id="whatceff_info1">' + val.nom_sle + '<br>' + val.heure_chat + '</label>\n' +
                    '</div>\n' +
                    '</div>'
                    )
                }else{
                    $("#chatbox").append(
                    '<div id="salle">\n' +
                    '<div class="col-sm-2" id="info_salle" >\n' +
                    '<label id="whatceff_info2">' + val.nom_sle + '<br>' + val.heure_chat + '</label>\n' +
                    '</div>\n' +
                    '<div class="col-sm-8" id="msg2">\n' +
                    '<label id="msg_text">' + val.message_chat + '</label>\n' +
                    '</div>\n' +
                    '<div class="col-sm-2"></div>\n' +
                    '</div>'
                    )
                }

            })
        }
    )
    //Old version
    /*$.post(
        './json/get_msg_chat.json.php?_=' + Date.now(),
        {
            id_exa: $("#Plan").attr("id_exa"),
            nbr_msg: $("#chatbox").children().length
        },
        function (data) {
            //Si data est différent de 0
            if (data != 0) {
                //Boucle sur la longueur de data
                for (var i = 0; i < data.length; i++) {
                    //Si l'id de la salle du data est égal à l'id de la salle du plan
                    if (data[i].id_sle == $("#Plan").attr("id_sle")) {
                        //Création des trois div salle, msg1, info_salle avec leur attribut
                        var div = document.createElement('div');
                        div.id = 'salle';
                        var div1 = document.createElement('div');
                        div1.setAttribute('class', 'col-sm-2');
                        var div2 = document.createElement('div');
                        div2.id = 'msg1';
                        div2.setAttribute('class', 'col-sm-8');
                        var div3 = document.createElement('div');
                        div3.setAttribute('class', 'col-sm-2');
                        div3.id = 'info_salle';

                        //Ajout du message dans la div message
                        var msg = document.createElement('label');
                        var msgText = document.createTextNode(data[i].message_chat);
                        msg.id = 'msg_text';
                        msg.appendChild(msgText);

                        //Ajout des informations du message
                        var info = document.createElement('label');
                        info.id = 'whatceff_info1';
                        var br = document.createElement('br');
                        var heureText = document.createTextNode(data[i].heure_chat);
                        var salleText = document.createTextNode(data[i].nom_sle);
                        //Ajout des info dans la div
                        info.appendChild(salleText);
                        info.appendChild(br);
                        info.appendChild(heureText);

                        //Ajout des divs dans la div du message
                        div2.appendChild(msg);
                        div3.appendChild(info);
                        div.appendChild(div1);
                        div.appendChild(div2);
                        div.appendChild(div3);
                        //Children égal le nombre d'enfant du chat
                        var children = document.getElementById('chatbox').childNodes;
                        //Ajout du message avant le première enfant
                        document.getElementById('chatbox').insertBefore(div, children[0]);

                        //Sinon fait la même chose mai à l'inverse pour indiquer que c'est
                        //l'autre salle qui envoie le message
                    } else {
                        var div = document.createElement('div');
                        div.id = 'salle';
                        var div1 = document.createElement('div');
                        div1.setAttribute('class', 'col-sm-2');
                        div1.id = 'info_salle';
                        var div2 = document.createElement('div');
                        div2.id = 'msg2';
                        div2.setAttribute('class', 'col-sm-8');
                        var div3 = document.createElement('div');
                        div3.setAttribute('class', 'col-sm-2');

                        var msg = document.createElement('label');
                        var msgText = document.createTextNode(data[i].message_chat);
                        msg.id = 'msg_text';
                        msg.appendChild(msgText);

                        var info = document.createElement('label');
                        info.id = 'whatceff_info2';
                        var br = document.createElement('br');

                        var heureText = document.createTextNode(data[i].heure_chat);
                        var salleText = document.createTextNode(data[i].nom_sle);
                        info.appendChild(salleText);
                        info.appendChild(br);
                        info.appendChild(heureText);

                        div1.appendChild(info);
                        div2.appendChild(msg);
                        div.appendChild(div1);
                        div.appendChild(div2);
                        div.appendChild(div3);
                        var children = document.getElementById('chatbox').childNodes;
                        document.getElementById('chatbox').insertBefore(div, children[0]);
                    }
                }
            }

        },
        "json"
    );*/
}

/**
 * Utiliser
 * Fonction de mise à jour du temps restant avant fin exa (chrono)
 */
function chronoExa() {
    // console.log("Chrono");
    if($("#btn_debut_exa").attr('start') == 1) {
        //Obtention des fins de l'examen
        var fin_normal = $('#fin_exa_normal').html();
        var fin_dys = $('#fin_exa_dys').html();
        /*console.log(fin_normal);
        console.log(fin_dys);*/

        //Création de variable avec l'heure et les minutes actuelles
        var date = new Date();
        var heure_actuel = parseInt(date.getHours());
        var minute_actuel = parseInt(date.getMinutes());

        //Séparation de la fin normal en heure et minute
        var str_fin_normal = fin_normal.split(":");
        var heure_fin_normal = parseInt(str_fin_normal[0]);
        var minute_fin_normal = parseInt(str_fin_normal[1]);

        //Séparation de la fin dys en heure et minute
        var str_fin_dys = fin_dys.split(":");
        var heure_fin_dys = parseInt(str_fin_dys[0]);
        var minute_fin_dys = parseInt(str_fin_dys[1]);

        //Fait le calcul pour l'heure et les minutes normal
        var temps_normal1 = (heure_fin_normal - heure_actuel) * 60;
        var temps_normal2 = minute_fin_normal - minute_actuel;

        //Fait le calcul pour l'heure et les minutes dys
        var temps_dys1 = (heure_fin_dys - heure_actuel) * 60;
        var temps_dys2 = minute_fin_dys - minute_actuel;

        //Calcul du temps_restant et du temps_restant dys avec les heures et minutes
        var temps_restant = temps_normal1 + temps_normal2;
        var temps_restant_dys = temps_dys1 + temps_dys2;

        // console.log("temps_restant_dys : "+temps_restant_dys);
        // console.log("temps_restant : "+temps_restant);
        //Obtention de la durée de l'examen
        var temps_exa = $('#duree_normal').html();
        // console.log("temps_exa : "+temps_exa);
        //Calcul du pourcentage
        var pourcentage = "(" + Math.round(temps_restant / temps_exa * 100) + "%)";
        // console.log("pourcentage : "+pourcentage);
        //Si le temps normal est plus petit que 0
        if (temps_restant <= 0) {
            //On affiche 0
            document.getElementById('pourcent').innerHTML = "0%";
            document.getElementById('temps_restant').innerHTML = "0";
        } else {
            //Sinon on affiche les résultats
            document.getElementById('pourcent').innerHTML = pourcentage;
            document.getElementById('temps_restant').innerHTML = temps_restant;
        }

        //Si le temps dys est plus petit que 0
        if ((temps_dys1 + temps_dys2) <= 0) {
            //On affiche 0
            document.getElementById('temps_restant_dys').innerHTML = "0";
        } else {
            //Sinon on affiche les résultats
            document.getElementById('temps_restant_dys').innerHTML = temps_restant_dys;
        }
    }else{
        var hrs = new Date().getHours();
        if (hrs < 10) {
            hrs = '0' + hrs;
        }
        var min = new Date().getMinutes();
        if (min < 10) {
            min = '0' + min;
        }

        var dbt = $("#debut_exa").val();

        d = new Date("", "","", dbt.substring(0, 2), dbt.substring(3, 5));

        d2 = new Date("","","",hrs, min);

        if(d < d2){
            $("#debut_exa").val(hrs + ":" + min);
        }
    }
}

/**
 * Utiliser
 * Lors du début de l'examen
 * Calcul du temps normal ??????
 */
function temps_normal() {
    //Création des variables avec les durées et l'heure de fin de l'examen
    var duree_exa = $('#duree_normal').val();
    var heure_exa = $("#debut_exa").val();
    var heure_debut_exa = heure_exa.split(":");

    //Calcul des minutes de fin d'examen
    var minutes_debut_exa = parseInt(heure_debut_exa[0])*60 + parseInt(heure_debut_exa[1]);
    var minutes_fin_exa = minutes_debut_exa + parseInt(duree_exa);

    //Calucl de l'heure
    var minutes_heure_fin_exa = minutes_fin_exa/60;
    //Si l'heure est un int
    if (Number.isInteger(minutes_heure_fin_exa)) {
        //L'heure égal le calcul
        var heure_fin = minutes_heure_fin_exa;
        //Et les minutes 0
        var minutes_fin = "0";
        //Sinon
    } else {
        //On sépare le calcul
        var minutes_minute_fin_exa = minutes_heure_fin_exa.toString().split('.');
        //L'heure égal le nombre entier
        var heure_fin = minutes_minute_fin_exa[0];
        //Les minutes égales les décimals fois soixante
        var minutes_fin = Math.floor(("0." + minutes_minute_fin_exa[1])*60);
    }

    //Si les minutes sont inférieurs à 10 
    if (minutes_fin < 10) {
        //On rajoute un 0
        minutes_fin = "0"+minutes_fin;
    }

    //Conjonction des heures et des minutes
    var fin_normal = heure_fin + ":" + minutes_fin;
    //Affichage dans le label
    document.getElementById('fin_exa_normal').innerHTML = fin_normal;
}

/**
 * Utiliser
 * Lors du début de l'examen
 * Calcul le temps dys ??????
 */
function temps_dys() {
    //Création des variables avec les durées et l'heure de fin de l'examen
    var duree_exa_dys = $('#duree_dys').val();
    var fin_normal = document.getElementById('fin_exa_normal').textContent;
    var heure_fin_normal = fin_normal.split(':');

    //Calcul des minutes de fin d'examen
    var minutes_debut_exa_dys = parseInt(heure_fin_normal[0])*60 + parseInt(heure_fin_normal[1]);
    var minutes_fin_exa_dys = minutes_debut_exa_dys + parseInt(duree_exa_dys);

    //Calucl de l'heure
    var minutes_heure_fin_exa_dys = minutes_fin_exa_dys/60;
    //Si l'heure est un int
    if (Number.isInteger(minutes_heure_fin_exa_dys)) {
        //L'heure égal le calcul
        var heure_fin = minutes_heure_fin_exa_dys;
        //Et les minutes 0
        var minutes_fin = "0";
        //Sinon
    } else {
        //On sépare le calcul
        var minutes_minute_fin_exa_dys = minutes_heure_fin_exa_dys.toString().split('.');
        //L'heure égal le nombre entier
        var heure_fin = minutes_minute_fin_exa_dys[0];
        //Les minutes égales les décimals fois soixante
        var minutes_fin = Math.floor(("0." + minutes_minute_fin_exa_dys[1])*60);
    }

    //Si les minutes sont inférieurs à 10
    if (minutes_fin < 10) {
        //On rajoute un 0
        minutes_fin = "0"+minutes_fin;
    }

    //Conjonction des heures et des minutes
    var fin_dys = heure_fin + ":" + minutes_fin;
    //Affichage dans le label
    document.getElementById('fin_exa_dys').innerHTML = fin_dys;
}

/**
 * Fonctions de mise à jour
 * Timer, Chat, Remarques
 */
setInterval(chronoExa, 2000);
setInterval(refresh_chat, 10000);
setInterval(refresh_vnt, 10000);

/**
 * Date : 09.01.2020
 * Remarques / Evénements
 * Ajout d'un événement
 */
function add_event(hrs_vnt,text_vnt,id_sle,id_exa,fin_exa = 0,id_can = 0) {
    $.ajax({
        type: "POST",
        url: "./json/add_event.json.php?_=" + Date.now(),
        data: {
            hrs_vnt: hrs_vnt,
            text_vnt: text_vnt,
            id_sle: id_sle,
            id_exa: id_exa,
            fin_exa: fin_exa,
            id_can: id_can
        },
        dataType: 'json',
        async: 'false',
        success: function (data) {
            $("#form_add_event").after(
                "<tr class='appended' id_event='" + data.id_vnt + "'>" +
                "<td class='text-center' id='hrs_" + data.id_vnt + "'>" + data.hrs_vnt + "</td>" +
                "<td class='text-center'>" + data.nom_sle + "</td>" +
                "<td id='texte_" + data.id_vnt + "'>" + data.text_vnt + "</td>" +
                "<td id='btn_area_" + data.id_vnt + "'>" +
                "<button class='btn btn-warning btn-xs' onclick='edit_event(" + data.id_vnt + ")'> <span class='glyphicon glyphicon-pencil' ></span>" + "</button>" +
                "<button class='btn btn-danger btn-xs' onclick='del_event(" + data.id_vnt + ")'> <span class='glyphicon glyphicon-trash' ></span>" + "</button>" +
                "</td>" +
                "</tr>"
            );
        }
    })
}

/**
 * Date : 09.01.2020
 * Remarques / Evénements
 * Activation de l'édition d'un événement
 */
function edit_event(id) {
    $(".event_heading").attr("edit_mode","1");

    var texte = $("td[id='texte_" + id + "']").text();
    $("td[id='texte_" + id + "']").text("");
    // console.log(texte);
    // var r = prompt("Modification de la remarque : ",texte);

    var textarea = document.createElement("textarea");
    textarea.id = "edit_textarea_" + id;
    textarea.cols = "60";
    textarea.rows = "3";
    textarea.value = texte;
    $("td[id='texte_" + id + "']").append(textarea);

    $("td[id='btn_area_" + id + "'] button").hide();

    $("td[id='btn_area_" + id + "']").append(
        "<button id='btn_conf_" + id + "' class='btn btn-success btn-xs' onclick='conf_edit_event(" + id + ")' id_rem='" + id + "' >" +
        "<span class='glyphicon glyphicon-ok' ></span>" + "</button> " +
        "<button id='btn_stop_" + id + "' class='btn btn-danger btn-xs' onclick='stop_edit_event(" + id + ")' id_rem='" + id + "' >" +
        "<span class='glyphicon glyphicon-remove' ></span>" + "</button>" +
        "<input id='event_hidden_" + id + "' type='hidden' value=\"" + texte + "\">"
    );
}

/**
 * Date : 09.01.2020
 * Remarques / Evenements
 * Edition d'un événement
 */
function conf_edit_event(id) {
    $(".event_heading").attr("edit_mode","0");
    var txt = $("textarea[id='edit_textarea_" + id + "']").val();
    // console.log(txt);

    $.ajax({
        type: "POST",
        url: "./json/modif_evenement.json.php?_=" + Date.now(),
        data: {
            id_vnt: id,
            text_vnt: txt
        },
        dataType: 'json',
        async: 'false',
        success: function (data) {
            console.log(data);
            $("#event_hidden_" + data.id_vnt).val(data.text_vnt);
            stop_edit_event(data.id_vnt);
        }
    })
}

/**
 * Date : 09.01.2020
 * Remarques / Evenements
 * Arrêt de l'édition d'un événement
 */
function stop_edit_event(id) {
    $(".event_heading").attr("edit_mode","0");
    var txetarea = $("#event_hidden_" + id);
    $("td[id='texte_" + id + "']").text(txetarea.val());
    txetarea.remove();
    $("textarea[id='edit_textarea_" + id + "']").remove();
    $("#btn_conf_" + id).remove();
    $("button[id='btn_stop_" + id + "']").remove();

    $("td[id='btn_area_" + id + "'] button").show();
}

/**
 * Date : 09.01.2020
 * Remarques / Evenements
 * Suppression d'un événement
 */
function del_event(id) {
    // console.log("id rem " + id);
    var r = confirm("Êtes vous sûr de vouloir supprimer la remarque?");
    if (r === true) {
        $.ajax({
            type: "POST",
            url: "./json/suppr_evenement.json.php?_=" + Date.now(),
            data: {
                id_vnt: id
            },
            dataType: 'json',
            async: 'false',
            success: function (data) {
                console.log(data);
                $("tr[id_event='" + id + "']").remove();
            }
        })
    }
}

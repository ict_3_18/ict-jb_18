<?php
session_start();
$aut = "USR_EXA";
require_once substr(__dir__, 0, strpos(__dir__, "files") + strlen("files")) . "/config/config.inc.php";

require(WAY . "/includes/secure.inc.php");
require(WAY . "/rapport_exa/modales/modal_rapport.mod.php");
require(WAY . "/rapport_exa/modales/commentaire_rapport.mod.php");
require_once(WAY . "/includes/autoload.inc.php");

$id_exa = $_POST['id_exa'];
$id_sle = $_POST['id_sle'];
$date_heure_exa = $_POST['date_heure_exa'];
$duree_exa = (int)$_POST['duree_exa'];
$duree_exa_dys = (int)$_POST['duree_exa_dys'];
$exa = New Examen($id_exa);
$id_pls = $exa->get_id_pls();
$heure_debut_exa = $exa->get_heure_db_exa();
$duree_exa = $exa->get_dur();
$evnt = New Evenement();
$chat = New Chat();
$sle = new Salle($id_sle);

$heure_exa = explode(" ", $date_heure_exa);
$tab_hrs_by_sle = $exa->get_heure_db_exa_by_sle($id_sle);
$start = false;
if ($tab_hrs_by_sle['debut_exa'] != "00:00:00") {
    $heure_exa = $tab_hrs_by_sle['debut_exa'];
    $duree_exa = $tab_hrs_by_sle['duree_exa'];
    $duree_exa_dys = $tab_hrs_by_sle['duree_dys_exa'];
    $start = true;
    $disabled = " disabled='disabled' ";
    $disabled_fin = "";
} else {
    $disabled = "";
    $disabled_fin = " disabled='disabled' ";
    $heure_exa = $heure_debut_exa;
}

?>

<div class="container-exa container-full">
    <div class="row" id="row_temps">
        <div class="col-md-6">
            <table id="tab_duree" class="table table-bordered col-md-12">
                <tr>
                    <th class="col-md-1 text-center">Début de l'examen</th>
                    <th class="col-md-1 text-center">Durée annoncée</th>
                    <th id="td_dys" class="col-md-1 text-center">Temps suppl. dys</th>
                    <th class="col-md-1 text-center">Fin normale</th>
                    <th id="td_dys" class="col-md-1 text-center">Fin candidats dys</th>
                    <th class="col-md-1 text-center">Temps restant :</th>
                    <th id="td_dys" class="col-md-1 text-center">Temps restant dys :</th>
                    <th class="col-md-1 text-center" rowspan="2">
                        <?php if (!$start) {
                            ?>
                            <button type="button" class="btn btn-sm btn-primary" id_exa="<?= $exa->get_id() ?>" id_sle="<?= $id_sle ?>" id="btn_debut_exa">Démarrer</button>
                            <?php
                        } else {
                            ?>
                            <input type="hidden" id="btn_debut_exa" start="1">
                            <?php
                        }
                        ?>
                        <button type="button" id="btn_terminer" class="btn btn-sm btn-warning" <?=$disabled_fin?>>Terminer</button>
                        <input type="submit" class="btn btn-sm btn-primary com_rapport" value="Rapport PDF">
                    </th>
                </tr>
                <tr>
                    <?php
                    if ($start){
                        ?>
                        <td class="text-center">
                            <label class='col-md-12' id="debut_exa"><?=$heure_exa?></label>
                        </td>
                        <td class="text-center">
                            <label class='col-md-12' id="duree_normal"><?=$duree_exa?></label>
                        </td class="text-center">
                        <td id="td_dys" class="text-center">
                            <label class='col-md-12' id="duree_dys"><?=$duree_exa_dys?></label>
                        </td>
                        <?php
                    }else{
                        ?>
                        <td class="text-center">
                            <input class='col-md-12' type="time" id="debut_exa" value="<?=$heure_exa?>" >
                        </td>
                        <td class="text-center">
                            <input class='col-md-12' type="number" id="duree_normal" value="<?=$duree_exa?>">
                        </td class="text-center">
                        <td id="td_dys" class="text-center">
                            <input class='col-md-12' type="number" id="duree_dys" value="<?=$duree_exa_dys?>">
                        </td>
                        <?php
                    }
                    ?>
                    <td class="text-center">
                        <?php
                        $heure_debut_exa = explode(":", $heure_exa);
                        $time_debut_exa = mktime($heure_debut_exa[0], $heure_debut_exa[1]);
                        $time_fin_exa = $time_debut_exa + ($duree_exa * 60);
                        ?>
                        <b id="fin_exa_normal"><?=date("H:i", $time_fin_exa)?></b>
                    </td>
                    <td id="td_dys" class="text-center">
                        <?php
                        $time_fin_dys = $time_fin_exa + $duree_exa_dys * 60;
                        ?>
                        <b id="fin_exa_dys"><?=date("H:i", $time_fin_dys)?></b>
                    </td>
                    <td class="text-center">
                        <b id="temps_restant"><?=$duree_exa?></b>
                        <b>&nbsp;min </b><b id="pourcent">(100%)</b>
                    </td>
                    <td class="text-center" id="td_dys">
                        <b id="temps_restant_dys"><?=($duree_exa + $duree_exa_dys)?></b><b>&nbsp;min </b>
                    </td>
                </tr>
            </table>

            <div class="center-block col-md-12">
                <!--Moyens-->
                <div class="col-md-6">
                    <div class="border">
                        <h4>Information de début d'examen : </h4>
                        <?php
                        //Affichage des moyens autorisés
                        $info = $exa->get_infos_begin_exa();
                        echo "" . str_replace(chr(10), "<br>", $info) . "";
                        ?>
                    </div>
                </div>
                <!--Moyens-->
                <div class="col-md-6">
                    <div class="border">
                        <h4>Moyens papier autorisés : </h4>
                        <?php
                        //Affichage des moyens autorisés
                        $moyen_aut = $exa->get_data_exa();
                        echo "" . str_replace(chr(10), "<br>", $moyen_aut) . "";
                        ?>
                    </div>
                </div>
				<br>
				
				<div class="col-md-6">
                    <div class="border">
                        <h4>Moyens électroniques autorisés :
                            <button title="Réseau informatique <?= ($exa->get_reseau_exa()) ? "autorisé" : "non-autorisé" ?>" class="btn btn-sm <?= ($exa->get_reseau_exa()) ? "btn-success" : "btn-danger" ?>">
                                <span class='glyphicon glyphicon-signal'></span>
                            </button>
                        </h4>
                        <?php
                        //Affichage des moyens autorisés
                        $moyen_aut = $exa->get_materiel_exa();
                        echo "" . str_replace(chr(10), "<br>", $moyen_aut) . "";
                        ?>
                    </div>
                </div>
                <!--Espace-->
                <div class="col-md-12"><br></div>
                <!--Chat-->
                <div class="col-md-12">
                    <div class="border">
                        <div class="col-sm-2"></div>
                        <input type="text" id="msg"/>
                        <button type="button" class="btn btn-primary" id="btn_whatceff">Envoyer</button>
                        <div id="chatbox">
                            <?php
                            //Affichage des messages dans le chat
                            $tab_msg = $chat->get_all_messages($id_exa);

                            foreach ($tab_msg as $msg) {
                                if ($msg['id_sle'] == $id_sle) {
                                    ?>
                                    <div id="salle">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-8" id="msg1">
                                            <label id="msg_text"><?=$msg['message_chat']?></label>
                                        </div>
                                        <div class="col-sm-2" id="info_salle">
                                            <label id="whatceff_info1"><?=$msg['nom_sle']?><br><?=$msg['heure_chat']?></label>
                                        </div>
                                    </div>
                                    <?php
                                } else {
                                    ?>
                                    <div id="salle">
                                        <div class="col-sm-2" id="info_salle" >
                                            <label id="whatceff_info2"><?=$msg['nom_sle']?><br><?=$msg['heure_chat']?></label>
                                        </div>
                                        <div class="col-sm-8" id="msg2">
                                            <label id="msg_text"><?=$msg['message_chat']?></label>
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6 center-block" id="Plan" id_sle="<?=$id_sle?>" id_exa="<?=$id_exa?>">
            <!--Plan de salle-->
            <div id="Plan2">
                <h2 class="text-center"><?= $sle->get_name_salle_by_id($id_sle);?></h2>
                <?php
                echo $sle->gen_plan_rapport(700, 350, $id_pls, $id_exa);
                ?>
            </div>

            <!--Remarques/Evénements-->
            <div class="center-block">
                <table class="table table-bordered">
                    <tr class="event_heading" edit_mode="0">
                        <th class="col-md-1">Heure</th>
                        <th class="col-md-1">Salle</th>
                        <th class="col-md-5">Evénement</th>
                        <th class="col-md-1">Outils</th>
                    </tr>

                    <tr class="text-center" id="form_add_event">
                        <td>
                            <input type="time" class="form-control" id="event_time_form" name="date_time" autocomplete="off" value="<?= date("H:i:s"); ?>">
                        </td>
                        <td id="event_sle_form" id_sle="<?=$id_sle?>">
                            <p><?=$sle->get_name_salle_by_id($id_sle)?></p></td>
                        <td>
                            <textarea class="form-control" name="remarque_text" id="event_text_form" rows="1"></textarea>
                        </td>
                        <td>
                            <button type="button" id="btn_add_event" class="btn btn-primary col-md-12">Ajouter</button>
                        </td>
                    </tr>

                    <?php
                    $tab_evenement = $evnt->get_vnt_exa($id_exa);

                    foreach ($tab_evenement as $evenement) {
                        echo "<tr class='appended' id_event='" . $evenement['id_vnt_exa'] . "'>";
                        echo "<td class='text-center' id='hrs_" . $evenement['id_vnt_exa'] . "'>" . $evenement['heure_vnt_exa'] . "</td>";
                        echo "<td class='text-center'>" . $sle->get_name_salle_by_id($evenement['ref_sle']) . "</td>";
                        echo "<td id='texte_" . $evenement['id_vnt_exa'] . "'>" . $evenement['event_exa'] . "</td>";
                        echo "<td id='btn_area_" . $evenement['id_vnt_exa'] . "'>";
                        //Edition
                        echo "<button class='btn btn-warning btn-xs' id='btn_edit_" . $evenement['id_vnt_exa'] . "'  onclick='edit_event(" . $evenement['id_vnt_exa'] . ")'> <span class='glyphicon glyphicon-pencil' ></span>" . "</button>";
                        //Suppression
                        echo "<button class='btn btn-danger btn-xs' id='btn_del_" . $evenement['id_vnt_exa'] . "'  onclick='del_event(" . $evenement['id_vnt_exa'] . ")'> <span class='glyphicon glyphicon-trash' ></span>" . "</button>";

                        echo "</td>";
                        echo "</tr>";
                    }
                    ?>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="center-block">
        <div class="col-md-1"></div>
    </div>
</div>
</div>
</div>
<script src="<?php echo URL; ?>/rapport_exa/js/examen.js"></script>
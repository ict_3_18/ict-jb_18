<!-- Modal pour ajouter un rapport-->
<div class="modal fade" id="modal_rapport">
    <input type="hidden" id="id_can" />
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h3 class="modal-title">Candidat <span id="num_can"></span></h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group col-md-3">
                            <label>Actions Rapides</label>
                            <button type="button" class="btn btn-danger col-md-12" id="candidat_fin">Fini l'examen</button>
                            <br>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="rem_text_modal">Remarques / Informations</label>
                            <textarea class="form-control" name="rem_text_modal" id="rem_text_modal" rows="3"></textarea>
                        </div>
                        <div class="col-md-3">
                            <label>&nbsp;</label>
                            <button type="button" id="btn_add_rem_modal" class="btn btn-primary col-md-12">Ajouter</button>
                            <br><br><br>
                            <button type="button" class="btn btn-warning col-md-12" data-dismiss="modal">Annuler</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
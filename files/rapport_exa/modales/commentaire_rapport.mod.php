<!-- Modal pour mettre fin à l'examen-->
<div class="modal fade" id="com_raport_modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="rapport_title">Fin examen</h4>
            </div>
            <?php
            $exa2 = new Examen($_POST['id_exa']);
            ?>
            <div class="modal-body">
                Procès verbal : <br>
                <textarea class="commentaire_rapport form-control" rows="3" cols="0"><?= $exa2->get_com_rapport()?></textarea>
                <br>
                <button type="button" class="btn btn-primary send_com_rapport" id="btn_fin_valider" id_exa="<?= $_POST['id_exa'] ?>">Valider</button>
                <button type="button" class="btn btn-danger" id="btn_fin_annuler" data-dismiss="modal">Annuler</button>
                <hr>
                <form method="post" action="./pdf/rapport.pdf.php" target="_blank">
                    <input type="hidden" name="id_exa" value="<?= $_POST['id_exa'] ?>">
                    <input type="submit" class="btn btn-sm btn-primary com_rapport" value="Rapport PDF">
                </form>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
<?php
session_start();
$aut = "USR_EXA";
require("./../config/config.inc.php");
require(WAY . "/includes/secure.inc.php");
require_once(WAY . "/includes/autoload.inc.php");
require_once(WAY . "/includes/head.inc.php");
?>
<div class="row">
    <div class="header">
        <h3>Bienvenue</h3>
    </div>
</div>

<div class="panel panel-primary">
    <div class="panel-heading">
        TPI
    </div>

    <div class="panel-body">
        <h2>Interface de développement pour TPI 2018</h2>
        <h3>Projet rapport d'examen :</h3>
        <p>Dépôt git : git@bitbucket.org:jacques_hirtzel/tpi_2018.git</p>
        <p>Branche : Rapport_18</p>
        <p>Dossier de travail : /rapport</p>
      
        
        <h3>Projet critères TPI :</h3>
        <p>Dépôt git : git@bitbucket.org:jacques_hirtzel/tpi_2018.git</p>
        <p>Branche : tpi_18</p>
        <p>Dossier de travail : /tpi</p>
      
      </div>
      <div class="panel-footer">
      </div>
   </div>
</div>

<div class="panel panel-primary">
    <div class="panel-heading">
        Rapport d'examens
    </div>

    <div class="panel-body">
        <?php
        //Affichage des examens dans la date est plus grande ou égale à aujourd'hui
        $tab_exa = $per->pdo->query("SELECT no_ich_exa,id_exa FROM t_examens WHERE date_heure_exa >= '2019-06-16' ORDER BY date_heure_exa")->fetchAll();
        foreach ($tab_exa AS $exa) {
            ?>
            <form method="post" action ="<?php echo URL . "rapport_exa/examen.php"; ?>" id="TestForm">
                <input type="hidden" name="id_exa" value="<?php echo $exa['id_exa']; ?>">
                <input type="submit" value="<?php echo $exa['no_ich_exa']; ?>">
            </form>
            <?php
        }
        ?>

    </div>
    <div class="panel-footer">
    </div>
</div>
</div>
</body>
</html>
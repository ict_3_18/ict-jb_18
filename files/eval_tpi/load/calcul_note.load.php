<?php
session_start();
require("./../../config/config.inc.php");
//require(WAY . "/includes/secure.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

?>
<div class="row">
    <div class="form-group col-md-6">
        <label for="comp_prof">Compétences professionnelles (50%)</label> 
        <input type="number" step=".5" min="1" max="6" class="form-control" id="comp_prof" name="comp_prof" placeholder="Comp. prof">
    </div>
</div>
<div class="row">
    <div class="form-group col-md-6">
        <label for="doc">Documentation (25%)</label> 
        <input type="number" step=".5" min="1" max="6" class="form-control" id="doc" name="doc" placeholder="Documentation">
    </div>
</div>
<div class="row">
    <div class="form-group col-md-6">
        <label for="pres">Entretien et présentation (25%)</label> 
        <input type="number" step=".5" min="1" max="6" class="form-control" id="pres" name="pres" placeholder="Entretien">
    </div>
</div>
<div class="row">
    <div class="form-group col-md-2">
        <button type="submit" class="btn btn-primary">Calcul</button>
    </div>
</div>
<div class="row">
    <div class="form-group col-md-12">
        <label for="nb_pts">Note</label> 
        <span  id="note" name="note" ></span>
    </div>
</div>   

<?php
session_start();
require("./../../config/config.inc.php");
//require(WAY . "/includes/secure.inc.php");
require_once(WAY . "/includes/autoload.inc.php");
//require_once(WAY . "/includes/head.inc.php");


//print_r($_SESSION['basket']);


?>
<table class="table table-bordered table-striped">
<tr>
	<th>Code</th>
	<th>Critère</th>
	<th>Description</th>
	<th></th>
</tr>
<?php 

foreach($_SESSION['basket'] AS $id_crt => $id){
    $crt = new Critere($id_crt);
    ?>
    <tr id="tr_crt_<?= $id_crt ?>">
    	<th><?= $crt->get_code() ?></th>
    	<th><?= $crt->get_nom() ?></th>
    	<td><?= $crt->get_description() ?></td>
    	<td>
    		<button class="btn btn-danger del_crt" id_crt="<?= $id_crt ?>">
    			<span class="glyphicon glyphicon-trash"></span>    
    		</button>
    	</tr>
        
    <?php 
}



?>
</table>


<div class="form-group col-md-6">
	<label for="email">Envoyer ces crières à :</label>
	<input type="email" class="form-control" id="email" placeholder="Votre email">
	<button id="send_mail_crt" class="btn btn-primary">Envoyer</button>
</div>

<div class="form-group col-md-6">
	<form action="./pdf/crt_lst.pdf.php" method="post">
		<button type="submit" id="print_lst_crt" class="btn btn-primary">Print</button>
	</form>
</div>



<script src="./js/basket.js"></script>
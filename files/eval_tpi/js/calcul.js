$(function(){
    $("#critere #nb_pts_max, #critere #nb_pts").change(function(){
        var nb_pts_max = $("#nb_pts_max").val();
        var nb_pts = $("#nb_pts").val();
        var note = 5/nb_pts_max*nb_pts+1;
        var note_cent = Math.round(note*100)/100;
        var note_demi = Math.round(note_cent*2)/2;
        $("#note").html(note_demi+" ("+note_cent+")");
    });
});
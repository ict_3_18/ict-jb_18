<?php
    session_start();
    require('../../class/fpdf/fpdf.php');
    $aut = "ADM_CONV;USR_CONV";
    require("../../config/config.inc.php");
    require_once(WAY."./includes/autoload.inc.php");

   // print_r($_POST);

    $can = new Candidat($_POST['id_can']);
    $note_exa = new Notes($can->get_id());
    
    $domaines = $note_exa->get_domaines();
    $no_candidat = substr($can->get_no_candidat(), -3);     //Cherche le numéro du candidats
    $matu = strpos($can->get_classe(), "MP");               //Contrôle si le candidats est en matu ou non
    
    $pdf = new FPDF();
    $pdf->AddPage();
    $pdf->SetMargins(10, 20, 20);
    
    /*
     * Affiche le titre (nom, prénom et numéro du candidat)
     */
    $pdf->SetFont('Arial', 'B', 14);
    $pdf->Cell(0,10, utf8_decode("Candidat " . $no_candidat . ", " . $can->get_prenom() . " " . $can->get_nom()), 0, 1);
    
    $pdf->SetLeftMargin(15);
    $pdf->SetFont('Arial','B',12);
    $pdf->Cell(0,10, utf8_decode("Compétences informatiques"), 0, 1);
    $pdf->SetLeftMargin(20);
    
    /*
     * Affichage du tableau des notes 
     */
    foreach ($domaines AS $dom) { //Passe les domaines en revu
        $avg = $note_exa->get_color_avg_from_avg($dom['id_dom']);
        $notes = $note_exa->get_notes_valides_to_dom($dom['id_dom']);
        
        if($dom['id_dom'] == 3){ // ECG --> n'est pas pris en compte
            continue;
            
        }elseif($dom['id_dom'] == 4){ // TPI --> n'est pas pris en compte
            continue;
            
        }elseif($dom['id_dom'] == 5){ // CIE
            $pdf->SetFont('Arial','B',10);
            $pdf->Cell(40,5, utf8_decode($dom['nom_dom']), 0, 1);

            foreach ($notes AS $note) { // Passe les notes du domaine en revu
                $pdf->SetFont('Arial','',8);
                $pdf->Cell(15,5, utf8_decode($note['num_mod']), 1);
                $pdf->Cell(145,5, utf8_decode($note['nom_mod']), 1);
                $pdf->Cell(0,5, utf8_decode(number_format($note['note_can'], 1)), 1, 1, 'C');
            }
            
            // Moyenne des notes CIE
            $pdf->SetFont('Arial','B',8);
            $pdf->Cell(150, 5, utf8_decode("Moyenne (20%)"), 1);
            $pdf->Cell(10, 5, utf8_decode(round($avg['avg'], 2)), 1, 0, 'C');
            $pdf->Cell(0, 5, utf8_decode(number_format(round($avg['avg'] * 2) / 2, 1)), 1, 1, 'C');
            $pdf->Ln();
            
        }elseif($dom['id_dom'] == 6){ //Formation scolaire
            $pdf->SetFont('Arial','B',10);
            $pdf->Cell(40,5, utf8_decode($dom['nom_dom']), 0, 1);

            foreach ($notes AS $note) { // Passe les notes du domaine en revu
                $pdf->SetFont('Arial','',8);
                $pdf->Cell(15,5, utf8_decode($note['num_mod']), 1);
                $pdf->Cell(145,5, utf8_decode($note['nom_mod']), 1);
                $pdf->Cell(0,5, utf8_decode(number_format($note['note_can'], 1)), 1, 1, 'C');
            }
            
            // Moyenne des notes formation scolaire
            $pdf->SetFont('Arial','B',8);
            $pdf->Cell(150, 5, utf8_decode("Moyenne (80%)"), 1);
            $pdf->Cell(10, 5, utf8_decode(round($avg['avg'], 2)), 1, 0, 'C');
            $pdf->Cell(0, 5, utf8_decode(number_format(round($avg['avg'] * 2) / 2, 1)), 1, 0, 'C');
            
        }elseif($dom['id_dom'] == 8 AND $matu === false){ // Compétences élargies
            $pdf->SetLeftMargin(15);
            $pdf->Ln();
            $pdf->SetFont('Arial','B',12);
            $pdf->Cell(145,10, utf8_decode($dom['nom_dom']));
            $pdf->Cell(0,5, "", 0, 1);
            $pdf->Cell(145,5, "");
            $pdf->SetFont('Arial','B',8);
            $pdf->Cell(0,5, "Moyenne", 1, 1, 'C');
            $pdf->SetLeftMargin(20);
            
            $pdf->SetFont('Arial','',8);
            $pdf->Cell(20,5, utf8_decode("Semestre :"), 1);
            
            for($i=1;$i<=8;$i++){
                $pdf->Cell(15,5, utf8_decode($i), 1, 0, 'C');
            }
            
            foreach ($notes AS $note) {
                $tab_comp_el[$note['num_mod']] = $note;
            }
            
            $pdf->Cell(0,5, "", 1, 1);
            $pdf->SetFont('Arial','',8);
            $pdf->Cell(20,5, utf8_decode("Notes :"), 1);
            
            //Affiche les notes des compétences élargies et laisse une case vide lorsqu'il n'y a pas de note
            for($i=91;$i<=98;$i++){
                if(key_exists($i, $tab_comp_el)){
                    $pdf->Cell(15,5, utf8_decode($tab_comp_el[$i]['note_can']), 1, 0, 'C');
                }else{
                    $pdf->Cell(15,5, "", 1);
                }
            }
            
            $pdf->SetFont('Arial','B',8);
            $pdf->Cell(0, 5, utf8_decode(number_format(round($avg['avg'] * 2) / 2, 1)) . " (" . utf8_decode(round($avg['avg'], 2)) . ")", 1, 0, 'C');
        }
    }
    //echo "toto";
    $pdf->SetLeftMargin(15);
    $pdf->Ln();
    $pdf->SetFont('Arial','B',12);
    $pdf->Cell(40,10, utf8_decode("Bulletin"), 0, 1);
    
    $pdf->SetLeftMargin(20);
    $pdf->SetFont('Arial','',8);
    
    $avg_dom = $note_exa->get_avg_to_dom();
    
    foreach($avg_dom AS $key => $avg){
        $avg_dom_round[$key] = round($avg*2)/2;
    }
    
    $pdf->SetFont('Arial','B',8);
    
    /*
     * La totalitée du code ci-dessous affiche les notes du bulletin
     * Si l'éléve est en matu alors l'affichage changera
     */
    
    if (isset($avg_dom_round['Formation scolaire']) && isset($avg_dom_round['CIE'])) {
        $avg_ci = (($avg_dom_round['Formation scolaire'] * 8) + ($avg_dom_round['CIE'] * 2)) / 10;
        
        $pdf->Cell(160,5, utf8_decode("Compétences en informatique (x3)"), 1);
        $pdf->Cell(0,5, utf8_decode(number_format(round($avg_ci,1), 1)), 1, 1, 'C');
        
        $pdf->SetFont('Arial','',8);
    }
    
    $pdf->SetFont('Arial','B',8);
    
    if (isset($avg_dom_round['Compétences élargies']) && $matu === false) {
        $pdf->Cell(160,5, utf8_decode("Compétences élargies (x2)"), 1);
        $pdf->Cell(0,5, utf8_decode(number_format(round($avg_dom_round['Compétences élargies'],1), 1)), 1, 1, 'C');
    }
    //echo "---".$can->get_termine_classe()."---";
    if($can->get_termine_classe() == 1 AND $can->get_stop_form_classe() == 0){ //Regarde si l'éléve à terminé la formation ou non

        if (isset($avg_dom_round['TPI'])) {
            $pdf->Cell(160,5, utf8_decode("TPI (x3)"), 1);
            $pdf->Cell(0,5, utf8_decode(number_format(round($avg_dom_round['TPI'],1), 1)), 1, 1, 'C');
        }

        if (isset($avg_dom_round['ECG']) && $matu === false) {
            $pdf->Cell(160,5, utf8_decode("ECG (x2)"), 1);
            $pdf->Cell(0,5, utf8_decode(number_format(round($avg_dom_round['ECG'],1), 1)), 1, 1, 'C');
        }

        if ($matu === false) {
            if (isset($avg_ci) && isset($avg_dom_round['TPI']) && isset($avg_dom_round['Compétences élargies']) && isset($avg_dom_round['ECG'])) {
                $avg = ((3 * $avg_ci) + (3 * $avg_dom_round['TPI']) + (2 * $avg_dom_round['Compétences élargies']) + (2 * $avg_dom_round['ECG'])) / 10;
            } else if (isset($avg_ci) && isset($avg_dom_round['TPI']) && isset($avg_dom_round['Compétences élargies'])) {
                $avg = ((3 * $avg_ci) + (3 * $avg_dom_round['TPI']) + (2 * $avg_dom_round['Compétences élargies'])) / 8;
            }
        }else{
            if (isset($avg_ci) && isset($avg_dom_round['TPI']))
                $avg = ($avg_ci + $avg_dom_round['TPI']) / 2;
        }

        $pdf->Cell(150,5, utf8_decode("Note globale"), 1);
        $pdf->Cell(10,5, utf8_decode(round($avg,2)), 1, 0, 'C');
        if ($avg != null){
            $pdf->Cell(0,5, utf8_decode(number_format(round($avg,1), 1)), 1, 1, 'C');
        }else{
            $pdf->Cell(0,5, "", 1, 1);
        }
    }
    $pdf->Output();
?>
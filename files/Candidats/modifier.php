<?php 
    session_start();
    $aut="ADM_CAN";
    require '../config/config.inc.php';
    require(WAY."/includes/secure.inc.php");    
    require WAY . "/includes/head.inc.php";
    require_once(WAY."/includes/autoload.inc.php");
    
    $can = new Candidat($_POST['modif']);
?>
    <div class="container">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3>Modification d'un candidat</h3>
                </div>
                <div class ="panel-body">
                    <form class="form-horizontal" id="modif_can_form" method="post" action="profil.php">
                        <div class="form-group">
                            <label class="control-label col-md-2" for="no_candidat">N° candidat * :</label>
                            <div class="col-md-10">
                                <input class="form-control" type="number" value='<?php echo $can->get_no_candidat();?>' id="no_candidat" name="no_candidat">
                                <input type="hidden" value='<?php echo $can->get_id();?>' id="id_can" name="id_can">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="login_can">Login * :</label>
                            <div class="col-md-10">
                                <input class="form-control" type="text" value='<?php echo $can->get_login();?>' placeholder="nom d'utilisateur du candidat" id="login_can" name="login_can">
                            </div>
                        </div>   
                        <div class="form-group">
                            <label class="control-label col-md-2" for="sexe_can">Genre :</label>
                            <div class="col-md-10">
                                <select class="form-control" id="sexe_can" name="sexe_can">
                                    <?php
                                        if($can->get_sexe() === "F"){
                                            echo "<option value='M'>M</option>
                                            <option value='F' selected='true'>F</option>";
                                        }else{
                                            echo "<option value='M' selected='true'>M</option>
                                            <option value='F'>F</option>";
                                        }
                                    ?>
                                </select>                    
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="nom_can">Nom * :</label>
                            <div class="col-md-10">
                                <input class="form-control" type="text" <?php echo "value='".$can->get_nom()."'"?> id="nom_can" name="nom_can">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="prenom_can">Prenom * :</label>
                            <div class="col-md-10">
                                <input class="form-control" type="text" <?php echo "value='".$can->get_prenom()."'"?> id="prenom_can" name="prenom_can">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="email_can">E-mail * :</label>
                            <div class="col-md-10">
                                <input class="form-control" type="text" <?php echo "value='".$can->get_email()."'"?> id="email_can" name="email_can">
                            </div>
                        </div>        
                        <div class="form-group">
                            <label class="control-label col-md-2" for="annee_can">année * :</label>
                            <div class="col-md-10">
                                <input class="form-control" type="number" <?php echo "value='".$can->get_annee()."'"?> id="annee_can" name="annee_can">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="systeme_can">Système :</label>
                            <div class="col-md-10">
                                <select class="form-control" id="systeme_can" name="systeme_can">
                                    <?php
                                        if($can->get_systeme()!=""){
                                            echo "<option value='Dual' selected='true'>Dual</option>
                                            <option value=''>Plein temps</option>";
                                        }else{
                                            echo "<option value='Dual'>Dual</option>
                                            <option value='' selected='true'>Plein temps</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="employeur_can">Employeur :</label>
                            <div class="col-md-10">
                                <select class="form-control" id="employeur_can" name="employeur_can">
                                    <?php
                                        $emp = new Entreprise();
                                        $tab_em = $emp->get_all();
                                        
                                        foreach($tab_em AS $empl){
                                            if($empl['id_emp']===$can->get_employeur()){
                                                echo "<option value=".$empl['id_emp']." selected='true'>".$empl['nom_emp']."</option>";
                                            }else{
                                                echo "<option value=".$empl['id_emp'].">".$empl['nom_emp']."</option>";
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="classe_can">Classe :</label>
                            <div class="col-md-10">
                                <select class="form-control" id="classe_can" name="classe_can">
                                    <?php
                                        $cla = new Classe();
                                        $tab_cla = $cla->get_all_with_grp();

                                        foreach($tab_cla AS $class){

                                            $value = $class['nom_grp']."  -  ".$class['nom_cla'];

                                            if($class['id_cla']===$can->get_classe()){
                                                echo "<option value=".$class['id_cla']." selected='true'>".$value."</option>";
                                            }else{
                                                echo "<option value=".$class['id_cla'].">".$value."</option>";
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="control-label col-md-2" for="dys_can">Temps supplémentaire</label>
                            <div class="col-md-10">
                                <input type="checkbox" class="form-control" id="dys_can" name="dys_can" <?= ($can->get_if_dys($can->get_id())) ? "checked" : "" ?> > 
								<?= ($can->get_if_dys($can->get_id())) ? $can->get_moyens_supp() : "" ?>
                            </div>
                        </div>
                        <div>
                            Les champs marqués * sont obligatoires
                        </div>
                        <div class="btn btn-group">
                            <input class="btn btn-primary" type='submit' value='Modifier' id="submit_modif">
                    </form>
                    <form class="form-horizontal" id="modif_can_form" method="post" action="profil.php">
                            <input type='hidden' value='<?php echo $can->get_id() ?>' id='modif' name='modif'>
                            <input type="submit" class="btn btn-primary" value='Profil' id="cancel_modif" type="button">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="./js/modif_candidat.js"></script>
    </body>
</html>
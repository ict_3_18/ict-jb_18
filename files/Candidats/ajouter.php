<?php 
    session_start();  
    $aut="ADM_CAN";
    require '../config/config.inc.php';
    require(WAY."/includes/secure.inc.php"); 
    require WAY . "/includes/head.inc.php";
    require_once(WAY."/includes/autoload.inc.php");  
?>
    <div class="container">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3>Ajout d'un candidat</h3>
                </div>
                <div class ="panel-body">
                    <form class="form-horizontal" id="modif_can_form" method="post" action="check.php">        
                        <div class="form-group">
                            <label class="control-label col-md-2" for="no_candidat">N° candidat * :</label>
                            <div class="col-md-10">
                                <input class="form-control" type="number" placeholder="<?= date('y')?>xx" id="no_candidat" name="no_candidat">
                            </div>
                        </div>                     
                        <div class="form-group">
                            <label class="control-label col-md-2" for="login_can">Login * :</label>
                            <div class="col-md-10">
                                <input class="form-control" type="text" value="" placeholder="CP-<?= date('y')?>xxx" id="login_can" name="login_can">
                            </div>
                        </div>     
                        <div class="form-group">
                            <label class="control-label col-md-2" for="sexe_can">Genre :</label>
                            <div class="col-md-10">
                                <select class="form-control" id="sexe_can" name="sexe_can">
                                    <option value="M">M</option>
                                    <option value="F">F</option>
                                </select>                    
                            </div>
                        </div>              
                        <div class="form-group">
                            <label class="control-label col-md-2" for="nom_can">Nom * :</label>
                            <div class="col-md-10">
                                <input class="form-control" type="text" placeholder="" id="nom_can" name="nom_can">
                            </div>
                        </div>        
                        <div class="form-group">
                            <label class="control-label col-md-2" for="prenom_can">Prenom * :</label>
                            <div class="col-md-10">
                                <input class="form-control" type="text" placeholder="" id="prenom_can" name="prenom_can">
                            </div>
                        </div>        
                        <div class="form-group">
                            <label class="control-label col-md-2" for="email_can">E-mail * :</label>
                            <div class="col-md-10">
                                <input class="form-control" type="text" placeholder="" id="email_can" name="email_can">
                            </div>
                        </div>        
                        <div class="form-group">
                            <label class="control-label col-md-2" for="annee_can">Année * :</label>
                            <div class="col-md-10">
                                <input class="form-control" type="number" placeholder="année du candidat" id="annee_can" name="annee_can" value="<?= date('Y')?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="systeme_can">Système :</label>
                            <div class="col-md-10">
                                <select class="form-control" id="systeme_can" name="systeme_can">
                                    <option value="">Plein temps</option>
                                    <option value="Dual">Dual</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="employeur_can">Employeur :</label>
                            <div class="col-md-10">
                                <select class="form-control" id="employeur_can" name="employeur_can">
                                    <?php
                                        $emp = new Entreprise();
                                        $tab_em = $emp->get_all();
                                        
                                        foreach($tab_em AS $empl){
                                            if($empl['nom_emp']=="ceff"){
                                                echo "<option selected=true value=".$empl['id_emp'].">".$empl['nom_emp']."</option>";
                                            }else{
                                                echo "<option value=".$empl['id_emp'].">".$empl['nom_emp']."</option>";
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="classe_can">Classe :</label>
                            <div class="col-md-10">
                                <select class="form-control" id="classe_can" name="classe_can">
                                    <?php
                                        $cla = new Classe();
                                        $tab_cla = $cla->get_all_with_grp_actif();
                                        
                                        foreach($tab_cla AS $class){
                                            echo "<option value=".$class['id_cla'].">".$class['nom_grp']." ".$class['nom_cla']."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="control-label col-md-2" for="dys_can">Temps supplémentaire</label>
                            <div class="col-md-10">
                                <input type="checkbox" class="form-control" id="dys_can">
                            </div>
                        </div>
                    <div>
                        Les champs marqués * sont obligatoires
                    </div>
                    <input class="btn btn-primary" type='submit' value ='Ajouter' id="submit_ajout">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="./js/ajout_candidat.js"></script>
    </body>
</html>
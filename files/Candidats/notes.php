<?php 
    session_start();
    $aut="ADM_CAN";
    require '../config/config.inc.php';
    require(WAY."/includes/secure.inc.php");  
    require WAY . "/includes/head.inc.php";
    require_once(WAY."/includes/autoload.inc.php");  
    
    $can = new Candidat($_POST['id_can']);
    


$note_exa = new Notes($can->get_id());

//print_r($_POST);
?>



<!-- Parties ouvertes -->
<div class="panel panel-primary">
    <div class="panel-heading">
        <h4>Notes </h4>
    </div>
    <div class="panel-body">
        <?php
        $domaines = $note_exa->get_domaines();
        foreach ($domaines AS $dom) {
            $avg = $note_exa->get_color_avg_from_avg($dom['id_dom']);
            $notes = $note_exa->get_notes_to_dom($dom['id_dom']);
            ?>
            <div class="col-md-<?php echo (count($domaines) > 2 ? 6 : 12); ?>">
                <div class="panel panel-<?php echo $avg['color']; ?>">
                    <div class="panel-heading">
                        <?php echo $dom['nom_dom']; ?>
                    </div>
                    <div class="panel-body">
                        <?php
                        foreach ($notes AS $note) {
                            if($note['remedie_note'] == 0){
                                echo '<div class="row' . ($note['note_can'] < 4 ? " text-red" : "") . '">';
                                echo '<div class="col-xs-1">' . $note['num_mod'] . '</div>';
                                echo '<div class="col-xs-9">' . $note['nom_mod'] . '</div>';
                                echo '<div class="col-xs-2">' . $note['note_can'] . '</div>';
                                echo '</div>';
                            }
                        }
                        echo '<div class="row bg-' . $avg['color'] . ' text-bold">';
                        echo '<div class="col-xs-8">Moyenne</div>';
                        echo '<div class="col-xs-1">' . round($avg['avg'],2) . '</div>';
                        echo '<div class="col-xs-1"><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></div>';
                        echo '<div class="col-xs-2">' . round($avg['avg'] * 2) / 2 . '</div>';
                        echo '</div>';
                        ?>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
        <div class="col-xs-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Bulletin
                </div>
                <div class="panel-body">
                    <?php
                    $avg_dom = $note_exa->get_avg_to_dom();
                    
                    foreach($avg_dom AS $key => $avg){
                        $avg_dom_round[$key] = round($avg*2)/2;
                    }
                    
                    $matu = strpos($can->get_classe(), "MP");

                    if (isset($avg_dom_round['Formation scolaire']) && isset($avg_dom_round['CIE'])) {
                        $avg_ci = (($avg_dom_round['Formation scolaire'] * 8) + ($avg_dom_round['CIE'] * 2)) / 10;
                    ?>
                        <div class="row<?php echo ($avg_dom_round['Formation scolaire'] < 4 ? " text-red" : ""); ?>">
                            <div class="col-xs-12">Compétences en informatique</div>
                        </div>
                        <div class="row<?php echo ($avg_dom_round['Formation scolaire'] < 4 ? " text-red" : ""); ?>">
                            <div class="col-xs-9 col-xs-offset-1">Formation scolaire (80%)</div>
                            <div class="col-xs-2"><?php echo round($avg_dom_round['Formation scolaire'] * 2) / 2; ?></div>
                        </div>
                        <div class="row<?php echo ($avg_dom_round['CIE'] < 4 ? " text-red" : ""); ?>">
                            <div class="col-xs-9 col-xs-offset-1">Cours interentreprises (20%)</div>
                            <div class="col-xs-2"><?php echo round($avg_dom_round['CIE'] * 2) / 2; ?></div>
                        </div>
                        <div class="row text-bold<?php echo ($avg_ci < 4 ? " bg-danger" : ($avg_ci > 4 ? " bg-success" : " bg-info")); ?>">
                            <div class="col-xs-8">Compétences en informatique (x3)</div>
                            <div class="col-xs-1"><?php echo round($avg_ci,2); ?></div>
                            <div class="col-xs-1"><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></div>
                            <div class="col-xs-2"><?php echo round($avg_ci,1); ?></div>
                        </div>
                        <br>
                    <?php
                    }
                    
                    if (isset($avg_dom_round['TPI'])) {
                    ?>
                        <div class="row text-bold<?php echo ($avg_dom_round['TPI'] < 4 ? " bg-danger" : ($avg_dom_round['TPI'] > 4 ? " bg-success" : " bg-info")); ?>">
                            <div class="col-xs-10">TPI (x3)</div>
                            <div class="col-xs-2"><?php echo $avg_dom_round['TPI']; ?></div>
                        </div>
                        <br>
                    <?php
                    }
                    
                    if (isset($avg_dom_round['Compétences élargies']) && $matu === false) {
                    ?>
                        <div class="row text-bold<?php echo ($avg_dom_round['Compétences élargies'] < 4 ? " bg-danger" : ($avg_dom_round['Compétences élargies'] > 4 ? " bg-success" : " bg-info")); ?>">
                            <div class="col-xs-10">Compétences élargies (x2)</div>
                            <div class="col-xs-2"><?php echo $avg_dom_round['Compétences élargies']; ?></div>
                        </div>
                        <br>
                    <?php
                    }
                    
                    if (isset($avg_dom_round['ECG']) && $matu === false) {
                    ?>
                        <div class="row text-bold<?php echo ($avg_dom_round['ECG'] < 4 ? " bg-danger" : ($avg_dom_round['ECG'] > 4 ? " bg-success" : " bg-info")); ?>">
                            <div class="col-xs-10">ECG (x2)</div>
                            <div class="col-xs-2"><?php echo $avg_dom_round['ECG']; ?></div>
                        </div>
                        <br>
                    <?php
                    }
                    
                    if ($matu === false) {
                        if (isset($avg_ci) && isset($avg_dom_round['TPI']) && isset($avg_dom_round['Compétences élargies']) && isset($avg_dom_round['ECG'])) {
                            $avg = ((3 * $avg_ci) + (3 * $avg_dom_round['TPI']) + (2 * $avg_dom_round['Compétences élargies']) + (2 * $avg_dom_round['ECG'])) / 10;
                        } else if (isset($avg_ci) && isset($avg_dom_round['TPI']) && isset($avg_dom_round['Compétences élargies'])) {
                            $avg = ((3 * $avg_ci) + (3 * $avg_dom_round['TPI']) + (2 * $avg_dom_round['Compétences élargies'])) / 8;
                        }
                    } else {
                        if (isset($avg_ci) && isset($avg_dom_round['TPI']))
                            $avg = ($avg_ci + $avg_dom_round['TPI']) / 2;
                    }
                    ?>
                    
                    <div class="row text-bold <?php if ($avg != null) echo ($avg < 4 ? " bg-danger" : ($avg > 4 ? " bg-success" : " bg-info")); ?>">
                        <div class="col-xs-10">Note globale</div>
                        <div class="col-xs-2"><?php if ($avg != null) echo round($avg,1); ?></div>
                    </div>
                    <br>
                    <div class="panel panel-primary">
                        <div class="row">
                            <div class="col-xs-12">
                                <b>La note globale est claculée comme suit :</b>
                                <ul>
                                    <li>3x la note "Compétences en informatique"</li>
                                    <li>3x la note "TPI"</li>
                                    <?php echo ($matu === false ? '<li>2x la note "Compétences élargies"</li>' : "") . (isset($avg_dom_round['ECG']) == true ? "<li>2x la note ECG</li>" : "") ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel-footer">
        Ceff industrie
    </div>
</div>
</div>
</body>
</html>
<?php
session_start();
require("../config/config.inc.php");
require(WAY . "/includes/autoload.inc.php");
require WAY . '/Public/includes/secure.inc.php';
require_once(WAY . "/Public/includes/head.inc.php");

$note_exa = new Notes($can->get_id());
$exa = new Examen($can->get_id());

?>
<div class="row">
    <div class="header">
        <h3>Bienvenue sur ICT-JB</h3>
    </div>
</div>

<!-- Parties ouvertes -->
<div class="panel panel-primary">
    <div class="panel-heading">
        <h4>Notes</h4>
    </div>
    <div class="panel-body">
        <?php
        $domaines = $note_exa->get_domaines();
        foreach ($domaines AS $dom) {
            $avg = $note_exa->get_color_avg_from_avg($dom['id_dom']);
            $notes = $note_exa->get_notes_to_dom($dom['id_dom']);
            ?>
            <div class="col-md-<?php echo (count($domaines) > 2 ? 6 : 12); ?>">
                <div class="panel panel-<?php echo $avg['color']; ?>">
                    <div class="panel-heading">
                        <?php echo $dom['nom_dom']; ?>
                    </div>
                    <div class="panel-body">
                        <?php
                        foreach ($notes AS $note) {
                            echo '<div class="row' . ($note['note_can'] < 4 ? " text-red" : "") . '">';
                            echo '<div class="col-md-2">' . $note['num_mod'] . '</div>';
                            echo '<div class="col-md-8">' . $note['nom_mod'] . '</div>';
                            echo '<div class="col-md-2">' . $note['note_can'] . '</div>';
                            echo '</div>';
                        }
                        echo '<div class="row bg-' . $avg['color'] . ' text-bold">';
                        echo '<div class="col-md-10">Moyenne</div>';
                        echo '<div class="col-md-2">' . $avg['avg'] . '</div>';
                        echo '</div>';
                        ?>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Bulletin
                </div>
                <div class="panel-body">
                    <?php
                    $avg_dom = $note_exa->get_avg_to_dom();
                    $matu = strpos($can->get_classe(), "MP");

                    if (isset($avg_dom['Formation scolaire']) && isset($avg_dom['CIE'])) {
                        $avg_ci = (($avg_dom['Formation scolaire'] * 8) + ($avg_dom['CIE'] * 2)) / 10;
                        ?>
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                Compétences en informatique (x3)
                            </div>
                            <div class="panel-body">
                                <div class="row<?php echo ($avg_dom['Formation scolaire'] < 4 ? " text-red" : ""); ?>">
                                    <div class="col-md-6">Compétences en informatique, formation scolaire (80%)</div>
                                    <div class="col-md-6"><?php echo $avg_dom['Formation scolaire']; ?></div>
                                </div>
                                <div class="row<?php echo ($avg_dom['CIE'] < 4 ? " text-red" : ""); ?>">
                                    <div class="col-md-6">Cours interentreprises (20%)</div>
                                    <div class="col-md-6"><?php echo $avg_dom['CIE']; ?></div>
                                </div>
                                <div class="row text-bold<?php echo ($note['note_can'] < 4 ? " bg-danger" : ($note['note_can'] > 4 ? " bg-success" : " bg-info")); ?>">
                                    <div class="col-md-6">Moyenne</div>
                                    <div class="col-md-6"><?php echo $avg_ci; ?></div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }

                    //print_r($note_exa);

                    if (isset($avg_dom['TPI'])) {
                        ?>
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                TPI (x3)
                            </div>
                            <div class="panel-body">
                                <div class="row text-bold<?php echo ($avg_dom['TPI'] < 4 ? " bg-danger" : ($avg_dom['TPI'] > 4 ? " bg-success" : " bg-info")); ?>">
                                    <div class="col-md-6">TPI</div>
                                    <div class="col-md-6"><?php echo $avg_dom['TPI']; ?></div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }

                    if (isset($avg_dom['Compétences élargies']) && $matu === false) {
                        ?>

                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                Compétences élargies (x2)
                            </div>
                            <div class="panel-body">
                                <div class="row text-bold<?php echo ($avg_dom['Compétences élargies'] < 4 ? " bg-danger" : ($avg_dom['Compétences élargies'] > 4 ? " bg-success" : " bg-info")); ?>">
                                    <div class="col-md-6">Compétences élargies</div>
                                    <div class="col-md-6"><?php echo $avg_dom['Compétences élargies']; ?></div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }

                    if (isset($avg_dom['ECG']) && $matu === false) {
                        ?>

                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                ECG (x2)
                            </div>
                            <div class="panel-body">
                                <div class="row text-bold<?php echo ($avg_dom['ECG'] < 4 ? " bg-danger" : ($avg_dom['ECG'] > 4 ? " bg-success" : " bg-info")); ?>">
                                    <div class="col-md-6">ECG</div>
                                    <div class="col-md-6"><?php echo $avg_dom['ECG']; ?></div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }

                    if ($matu === false) {
                        if (isset($avg_ci) && isset($avg_dom['TPI']) && isset($avg_dom['Compétences élargies']) && isset($avg_dom['ECG'])) {
                            $avg = ((3 * $avg_ci) + (3 * $avg_dom['TPI']) + (2 * $avg_dom['Compétences élargies']) + (2 * $avg_dom['ECG'])) / 10;
                        } else if (isset($avg_ci) && isset($avg_dom['TPI']) && isset($avg_dom['Compétences élargies'])) {
                            $avg = ((3 * $avg_ci) + (3 * $avg_dom['TPI']) + (2 * $avg_dom['Compétences élargies'])) / 8;
                        }
                    } else {
                        if (isset($avg_ci) && isset($avg_dom['TPI']))
                            $avg = ((3 * $avg_ci) + (3 * $avg_dom['TPI'])) / 6;
                    }
                    ?>
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            Note globale
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    La note globale est claculée comme suit : 3x la note "Compétences en informatique" + 3x la note "TPI"<?php echo ($matu === false ? ' + 2x la note "Compétences élargies"' : "") . (isset($avg_dom['ECG']) == true ? " + 2x la note ECG" : "") ?>
                                </div>
                            </div>
                            <div class="row text-bold <?php if ($avg != null) echo ($avg < 4 ? " bg-danger" : ($avg > 4 ? " bg-success" : " bg-info")); ?>">
                                <div class="col-md-6">Note globale</div>
                                <div class="col-md-6"><?php if ($avg != null) echo $avg; ?></div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel-footer">
        Ceff industrie
    </div>
</div>
</div>
</body>
</html>
<?php
session_start();
$aut="USR_CAN";
require '../config/config.inc.php';
require(WAY."/includes/secure.inc.php");
require WAY . "/includes/head.inc.php";
require_once(WAY."/includes/autoload.inc.php");

if(isset($_POST['modif'])){
    $can = new Candidat($_POST['modif']);
}else{
    $can = new Candidat($_POST['id_choice_can']);
}
?>

<div class="container">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h4>Infos candidat</h4>
            </div>
            <div class ="panel-body">
                <span class="col-md-12">
                    <h3><?php echo $can->get_prenom()." ".$can->get_nom()." "."(".$can->get_no_candidat_minus().")";?></h3>
                <hr>
                </span>

                <span class="col-md-8">

                    <?php
                    $emp = new Entreprise($can->get_employeur());
                    $contact = new Personne($emp->get_pers_contact_emp());

                    echo "N° Candidat : " . $can->get_no_candidat_minus() . "<br>";
                    echo "Login : " . $can->get_login() . "<br>";
                    echo "Classe : " . $can->get_nom_classe() . "<br>";
                    echo "Email : <a href=mailto:".$can->get_email().">" . $can->get_email() . "</a><br>";
                    echo "Année d'entrée : " . $can->get_annee() . "<br>";

					if($can->get_dys() == 1){
						echo "<br><b>Moyens supplémentaires accordés : " . $can->get_moyens_supp() . "</b><br>";
					}
					echo "<hr>";

                    //echo "<span class='glyphicon glyphicon-envelope' onclick=window.location='mailto:".$can->get_email()."'></span><br>";
                    if($can->get_systeme() == ""){
                        echo "Système : Plein temps <br>";
                        echo "Employeur : " . $emp->get_nom() . "<br>";
                    }else{
                        echo "<b>Système : " . $can->get_systeme() . "</b><br>";
                        echo "<br><b>Infos employeur </b>";
                        echo "<hr>";
                        echo $emp->get_nom() . "<br>";
                        echo $emp->get_adresse_emp() . "<br>";
                        echo $emp->get_npa_emp() . " " . $emp->get_localite_emp() . "<br>";
                        echo "<br>Personne de contact : ".$contact->get_prenom()." ".$contact->get_nom() . "<br>";
                        echo "Email : <a href=mailto:".$contact->get_email().">".$contact->get_email() . "</a><br>";
                        if($contact->get_tel_bureau()){
                            echo "Tel. bureau : " . $contact->get_tel_bureau() . "<br>";
                        }
                        if($contact->get_tel_mobile()){
                            echo "Tel. mobile : " . $contact->get_tel_mobile() . "<br>";
                        }
                    }
                    ?>
                    <br><br>


                </span>

                <span class="col-md-4">
                    <?php
                    if($per->check_aut("ADM_CAN") || $emp->get_pers_contact_emp() == $_SESSION["id"]) {
                        ?>
                        <form id='profil_form' method='post' action='./pdf/bulletin.pdf.php'>
                        <input type='hidden' value='<?= $can->get_id() ?>' id='id_can' name='id_can'>
                        <input class="btn btn-primary col-xs-8" type='submit' value='Bulletin' id="edit">
                    </form>
                        <?php
                    }

                    if($per->check_aut("ADM_CAN")) {
                        ?>
                        <form id='profil_form' method='post' action='modifier.php'>
                        <input type='hidden' value='<?= $can->get_id() ?>' id='modif' name='modif'>
                        <input class="btn btn-warning col-xs-8" type='submit' value='Editer' id="edit">
                    </form>
                        <?php
                    }
                    ?>
            </span>
            </div>
        </div>
    </div>
</div>
</body>
</html>
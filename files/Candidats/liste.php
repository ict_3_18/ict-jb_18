<?php
    session_start();
    $aut="ADM_CAN";
    require '../config/config.inc.php';
    require(WAY."/includes/secure.inc.php");
    require WAY . "/includes/head.inc.php";
    require_once(WAY . "/includes/autoload.inc.php");
    
    $grp = new Groupe($_POST['liste']);
    $list_grp = $grp->get_tab_can_by_grp_no_archive_no_termine($_POST['liste']);
    $can = new Candidat();
    $color = $can->get_color_grp_from_id($list_grp[0]['id_can']);
?>
<div class="container">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3>Liste des candidats <?php echo $grp->get_nom_grp(); ?></h3>
            </div>
            <div id="liste_candidats">
                <table class="table table-bordered">
                    <tr>
                    <th>N°</th>
                    <th>Profil</th>
                    <th>Nom/Prénom</th>
                    <th>Genre</th>
                    <th>Année</th>
                    <th>Classe</th>
                    <th>Dual</th>
                    <th>Notes</th>
                    <th>PDF</th>
                    </tr>
                    <?php
                        foreach ($list_grp AS $candidat){
                            echo "<tr>";
                            $can = new Candidat($candidat['id_can']);
                            echo "<td class='c".$color."'>";
                            echo $can->get_no_candidat_minus();
                            echo "</td>";

                            ?>

                            <!-- Profil icon -->
                            <form id='liste_form' action='profil.php' method='post'>
                                <td align="center" class='c<?= $color ?>'>
                                    <label for='profil_<?= $can->get_id() ?>'>
                                        <span class='glyphicon glyphicon-user' title="<?= $can->get_nom()." ".$can->get_prenom() ?>"></span>
                                    </label>
                                    <input type=submit id=profil_<?= $can->get_id() ?> class='btn-primary' value='<?= $can->get_id() ?>' name='modif' visibility='hidden'>
                                </td>
                            </form>

                            <?php

                            echo "<td class='c".$color."'><a href='mailto:".$can->get_email()."'>".$can->get_nom()." ".$can->get_prenom()."</a></td>"
                            ."<td class='c".$color."'>".$can->get_sexe()."</td>"
                            ."<td class='c".$color."'>".$can->get_annee()."</td>"
                            ."<td class='c".$color."'>".$can->get_nom_classe()."</td> <td class='c".$color."'>";
                            
                            if($can->get_nom_employeur()!="ceff"){
                                echo $can->get_nom_employeur();
                            }
                            
                            ?>

                            <!-- Notes icon -->
                            <form id='liste_form' action='notes.php' method='post'>
                                <td align="center" class="c<?= $color ?>">
                                    <label for="notes_<?= $can->get_id() ?>">
                                        <span class='glyphicon glyphicon-education black' title="<?= $can->get_nom()." ".$can->get_prenom() ?>"></span>
                                    </label>
                                    <input type="submit" class="submit_notes" id="notes_<?= $can->get_id() ?>" value="<?= $can->get_id() ?>" name="id_can">
                                </td>
                            </form>

                            <!-- PDF icon -->
                            <form id='liste_form' action='./pdf/bulletin.pdf.php' method='post'>
                                <td align="center" class="c<?= $color ?>">
                                    <label for="bulletin_<?= $can->get_id() ?>">
                                        <span class='glyphicon glyphicon-print black' title="<?= $can->get_nom()." ".$can->get_prenom() ?>"></span>
                                    </label>
                                    <input type="submit" class="submit_bulletin" id="bulletin_<?= $can->get_id() ?>" value="<?= $can->get_id() ?>" name="id_can" >
                                </td>
                            </form>
                        </tr>
            
                    <?php
                        }
                    ?>
                        
                    <style>
                        .c<?= $color ?>{
                            background-color:#<?= $color ?>;
                        }
                    </style>
                </table>
            </div>     
        </div>
    </div>
</div>
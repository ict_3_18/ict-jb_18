$(function(){
    $("#modif_can_form").validate(
        {
            rules:{
                no_candidat:{
                    required:true,
                    maxlength:11
                },
                nom_can:{
                    required:true,
                    maxlength:30
                },
                prenom_can:{
                    required:true,
                    maxlength:30
                },
                email_can:{
                    required:true,
                    email:true,
                    maxlength:150
                },
                annee_can:{
                    required:true,
                    maxlength:10
                },
                login_can:{
                    maxlength:8,
                    required:true
                },
                classe_can:{
                    required:true
                }
            },
            messages:{
                no_candidat:{
                    required: "Veuillez saisir le numéro du candidat",
                    maxlength: "Veuillez mettre maximum 11 chiffres"
                },
                nom_can:{
                    required: "Veuillez saisir le nom du candidat",
                    maxlength: "Veuillez mettre maximum 30 caractères"
                },
                prenom_can:{
                    required: "Veuillez saisir le prenom du candidat",
                    maxlength: "Veuillez mettre maximum 30 caractères"
                },
                email_can:{
                    required: "Veuillez saisir l'email du candidat",
                    email: "Veuillez écrire un email",
                    maxlength:"Veuillez mettre maximum 150 caractères"
                },
                annee_can:{
                    required: "Veuillez saisir l'année du candidat",
                    maxlength : "Veuillez mettre maximum 10 chiffres"
                } ,
                login_can:{
                    maxlength:"Veuillez mettre maximum 6 caractères",
                    required: "Veuillez saisir le login du candidat"
                },
                classe_can:{
                    required:"Veuillez sélectionner"
                }
            },
            submitHandler: function(form){
                if ($("#dys_can").is(":checked")) {
                    var dys_can = 1;
                } else {
                    var dys_can = 0;
                }
                $.post(
                    "./json/ajout_candidat.json.php?_="+Date.now(),
                    {
                        no_candidat_can:$("#no_candidat").val(),
                        sexe_can:$("#sexe_can").val(),
                        nom_can:$("#nom_can").val(),
                        prenom_can:$("#prenom_can").val(),
                        email_can:$("#email_can").val(),
                        annee_can:$("#annee_can").val(),
                        systeme_can:$("#systeme_can").val(),
                        employeur_can:$("#employeur_can").val(),
                        classe_can:$("#classe_can").val(),
                        dys_can:dys_can,
                        login_can:$("#login_can").val()
                    },
                    function result(data, status) {
                        if (data.reponse === true) {
                            message("Le candidat a bien &eacutet&eacute cr&eacute&eacutee ! <br>", "success");
                            $("#ajout_per_form").trigger("reset");
                        }
                        if (data.reponse === false) {
                            message("Le candidat n'a pas pu être ajouté !", "danger");
                        }
                        $("#loading").css("display", "none");
                    }
                ).done(function () {
                    $("#no_candidat").val('');
                    $("#login_can").val('');
                    $("#nom_can").val('');
                    $("#prenom_can").val('');
                    $("#email_can").val('');
                    function remove_message(){
                        message('');
                    }
                    setTimeout(remove_message, 1000)
                });
            }
        }
    )
});
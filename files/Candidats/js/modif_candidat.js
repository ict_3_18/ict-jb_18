$(function(){
    $("#modif_can_form").validate(
        {
            rules:{
                no_candidat:{
                    required:true,
                    maxlength:11
                },
                nom_can:{
                    required:true,
                    maxlength:30
                },
                prenom_can:{
                    required:true,
                    maxlength:30
                },
                email_can:{
                    required:true,
                    email:true,
                    maxlength:150
                },
                annee_can:{
                    required:true,
                    maxlength:10
                },
                login:{
                    maxlength:6,
                    required:true
                }
            },
            messages:{
                no_candidat:{
                    required: "Veuillez saisir le num?ro du candidat",
                    maxlength: "Veuillez mettre maximum 11 chiffres"
                },
                nom_can:{
                    required: "Veuillez saisir le nom du candidat",
                    maxlength: "Veuillez mettre maximum 30 caract�res"
                },
                prenom_can:{
                    required: "Veuillez saisir le prenom du candidat",
                    maxlength: "Veuillez mettre maximum 30 caract�res"
                },
                email_can:{
                    required: "Veuillez saisir l'email du candidat",
                    email: "Veuillez �crire un email",
                    maxlength:"Veuillez mettre maximum 150 caract�res"
                },
                annee_can:{
                    required: "Veuillez saisir l'ann�e du candidat",
                    maxlength : "Veuillez mettre maximum 10 chiffres"
                }  ,
                login_can:{
                    maxlength:"Veuillez mettre maximum 6 caractères",
                    required: "Veuillez saisir le login du candidat"
                }
            },
            submitHandler: function(form){
                if ($("#dys_can").is(":checked")) {
                    var dys_can = 1;
                } else {
                    var dys_can = 0;
                }
                $.post(
                    "./json/modif_candidat.json.php?_="+Date.now(),
                    {
                        no_candidat_can:$("#no_candidat").val(),
                        id_can:$("#id_can").val(),
                        sexe_can:$("#sexe_can").val(),
                        nom_can:$("#nom_can").val(),
                        prenom_can:$("#prenom_can").val(),
                        email_can:$("#email_can").val(),
                        annee_can:$("#annee_can").val(),
                        systeme_can:$("#systeme_can").val(),
                        employeur_can:$("#employeur_can").val(),
                        classe_can:$("#classe_can").val(),
                        dys_can:dys_can,
                        login_can:$("#login_can").val()
                    },
                    function result(data, status) {
                        if (data.reponse === true) {
                            message("Modifications effectue avec succés ! ! <br>", "success");
                        }
                        if (data.reponse === false) {
                            message("Les modifications n'ont pas pus être effectuée", "danger");
                        }
                        $("#loading").css("display", "none");
                    }
                ).done(function () {
                    function remove_message(){
                        message('');
                    }
                    setTimeout(remove_message, 1000)
                })
            }
        }
    );
});
<?php
    session_start();
    $aut = "ADM_USR"; 
    require_once("./../config/config.inc.php");
    require_once(WAY . "./includes/secure.inc.php");
    require_once( WAY ."/includes/autoload.inc.php");
    require_once( WAY ."/includes/head.inc.php");
    
    $grp = new Groupe(); 
    $cla = new Classe();
    $can = new Candidat();  

    $ref_exa = 0; 
    if(isset($_POST['ref_exa'])){
        $ref_exa = $_POST['ref_exa']; 
    }
?>
          <div class="panel panel-primary">
             <div class="panel-heading">
                Ajout d'un candidat(s) à un examen
             </div>
          
             <div class="panel-body">
                 
                <form id="add_candidat_examen_form" action="check.php" method="post">
                     
                   <!--  Groupe -->
                   <div class="form-group row">
                      <label for="ref_grp" class="col-sm-1 col-form-label">Groupe</label>
                      <div class="col-sm-5">
                        <select name="grp_exa" id="ref_grp" class="form-control">
                            <?php 
                                foreach ($grp->get_all_actifs() as $g){
                                    echo "<option value=\"".$g["id_grp"]."\" >".$g["nom_grp"]."</option>"; 
                                }
                            ?>
                        </select>
                      </div>
                   </div>
                      
                    <!--  Classe  -->
                   <div class="form-group row">
                      <label for="ref_cla" class="col-sm-1 col-form-label">Classe</label>
                      <div class="col-sm-5">
                        <select name="cla_exa" id="ref_cla" class="form-control">

                        </select>
                      </div>
                   </div>
                      
                   <!--  Candidat -->
                   <div class="form-group row">
                      <label for="ref_can" class="col-sm-1 col-form-label">Candidat</label>
                      <div class="col-sm-5">
                        <select name="can_exa" id="ref_can" class="form-control">

                        </select>
                      </div>
                   </div>
                    
                   <!-- Bouton submit et reset -->
                  <div class="form-group row">
                     <div class="col-sm-offset-8 col-sm-2">
                         <input type="submit" class="form-control btn btn-primary submit" id="submit_conf" value="Ajouter">
                     </div>
                     <div class="col-sm-2"> 
                        <input type="reset" class="form-control btn btn-warning" id="reset_conf" value="Annuler">;
                     </div>
                  </div> 

                </form>
            </div>
              
         </div>
        <!-- <script src="./js/add_candidat_examen.js"></script> -->
        <script>
            $(function(){                   
                //Tableaux pour les groupes :
                var tab_grp_cla = <?php echo json_encode($grp->get_tab_cla_all_grp()); ?>;
                var tab_grp_can = <?php echo json_encode($grp->get_tab_can_all_grp()); ?>; 
                //Tableau pour les classes :
                var tab_cla_can = <?php echo json_encode($cla->get_tab_can_all_cla()); ?>;
                
                function complete_option(id_select_to_add_option, array, val){
                    var option_str = "";  //String des <options> du select 
                        $.each(array, function(key, tab) {//Parcours le tableau et donne la key avec la tableau 
                            if(val == key){//Si la value du champ selectionné dans le select est la key du tableau 
                                $.each(tab, function(key, row) {
                                    var id = row[0]; 
                                    var text = row[1]; 
                                    option_str+= "<option value="+id+">"+text+"</option>\n"; //Ajoute l'option à la string 
                                });
                            }
                        });
                    $(id_select_to_add_option).html(option_str);//Ajoute le html 
                }
                
                $("select").change(function(){
                    switch($(this).attr("id")){
                        case "ref_grp":   
                            complete_option("#ref_cla", tab_grp_cla, $(this).val()); 
                            complete_option("#ref_can", tab_grp_can, $(this).val());
                            break;
                        case "ref_cla":
                            complete_option("#ref_can", tab_cla_can, $(this).val());
                            break;
                    }    
                });
                
                //Validate 
                $("#add_candidat_examen_form").validate(
                  {
                     rules: {
                        cla_exa:{
                           required: true,
                        },
                     },
                     messages:{
                        cla_exa:{
                           required: "Merci de selectionner une classe",
                        },
                     },
                     submitHandler: function(form) {
                        var ref_exa_str = <?php echo $ref_exa ?>;
                        var ref_exa = parseInt(ref_exa_str); 
                        
                        tab_post = {}; 
                        
                        if(ref_exa >= 1){  //ref_exa >= 1 si setté 
                            tab_post["ref_exa"] = ref_exa; 
                        } 
                        if($("#ref_grp").val()){
                            tab_post["ref_grp"] = $("#ref_grp").val(); 
                        }
                        if($("#ref_cla").val()){
                            tab_post["ref_cla"] = $("#ref_cla").val(); 
                        }
                        if($("#ref_can").val()){
                            tab_post["ref_can"] = $("#ref_can").val(); 
                        }
                        
                        $.post("./json/add_candidat_examen.json.php?_="+Date.now(),tab_post); 
                     }
                  }
               );
                
            }); 
        </script>
        
    </body>
</html>
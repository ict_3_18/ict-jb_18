<?php
    header("Content-Type: application/json");
        
    session_start();
    $aut="ADM_CAN";
    require '../../config/config.inc.php';
    require(WAY."/includes/secure.inc.php"); 
    
    require_once WAY.'/includes/autoload.inc.php';
        
    $can = new Candidat();
    
    //print_r($_POST);
    
    $tab['no_candidat_can']=$_POST['no_candidat_can'];
    $tab['sexe_can']=$_POST['sexe_can'];
    $tab['nom_can']=$_POST['nom_can'];
    $tab['prenom_can']=$_POST['prenom_can'];
    $tab['email_can']= strtolower($_POST['email_can']);
    $tab['annee_can']=$_POST['annee_can'];
    $tab['systeme_can']=$_POST['systeme_can'];
    $tab['employeur_can']=$_POST['employeur_can'];
    $tab['classe_can']=$_POST['classe_can'];
    $tab['dys_can']=$_POST['dys_can'];
    $tab['login_can']=$_POST['login_can'];
    
    $return = $can->add_can($tab);

    if($return) {
        $tab['reponse'] = true;
    }else{
            $tab['reponse'] = false;
    }
    echo json_encode($tab);
?>
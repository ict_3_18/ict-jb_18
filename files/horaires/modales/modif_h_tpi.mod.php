<div class="modal fade" id="modif_h_tpi_modal" tabindex="-1" role="dialog" aria-labelledby="modif_h_tpi_modalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Modifier une période</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Début horaire</label>
                    <input id="debut_hor" type="time" class="form-control" value="00:00:00">
                </div>
                <div class="form-group">
                    <label>Fin horaire</label>
                    <input id="fin_hor" type="time" class="form-control" value="00:00:00">
                </div>
            </div>
            <div class="modal-footer" id="modal-footer">
                <button id="valid_edit" type="button" class="btn btn-primary" data-dismiss="modal">Modifier</button>
                <button id="cancel_edit" type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                <input type="hidden" id="id_hor_mod">
                <input type="hidden" id="id_tpi_mod">
                <input type="hidden" id="date_hor_mod_edit">
            </div>
        </div>
    </div>
</div>
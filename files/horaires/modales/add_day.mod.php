<div class="modal fade" id="btn_add_day_modal" tabindex="-1" role="dialog" aria-labelledby="btn_add_day_modalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Ajout d'une période</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Date du jour</label>
                    <input id="date_hor_add_mod" type="date" class="form-control" value="<?= date('Y-m-d') ?>">
                </div>
                <div class="form-group">
                    <label>Périodes</label>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input id="debut_hor_add_mod" type="time" class="form-control" value="00:00:00">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input id="fin_hor_add_mod" type="time" class="form-control" value="00:00:00">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer" id="modal-footer">
                <button id="valid_add" type="button" class="btn btn-primary" data-dismiss="modal">Ajouter</button>
                <button id="cancel_add" type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                <input type="hidden" id="id_tpi_add_mod">
                <input type="hidden" id="id_can_add_mod">
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="copy_h_tpi_modal" tabindex="-1" role="dialog" aria-labelledby="copy_h_tpi_modalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Copier une journée</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Date du jour</label>
                    <input id="date_hor_mod" type="date" class="form-control">
                </div>
            </div>
            <div class="modal-footer" id="modal-footer">
                <button id="valid_copy" type="button" class="btn btn-primary" data-dismiss="modal">Copier</button>
                <button id="cancel_copy" type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
            </div>
        </div>
    </div>
</div>
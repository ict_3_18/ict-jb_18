<?php
/**
 * @author Beruwalage Julian
 * @description Ce fichier s'occupe de retourner les informations d'une pérode de l'horaire TPI et de json_encode les informations.
 */

header("Content-Type: application/json");
require("../../config/config.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

session_start();
$aut = 'ADM_TPI;USR_TPI';

require(WAY . "./includes/secure.inc.php");

try {
    $hor = new Horaire();
    $id_hor = $_POST['id_hor'];
    $infos = $hor->get_info_h_tpi($id_hor);
//    print_r();
}catch (PDOException $e){
    echo $e->getMessage(),'<br/><br/>';
    echo _e("Erreur d'ajout");
}
//print_r($infos);

echo json_encode($infos);
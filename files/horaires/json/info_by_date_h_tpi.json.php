<?php
/**
 * @author Beruwalage Julian
 * @description Ce fichier s'occupe de retourner les informations de l'horaire TPI d'une date et de json_encode le tableau.
 */

header("Content-Type: application/json");
require("../../config/config.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

session_start();
$aut = 'ADM_TPI;USR_TPI';

require(WAY . "./includes/secure.inc.php");

try {
    $hor = new Horaire();
    $date_hor= $_POST['date_hor'];
    $id_tpi = $_POST['id_tpi'];
    $infos = $hor->get_info_by_date_h_tpi($date_hor,$id_tpi);
    $tab_infos_temp = [];
    $tab_infos = [];
    foreach ($infos as $inf){
        $tab_infos_temp['id_hor'] = $inf['id_hor'];
        $date_db_format_U = strtotime("+7 day", date("U", strtotime($inf['date_hor'])));
        $tab_infos_temp['date_hor'] = date("Y-m-d",$date_db_format_U);
        $tab_infos_temp['debut_hor'] = $inf['debut_hor'];
        $tab_infos_temp['fin_hor'] = $inf['fin_hor'];
        $tab_infos_temp['id_tpi'] = $inf['id_tpi'];
        $tab_infos_temp['pauses_hor'] = $inf['pauses_hor'];
        $tab_infos_temp['ref_can'] = $inf['ref_can'];
        $tab_infos_temp['total_day_hor'] = $inf['total_day_hor'];

        //test
        $tab_infos_temp['old_date_hor'] = $inf['date_hor'];

        array_push($tab_infos, $tab_infos_temp);
    }
}catch (PDOException $e){
    echo $e->getMessage(),'<br/><br/>';
    echo _e("Erreur d'ajout");
}

echo json_encode($tab_infos);
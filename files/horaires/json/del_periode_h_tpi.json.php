<?php
/**
 * @author Beruwalage Julian
 * @description Ce fichier s'occupe de supprimer une periode de l'horaire TPI et de json_encode l'id du TPI et l'id de l'horaire TPI.
 */

header("Content-Type: application/json");
require("../../config/config.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

session_start();
$aut = 'ADM_TPI;USR_TPI';

require(WAY . "./includes/secure.inc.php");

try {
    $tab = ['id_hor' => $_POST['id_hor'], 'id_tpi' => $_POST['id_tpi']];
    $hor = new Horaire();
    $hor->del_periode_h_tpi($tab);
}catch (PDOException $e){
    echo $e->getMessage(),'<br/><br/>';
    echo _e("Erreur d'ajout");
}

echo json_encode($tab);
<?php
/**
 * @author Beruwalage Julian
 * @description Ce fichier s'occupe de l'édition d'une période de l'horaire TPI et de json_encode le tableau.
 */

header("Content-Type: application/json");
require("../../config/config.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

session_start();
$aut = 'ADM_TPI;USR_TPI';

require(WAY . "./includes/secure.inc.php");

try {
    $tab = ['date_hor' => $_POST['date_hor'], 'id_hor' => $_POST['id_hor'], 'debut_hor' => $_POST['debut_hor'], 'fin_hor' => $_POST['fin_hor'], 'id_tpi' => $_POST['id_tpi']];
    $hor = new Horaire();
    $tab_already = $hor->get_info_h_tpi_by_id_tpi_and_date($tab);
    $result = false;
    if(!empty($tab_already)) {
        foreach ($tab_already as $already) {
            $debut = date('H:i',strtotime($already['debut_hor']));
            $fin = date('H:i',strtotime($already['fin_hor']));
            if ($debut >= $tab['fin_hor']){
                $result = false;
            }else if($fin <= $tab['debut_hor']){
                $result = false;
            }else{
                $result = true;
                break;
            }
        }
        if (!$result){
            $hor->edit_h_tpi($tab);
        }else{
            echo json_encode(false);
            exit();
        }
    }else{
        $hor->edit_h_tpi($tab);
    }

}catch (PDOException $e){
    echo $e->getMessage(),'<br/><br/>';
    echo _e("Erreur d'ajout");
}
echo json_encode($tab);
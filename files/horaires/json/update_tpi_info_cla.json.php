<?php
/**
 * @author Beruwalage Julian
 * @description Ce fichier s'occupe d' update la date de début du TPI et de récupérer tous l'horaire cadre de la classe du candidat (h_cadre) et de json_encode le tableau.
 */

header("Content-Type: application/json");
require("../../config/config.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

session_start();
$aut = 'ADM_TPI;USR_TPI';

require(WAY . "./includes/secure.inc.php");

try {
    $tab = ['date_debut_tpi' => $_POST['add_datepicker'], 'id_cla' => $_POST['add_id_cla'], 'duree_max_tpi' => $_POST['add_duree_max_tpi'], 'duree_min_tpi' => $_POST['add_duree_min_tpi'], 'id_can' => $_POST['add_id_can']];
    $tpi = new Tpi();
    $hor = new Horaire();
    $tab['id_tpi'] = $_POST['add_id_tpi'];
    $tpi->update_date($tab);
    $tab['h_cadre'] = $hor->get_infos_cad_by_classe($tab['id_cla']);
}catch (PDOException $e){
    echo $e->getMessage(),'<br/><br/>';
    echo _e("Erreur d'ajout");
}

echo json_encode($tab);
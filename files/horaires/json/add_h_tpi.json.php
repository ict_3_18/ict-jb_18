<?php
/**
 * @author Beruwalage Julian
 * @description Ce fichier s'occupe de générer l'horaire tpi par rapport à l'horaire. Si l'horaire existe déjà lors de la génération on vide les TPI du candidat et on régénère l'horaire.
 * Les différents algorythmes de calcules sont expliqués plus en détails
 */

session_start();
header("Content-Type: application/json");
require("../../config/config.inc.php");
require_once(WAY . "/includes/autoload.inc.php");
$aut = 'ADM_TPI;USR_TPI';
require(WAY . "./includes/secure.inc.php");

try {
    $tab = ['datepicker' => $_POST['add_datepicker'], 'id_cla' => $_POST['add_id_cla'], 'duree_max_tpi' => $_POST['add_duree_max_tpi'], 'duree_min_tpi' => $_POST['add_duree_min_tpi'], 'id_can' => $_POST['add_id_can'], 'id_tpi' => $_POST['add_id_tpi'], 'h_cadre' => $_POST['add_h_cadre']];
    $tpi_h = new Horaire();
    $vac = new Vacance();
    $tab_vac = $vac->get_all_vac();

    /**
     * Suppression de l'horaire du candidat
     */
    $tpi_h->del_h_tpi($tab['id_can']);

    /**
     * Permet de transformer la date entrée par l'expert au format 0(lundi) à 6 (dimanche)
     * Le -1 c'est parce que le format met 1(lundi) à 7(dimanche) ce qui me permet d'avoir la même notation que l'horaire cadre
     *
     * @Test voir ce que retourne la date : echo $datepicker_format_N;
     */
    $datepicker_format_N = (date('N', strtotime($tab['datepicker'])) % 7) - 1;

    /**
     * Permet de regrouper les périodes par jour cadre dans un tableau ($tab_h_cadre_by_jour_cad)
     *
     * $tab_h_cadre_by_jour_cad :
     * Array
     * (
     *     [0] => Array
     *          (
     *              [jour_cad] => 0
     *              [ref_cla] => 70
     *              [ref_can] => 464
     *              [pause_cad] => 0
     *              [heures] => Array
     *                   (
     *                      [0] => Array
     *                          (
     *                              [debut] => 10:45:00
     *                              [fin] => 11:45:00
     *                          )
     *                      [1] => Array
     *                          (
     *                              [debut] => 12:45:00
     *                              [fin] => 13:55:00
     *                          )
     *                    )
     *          )
     *     [1] => Array .....
     *
     * @Test voir ce qu'il y a dans le tableau : print_r($tab_h_cadre_by_jour_cad);
     */
    $tab_h_cadre_by_jour_cad = [];
    $tab_h_jour_cad = [];
    $current_h_jour_cad = 0;
    foreach ($tab['h_cadre'] AS $h_cadre) {
        if ($current_h_jour_cad != $h_cadre['jour_cad']) {
            $current_h_jour_cad = $h_cadre['jour_cad'];
            if (!empty($tab_h_jour_cad)) {
                array_push($tab_h_cadre_by_jour_cad, $tab_h_jour_cad);
            }
            $tab_h_jour_cad = [];
        }
        $tab_h_jour_cad['jour_cad'] = $h_cadre['jour_cad'];
        $tab_h_jour_cad['ref_cla'] = $h_cadre['ref_cla'];
        $tab_h_jour_cad['ref_can'] = $tab['id_can'];
        $tab_h_jour_cad['ref_cla'] = $h_cadre['ref_cla'];
        $tab_h_jour_cad['pause_cad'] = $h_cadre['pause_cad'];
        $tab_h_jour_cad['heures'][] = [
            'debut' => $h_cadre['debut_cad'],
            'fin' => $h_cadre['fin_cad']
        ];
    }
    array_push($tab_h_cadre_by_jour_cad, $tab_h_jour_cad);

    //print_r($tab_h_cadre_by_jour_cad);


    /**
     * Permet de stocker les jours de la semaine cadre (0 à 6) ou le candidat est en pratique dans un tableau
     * On élimine les doublons pour n'avoir qu'une fois le jour de la semaine
     *
     * @Test Voir ce qu'il y a dans le tableau : print_r($tab_jour_cad);
     */
    $tab_jour_cad = [];
    foreach ($tab['h_cadre'] AS $cadre) {
        array_push($tab_jour_cad, $cadre['jour_cad']);
    }
    $tab_jour_cad = array_values(array_unique($tab_jour_cad));

    /**
     * Permet de retourner la date de début du TPI (
     *
     * Explication : On parcourt le tableau $tab_jour_cad
     * On regarde si $num_jour_sem (variable qui commence a null) est égale a null, on set $num_jour_sem avec le premier jour du tableau
     *
     * Si le $num_jour_sem est déjà setter, on regarde si ($datepicker_format_N - $num_jour_sem) est plus grand que ($num_jour_sem - $datepicker_format_N)
     * On set le $num_jour_sem avec le $jour_cad
     *
     * Sinon on ne le set pas
     *
     * abs : valeur absolue (Si le nombre est un nombre à virgule flottante (float), le type retourné est aussi un nombre à virgule flottante (float), sinon, c'est un entier (entier))
     * résumer : On retourne simplement le jour cadre de la semaine le plus proche du jour entrer par l'expert (si le jour entré est entre 2 jour cadre on retourne le plus petit "plus proche")
     *
     * @Test voir le jour de la semaine que ca retourne : echo $num_jour_sem;
     */
    $num_jour_sem = null;
    foreach ($tab_jour_cad as $jour_cad) {
        if ($num_jour_sem === null || abs($datepicker_format_N - $num_jour_sem) > abs($jour_cad - $datepicker_format_N)) {
            $num_jour_sem = $jour_cad;
        }
    }
    $num_jour_sem;

    /**
     * Permet d'avoir la date de début du TPI sous différents format
     *
     * $date_db_format_U : timestamp du jour de début du TPI
     * @Test voir ce que retourne la date : echo $date_db_format_U;
     *
     * $date_db_format_N : numéro du jour de début du TPI (0 à 6)
     * @Test voir ce que retourne la date : echo $date_db_format_N;
     *
     * $date_db_format_Ymd : date EN du jour de début du TPI
     * @Test voir ce que retourne la date : echo $date_db_format_Ymd;
     */
    $date_db_format_U = strtotime("this week +" . $num_jour_sem . " day", date("U", strtotime($tab['datepicker'])));
    $date_db_format_N = date('N', $date_db_format_U) - 1;
    $date_db_format_Ymd = date('Y-m-d', $date_db_format_U);


    /**
     * Variables et tableaux utiles dans la WHILE
     */
    $tab_h_tpi = [];
    $tab_h_tpi_hours = [];
    $tab_jour_cad_temp = [];

    $i = 0;
    $duree_max_tpi = $tab['duree_max_tpi'] * 60;
    $duree_min_tpi = $tab['duree_min_tpi'] * 60;
    $date_jour = $date_db_format_Ymd;

    $date_jour_temp;
    $date_final;

    $first_week = true;
    $first_day = true;
    $vac_test = false;


    /**
     * Permet de créer l'horaire TPI et de le stocker dans un tableau
     */
    // création de l'horaire TPI et placement dans un tableau. l'horaire ne dépassera pas la durée max du TPI
    while ($i <= $duree_max_tpi) {

        foreach ($tab_h_cadre_by_jour_cad as $h) {

            $tab_h_tpi_hours = [];
            $total_all_day = 0;


            /**
             * Permet de mettre les jours cadre qui sont plus petit que le jour de commencement du TPI dans le tableau temporaire
             *
             * Si c'est le premier jour et que le jour cadre est égale au jour de commencement du TPI
             * On parcour le tableau des jour de pratique
             *
             * Si le jour de pratique est plus petit que le jour cadre
             * On ajoute les jour de pratique dans le tableau temporaire
             *
             * On set le premier jour a false
             */
            if ($first_day && $h['jour_cad'] == $date_db_format_N) {
                foreach ($tab_jour_cad as $jour_cad) {
                    if ($jour_cad < $h['jour_cad']) {
                        array_push($tab_jour_cad_temp, $jour_cad);
                    }
                }
                $first_day = false;
            }
            /**
             * Permet qu'une fois que le premier jour a été tester de setter la date final (format Y-m-d)
             *
             * -----------------------------------------------------------------
             *
             * Si le jour cadre n'est pas présent dans le tableau temporaire
             * On l'ajoute dans le tableau temporaire
             *
             * Si la date finale est vide (false)
             * On set la date temporaire avec la date de début du TPI (jour de commencement du TPI au format Y-m-d)
             *
             * Si elle n'est pas vide (true)
             * On set la date temporaire avec la date final
             *
             * date finale : retourne les dates des jours cadre d'une semaine
             *
             * -----------------------------------------------------------------
             *
             * Si le jour cadre est présent dans le tableau temporaire
             * On vide le tableau temporaire et on ajoute le jour cadre dans le tableau temporaire
             *
             * * Si la date finale est vide (false)
             * On set la date temporaire avec la date de début du TPI (jour de commencement du TPI au format Y-m-d)
             *
             * Si elle n'est pas vide (true)
             * On set la date temporaire avec la date final
             *
             * date finale : retourne les dates des jours cadre de la semaine suivante
             */
            if (!$first_day) {
                if (!in_array($h['jour_cad'], $tab_jour_cad_temp)) {
                    array_push($tab_jour_cad_temp, $h['jour_cad']);
                    if (empty($date_final)) {
                        $date_jour_temp = $date_jour;
                    } else {
                        $date_jour_temp = $date_final;
                    }
                    $date_final = date("Y-m-d", strtotime("this week +" . $h['jour_cad'] . " day", date("U", strtotime($date_jour_temp))));
                } else {
                    $tab_jour_cad_temp = [];
                    array_push($tab_jour_cad_temp, $h['jour_cad']);
                    if (empty($date_final)) {
                        $date_jour_temp = $date_jour;
                    } else {
                        $date_jour_temp = $date_final;
                    }
                    $date_final = date("Y-m-d", strtotime("next week +" . $h['jour_cad'] . " day", date("U", strtotime($date_jour_temp))));
                }
            }

            /**
             * Permet de regrouper les heures par jour cadre (format Y-m-d) et les autres informations utiles à la création de l'horaire cadre dans un tableau ($tab_h_tpi_hours)
             *
             * Si c'est la première semaine
             * On test si le jour de commencement du TPI est égale au jour cadre
             * On ajoute les informations utiles du jour de commencement du TPI dans le tableau ($tab_h_tpi_hours)
             *
             * Si ce n'est pas la première semaine
             * On ajoute les informations utiles des autres jours du TPI dans le tableau ($tab_h_tpi_hours)
             */
                foreach ($h['heures'] as $hour) {
                    if ($first_week) {
                        if ($date_db_format_N == $h['jour_cad']) {
                            foreach ($tab_vac as $vac){
                                if($date_final >= $vac['debut_vac'] && $date_final <= $vac['fin_vac']){
                                    $vac_test = true;
                                }else{
                                    $vac_test = false;
                                }
                            }
                            if($vac_test == false) {
                                $total_day = strtotime($hour['fin']) - strtotime($hour['debut']);
                                $total_day_min = ((date("G", $total_day) - 1) * 60) + (date("i", $total_day));
                                $total_all_day += $total_all_day;
                                $tab_h_tpi_hours['jour_cad'] = $h['jour_cad'];
                                $tab_h_tpi_hours['date_hor'] = $date_final;
                                $tab_h_tpi_hours['ref_cla'] = $h['ref_cla'];
                                $tab_h_tpi_hours['ref_can'] = $h['ref_can'];
                                $tab_h_tpi_hours['total'] = $total_all_day;
                                $tab_h_tpi_hours['pause_cad'] = $h['pause_cad'];
                                $tab_h_tpi_hours['heures'][] = [
                                    'debut_hor' => $hour['debut'],
                                    'fin_hor' => $hour['fin'],
                                    'total_day_hor' => $total_day_min
                                ];
                                $i += $total_day_min;
                                $first_week = false;
                            }else{
                                $total_day = strtotime($hour['fin']) - strtotime($hour['debut']);
                                $total_day_min = ((date("G", $total_day) - 1) * 60) + (date("i", $total_day));
                                $total_all_day += $total_all_day;
                                $tab_h_tpi_hours['jour_cad'] = $h['jour_cad'];
                                $tab_h_tpi_hours['date_hor'] = $date_final;
                                $tab_h_tpi_hours['ref_cla'] = $h['ref_cla'];
                                $tab_h_tpi_hours['ref_can'] = $h['ref_can'];
                                $tab_h_tpi_hours['total'] = $total_all_day;
                                $tab_h_tpi_hours['pause_cad'] = $h['pause_cad'];
                                $tab_h_tpi_hours['heures'][] = [
                                    'debut_hor' => $hour['debut'],
                                    'fin_hor' => $hour['fin'],
                                    'total_day_hor' => $total_day_min
                                ];
                                $first_week = false;
                            }
                        }
                    } else {
                        foreach ($tab_vac as $vac){
                            if($date_final >= $vac['debut_vac'] && $date_final <= $vac['fin_vac']){
                                $vac_test = true;
                            }else{
                                $vac_test = false;
                            }
                        }
                        if($vac_test == false) {
                            $total_day = strtotime($hour['fin']) - strtotime($hour['debut']);
                            $total_day_min = ((date("G", $total_day) - 1) * 60) + (date("i", $total_day));
                            $total_all_day += $total_day_min;
                            $tab_h_tpi_hours['jour_cad'] = $h['jour_cad'];
                            $tab_h_tpi_hours['date_hor'] = $date_final;
                            $tab_h_tpi_hours['ref_cla'] = $h['ref_cla'];
                            $tab_h_tpi_hours['ref_can'] = $h['ref_can'];
                            $tab_h_tpi_hours['total'] = $total_all_day;
                            $tab_h_tpi_hours['pause_cad'] = $h['pause_cad'];
                            $tab_h_tpi_hours['heures'][] = [
                                'debut_hor' => $hour['debut'],
                                'fin_hor' => $hour['fin'],
                                'total_day_hor' => $total_day_min
                            ];
                            $i += $total_day_min;
                        }else{
                            $total_day = strtotime($hour['fin']) - strtotime($hour['debut']);
                            $total_day_min = ((date("G", $total_day) - 1) * 60) + (date("i", $total_day));
                            $total_all_day += $total_day_min;
                            $tab_h_tpi_hours['jour_cad'] = $h['jour_cad'];
                            $tab_h_tpi_hours['date_hor'] = $date_final;
                            $tab_h_tpi_hours['ref_cla'] = $h['ref_cla'];
                            $tab_h_tpi_hours['ref_can'] = $h['ref_can'];
                            $tab_h_tpi_hours['total'] = $total_all_day;
                            $tab_h_tpi_hours['pause_cad'] = $h['pause_cad'];
                            $tab_h_tpi_hours['heures'][] = [
                                'debut_hor' => $hour['debut'],
                                'fin_hor' => $hour['fin'],
                                'total_day_hor' => 0
                            ];
                        }
                    }
                }
            /**
             * Permet d'ajouter toutes les infos du tableau ($tab_h_tpi_hours) dans un nouveau tableau ($tab_h_tpi)
             * Contient tous l'horaire du TPI
             *
             * @Test voir ce que retourne le tableau : print_r($tab_h_tpi);
             */
            if (!empty($tab_h_tpi_hours)) {
                array_push($tab_h_tpi, $tab_h_tpi_hours);
            }
        }
    }
//    print_r($tab_h_tpi);

    /**
     * Permet d'ajouter l'horaire des TPI dans la base de donnée
     * Les données sont stocker dans un tableau ($tab_h_tpi_final)
     */
    // tableau final avec toutes les infos pour la création d'un horaire
    $tab_h_tpi_final = [];
    $tab_h_jour_cad_final = [];
    $duration = 0;
    $current_h_jour_cad_final = 0;
    foreach ($tab_h_tpi as $h_tpi) {
        foreach ($h_tpi['heures'] as $h) {
            $duration += $h['total_day_hor'];
            if($duration < $duree_max_tpi) {
                $tab_h_jour_cad_final['date_hor'] = $h_tpi['date_hor'];
                $tab_h_jour_cad_final['debut_hor'] = $h['debut_hor'];
                $tab_h_jour_cad_final['fin_hor'] = $h['fin_hor'];
                $tab_h_jour_cad_final['total_day_hor'] = $h['total_day_hor'];
                $tab_h_jour_cad_final['pauses_hor'] = $h_tpi['pause_cad'];
                $tab_h_jour_cad_final['ref_can'] = $h_tpi['ref_can'];
                $tab_h_jour_cad_final['full_day_hor'] = $h_tpi['total'];
                $tab_h_jour_cad_final['id_tpi'] = $tab['id_tpi'];
                $tpi_h->add_h_tpi($tab_h_jour_cad_final);
                array_push($tab_h_tpi_final, $tab_h_jour_cad_final);
            }
        }
    }

} catch (PDOException $e) {
    echo $e->getMessage(), '<br/><br/>';
    echo _e("Erreur d'ajout");
}

echo json_encode($tab_h_tpi_final);
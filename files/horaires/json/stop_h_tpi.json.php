<?php
/**
 * @author Beruwalage Julian
 * @description Ce fichier s'occupe de supprimer les horaires TPI qui sont après la période ou le maître expert à stopper le TPI et de json_encode l'id TPI.
 */

header("Content-Type: application/json");
require("../../config/config.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

session_start();
$aut = 'ADM_TPI;USR_TPI';

require(WAY . "./includes/secure.inc.php");
//todo : modification aporter !!!!!!!!!!! présentation
try {
    $hor = new Horaire();
    $tab['id_hor'] = $_POST['id_hor'];
    $tab['id_tpi'] = $_POST['id_tpi'];
    $tab['date_hor'] = $_POST['date_hor'];
    $tab_hor_del = $hor->get_info_h_tpi_stop($tab);
    $tab_temp = [];
    foreach ($tab_hor_del as $hor_del){
        $tab_temp['id_hor'] = $hor_del['id_hor'];
        $tab_temp['id_tpi'] = $hor_del['id_tpi'];
        $tab_temp['date_hor'] = $hor_del['date_hor'];
        $hor->del_periode_h_tpi($tab_temp);
    }

    $infos = $_POST['id_tpi'];
}catch (PDOException $e){
    echo $e->getMessage(),'<br/><br/>';
    echo _e("Erreur d'ajout");
}
//print_r($infos);
echo json_encode($infos);
<?php
/**
 * @author Beruwalage Julian
 * @description Ce fichier s'occupe de copier l'horaire et de json_encode l'id du tpi
 */

header("Content-Type: application/json");
require("../../config/config.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

session_start();
$aut = 'ADM_TPI;USR_TPI';

require(WAY . "./includes/secure.inc.php");

try {

    $hor = new Horaire();
    $vac = new Vacance();
    $tab_vac = $vac->get_all_vac();

    $same = null;
    $tab = [];


    foreach ($_POST['tab_by_date'] as $hor_data) {
        foreach ($tab_vac as $vac) {
            if (!($_POST['new_date_hor'] >= $vac['debut_vac'] && $_POST['new_date_hor'] <= $vac['fin_vac'])) {
                if ($same == null) {
                    foreach ($hor_data as $hor_data_by_day) {
                        $tab['date_hor'] = $_POST['new_date_hor'];
                        $tab['debut_hor'] = $hor_data_by_day['debut_hor'];
                        $tab['fin_hor'] = $hor_data_by_day['fin_hor'];
                        $tab['pauses_hor'] = $hor_data_by_day['pauses_hor'];
                        $tab['total_day_hor'] = $hor_data_by_day['total_day_hor'];
                        $tab['ref_can'] = $hor_data_by_day['ref_can'];
                        $tab['id_tpi'] = $hor_data_by_day['id_tpi'];
                        $infos = $hor_data_by_day['id_tpi'];

//                        //test
//                        $tab_old = ['date_hor' => $hor_data_by_day['old_date_hor'],'debut_hor' => $hor_data_by_day['debut_hor'], 'fin_hor' => $hor_data_by_day['fin_hor'],
//                            'total_day_hor' => $hor_data_by_day['total_day_hor'],'pauses_hor' => $hor_data_by_day['pauses_hor'], 'ref_can' => $hor_data_by_day['ref_can'],
//                            'id_tpi' => $hor_data_by_day['id_tpi']];
////                        if ($hor_data_by_day['old_date_hor'])
//                        $tab_old_result = $hor->get_info_h_tpi_by_id_tpi_and_date($tab_old);

//                        $result = array_diff($tab_old_result, $tab);
//                        print_r($result);



                        $hor->copy_by_date_h_tpi($tab);
                        $same = $vac['debut_vac'];
                    }
                }
            }else{
                return false;
            }
        }
    }
} catch (PDOException $e) {
    echo $e->getMessage(), '<br/><br/>';
    echo _e("Erreur d'ajout");
}


//print_r($tab_old_result);

echo json_encode($infos);
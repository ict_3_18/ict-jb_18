<?php
 /**
 * @author Beruwalage Julian
 * @description Ce fichier s'occupe d'ajouter une période dans l'horaire TPI et de json_encode l'id du tpi.
 */

header("Content-Type: application/json");
require("../../config/config.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

session_start();
$aut = 'ADM_TPI;USR_TPI';

require(WAY . "./includes/secure.inc.php");

try {
    $tab = ['date_hor' => $_POST['date_hor'],'debut_hor' => $_POST['debut_hor'],'fin_hor' => $_POST['fin_hor'],'id_tpi' => $_POST['id_tpi'], 'ref_can' => $_POST['id_can']];
    $hor = new Horaire();
    $tab_already = $hor->get_info_h_tpi_by_id_tpi_and_date($tab);
    $total_day = strtotime($_POST['fin_hor']) - strtotime($_POST['debut_hor']);
    $total_day_min = ((date("G", $total_day) - 1) * 60) + (date("i", $total_day));
    $tab['total_day_hor'] = $total_day_min;
    $tab['pauses_hor'] = 0;
    $result = false;
    if(!empty($tab_already)) {
        foreach ($tab_already as $already) {
            $debut = date('H:i',strtotime($already['debut_hor']));
            $fin = date('H:i',strtotime($already['fin_hor']));
            if ($debut >= $tab['fin_hor']){
                $result = false;
            }else if($fin <= $tab['debut_hor']){
                $result = false;
            }else {
                $result = true;
                break;
            }
        }
        if (!$result){
            $hor->add_h_tpi($tab);
        }else{
            echo json_encode(false);
            exit();
        }
    }else{
        $hor->add_h_tpi($tab);
    }
}catch (PDOException $e){
    echo $e->getMessage(),'<br/><br/>';
    echo _e("Erreur d'ajout");
}
echo json_encode($tab['id_tpi']);

<?php
/**
 * @author Beruwalage Julian
 * @description Ce fichier permet de créer et de réutiliser des fonctions
 */

/**
 * Permet de transformer des minutes en heures : minutes
 * @param $time
 * @param string $format
 * @return string|void
 */
function hours_minutes($time, $format = '%02d:%02d')
{
    if ($time < 1) {
        return;
    }
    $hours = floor($time / 60);
    $minutes = ($time % 60);
    return sprintf($format, $hours, $minutes);
}
<?php
/**
 * @author Beruwalage Julian
 * @description Ce fichier s'occupe de load l'horaire TPI
 */

session_start();
$aut = "ADM_TPI;USR_TPI";
require_once("./../../config/config.inc.php");
require_once(WAY . "./includes/secure.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

include_once('../functions.php');


if (!empty($_POST['ref_tpi'])) {
    $hor_tpi = new Horaire();
    $tab_tpi = $hor_tpi->get_horaire_tpi($_POST['ref_tpi']);


    $vac = new Vacance();
    $tab_vac = $vac->get_all_vac();

    $total_hours_minutes_by_day = 0;
    $duree = 0;
    $same = null;
    if (!empty($tab_tpi)) :
        ?>

        <div class="row h_header">
            <div class="col">
                <div class="col-md-1">
                    <div class="row">
                        <h5>Date</h5>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="row">
                        <h5>Période</h5>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="row">
                        <h5>Temps par période</h5>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="row">
                        <h5>Total du jour</h5>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="row">
                        <h5>Total des périodes</h5>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?php foreach ($tab_tpi AS $tpi) :
        $date_hor = date("d.m.y", strtotime($tpi['date_hor']));
        $date_jour = date("N", strtotime($tpi['date_hor']));
        $tab_jour = array('1' => "Lundi", '2' => "Mardi", '3' => "Mercredi", '4' => "Jeudi", '5' => "Vendredi", '6' => "Samedi", '7' => "Dimanche");

        $total_hours_minutes = $tpi['total'];
        $one_day_hor = explode(",", $tpi['full_day_hor']);
        $day_hor = explode(",", $tpi['total_full_day_hor']);

        $duree_min_tpi = $tpi['duree_min_tpi_cla'] * 60;
        $duree_max_tpi = $tpi['duree_max_tpi_cla'] * 60;

        $vac_test = false;

        $db_vac;
        $fin_vac;
        $nom_vac;

        $total_hours_minutes_by_day += $total_hours_minutes;
        foreach ($tab_vac as $vac) :
            if ($tpi['date_hor'] >= $vac['debut_vac'] && $tpi['date_hor'] <= $vac['fin_vac']) :
                $db_vac = date("d.m.y", strtotime($vac['debut_vac']));
                $fin_vac = date("d.m.y", strtotime($vac['fin_vac']));
                $nom_vac = $vac['nom_vac'];
                $vac_test = true;
                ?>
            <?php else :
                $vac_test = false; ?>
            <?php endif;
        endforeach; ?>
        <?php if ($vac_test == false) : ?>
        <div class="row h_content">
            <div class="col">
                <div class="col-md-1">
                    <div class="row">
                        <p id="h_date">
                            <?= $tab_jour[$date_jour]; ?>
                            <?= $date_hor; ?>
                        </p>
                    </div>
                    <div class="row">
                    <span class="glyphicon glyphicon-duplicate text-primary copy_h_tpi" data-toggle='modal'
                          data-target='#copy_h_tpi_modal' id_tpi="<?= $tpi['id_tpi']; ?>" date_hor="<?= $tpi['date_hor']; ?>"></span>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="row">
                        <?php foreach ($one_day_hor as $hor) : ?>
                            <p><?= $hor; ?></p>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="col-md-2 total_periode">
                    <div class="row">
                        <?php foreach ($day_hor as $hor) :
                            $id_line = explode('.', $hor)[0];
                            $hor_value = explode('.', $hor)[1];
                            $duree += $hor_value;
                            if ($duree > $duree_min_tpi && $duree < $duree_max_tpi) : ?>
                                <p><?= hours_minutes($hor_value) . " "; ?><span
                                            class="glyphicon glyphicon-pencil text-primary edit_h_tpi"
                                            id_hor="<?= $id_line; ?>" data-toggle='modal'
                                            data-target='#modif_h_tpi_modal'></span><span
                                            class="glyphicon glyphicon-trash text-danger del_h_tpi"
                                            id_hor="<?= $id_line; ?>" id_tpi="<?= $tpi['id_tpi']; ?>"></span><span
                                            class="glyphicon glyphicon-ban-circle text-danger stop_h_tpi"
                                            id_hor="<?= $id_line; ?>" id_tpi="<?= $tpi['id_tpi']; ?>"
                                            date_hor="<?= $tpi['date_hor']; ?>"></span></p>
                                <?php ?>
                            <?php else : ?>
                                <p><?= hours_minutes($hor_value) . " "; ?><span
                                            class="glyphicon glyphicon-pencil text-primary edit_h_tpi"
                                            id_hor="<?= $id_line; ?>" data-toggle='modal'
                                            data-target='#modif_h_tpi_modal'></span><span
                                            class="glyphicon glyphicon-trash text-danger del_h_tpi"
                                            id_hor="<?= $id_line; ?>" id_tpi="<?= $tpi['id_tpi']; ?>"></span></p>
                            <?php endif; endforeach; ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="row">

                        <h4><?= hours_minutes($total_hours_minutes); ?></h4>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="row">
                        <?php ?>
                        <h4><?= hours_minutes($total_hours_minutes_by_day); ?></h4>
                    </div>
                </div>
            </div>
        </div>
    <?php else : ?>
        <?php if ($same == null) : ?>
            <div class="row h_content_vac">
                <div class="col">
                    <h4><?= $nom_vac . " " . $db_vac . " - " . $fin_vac; ?></h4>
                    <?php $same = $db_vac; ?>
                </div>
            </div>
        <?php endif; endif; ?>
    <?php endforeach;
} ?>
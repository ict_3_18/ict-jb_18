<?php
/**
 * @author Beruwalage Julian
 * @description Ce fichier s'occupe de load l'horaire TPI
 */

session_start();
$aut = "ADM_TPI;USR_TPI";
require_once("./../../config/config.inc.php");
require_once(WAY . "./includes/secure.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

include_once('../functions.php');

$tpi = new Tpi($_POST['id_tpi']);
echo $tpi->gen_horaire_tpi();
$(function () {
    /**
     * @author Beruwalage Julian
     * @description Ce fichier s'occupe de tout ce qu'il se passe sur la page /horaires/index.php
     *
     *                              ce fichier est séparer en 3 zones : - Variables / tableaux
     *                                                                  - Events
     *                                                                  - Fonctions
     */

    /** ------------------------------------------ Variables / tableaux ------------------------------------------------ **/

    tab_by_date = [];

    /** ------------------------------------------ Events ------------------------------------------------ **/

    /**
     * Quand on clique sur le bouton de génération on modifie la date de début du TPI et on ajoute l'horaire des TPI dans la base
     * Une fois l'ajout fait, on load l'horaire (affichage)
     */
    $('#btn_gen_h_tpi').on('click', function () {
        $.post(
            './json/update_tpi_info_cla.json.php', {
                add_id_tpi: $('#id_tpi').val(),
                add_id_cla: $('#id_cla').val(),
                add_duree_max_tpi: $('#duree_max_tpi').val(),
                add_duree_min_tpi: $('#duree_min_tpi').val(),
                add_id_can: $('#id_can').val(),
                add_datepicker: $('#datepicker_h_tpi').val()
            },
            function (data) {
                $.post(
                    './json/add_h_tpi.json.php', {
                        add_h_cadre: data.h_cadre,
                        add_datepicker: data.date_debut_tpi,
                        add_id_tpi: data.id_tpi,
                        add_id_cla: data.id_cla,
                        add_duree_max_tpi: data.duree_max_tpi,
                        add_duree_min_tpi: data.duree_min_tpi,
                        add_id_can: data.id_can

                    },
                    function (data2) {
                        load_h_tpi(data2[0].id_tpi);
                    });
            });
    });

    $('#btn_del_h_tpi').on('click', function () {
        id_can = $(this).attr('id_can');
        id_tpi = $(this).attr('id_tpi');
       $.post(
           './json/del_h_tpi.json.php', {
               id_can: id_can,
               id_tpi: id_tpi
           },
           function (data) {
               load_h_tpi(id_tpi);
           });
    });

    /* edition horaire **/
    $('#edit_h_tpi').on('click', function () {
        id_can = $(this).attr('id_can');
        id_tpi = $(this).attr('id_tpi');
        load_h_tpi(id_tpi);
        $("#print_h_tpi").css("display","inline");
        $("#edit_h_tpi").css("display","none");
    });


    /* vue horaire **/
    $('#print_h_tpi').on('click', function () {
        id_can = $(this).attr('id_can');
        id_tpi = $(this).attr('id_tpi');
        $('#horaire_list').load(
            './load/horaire_view.load.php',
            {
                id_tpi: id_tpi,
                id_can: id_can
            }
        );

    });




    /**
     * Quand on clique sur le bouton d'édition d'une période, on récupère les informations de la période et on les set dans la modal d'édition
     */
    $(document).on('click', '.edit_h_tpi', function () {
        id_hor = $(this).attr('id_hor');
        $.post(
            './json/info_h_tpi.json.php', {
                id_hor: id_hor
            },
            function (data) {
                // $('#debut_hor').val(data[0].debut_hor);
                // $('#fin_hor').val(data[0].fin_hor);
                $('#id_hor_mod').val(id_hor);
                $('#id_tpi_mod').val(data[0].id_tpi);
                $('#date_hor_mod_edit').val(data[0].date_hor);
            });
    });

    /**
     * Quand on clique sur le bouton de validation de la modal d'édition on modifie les valeurs changer
     * Une fois la modification faite, on load l'horaire (affichage)
     */
    $(document).on('click', '#valid_edit', function () {
        id_hor = $('#id_hor_mod').val();
        id_tpi = $('#id_tpi_mod').val();
        $.post(
            './json/edit_h_tpi.json.php', {
                id_hor: id_hor,
                debut_hor: $('#debut_hor').val(),
                fin_hor: $('#fin_hor').val(),
                id_tpi: id_tpi,
                date_hor: $('#date_hor_mod_edit').val()
            },
            function (data) {
                if(!data){
                    $('#custom-alert .message').html('L\'heure que vous essayez d\'entrée est déjà présente');
                    $('#custom-alert').addClass('alert-danger');
                    $('#custom-alert').show();
                    $('html,body').animate({scrollTop : $('html').offset().top}, 300);
                }
                load_h_tpi(id_tpi);
            });
    });

    /**
     * Quand on clique sur le bouton de suppression d'une période on supprime la période en question
     * Une fois la suppression faite, on load l'horaire (affichage)
     */
    $(document).on('click', '.del_h_tpi', function () {
        id_hor = $(this).attr('id_hor');
        id_tpi = $(this).attr('id_tpi');
        $.post(
            './json/del_periode_h_tpi.json.php', {
                id_hor: id_hor,
                id_tpi: id_tpi
            },
            function (data) {
                load_h_tpi(data.id_tpi);
            });
    });

    /**
     * Quand on clique sur le bouton de copy d'un jour, on récupère toutes les informations du jour et on les mets dans un tableau et on set la date dans la modal
     * La date est setter 7 jours après la date sélectionner
     */
    $(document).on('click', '.copy_h_tpi', function () {
        date_hor = $(this).attr('date_hor');
        id_tpi = $(this).attr('id_tpi');
        tab_by_date = [];
        $.post(
            './json/info_by_date_h_tpi.json.php', {
                date_hor: date_hor,
                id_tpi: id_tpi
            },
            function (data) {
                $('#date_hor_mod').val(data[0].date_hor);
                tab_by_date.push(data);
            });
    });

    /**
     * Quand on clique sur le bouton de validation de la copy de la modal, on ajoute les périodes à la date sélectionner dans la base (t_h_tpi)
     * Une fois l'ajout fait, on load l'horaire (affichage)
     */
    $(document).on('click', '#valid_copy', function () {
        $.post(
            './json/copy_by_date_h_tpi.json.php', {
                tab_by_date: tab_by_date,
                new_date_hor: $('#date_hor_mod').val()
            },
            function (data) {
                tab_by_date = [];
                load_h_tpi(data);
            });
    });

    //todo : modification apporter !!!!!!!!!!! présentation
    /**
     * Quand on clique sur le bouton pour stopper le TPI, on supprime les heures / dates qui sont après
     * Une fois la suppression faite, on load l'horaire (affichage)
     */
    $(document).on('click', '.stop_h_tpi', function () {
        id_hor = $(this).attr('id_hor');
        id_tpi = $(this).attr('id_tpi');
        date_hor = $(this).attr('date_hor');
        $.post(
            './json/stop_h_tpi.json.php', {
                id_hor: id_hor,
                id_tpi: id_tpi,
                date_hor: date_hor
            },
            function (data) {
                load_h_tpi(data);
            });
    });

    /**
     * Quand on clique sur le bouton d'ajout d'une période, on set l'id du tpi et l'id du candidat dans la modal
     */
    $(document).on('click', '#btn_add_day', function () {
        id_tpi = $(this).attr('id_tpi');
        id_can = $(this).attr('id_can');
        $('#id_tpi_add_mod').val(id_tpi);
        $('#id_can_add_mod').val(id_can);
    });

    /**
     * Quand on clique sur le bouton de validation d'ajout de période de la modal, on ajoute les périodes à la date sélectionner dans la base (t_h_tpi)
     * Une fois l'ajout fait, on load l'horaire (affichage)
     */
    $(document).on('click', '#valid_add', function () {
        id_tpi = $('#id_tpi_add_mod').val();
        $.post(
            './json/add_periode_h_tpi.json.php', {
                date_hor: $('#date_hor_add_mod').val(),
                debut_hor: $('#debut_hor_add_mod').val(),
                fin_hor: $('#fin_hor_add_mod').val(),
                id_tpi: id_tpi,
                id_can: $('#id_can_add_mod').val()
            },
            function (data) {
                if (!data){
                    $('#custom-alert .message').html('L\'heure que vous essayez d\'entrée est déjà présente');
                    $('#custom-alert').addClass('alert-danger');
                    $('#custom-alert').show();
                    $('html,body').animate({scrollTop : $('html').offset().top}, 300);
                }
                load_h_tpi(id_tpi);
            });
    });

    $(document).on('click', '#custom-alert .close', function () {
       $(this).parent().hide();
    });

});

/** ------------------------------------------ Fonctions ------------------------------------------------ **/

function load_h_tpi(id_tpi) {
    $('#horaire_list').load('./load/horaire.load.php', {ref_tpi: id_tpi});
}
<?php
/**
 * @author Beruwalage Julian
 * @description Ce fichier s'occupe d'afficher la page horaires/index.php.
 */

session_start();
require("./../config/config.inc.php");
$aut = "ADM_TPI;USR_TPI";
require(WAY . "/includes/secure.inc.php");
require_once(WAY . "/includes/autoload.inc.php");
require_once(WAY . "/includes/head.inc.php");
require("./modales/modif_h_tpi.mod.php");
require("./modales/copy_h_tpi.mod.php");
require("./modales/add_day.mod.php");

// Type d'erreur à afficher
error_reporting(E_ALL);

$tpi_id = $_POST['id_tpi'];
$tpi = new Tpi($tpi_id);
$can = new Candidat($tpi->get_id_can());
$cla = new Classe($can->get_classe());
$id_cla = $cla->get_id_cla();
$id_can = $can->get_id();
$hor = new Horaire();

?>
<div class="alert" id="custom-alert">
    <a href="#" class="close">×</a>
    <strong class="bold"></strong><span class="message"></span>
</div>
<div class="col h_general">
    <div class="row">
        <div class="col-md-6">
            <div class="col-md-12">
                <h2>Candidat : <?= $tpi->get_prenom_can() . " " . $tpi->get_nom_can(); ?></h2>
            </div>
            <div class="col-md-12">
                <h4>Classe : <?= $tpi->get_classe(); ?></h4>
            </div>
            <div class="col-md-12" id="date">
                <div class="input-group">
                    <div class="row">
                        <h3 class="h3_date" for="db_tpi">Début du TPI</h3>
                        <div class="input-group input_date">
                            <input class="form-control" id="datepicker_h_tpi" type="date" value="<?= date('Y-m-d'); ?>">
                            <span class="addon_h_tpi input-group-addon glyphicon glyphicon-calendar"></span>
                            <input type="hidden" id="id_tpi" value="<?= $tpi_id; ?>">
                            <input type="hidden" id="id_cla" value="<?= $id_cla; ?>">
                            <input type="hidden" id="id_can" value="<?= $id_can; ?>">
                            <input type="hidden" id="duree_min_tpi" value="<?= $cla->get_duree_min_tpi_cla(); ?>">
                            <input type="hidden" id="duree_max_tpi" value="<?= $cla->get_duree_max_tpi_cla(); ?>">
                        </div>
                    </div>
                </div>
                    <div class="row">
                        <button class="btn btn-primary" id="btn_gen_h_tpi">Générer</button>
                        <button class="btn btn-danger" id="btn_del_h_tpi" id_can="<?= $id_can; ?>" id_tpi="<?= $tpi_id; ?>">Supprimer</button>
                        <button class="btn btn-primary" id="print_h_tpi" id_can="<?= $id_can; ?>" id_tpi="<?= $tpi_id; ?>">Visualisation</button>
                        <button class="btn btn-primary" id="edit_h_tpi" id_can="<?= $id_can; ?>" id_tpi="<?= $tpi_id; ?>">Edition</button>

                    </div>


            </div>
            <div class="col-md-12">
                <div class="h_cadre_content" id="h_cadre_liste" id_cla="<?= $id_cla; ?>">

                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="h_t_content" id="horaire_list" ref_tpi="<?= $tpi_id; ?>">

            </div>
        </div>
    </div>
</div>

<button type="button" id="btn_add_day" data-toggle='modal' data-target='#btn_add_day_modal'
        id_tpi='<?= $tpi_id; ?>' id_can='<?= $id_can; ?>'>
    <span class="glyphicon glyphicon-plus"></span>
</button>
<script>$("#h_cadre_liste").load("../groupes/load/horaire_cadre.load.php", {id_cla: <?= $id_cla; ?>});</script>
<script>$("#horaire_list").load("./load/horaire_view.load.php", {id_tpi: <?= $tpi_id; ?>});</script>
<script src="js/horaire.js"></script>




$(function(){
    $('#search').autocomplete(
        {
            source: $("#url").val() + "/json/search_can.json.php",
            minLength: 3,
            select: function(event, ui) {
                $('#id_choice_can').val(ui.item.id);
                $("#form_choice_can").submit();
            }
        }
    );
});
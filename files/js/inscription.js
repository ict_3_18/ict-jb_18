$(function(){

   $.validator.addMethod("PWCHECK",
      function(value,element) {
         if(/^(?=.*?[A-Z]{1,})(?=(.*[a-z]){1,})(?=(.*[0-9]){1,})(?=(.*[$@$!%*?&]){1,}).{8,}$/.test(value)){
            return true;
         }else{
            return false;
         };
      }
   ); 
  
   $("#inscription_form").validate(
      {
         rules: {
            nom_per:{
               required: true,
               minlength: 2,
            },
            prenom_per:{
               required: true,
               minlength: 2,
            },
            email_per: {
               required: true,
               email: true
            },
            password:{
               required: true,
               PWCHECK: true,
            },
            password_conf: {
               required: true,
               equalTo: "#password"
            }
         },
         messages:{
            nom_per: {
               required: "Votre nom est indispensable à l'ouverture d'un compte",
               minlength: "Votre nom doit être composé de 2 caractères au minimum"
            },
            prenom_per: {
               required: "Votre prénom est indispensable à l'ouverture d'un compte",
               minlength: "Votre prénom doit être composé de 2 caractères au minimum"
            },
            email_per: {
               required: "Votre adresse e-mail est indispensable à l'ouverture d'un compte",
               email: "Votre adresse e-mail doit avoir le format suivant : name@domain.com"
            },
            password:{
               required: "Veuillez saisir votre mot de passe",
               PWCHECK:"Le mot de passe doit comporter au minimum 8 caractères, dont une minuscule, une majuscule, un chiffre et un caractère spécial."
            },
            password_conf:{
               equalTo: "Les mots de passe ne sont pas identiques",
               required: "Veuillez saisir une deuxième fois votre mot de passe",
            },
         },
         submitHandler: function(form) {
            console.log("formulaire envoyé");
            
            $.post(
               "./json/inscription.json.php?_="+Date.now(),
               {
                  nom_per:$("#nom_per").val(),
                  prenom_per:$("#prenom_per").val(),
                  email_per:$("#email_per").val(),
                  password_new_per:$("#password").val()
               },
               function result(data,status){
                  //Ajoute le message
                  //message(data.message.texte,data.message.type);
                  
               }
            )
            
         }
      }
   );
});
<?php
/**
 * Created by PhpStorm.
 * User: cp-15hup
 * Date: 26.02.2019
 * Time: 11:25
 */
require("./../config/config.inc.php");

$pdo = new PDO('mysql:dbname=' . BASE_NAME . ';host=' . SQL_HOST, SQL_USER, SQL_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8', PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC));

//Création de la table t_fonctions_tpi (afin de stocker les différents rôles liés à un tpi)
$query ="CREATE TABLE `t_fonctions_tpi` ( `id_fnt` INT UNSIGNED NOT NULL AUTO_INCREMENT , `fonction_fnt` VARCHAR(150) NOT NULL , `name_fnt` VARCHAR(150) NOT NULL , PRIMARY KEY (`id_fnt`), `order_fnt` INT UNSIGNED ) ENGINE = InnoDB;";
$stmt = $pdo->prepare($query);
$stmt->execute();

//ajout des différents rôles nécessaire au bon déroulement d'un TPI
$query ="SELECT fonction_fnt FROM t_fonctions_tpi WHERE (fonction_fnt LIKE 'Maître d\'apprentissage' OR fonction_fnt LIKE 'Expert responsable' OR fonction_fnt LIKE 'Expert accompagnant' OR fonction_fnt LIKE 'Coach')";
$stmt = $pdo->prepare($query);
$stmt->execute();
$tab_fonctions = $stmt->fetchAll();
if(empty($tab_fonctions)) {
    $query = "INSERT INTO `t_fonctions_tpi` (`id_fnt`, `fonction_fnt`, `name_fnt`,`order_fnt`) VALUES (NULL, 'Maître d\'apprentissage', 'Maître d\'apprentissage',1), (NULL, 'Expert responsable', 'Expert responsable',3), (NULL, 'Expert accompagnant', 'Expert accompagnant',4), (NULL, 'Coach', 'Coach',2)";
    $stmt = $pdo->prepare($query);
    $stmt->execute();
}

//Crréation de la table t_tpi_per (afin de lier les rôles au personnes et à un TPI)
$query = "CREATE TABLE `t_tpi_per` (
  `id_tpi_per` int(10) UNSIGNED NOT NULL,
  `id_tpi` int(10) UNSIGNED NOT NULL,
  `id_per` int(10) UNSIGNED NOT NULL,
  `id_fnt` int(10) UNSIGNED NOT NULL
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

  ALTER TABLE `t_tpi_per`
  ADD PRIMARY KEY (`id_tpi_per`),
  ADD KEY `id_tpi` (`id_tpi`),
  ADD KEY `id_per` (`id_per`),
  ADD KEY `id_fnt` (`id_fnt`);
  
  ALTER TABLE `t_tpi_per`
  MODIFY `id_tpi_per` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
  
  ALTER TABLE `t_tpi_per`
  ADD CONSTRAINT `t_tpi_per_ibfk_1` FOREIGN KEY (`id_per`) REFERENCES `t_personnes` (`id_per`),
  ADD CONSTRAINT `t_tpi_per_ibfk_2` FOREIGN KEY (`id_tpi`) REFERENCES `t_tpi` (`id_tpi`),
  ADD CONSTRAINT `t_tpi_per_ibfk_3` FOREIGN KEY (`id_fnt`) REFERENCES `t_fonctions_tpi` (`id_fnt`);
  ";
$stmt = $pdo->prepare($query);
$stmt->execute();



//Migration des données (fonctions/rôles) depuis t_tpi à t_tpi_per
$query ="SELECT id_tpi, ref_exp_1, ref_exp_2, ref_maitre_app FROM t_tpi";
$stmt = $pdo->prepare($query);
$stmt->execute();
$tab_per_by_tpi = $stmt->fetchAll();
foreach ($tab_per_by_tpi as $tpi){
    $id_tpi = $tpi['id_tpi'];
    $query = "INSERT INTO t_tpi_per (id_tpi,id_per,id_fnt) VALUES (".$id_tpi.", ".$tpi['ref_exp_1'].",(SELECT id_fnt FROM t_fonctions_tpi WHERE fonction_fnt LIKE 'Expert responsable'))";
    echo $query."<br>";
    $stmt = $pdo->prepare($query);
    $stmt->execute();
    $query = "INSERT INTO t_tpi_per (id_tpi,id_per,id_fnt) VALUES (".$id_tpi.", ".$tpi['ref_exp_2'].",(SELECT id_fnt FROM t_fonctions_tpi WHERE fonction_fnt LIKE 'Expert accompagnant'))";
    $stmt = $pdo->prepare($query);
    $stmt->execute();
    $query = "INSERT INTO t_tpi_per (id_tpi,id_per,id_fnt) VALUES (".$id_tpi.", ".$tpi['ref_maitre_app'].",(SELECT id_fnt FROM t_fonctions_tpi WHERE fonction_fnt LIKE \"Maître d\'apprentissage\"))";
    $stmt = $pdo->prepare($query);
    $stmt->execute();
}

//suppression des champs obsolètes
$query = "ALTER TABLE `t_tpi`
  DROP `ref_exp_1`,
  DROP `ref_exp_2`,
  DROP `ref_maitre_app`;";
$stmt = $pdo->prepare($query);
$stmt->execute();

ECHO "UPDATE DONE";


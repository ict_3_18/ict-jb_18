$(function () {

    $(".convoquer").on( "click", function () {
        $val = 0;
        if ($(this).is(':checked')){
            $val = 1;
        }
        $.post(
                "./json/index.json.php?_=" + Date.now(),
                {
                    id_exa: $(this).attr("id"),
                    val: $val
                },
                function result(data, status) {
                    if (data.reponse) {
                        if($("#spanId"+ data.id).hasClass("glyphicon-ok")){
                            $("#spanId"+ data.id).addClass("glyphicon-remove");
                            $("#spanId"+ data.id).removeClass("glyphicon-ok");
                            console.log("true -> Vu" + data.id)
                        }else{
                            $("#spanId"+ data.id).addClass("glyphicon-ok");
                            $("#spanId"+ data.id).removeClass("glyphicon-remove");
                            console.log("false -> Croix" + data.id)
                        }
                        message(data.message.texte, data.message.type);
                    }else {
                        message(data.message.texte, data.message.type);
                    }
                }
        );
    });

    /**
     * Activation de la Modale des remarques, nettoye le tableau et reprend la date
     * Appel au JSON get_remarque_per pour retourner les divers remarques de la paresonne et leur nom
     */
    $('.conf_convoc_btn').click(function (e) {
        //Nettoyage & reprise de données de la page principale
        $("#modal_conf_convoc table .appended").remove();
        $("#modal_conf_convoc textarea").removeAttr("style");
        $("#modal_conf_convoc textarea").val("");
        $('#modal_conf_convoc').modal("toggle");
        var id_exa = $(this).attr("id_exa");
        $.ajax({
            type: "POST",
            url: "./json/get_all_candidat_exa.json.php?_=" + Date.now(),
            data: {
                id_exa : id_exa
            },
            dataType: 'json',
            async: 'false',
            success: function (data) {
                $.each(data.confirme, function (key, val) {
                    var appendData = "";

                    appendData +=   "<tr class='success appended' '>" +
                        "<td>" + val.nom_can + "</td>" +
                        "<td>" + val.prenom_can + "</td>" +
                        "<td>" + val.nom_grp + " - " + val.nom_cla +"</td>" +
                        "</tr>";

                    $("#modal_conf_convoc #tab_confirmed").append(appendData);
                });

                var tabAllId = new Array();
                $.each(data.no_confirme, function (key, val) {
                    tabAllId[key] = val.id_can;
                    var appendData = "";

                    appendData +=   "<tr class='danger appended' '>" +
                        "<td>" + val.nom_can + "</td>" +
                        "<td>" + val.prenom_can + "</td>" +
                        "<td>" + val.nom_grp + " - " + val.nom_cla +"</td>" +
                        '<td>' +
                        '<form method="post" action="./send_rappel.php" target="_blank">' +
                        '<input type="hidden" name="id_can" value="' + val.id_can + '">' +
                        '<button type="submit" id="' + val.id_can + '" class="glyphicon glyphicon-envelope"></button>' +
                        '</form>' +
                        '</td>' +
                        "</tr>";

                    $("#modal_conf_convoc #tab_waiting").append(appendData);
                });
                $("#modal_conf_convoc #tab_waiting").prepend("<tr class='appended'><th>Nom</th><th>Prénom</th><th>Classe</th><th>" +
                        '<form method="post" action="./send_rappel.php" target="_blank">' +
                        '<input type="hidden" name="all_id" value="' + tabAllId + '">' +
                        '<button type="submit" class="glyphicon glyphicon-envelope"></button>' +
                        '</form>' +
                    "</th></tr>");
                console.log(tabAllId)
            }
        })
    });



});
<?php
/**
 * Created by PhpStorm.
 * User: CP-16RDM
 * Date: 04.09.2019
 * Time: 13:29
 */
session_start();
$aut = "ADM_CONV;USR_CONV";

require("../config/config.inc.php");
require(WAY . "./includes/secure.inc.php");
require_once(WAY . "/includes/autoload.inc.php");
require_once(WAY . "/includes/head.inc.php");

function send_mail($id_can){
    $can = new Candidat($id_can);
    $mail = $can->get_email();

    $headers = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
    $headers .= 'From: ICT-JB.net <webmaster@ict-jb.net>' . "\r\n";
    $headers .= 'Reply-To: webmaster@ict-jb.net' . "\r\n";
    $headers .= "X-Sender: <www.ict-b.net>" . "\r\n";;
    $headers .= "X-Mailer: PHP" . "\r\n";;
    $headers .= "X-auth-smtp-user: webmaster@ict-jb.net" . "\r\n";;
    $headers .= "X-abuse-contact: webmaster@ict-jb.net" . "\r\n";;
    $expe = "webmaster@ict-jb.net";

    $date_time = date("D, d M Y H:i:s O");
    $headers .= 'DATE: ' . $date_time . "\r\n";


    $sujet_mail = "Convocation au(x) examen(s) - Candidat N° " . $can->get_no_candidat_minus();
    $mess_mail = utf8_decode("Bonjour,<br><br>
                                    De nouvelles convocations sont disponibles sur le site <a href='https://new.ict-jb.net/Public/login.php'>ICT-JB</a><br><br>
                                    Merci de vous connecter et de confirmer que vous avez pris connaissance des dates, heures et lieux des examens.<br><br>
                                    Meilleures salutations<br><br>
                                    Jacques Hirtzel<br>
                                    Expert en chef Berne francophone");
    $destinataire_mail = $mail;
    mail($destinataire_mail, $sujet_mail, $mess_mail, $headers);

    $str = "";
    $str .=  "<hr>";
    $str .= '<span class="center">';
    $str .= "<b>Destinataire :</b> " . $can->get_prenom() . " " . $can->get_nom() . " (" . $mail . ") <br>";
    $str .= "</span>";

    echo $str;
}

$convoc = new Convocations();
$candid = new Candidat();

if(array_key_exists("id_can",$_POST)){
    echo "<h1 class='text-center'>Envoi du mail suivant</h1>";
    $candid->set_conf_convoc_can_by_id_can_reset($_POST['id_can']);
    send_mail($_POST['id_can']);
}

if(array_key_exists("id_cla",$_POST)){
    $cla = new Classe($_POST['id_cla']);
    $tab_cla = $cla->get_tab_can_one_cla($_POST['id_cla']);
    $nb_mails = 0;
    foreach($tab_cla as $ligne_cla){
        if($convoc->get_future_exam_can($ligne_cla['id_can'])){
            $nb_mails++;
        }
    }
    echo "<h1 class='text-center'>Envoi des " . $nb_mails . " mails suivants</h1>";
    foreach($tab_cla as $ligne_cla){
        if($convoc->get_future_exam_can($ligne_cla['id_can'])){
            $candid->set_conf_convoc_can_by_id_can_reset($ligne_cla['id_can']);
            send_mail($ligne_cla['id_can']);
        }
    }

}

if(array_key_exists("id_grp",$_POST)){
    $grp = new Groupe($_POST['id_grp']);
    $tab_all_candidats = $grp->get_tab_can_all_grp();
    $tab_can_this_grp = $tab_all_candidats[$_POST['id_grp']];
    $nb_mails = 0;
    foreach ($tab_can_this_grp as $ligne_can) {
        if($convoc->get_future_exam_can($ligne_can[0])){
            $nb_mails++;
        }
    }
    echo "<h1 class='text-center'>Envoi des " . $nb_mails . " mails suivants</h1>";
    foreach ($tab_can_this_grp as $ligne_can) {
        if($convoc->get_future_exam_can($ligne_can[0])){
            $candid->set_conf_convoc_can_by_id_can_reset($ligne_can[0]);
            send_mail($ligne_can[0]);
        }
    }

}


<?php

session_start();

if (!empty($_POST)) {
    $aut = "ADM_CONV;USR_CONV";
    require("../../config/config.inc.php");
    require(WAY . "./includes/secure.inc.php");
    
    // Instanciation de la classe dérivée
    $pdf = new PdfConvoc();
    //tableau des candidats
    $tab_can = array();
    //tableau des duals
    $tab_dual = array();
    //convocation
    $convocation = new Convocations();

    if (isset($_POST['id_can'])) {//si un seul candidat
        array_push($tab_can, array('id_can' => $_POST['id_can']));
        $can = new Candidat($_POST['id_can']);
        if ($can->get_systeme() != '')
            array_push($tab_dual, array('id_can' => $_POST['id_can']));
    } else if (isset($_POST['id_grp'])) {//si toutles candidats d'un groupe
        $tab_can = $convocation->get_all_can_in_grp($_POST['id_grp']);
        $tab_dual = $convocation->get_all_dual_in_grp($_POST['id_grp']);
    } else if (isset($_POST['id_cla'])) {//si tout les candidats d'une classe
        $tab_can = $convocation->get_all_can_in_cla($_POST['id_cla']);
        $tab_dual = $convocation->get_all_dual_in_cla($_POST['id_cla']);
    }

    foreach ($tab_dual AS $can) {
        $pdf->gen_convoc(new Candidat($can['id_can']));
    }

    foreach ($tab_can AS $can) {
        $pdf->gen_convoc(new Candidat($can['id_can']), 'COPIE');
    }

    $pdf->accuse_reception($tab_can);

    $pdf->Output();
} else {

    require("../../config/config.inc.php");

    require(WAY . "Public/includes/secure.inc.php");

    $pdf = new PdfConvoc();

    $can = new Candidat($_SESSION['id']);

    $pdf->gen_convoc($can);

    $pdf->Output();
}

?>

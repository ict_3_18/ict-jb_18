<?php
session_start();
$aut = "ADM_CONV;USR_CONV";

require("../config/config.inc.php");
require(WAY . "./includes/secure.inc.php");
require(WAY . "Convocations/includes/heading.inc.php");

$examens = new Examen();
$convocation = $examens->get_future_exam();
$convoc = new Convocations();
?>

<div class="panel-body">
    <div class="row text-center ligne titre-conv">
        <div class="col-md-2">Elèves</div>
        <div class="col-md-1">Examen</div>
        <div class="col-md-4">Date</div>
        <div class="col-md-2">Candidats</div>
        <div class="col-md-2">A convoquer</div>
        <div class="col-md-1">Vu</div>
    </div>

    <?php
    foreach ($convocation AS $conv) {
        $couleurs = $examens->get_couleur_examen($conv['id_exa']);
        $color = "lightgrey";
        if ($conv['definitif_exa']) {
            $color = "white";
        }
        ?>
        <div class="row text-center ligne" style="background-color: <?php echo $color; ?>">
            <label class="convocations" for="<?php echo $conv['id_exa']; ?>">
                <div class="col-md-2">
                    <?php
                    foreach ($couleurs AS $c) {
                        echo '<div class="col-md-' . (12 / count($couleurs)) . ' color" style="background-color:' . $c['color_grp'] . '; border: solid 5px ' . $color . '">' . $c['nom_grp'] . '</div>';
                    }
                    ?>
                </div>
                <div class="col-md-1"><?php echo $conv['no_ich_exa'] ?></div>
                <div class="col-md-4"><?php echo ucfirst(utf8_encode(strftime("%A %d %B %Y %H:%M", strtotime($conv['date_heure_exa'])))); ?></div>
                <div class="col-md-2"><?php echo $conv['nb_can']; ?></div>
                <div class="col-md-2">
                    <?php
                    if ($conv['definitif_exa']) {
                        if ($conv['convoc_exa']) {
                            echo '<input type="checkbox" class="convoquer" id="' . $conv['id_exa'] . '" checked>';
                            echo '<span class="glyphicon glyphicon-ok" id="spanId'.$conv['id_exa'].'" aria-hidden="true"></span>';
                            echo '</input>';
                        } else {
                            echo '<input type="checkbox" class="convoquer" id="' . $conv['id_exa'] . '">';
                            echo '<span class="glyphicon glyphicon-remove" id="spanId'.$conv['id_exa'].'" aria-hidden="true"></span>';
                            echo '</input>';
                        }
                    } else {
                        echo 'Pas définitif';
                    }
                    ?>
                </div> </label>
                <div class="col-md-1 conf_convoc_btn" id_exa="<?= $conv['id_exa'];?>">
                    <?php
                    $tab_convoc = $convoc->get_all_conf_by_id_exa($conv['id_exa']);
                    echo count($tab_convoc) . "/" . $conv['nb_can'];
                    ?>
                </div>

        </div>
        <?php
    }
    ?>

</div>

</div>
</div>
<script src="<?php echo URL; ?>Convocations/js/index.js"></script>
<?php
require("./modales/confirmation_convoc.mod.php");
?>
</body>
</html>

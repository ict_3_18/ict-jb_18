<?php
session_start();

header("Content-Type: application/json");

$aut = "ADM_CONV";
//print_r($_POST);

require '../../config/config.inc.php';
require(WAY . "includes/secure.inc.php");

$examen = new Examen($_POST['id_exa']);

if ($examen->change_convocation($_POST['val'])) {
    $tab['reponse'] = true;
    $tab['message']['texte'] = "La convocation à bien été modifié.";
    $tab['message']['type'] = "success";
    $tab['id'] = $_POST['id_exa'];
} else {
    $tab['reponse'] = false;
    $tab['message']['texte'] = "Un problème est survenu";
    $tab['message']['type'] = "danger";
    $tab['id'] = $_POST['id_exa'];
}

echo json_encode($tab);

?>
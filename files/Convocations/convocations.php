<?php
session_start();
$aut = "ADM_CONV;USR_CONV";

require("../config/config.inc.php");
require(WAY . "./includes/secure.inc.php");
require(WAY . "Convocations/includes/heading.inc.php");

if (isset($_POST['grp'])) {
    $tab_convoc = $convocations->get_future_exam_grp($_POST['grp']);
} else if (isset($_POST['cla'])) {
    $tab_convoc = $convocations->get_future_exam_cla($_POST['cla']);
} else {
    header('Location: /ict-jb_18/files/Convocations/');
}
?>

<div class="panel-body">
    <?php
    if (count($tab_convoc) > 0) {
        ?>
        <div class="row text-center ligne titre-conv">
<!--            <label class="convocations" for="conv_all">-->
                <div class="col-md-4">Prénom, nom</div>
                <div class="col-md-2">Candidat n°</div>
                <div class="col-md-2">Nombre examens</div>
                <div class="col-md-2">
                    <form method="post" action="./pdf/convoc.pdf.php" target="./convocations.php">
                        <?php
                        if (isset($_POST['grp'])) {
                            echo '<input type="hidden" name="id_grp" value="' . $_POST['grp'] . '">';
                        } else if (isset($_POST['cla'])) {
                            echo '<input type="hidden" name="id_cla" value="' . $_POST['cla'] . '">';
                        }
                        ?>
                        <button type="submit" class="btn btn-link col-md-12" id="conv_all">Convoquer tout le monde</button>
                    </form>
                </div>
                <div class="col-md-2 ">
                    <form method="post" action="./send_mail.php" target="_blank">
                        <?php
                        if (isset($_POST['grp'])) {
                            echo '<input type="hidden" name="id_grp" value="' . $_POST['grp'] . '">';
                        } else if (isset($_POST['cla'])) {
                            echo '<input type="hidden" name="id_cla" value="' . $_POST['cla'] . '">';
                        }
                        ?>
                        <button type="submit" class="btn btn-link col-md-12 glyphicon glyphicon-envelope" id="conv_all"></button>
                    </form>
                </div>
<!--            </label>-->
        </div>

        <?php
        foreach ($tab_convoc AS $convoc) {
            $color = "lightgrey";
            if ($convoc['definitif_exa']) {
                $color = $convoc['color_grp'];
            }
            ?>
            <div class="row text-center ligne" style="background-color: <?php echo $color; ?>">
<!--                <label class="convocations" for="--><?php //echo $convoc['id_can']; ?><!--">-->
                    <div class="col-md-4"><?php echo $convoc['nom_can'] . ' ' . $convoc['prenom_can']; ?></div>
                    <div class="col-md-2"><?php echo($convoc['no_candidat_can'] > 999 ? substr($convoc['no_candidat_can'], 1) : $convoc['no_candidat_can']); ?></div>
                    <div class="col-md-2"><?php echo $convoc['nb_exa']; ?></div>
                    <div class="col-md-2">
                        <form method="post" action="./pdf/convoc.pdf.php" target="_blank">
                            <input type="hidden" name="id_can" value="<?php echo $convoc['id_can']; ?>">
                            <button type="submit" id="<?php echo $convoc['id_can']; ?>" class="btn btn-link">Convoquer</button>
                        </form>
                    </div>
                    <div class="col-md-2 ">
                        <form method="post" action="./send_mail.php" target="_blank">
                            <input type="hidden" name="id_can" value="<?php echo $convoc['id_can']; ?>">
                            <button type="submit" id="<?php echo $convoc['id_can']; ?>" class="btn btn-link glyphicon glyphicon-envelope"></button>
                        </form>
                    </div>
<!--                </label>-->
            </div>
            <?php
        }
    } else {
        echo '<div class="col-md-12 text-center" style="font-size: 30px;">Aucunes convocations trouvées</div>';
    }
    ?>

</div>

</div>
</div>
</body>
</html>

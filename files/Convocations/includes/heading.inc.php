<?php
setlocale(LC_TIME, 'fr', 'fr_FR', 'fr_FR.ISO8859-1');
$convocations = new Convocations();

require(WAY . "includes/head.inc.php");
?>

<div class="panel panel-primary">
    <div class="panel-heading">
        <a href="./">Convocations</a>
        <div class="dropdown">
            <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                Groupes
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <?php
                foreach ($convocations->get_all_nom_grp() AS $grp) {
                    echo '<li><a onclick="$(this).children().submit()"><form action="./convocations.php" method="post"><input type="hidden" name="grp" value="' . $grp['id_grp'] . '" /></form>' . $grp['nom_grp'] . '</a></li>';
                }
                ?>
            </ul>
        </div>
        <div class="dropdown">
            <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                Classes
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <?php

                foreach ($convocations->get_all_nom_cla() AS $cla) {
                    $classe = new Classe($cla['id_cla']);
                    $groupe = new Groupe($classe->get_ref_grp());
                    $formation_name = $groupe->get_nom_grp();



                    echo '<li><a onclick="$(this).children().submit()"><form action="./convocations.php" method="post"><input type="hidden" name="cla" value="' . $cla['id_cla'] . '" /></form>' . $formation_name . ' - ' . $cla['nom_cla'] . '</a></li>';
                }


                ?>
            </ul>
        </div>
    </div>
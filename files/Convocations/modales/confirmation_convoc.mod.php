<!-- Fenêtre Modale liée au choix de la classe -->
<div class="modal fade" id="modal_conf_convoc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h2 class="branche">Confirmation des convocations aux examens</h2>
            </div>
            <div class="modal-body">
                <h4>En attente</h4>
                <table class="table" id="tab_waiting">
                    <tr>
                    </tr>
                </table>
                <br>
                <h4>Confirmé</h4>
                <table class="table" id="tab_confirmed">
                    <tr>
                       <th>Nom</th>
                       <th>Prénom</th>
                       <th>Classe</th>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>




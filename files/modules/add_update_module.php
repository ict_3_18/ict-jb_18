<?php
    session_start();
    $aut = "ADM_USR"; 
    require_once("./../config/config.inc.php");
    require_once(WAY . "./includes/secure.inc.php");
    require_once( WAY ."/includes/autoload.inc.php");
    require_once( WAY ."/includes/head.inc.php");
    
    if(isset($_POST["id_desc_mod"])){ //Si on a recu un module 
        $mod = new Module($_POST["id_desc_mod"]); 
    }
?>

           <div class="row">
             <div class="header">
                <h3>Module</h3>
             </div>
           </div>   
          
          <div class="panel panel-primary">
             <div class="panel-heading">
                Ajout d'un module
             </div>
          
             <div class="panel-body">
                <form id="add_update_module_form" action="check.php" method="post">
                    
                   <!-- Hidden avec id si setté -->
                   <?php
                        if(isset($mod)){
                           echo "<input type=\"hidden\" name=\"id_desc_mod\" id=\"id_desc_mod\" value=\"".$_POST["id_desc_mod"]."\">";  
                        }
                   ?>
                   
                   <!-- Nom -->
                   <div class="form-group row">
                      <label for="nom_mod" class="col-sm-1 col-form-label">Nom</label>
                      <div class="col-sm-11">
                          <input type="text" class="form-control" id="nom_mod" name="nom_mod" 
                                 placeholder="Nom du module" value="<?php echo (isset($mod)) ? $mod->get_nom() : ''; ?>">
                      </div>
                   </div>
                   
                   
                   <!-- Numero -->
                   <div class="form-group row">
                      <label for="num_mod" class="col-sm-1 col-form-label">Numero</label>
                      <div class="col-sm-11">
                         <input type="text" class="form-control" id="num_mod" name="num_mod" 
                                placeholder="117, 226A, ..." value="<?php echo (isset($mod)) ? $mod->get_num() : ''; ?>">
                      </div>
                   </div>
                   
                   
                   <!-- description -->
                   <div class="form-group row">
                      <label for="description_mod" class="col-sm-1 col-form-label">Description</label>
                      <div class="col-sm-11">
                          <input type="text" class="form-control" id="description_mod" name="description_mod" 
                                 placeholder="Description du module" value="<?php echo (isset($mod)) ? $mod->get_description() : ''; ?>">
                      </div>
                   </div>
                   
                   
                   <!-- lien -->
                   <div class="form-group row">
                      <label for="lien_desc" class="col-sm-1 col-form-label">Lien</label>
                      <div class="col-sm-11">
                          <input type="text" class="form-control" id="lien_desc" name="lien_desc" 
                                 placeholder="http://" value="<?php echo (isset($mod)) ? $mod->get_lien_desc() : ''; ?>">
                      </div>
                   </div>
                   
                   
                   <!-- Competence -->
                   <div class="form-group row">
                      <label for="competence_mod" class="col-sm-1 col-form-label">Compétence</label>
                      <div class="col-sm-11">
                          <input type="text" class="form-control" id="competence_mod" name="competence_mod" 
                                 placeholder="Competence du module" value="<?php echo (isset($mod)) ? $mod->get_competence() : ''; ?>">
                      </div>
                   </div>
                   
                   <!-- Objectifs 1 à 7 -->
                   <?php
                    $num_obj = 1 ; 
                    while($num_obj <= 7){ //Parcours les objectifs 
                        
                        $value_objectif = "" ; //Valeur par défaut de l'objectif
                        if(isset($mod)){ //Regarde si on a un module 
                            $tab_objectifs = $mod->get_objectifs(); //Tableau des 7 objectifs 
                            if($tab_objectifs[$num_obj] != $value_objectif){ //Si l'objectif n'est pas vide 
                                $value_objectif = trim($tab_objectifs[$num_obj]); //Remplace la value par l'objectif 
                            }
                        }
                        
                        echo "<div class=\"form-group row\">" ; 
                            echo "<label for=\"obj_".$num_obj."\" class=\"col-sm-1 col-form-label\">Objectif ".$num_obj."</label>"; 
                            echo "<div class=\"col-sm-11\">"; 
                                 echo "<input type=\"text\" class=\"form-control\" id=\"obj_".$num_obj."\" value=\"".$value_objectif."\" "
                                         . " name=\"obj_".$num_obj."\" placeholder=\"Objectif ".$num_obj."\">"; 
                            echo "</div>"; 
                        echo "</div>"; 
                        
                        $num_obj++;
                    }
                   ?>        
                   
                  <!-- Bouton submit et reset -->
                  <div class="form-group row">
                     <div class="col-sm-offset-8 col-sm-2">
                         <input type="submit" class="form-control btn btn-primary submit" id="submit_conf" 
                                value="<?php echo (isset($mod)) ? 'Modifier' : 'Ajouter'; ?>">
                     </div>
                     <div class="col-sm-2">
                         
                         <?php
                         if(isset($mod)){
                             //Affiche un lien pour revenir sur l'index 
                            echo "<a href=\"index.php\" class=\"form-control btn btn-warning\">Retour</a>";     
                         }else{
                             //Affiche le bouton reset 
                             echo "<input type=\"reset\" class=\"form-control btn btn-warning\" id=\"reset_conf\" value=\"Annuler\">";
                         }
                         ?>
                         
                     </div>
                  </div>
                  
               </form>
               
            </div>
             
            <div class="panel-footer">
         
            </div>
              
          </div>

          <script src="./js/add_update_module.js"></script>
          
    </body>
</html>
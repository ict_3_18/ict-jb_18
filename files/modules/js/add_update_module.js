$(function(){
   $("#add_update_module_form").validate(
      {
         rules: {
            nom_mod:{
               required: true,
            },
            num_mod:{
               required: true,
            },
            description_mod:{
               required: true,
               minlength:20,
            },
            lien_desc:{
               required: true,
               url:true, 
            },
            competence_mod:{
               required: true,
            },
         },
         messages:{
            nom_mod:{
               required: "Merci d'entrer un nom de module",
            },
            num_mod:{
               required: "Merci d'entrer un numero de module",
            },
            description_mod:{
               required: "Merci d'entrer une description pour ce module",
               minlength:"Description trop courte", 
            },
            lien_desc:{
               required: "Merci d'entrer un lien pour la description pour ce module",
               url: "Merci d'entrer un URL valide ex: https://www.google.ch",
            },
            competence_mod:{
               required: "Merci d'entrer une compétence pour ce module",
            },
         },
         submitHandler: function(form) {
            console.log("module envoyé");
            
            var tab_post = {}; // != new Array() !!!
            for(var i = 1 ; i <= 7 ; i++){
                //Remplit le tableau avec les objectifs 
                tab_post["obj_"+i] = $("#obj_"+i).val(); 
            }
            
            //Remplit le tableau le reste des input
            tab_post["nom_mod"] = $("#nom_mod").val(); 
            tab_post["num_mod"] = $("#num_mod").val(); 
            tab_post["description_mod"] = $("#description_mod").val(); 
            tab_post["lien_desc"] = $("#lien_desc").val(); 
            tab_post["competence_mod"] = $("#competence_mod").val(); 

            //Envoie le tableau en POST
            if($("#id_desc_mod").length && $("#id_desc_mod").val().length){ //Si le input existe et que il a une value
                
                tab_post["id_desc_mod"] = $("#id_desc_mod").val();//ajoute l'id du module dans le tableau 
                
                $.post("./json/update_module.json.php?_="+Date.now(),tab_post,               
                function result(data, status) {
                    if(data.message){
                        console.log(data.message.texte);
                        message(data.message.texte,data.message.type);
                    }
                    if(data.reponse){
                        //window.location.assign("../examens/liste_examen.php");
                    }
                },
                'json' 
                );
                
            }else{
                
                $.post("./json/add_module.json.php?_="+Date.now(),tab_post,               
                function result(data, status) {
                    if(data.message){
                        console.log(data.message.texte);
                        message(data.message.texte,data.message.type);
                    }
                    if(data.reponse){
                        //window.location.assign("../examens/liste_examen.php");
                    }
                },
                'json' 
                );
                
            }
            
         }
      }
   );
});
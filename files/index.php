<?php
$currentCookieParams = session_get_cookie_params();
session_set_cookie_params(
        0, //expires at end of session  
        $currentCookieParams['path'], //path  
        $currentCookieParams['domain'], //domain  
        $secure = true, // secure
        $httponly = true // httponly
);
session_start();
$aut = "USR_SIT";
require("./config/config.inc.php");
require_once(WAY . "/includes/secure.inc.php");
require_once(WAY . "/includes/head.inc.php");
//require_once(WAY . "/includes/autoload.inc.php");


$exa = new Examen();
$mod = new Module();
$per = new Personne($_SESSION['id']);

$nomId = $per->get_nom();
$prenomId = $per->get_prenom();
?>

<div class="row" id="principal">
    <div class="header">
        <h3>Bienvenue, <?= $prenomId ?></h3>
    </div>
    <hr>
    <h4>Voici la liste des modules où tu es impliqué(e) en tant qu'enseignant(e) et/ou expert(e)</h4>
    <hr>
    <?php
    $tab_exa = $exa->list_exam();
    $data_lines = array();
    ?>
    <div class="col-md-12">
        <div class="panel panel-primary ">
            <div class="panel-heading">
                Liste des examens
            </div>
            <div class="panel-body" id="list_exa">




            </div>
        </div>    
    </div>
</div>

<script src="./js/index.js"></script>
<script src="./Rapport/js/homepage.js"></script>
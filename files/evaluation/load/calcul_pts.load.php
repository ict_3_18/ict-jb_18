<?php
session_start();
require("./../../config/config.inc.php");
//require(WAY . "/includes/secure.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

?>
<div class="row">
    <div class="form-group col-md-6">
        <label for="nb_pts_max">Nb de points maximum possible</label> 
        <input type="number" step="3" class="form-control" id="nb_pts_max" name="nb_pts_max" placeholder="Nb de pts max">
    </div>
</div>
<div class="row">
    <div class="form-group col-md-6">
        <label for="nb_pts">Nb de points obtenus</label> 
        <input type="number" class="form-control" id="nb_pts" name="nb_pts" placeholder="Nb de pts">
    </div>
</div>
<div class="row">
    <div class="form-group col-md-2">
        <button type="submit" class="btn btn-primary">Calcul</button>
    </div>
</div>
<div class="row">
    <div class="form-group col-md-6">
        <label for="nb_pts">Note</label> 
        <span  id="note" name="note" ></span>
    </div>
</div>   

<?php
session_start();
require("./../../config/config.inc.php");
//$aut = "ADM_TPI";
//require(WAY . "/includes/secure.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

//print_r($_POST);
//print_r($_SESSION['ict']['criteres']);

$crt = new Critere();
if(isset($_POST['num_crit'])){
    $_POST['num_crit'] = str_replace(",",".",$_POST['num_crit']);
}
if (isset($_POST['key_crit'])){
   // $_POST['num_crit'] = $_SESSION['ict']['criteres'][$_POST['key_crit']];
    $key_crit = $_POST['key_crit'];
    $_POST['num_crit'] = $_POST['key_crit'];
    $crt->init_by_num($_SESSION['ict']['criteres'][$key_crit]);
    $_POST['num_crit'] = "1.".$crt->get_code();

}else{
    $crt->init_by_num($_POST['num_crit']);
    $key_crit = array_search($_POST['num_crit'],$_SESSION['ict']['criteres']);
}

if ($crt->init_by_num($_POST['num_crit'])){

   // echo $key_crit = array_search(strval($_POST['num_crit']),$_SESSION['ict']['criteres']);
    if($key_crit == 0){
        $prev = sizeof($_SESSION['ict']['criteres'])-1;
    }else{
        $prev = $key_crit-1;
    }
    if($key_crit == sizeof($_SESSION['ict']['criteres'])-1){
        $next = 0;
    }else{
        $next = $key_crit+1;
    }

    if($_POST['num_crit'] < 100){
?>
<table class="table">
    <tr>
        <td class="col-sm-2">
            <button type="button" class="btn btn-primary previous" key_crit="<?php echo $prev; ?>"><span class="glyphicon glyphicon-arrow-left"></span> <?php echo $_SESSION['ict']['criteres'][$prev]; ?></button>
        </td>
        <td class="col-sm-8">
        </td>
        <td class="col-sm-2">
            <button type="button" class="btn btn-primary next pull-right" key_crit="<?php echo $next; ?>"><?php echo $_SESSION['ict']['criteres'][$next]; ?> <span class="glyphicon glyphicon-arrow-right"></span></button>
        </td>
    </tr>
</table>
<?php 
}
?>

<table class="table table-bordered">
    <tr class="titre">
        <th colspan="2">
        	<?php 
            if($_POST['num_crit'] > 100){
				$thm = new Theme($crt->get_id_thm());
                echo "<button class=\"btn btn-primary crit_gen\">".$thm->get_nom()."</button> ";
            }
            echo $_POST['num_crit']." ".$crt->get_nom(); 
            ?>
            
        </th>
    </tr>

    <tr class="description">
        <td colspan="2"><?php echo $crt->get_description(); ?></td>
    </tr>
    <tr class="pt3">
        <td>3&nbsp;points</td>
        <td><?php echo $crt->get_pt_3(); ?></td>
    </tr>
    <tr class="pt2">
        <td>2&nbsp;points</td>
        <td><?php echo $crt->get_pt_2(); ?></td>
    </tr>
    <tr class="pt1">
        <td>1&nbsp;point</td>
        <td><?php echo $crt->get_pt_1(); ?></td>
    </tr>
    <tr class="pt0">
        <td>0&nbsp;point</td>
        <td><?php echo $crt->get_pt_0(); ?></td>
    </tr>
</table>

<?php 
if($_POST['num_crit'] < 100){
?>

<table class="table">
    <tr>
        <td class="col-sm-2">
            <button type="button" class="btn btn-primary previous" key_crit="<?php echo $prev; ?>"><span class="glyphicon glyphicon-arrow-left"></span> <?php echo $_SESSION['ict']['criteres'][$prev]; ?></button>
        </td>
        <td class="col-sm-8">
        </td>
        <td class="col-sm-2">
            <button type="button" class="btn btn-primary next pull-right" key_crit="<?php echo $next; ?>"><?php echo $_SESSION['ict']['criteres'][$next]; ?> <span class="glyphicon glyphicon-arrow-right"></span></button>
        </td>
    </tr>
</table>
<?php
}
}else{
    ?>
    
    <p>Il n'y a pas de critère correspondant à ce numéro (<? echo $_POST['key_crit']; ?>)</p>
    
    <?php
}
?>
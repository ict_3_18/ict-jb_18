<?php
session_start();
require("./../../config/config.inc.php");
//require(WAY . "/includes/secure.inc.php");
require_once(WAY . "/includes/autoload.inc.php");
//require_once(WAY . "/includes/head.inc.php");

//print_r($_POST);

if($_POST['num_crit'] == 0){
    $thm = new Theme();
    $tab_thm = $thm->get_all();
    echo "<form class=\"form-inline\">";
        echo "<div class=\"form-group\">";
            echo "<label class=\"control-label col-md-3\" for\"id_thm\"=>Filtrer par :</label>";
            echo "<div class=\"col-md-6\">";
                echo "<select class=\"form-control\" id=\"id_thm\">";
                echo "<option value=\"0\">Choisir un thème</option>";
                foreach($tab_thm AS $theme){
                    echo "<option value=\"".$theme['id_thm']."\">".$theme['nom_thm']."</option>";
                }
                echo "</select>";
             echo "</div>";
        echo "</div>";
    echo "</form>";
    
    $crt = New Critere();
    //$tab_crt_by_thm = $crt->get_all_specifiquesby_thm();
    //print_r($tab_crt[0]);
    
    foreach($tab_thm AS $theme){
        echo "<table class=\"table table-bordered table-stripped theme thm_".$theme['id_thm']."\">";
        $tab_crt_by_thm = $crt->get_all_specifiques_by_thm($theme['id_thm']);
        foreach($tab_crt_by_thm AS $crit){
            ?>
            <tr class="titre critere_spec critere thm_<?= $crit['id_thm'] ?>">
                <th><?= $crit['code_crt'] ?></th>
                <th><?= $crit['nom_crt'] ?></th>
                <th class="basket_theme nom_theme">
                	<button class="btn btn-default btn_basket btn_crt_<?= $crit['id_crt'] ?>
                	<?php 
                	if(in_array($crit['id_crt'],$_SESSION['basket'])){
                	    echo " btn-success ";
                	}
                	?>
                	" id_crt="<?= $crit['id_crt'] ?>"><span class="glyphicon glyphicon-shopping-cart text-default"></span></button>
                </th>
            	<th class="nom_theme"><?= $crit['nom_thm'] ?></th>
            <tr>
            <tbody class="critere_spec">
                <tr class="description critere_spec thm_<?= $crit['id_thm'] ?>">
                	<td colspan="4"><?= str_replace(chr(10),"<br>",$crit['description_crt'])?></td>
            	</tr>
            	<tr class="pt3 critere_spec thm_<?= $crit['id_thm'] ?>">
                	<td>3&nbsp;points</td>
                	<td colspan="4"><?= str_replace(chr(10),"<br>",$crit['pt_3'])?></td>
            	</tr>
                <tr class="pt2 critere_spec thm_<?= $crit['id_thm'] ?>">
                    <td>2&nbsp;points</td>
                	<td colspan="4"><?= str_replace(chr(10),"<br>",$crit['pt_2'])?></td>
                </tr>
                <tr class="pt1 critere_spec thm_<?= $crit['id_thm'] ?>">
                    <td>1&nbsp;points</td>
                	<td colspan="4"><?= str_replace(chr(10),"<br>",$crit['pt_1'])?></td>
                </tr>
                <tr class="pt0 critere_spec thm_<?= $crit['id_thm'] ?>">
                    <td>0&nbsp;point</td>
                	<td colspan="4"><?= str_replace(chr(10),"<br>",$crit['pt_0'])?></td>
                </tr>
            </tbody>    
            <?php
        }
        echo "</table>";
    }
}
?>
<script src="./js/basket.js"></script>
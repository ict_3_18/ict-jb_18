<nav class="navbar navbar-default">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="navbar-brand" href="index.php">Evaluation TPI</a>
		</div>
    	<div class="navbar-form navbar-left">
       
        	<div class="col-md-3">
            	<div class="input-group">
              		<input type="text" class="form-control" id="num_crit" name="num_crit" placeholder="N° du critère">
              		<span class="input-group-btn">
                    	<button type="submit" class="btn btn-default" type="button">
                      		<span class="glyphicon glyphicon-search"></span>
                    	</button>
              		</span>
            	</div>
          	</div>
        	<div class="col-md-3">
            	<div class="input-group">
                	<button class="btn btn-success" id="crt_lst">Critères spécifiques</button>
                </div>
        	</div>
        	<div class="col-md-3">
        		<div class="input-group">
        			<button class="btn btn-primary" id="calc_note_par">Calcul note partielle</button>
        		</div>
        	</div>
        	<div class="col-md-3">
                <div class="input-group">
        			<button class="btn btn-primary" id="calc_note_fin">Calcul note finale</button>
                </div>
        	</div>
        </div>
        <div class="navbar-form navbar-right">
			<div class="col-md-2">
				<div class="input-group">
            		<button class="btn btn-default" id="btn_basket">
            			<span class="glyphicon glyphicon-shopping-cart text-default"></span>
            			<span id="nb_crt_basket"><?php if(isset($_SESSION['basket'])){sizeof($_SESSION['basket']);}?></span>
            		</button>
				</div>
			</div>
		</div>
	</div>
</nav>
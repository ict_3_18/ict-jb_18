	<?php
session_start();
//session_destroy();
require("./../config/config.inc.php");

//require(WAY . "/includes/secure.inc.php");
require_once(WAY . "/includes/autoload.inc.php");
    $per = new Personne();
require_once(WAY . "/includes/head.inc.php");
require("./includes/navbar.inc.php");


$crt_list = new Critere();
$tab_crt = $crt_list->get_all_criteres();
//


if(!isset($_SESSION['ict']['criteres'])){
    $_SESSION['ict']['criteres'] = $tab_crt;
}

//int_r($_SESSION);
//ho $_POST['num_crit'];
?>

<div id="critere" class="critere container">
    <div class="col-md-12">
        <h1>Informations relatives à l'appréciation du TPI</h1>
        <h3>TOUS LES TERMES UTILISES DOIVENT ETRE COMPRIS DANS LEUR SENS EPICENE.</h3>
        
        <table class="table table-bordered">
            <tr>
                <td>Appréciation : 	</td>
                <td>Le niveau de compétence lié au critère (0-3 points par critère).</td>
            </tr>
            <tr>
                <td>Critères de détail :</td>
                <td>Selon l'énoncé du travail, ces critères peuvent être complétés ou non-évalués (biffer le critère non-retenu) </td>
            </tr>
            <tr>
                <td>Attribution des notes :	</td>
                <td>Selon le barème fédéral, seules les notes entières et les demi-notes sont admises.</td>
            </tr>
            <tr>
                <td>Document à rendre :</td>
                <td>Le document d'évaluation à rendre au chef expert est le « Formulaire d’évaluation de TPI BEJUNE 2014 » complété et signé. </td>
            </tr>
            <tr>
                <td>Confidentialité :</td>
                <td>La communication aux candidats ou à des tiers d'informations concernant le résultat de toutes ou parties de la procédure de qualification est strictement interdite. Seul le service cantonal responsable des procédures de qualification est habilité à transmettre les résultats.</td>
            </tr>
        </table>
        <table class="table table-bordered">
            <tr class="titre">
                <th>Appréciation</th><th>Critère(s)</th><th>Compétence</th><th>Point(s)</th>
            </tr>
            <tr class="pt3">
                <td>Très bon, quantitativement et qualitativement</td>
                <td>Complètement rempli(s)</td>
                <td>Acquise</td>
                <td>3</td>
        	</tr>
        	<tr class="pt2">
                <td>Suffisant - Bon</td>
                <td>Majoritairement rempli(s)</td>
                <td>Acquise</td>
                <td>2</td>
        	</tr>
            <tr class="pt1">
                <td>Insuffisant</td>
                <td>Partiellement rempli(s)</td>
                <td>Non-acquise</td>
                <td>1</td>
        	</tr>
        	 <tr class="pt0">
                <td>Inutilisable ou non exécuté</td>
                <td>Pas ou très peu rempli(s)</td>
                <td>Non-acquise</td>
                <td>0</td>
        	</tr>
        </table>
        <table class="table">
            <tr>
                <td>
                    <button type="button" class="btn btn-primary previous" key_crit="<?= sizeof($_SESSION['ict']['criteres'])-1;?>">
                        <?= $_SESSION['ict']['criteres'][sizeof($_SESSION['ict']['criteres'])-1];?> <span class="glyphicon glyphicon-arrow-left"></span>
                    </button>
                </td>
                <td>
                    <button type="button" class="btn btn-primary next pull-right" key_crit="1">
                        <span class="glyphicon glyphicon-arrow-right"> <?= $_SESSION['ict']['criteres'][0];?></span>
                    </button>
                    
                </td>
            </tr>
        </table>
    </div>
</div>
<script src="<?php echo URL;?>/evaluation/js/evaluation.js"></script>
</body>
</html>
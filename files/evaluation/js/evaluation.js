$(function(){
   
   $("#submit_crit").click(function(){
      
      //console.log("critère");
      $("#critere").load("./load/critere.load.php",{num_crit:$("#num_crit").val()});
      $("#num_crit").val("");
       
   });


   
   $("#crt_lst").click(function(){
	      
      //console.log("critère");
      $("#critere").load("./load/criteres.load.php",{num_crit:0});
      $("#num_crit").val("");
       
   });
   
   
   $("#btn_basket").click(function(){
	   $("#critere").load("./load/basket.load.php");
   });

   
   $("#num_crit").change(function(){
      
      //console.log("critère");
      $("#critere").load("./load/critere.load.php",{num_crit:$("#num_crit").val()});
      $("#num_crit").val("");  
   });
   
   $("#critere").on("click",".next",function(){
      $("#critere").load("./load/critere.load.php",{key_crit:$(".next").attr("key_crit")});
   });
   
   $("#critere").on("click",".previous",function(){
      $("#critere").load("./load/critere.load.php",{key_crit:$(".previous").attr("key_crit")});
   });
   
   
   $("#calc_note_par").click(function(){
      $("#critere").load("./load/calcul_pts.load.php");
   });
   
   $("#critere").on("change", "#nb_pts_max, #nb_pts", function(){
        var nb_pts_max = $("#nb_pts_max").val();
        var nb_pts = $("#nb_pts").val();
        var note = 5/nb_pts_max*nb_pts+1;
        var note_cent = Math.round(note*100)/100;
        var note_demi = Math.round(note_cent*2)/2;
        $("#note").html(note_demi+" ("+note_cent+")");
    });
   
   
    $("#calc_note_fin").click(function(){
      $("#critere").load("./load/calcul_note.load.php");
   });
   
   $("#critere").on("change","#comp_prof, #doc, #pres", function(){
      //console.log("test");
        var comp_prof = $("#comp_prof").val();
        var doc = $("#doc").val();
        var pres = $("#pres").val();
        
        var note = Math.round((((comp_prof*1+comp_prof*1+doc*1+pres*1)/4) * 10))/10;
        $("#note").html("("+comp_prof+" + "+comp_prof+" + "+doc+" + "+pres+") / 4 = "+ note);
    });
   
  
   
   /*$(".critere").on("click", 
      function swipeHandler( event ){
         console.log("click");
         window.alert("click");
    //$( event.target ).addClass( "swipe" );
      }
   );*/
   
 /*  $(".critere").on("swipe", 
      function swipeHandler( event ){
         console.log("swipe");
         window.alert("swipe");
    //$( event.target ).addClass( "swipe" );
      }
   );
   
   */
   /*
   
    // Bind the swipeHandler callback function to the swipe event on div.box
  $( "div.box" ).on( "swipe", swipeHandler );
 
  // Callback function references the event target and adds the 'swipe' class to it
  function swipeHandler( event ){
     console.log("swipe");
    $( event.target ).addClass( "swipe" );
  }
   */
   
});
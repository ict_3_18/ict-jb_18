$(function(){
	
	$("#id_thm").change(function(){
		//console.log($("#id_thm").val());
		if($("#id_thm").val() != 0){
			$("table.theme").css("display","none");
			$("table.thm_"+$("#id_thm").val()).css("display","block");
		}else{
			$("table.theme").css("display","block");
		}
	});

	
	
	
	$(".btn_basket").click(function(){
		//console.log($(this).attr("id_crt"));
		if($(this).hasClass("btn-success")){
			$.post(
					"./json/del_crt_to_basket.json.php",
					{
						id_crt:$(this).attr("id_crt")
					},
					function del_crt(data,status){
						$(".btn_crt_"+data.del_crt).removeClass("btn-success");
						refresh_basket(data,status);	
					}
					
			);
		}else{
			$.post(	
					"./json/add_crt_to_basket.json.php",
					{
						id_crt:$(this).attr("id_crt")
					},
					function add_crt(data,status){
						$(".btn_crt_"+data.add_crt).addClass("btn-success");
						refresh_basket(data,status);	
					}
			);
		}
	});

	function refresh_basket(data,status){
		$("#nb_crt_basket").html(" ("+data.nb_crt+")");
		//console.log("refresh");
	}

	
	
	
	$("#send_mail_crt").click(function(){
		$.post(
				"./json/send_mail_crt.json.php"
		);
	});
	
	$(".del_crt").click(function(){
		$.post(
				"./json/del_crt_to_basket.json.php",
				{
					id_crt:$(this).attr("id_crt")
				},
				function del_crt(data,status){
					$("#tr_crt_"+data.del_crt).remove();
					$(".btn_crt_"+data.add_crt).removeClass("btn-success");
					refresh_basket(data,status);	
				}
				
		);
	});
	
});
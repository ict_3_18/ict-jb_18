$(function () {

    $(".convoquer").click(function () {
        $val = 0;
        if ($(this).is(':checked'))
            $val = 1;
        $.post(
                "./json/index.json.php?_=" + Date.now(),
                {
                    id_exa: $(this).attr("id"),
                    val: $val
                },
                function result(data, status) {
                    if (data) {
                        window.location.reload();
                    }
                }
        );
    });
});
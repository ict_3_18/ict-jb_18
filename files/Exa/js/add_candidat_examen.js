$(function(){   
    
    $(".select_can").change(function(){ //Quand un des select change 
        switch($(this).attr("id")){
            case "ref_grp": //On a selectionné un groupe 
                    // Affiche uniquement les classes de ce groupe 
                    if(!$(this).val() || $("#ref_cla option:selected").val() !== $(this).val()){//Si on a selectionné 'selectionne qqch ' ou
                                                                                               //Si ref_cla est deja selectionné avec un groupe différent de celui que on select
                        $("#ref_cla").append("<option value='' selected >Selectionne qqch...</option>"); 
                        $("#ref_can").append("<option value='' selected >Selectionne qqch...</option>");
                    }
                    $("#ref_cla option").hide(); //Cache toutes les classes 
                    $("#ref_cla option[id_grp='"+$(this).val()+"']").show(); //Affiche les classes qui sont dans le groupe 
                    $("#ref_can option").hide(); //Cache tout les candidats 
                    $("#ref_cla option").each(function(){ //Pour toutes les classes 
                        if($(this).css('display') !== 'none'){ //visible --> qui appartiennet au groupe  // ne fonctionne pas avec :visible 
                            $("#ref_can option[id_cla='"+$(this).val()+"']").show();//Affiche tout les candidats qui sont dans les classes qui sont visible qui appartiennet donc au groupe 
                        }
                    });
                break;
            case "ref_cla": //On a selectionné une classe 
                    if(!$(this).val() || $("#ref_can option:selected").val() !== $(this).val()){//Si on a selectionné 'selectionne qqch ' ou
                                                                                               //Si ref_can est deja selectionné avec une classe différent de la classe que on select
                        $("#ref_can").append("<option value='' selected >Selectionne qqch...</option>");
                    }
                    var id_grp = $("option:selected",this).attr("id_grp"); //Id du groupe greffé sur le option de la classe 
                    $("#ref_grp").val(id_grp);//selectionne le groupe associé a la classe 
                    // Affiche uniquement les candidats de cette classe
                    $("#ref_can option").hide(); //Cache tout les candidats 
                    $("#ref_can option[id_cla='"+$(this).val()+"']").show();//Affiche les candidats qui sont dans la classe 
                                                                            //ref_can est greffé sur l'option de la classe  
                break;
            case "ref_can": //On a selectionné un candidat 
                    var id_cla = $("option:selected",this).attr("id_cla"); //prends l'id de la classe greffé sur l'option du candidat 
                    $("#ref_cla").val(id_cla);//Selectionne la classe apartenant au candidat 
                    var id_grp = $("#ref_cla option:selected").attr("id_grp");//prends l'id du groupe greffé sur l'option de la classe 
                    $("#ref_grp").val(id_grp); //selectionne le groupe associé à la classe 
                break;
        }
    });

    //Cache la ligne d'un candidat qui a été ajouté dynamiquement 
    function remove_row_can(id_can){
        $(".remove_can_exa").each(function(){ //Parcours toutes les classes remove_can_exa du document 
            if($(this).attr("id_can") === id_can){
                $(this).parent().parent().remove(); //Supprime la personne supprimée de la base 
            }
        });
        $("#number_can_already_in_exa").html($("#can_in_exa tbody tr").length); //Affiche le bon nombre de candidats dans l'examen
    }

    //Id de l'examen
    var ref_exa = $("#load_list_can").attr('ref_exa');
    //LOAD : permet de générer un partie du html dans cette page avec du code sur une autre page en passant la ref de l'exa en post 
    $("#load_list_can").load("./load/list_can.load.php", {ref_exa:ref_exa});
    
    //Pour supprimer un candidat 
    $(document).on ("click", ".remove_can_exa", function () { //On doit utiliser le $(document) quand c'est des objets dynamiques   
        var ref_can = $(this).attr("id_can"); 
        $.post("./json/remove_candidat_examen.json.php?_="+Date.now(),{all_can_id:{0:ref_can}, ref_exa:ref_exa},
            function result(data, status) {
                if(data.message){
                    //console.log(data.message.texte);
                    message(data.message.texte,data.message.type);
                }
                if(data.reponse){
                    //window.location.assign("../examens/liste_examen.php");
                    remove_row_can(ref_can); 
                }
            },
            'json' 
        );
    });
    
    //Pour supprimer tous les candidats dans l'examen 
    $(document).on ("click", ".remove_all_can_exa", function () { //On doit utiliser le $(document) quand c'est des objets dynamiques   
        all_can_id = []; //Tableau avec la reference de tout les candidats 
        $(".remove_can_exa").each(function(){ //Parcours toutes les classes remove_can_exa du document 
            all_can_id.push($(this).attr("id_can"));//Push tout les id des candidats dans le tableau 
        });
        //Post avec tout les id 
        $.post("./json/remove_candidat_examen.json.php?_="+Date.now(),{all_can_id:all_can_id, ref_exa:ref_exa},
            function result(data, status) {
                if(data.message){
                    //console.log(data.message.texte);
                    message(data.message.texte,data.message.type);
                }
                if(data.reponse){
                    //window.location.assign("../examens/liste_examen.php");
                    for(var i = 0 ; i < all_can_id.length ; i++){ 
                        remove_row_can(all_can_id[i]);  //Supprime tous les candidats un par un 
                    }
                }
            },
            'json' 
        );
    });

    $(document).on ("click", "#submit_conf", function () { 
        tab_post = {}; 
        if($("#ref_grp").val()){
            tab_post["ref_exa"] = ref_exa; 
            tab_post["ref_grp"] = $("#ref_grp").val(); 
            tab_post["ref_cla"] = $("#ref_cla").val(); 
            tab_post["ref_can"] = $("#ref_can").val(); 
        }else{
            message("Sélectionnez au moins un groupe","danger"); 
        }
        
        var number_can_in_exa_before_add = $("#can_in_exa tbody tr").length; //Nombre de candidats avant l'ajout 
        //Envoie le POST avec { ref_exa:ref_exa, ref_grp:ref_grp, ref_cla:ref_cla, ref_can:ref_can }
        $.post("./json/add_candidat_examen.json.php?_="+Date.now(),tab_post,
            function result(data, status) {
                if(data.message){
                    //console.log(data.message.texte);
                    message(data.message.texte,data.message.type); 
                }
                if(data.reponse){ 
                    //window.location.assign("../examens/liste_examen.php");
                    $("#load_list_can").load("./load/list_can.load.php", {ref_exa:data.ref_exa} , function(){
                        //Call BACK after load 
                        var number_can_in_exa_now = $("#can_in_exa tbody tr").length; //Nombre de candidats après l'ajout 
                        var number_add_can = number_can_in_exa_now - number_can_in_exa_before_add; //Nombre de candidats ajoutés 
                        message(number_add_can+" candidat(s) ajouté(s)",'success');//Affiche le nombre de candidats ajoutés 
                    });   
                }
            },
            'json' 
        );

    });
}); 
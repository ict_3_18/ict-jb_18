$(function () {

    $("#retour").on("click", function () {
        location.assign("liste_examen.php");
    });

    $("#submit_exp_conf").click(function () {
        setInterval(function(){
            window.location.reload();
        },250);
        
    });

    $("#submit_ens_conf").click(function () {
        setInterval(function(){
            window.location.reload();
        },250);
    });
    
    $(".btn_del").click(function () {
        setInterval(function(){
            window.location.reload();
        },250);
    });



    $("#add_experts_examen").validate(
            {
                submitHandler: function (form) {
                    $.post(
                            "./json/add_expert_exam.json.php?_=" + Date.now(),
                            {
                                id_exa: $("#id_exa").val(),
                                exp_exa: $("#exp_exa").val()
                            },
                    function result(data, status) {
                        //Ajoute le message
                        //message(data.message.texte, data.message.type);

                        //reset le form si ok
                        if (data.reponse) {
                            //$("#submit_conf").val("Modifier TOUT");
                        }
                    }

                    );
                }


            });

    $("#add_enseignant_examen").validate(
            {
                submitHandler: function (form) {
                    $.post(
                            "./json/add_enseignant_exam.json.php?_=" + Date.now(),
                            {
                                id_exa: $("#id_exa").val(),
                                ens_exa: $("#ens_exa").val()
                            },
                    function result(data, status) {
                        //Ajoute le message
                        //message(data.message.texte, data.message.type);

                        //reset le form si ok
                        if (data.reponse) {
                            //$("#submit_conf").val("Modifier TOUT");
                        }
                    }

                    );
                }


            });
            
    $(".btn_del").click(function() {
            
            $.post(
                    "./json/remove_exp_ens_exam.json.php?_=" + Date.now(),
                    {
                        id_per: $(this).attr("id_per"), 
                        id_exa: $("#id_exa").val()
                    }
                );
                
            });
});
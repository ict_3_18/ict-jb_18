$(function () {

    var inputs = 0;

    $("#sal_exa").change(function () {
        $('#sal_exa2').show(500);
        testamount();
        console.log(inputs + " Salle selectionnée");
        $("#sal_exa2").change(function () {
            $('#sal_exa3').show(500);
            testamount();
            console.log(inputs + " Salles selectionnées");
            $("#sal_exa3").change(function () {
                $('#sal_exa4').show(500);
                testamount();
                console.log(inputs + " Salles selectionnées");
                $("#sal_exa4").change(function () {
                    $('#sal_exa5').show(500);
                    testamount();
                    console.log(inputs + " Salles selectionnées");
                    $("#sal_exa5").change(function () {
                        $('#sal_exa6').show(500);
                        testamount();
                        console.log(inputs + " Salles selectionnées");
                        $("#sal_exa6").change(function () {
                            testamount();
                            console.log(inputs + " Salles selectionnées");
                        });
                    });
                });
            });
        });
    });

    function testamount() {
        if ($("#sal_exa").is(":visible")) {
            if ($('#sal_exa').val()) {
                inputs = 1;
                if ($("#sal_exa2").is(":visible")) {
                    if ($('#sal_exa2').val()) {
                        inputs = 2;
                        if ($("#sal_exa3").is(":visible")) {
                            if ($('#sal_exa3').val()) {
                                inputs = 3;
                                if ($("#sal_exa4").is(":visible")) {
                                    if ($('#sal_exa4').val()) {
                                        inputs = 4;
                                        if ($("#sal_exa5").is(":visible")) {
                                            if ($('#sal_exa5').val()) {
                                                inputs = 5;
                                                if ($("#sal_exa6").is(":visible")) {
                                                    if ($('#sal_exa6').val()) {
                                                        inputs = 6;
                                                    } else {
                                                        inputs = 5;
                                                    }
                                                }
                                            } else {
                                                inputs = 4;
                                                $('#sal_exa6').hide(500).val("");
                                            }
                                        }
                                    } else {
                                        inputs = 3;
                                        $('#sal_exa5').hide(500).val("");
                                        $('#sal_exa6').hide(500).val("");
                                    }
                                }
                            } else {
                                inputs = 2;
                                $('#sal_exa4').hide(500).val("");
                                $('#sal_exa5').hide(500).val("");
                                $('#sal_exa6').hide(500).val("");
                            }
                        }
                    } else {
                        inputs = 1;
                        $('#sal_exa3').hide(500).val("");
                        $('#sal_exa4').hide(500).val("");
                        $('#sal_exa5').hide(500).val("");
                        $('#sal_exa6').hide(500).val("");
                    }
                }
            } else {
                inputs = 0;
                $('#sal_exa2').hide(500).val("");
                $('#sal_exa3').hide(500).val("");
                $('#sal_exa4').hide(500).val("");
                $('#sal_exa5').hide(500).val("");
                $('#sal_exa6').hide(500).val("");
            }
        }
    }

    function get_salles() {
        switch (inputs) {
            case 0 :
                var sal;
                break;
            case 1 :
                sal = new Array($("#sal_exa").val());
                break;
            case 2 :
                sal = new Array($("#sal_exa").val(), $("#sal_exa2").val());
                break;
            case 3 :
                sal = new Array($("#sal_exa").val(), $("#sal_exa2").val(), $("#sal_exa3").val());
                break;
            case 4 :
                sal = new Array($("#sal_exa").val(), $("#sal_exa2").val(), $("#sal_exa3").val(), $("#sal_exa4").val());
                break;
            case 5 :
                sal = new Array($("#sal_exa").val(), $("#sal_exa2").val(), $("#sal_exa3").val(), $("#sal_exa4").val(), $("#sal_exa5").val());
                break;
            case 6 :
                sal = new Array($("#sal_exa").val(), $("#sal_exa2").val(), $("#sal_exa3").val(), $("#sal_exa4").val(), $("#sal_exa5").val(), $("#sal_exa6").val());
                break;

        }
        return sal;
    }

    $("#remove_salle").on("click", function () {
        inputs = 0;
        $('#sal_exa').val("");
        $('#sal_exa2').hide(1000).val("");
        $('#sal_exa3').hide(1000).val("");
        $('#sal_exa4').hide(1000).val("");
        $('#sal_exa5').hide(1000).val("");
        $('#sal_exa6').hide(1000).val("");
    });

    $("#reset_conf").on("click", function () {
        if ($("#submit_conf").val() === "Modifier" || $("#submit_conf").val() === "Modifier TOUT") {
            location.reload();
        }
    })

    $("#add_examen_form").validate(
            {
                rules: {
                    num_nom_exa: {
                        required: true
                    },
                    ann_exa: {
                        required: true
                    },
                    ver_exa: {
                        required: false
                    },
                    id_dom: {
                        required: true
                    },
                    date_hrs_exa: {
                        required: true
                    },
                    dur_exa: {
                        required: false
                    }
                },
                messages: {
                    num_nom_exa: {
                        required: "Le numéro de l'examen est indispensable"
                    },
                    ann_exa: {
                        required: "L'année de l'examen est indispensable"
                    },
                    id_dom: {
                        required: "Le domaine de l'examen est indispensable"
                    },
                    date_hrs_exa: {
                        required: "La date de l'examen est indispensable"
                    }
                },
                submitHandler: function (form) {

                    if ($("#submit_conf").val() === "Ajouter") {
                        $.post(
                            "./json/add_examen.json.php?_=" + Date.now(),
                            {
                                num_nom_exa: $("#num_nom_exa").val(),
                                ann_exa: $("#ann_exa").val(),
                                ver_exa: $("#ver_exa").val(),
                                id_dom: $("#id_dom").val(),
                                date_hrs_exa: $("#date_hrs_exa").val(),
                                dur_exa: $("#dur_exa").val()

                            },
                            function result(data, status) {
                                //Ajoute le message
                                message(data.message.texte, data.message.type);

                                //reset le form si ok
                                if (data.reponse) {
                                    //$("#add_examen_form").trigger("reset");
                                    $("#submit_conf").val("Modifier");
                                    $("#first_title").text("Modifier un examen après l'ajout");
                                    $("#second_title").text("Modifier un examen");
                                    $("#div_sal").show();
                                    $("#id_exa").val(data.id);
                                }
                            }
                        );
                    } else if ($("#submit_conf").val() === "Modifier") {
                        $.post(
                            "./json/add_modify_examen.json.php?_=" + Date.now(),
                            {
                                id_exa: $("#id_exa").val(),
                                num_nom_exa: $("#num_nom_exa").val(),
                                ann_exa: $("#ann_exa").val(),
                                ver_exa: $("#ver_exa").val(),
                                id_dom: $("#id_dom").val(),
                                date_hrs_exa: $("#date_hrs_exa").val(),
                                dur_exa: $("#dur_exa").val(),
                                tmp_dys:"",
                                salles: get_salles()

                            },
                            function result(data, status) {
                                //Ajoute le message
                                message(data.message.texte, data.message.type);

                                //reset le form si ok
                                if (data.reponse) {
                                    //$("#submit_conf").val("Modifier TOUT");
                                }
                            }
                        );
                    }else if ($("#submit_conf").val() === "Modifier TOUT") {
                        $.post(
                            "./json/add_modify_examen.json.php?_=" + Date.now(),
                            {
                                id_exa: $("#id_exa").val(),
                                num_nom_exa: $("#num_nom_exa").val(),
                                ann_exa: $("#ann_exa").val(),
                                ver_exa: $("#ver_exa").val(),
                                id_dom: $("#id_dom").val(),
                                date_hrs_exa: $("#date_hrs_exa").val(),
                                dur_exa: $("#dur_exa").val(),
                                tmp_dys:"",
                                salles: get_salles()

                            },
                            function result(data, status) {
                                //Ajoute le message
                                message(data.message.texte, data.message.type);

                                //reset le form si ok
                                if (data.reponse) {

                                }
                            }
                        );
                    }
                }
            }
    );
});

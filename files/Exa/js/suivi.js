$(function () {
    $("#form_scan").validate({
        rules: {
            code: {
                required: true,
                minlength: 13,
                maxlength: 13
            }
        },
        messages: {
            code: {
                required: "Veuillez scanner le code",
                minlength: "Votre code est trop court",
                maxlength: "Votre code est trop long"
            }
        },
        submitHandler: function (form) {
            $.post(
                "./json/suivi_exa.json.php?_=" + Date.now(),
                {
                    code: $("#code").val()
                }
            );
        }
    });
});
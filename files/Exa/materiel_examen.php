<?php
session_start();
$aut = "ADM_USR";
require("./../config/config.inc.php");
require(WAY . "./includes/secure.inc.php");
require_once(WAY . "/includes/head.inc.php");
require_once(WAY . "/includes/autoload.inc.php");
$id_exa = $_POST['ref_exa'];
$exa = new Examen($id_exa);
print_r($_POST);
?>

<div class="row">
    <div class="header">
        <h3 id="first_title">Matériel a disposition</h3> 
    </div>
</div>
<!-- Parties overtes -->
<div class="col-md-12">
    <div class="panel panel-primary">
        <div class="panel-heading" id="second-title">
            Matériel disponible
        </div>
        <div class="panel-body">
            <form id="materiel_examen_form">
                <div class="form-group row">
                    <label for="aut_exa" class="col-sm-2 col-form-label">Matériel disponible</label>
                    <div class="col-sm-6">
                        <textarea type="text" id="materiel_exa" name="materiel_exa" class="form-control" ><?= $exa->get_materiel_exa()?></textarea>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-6">
                        <input type="hidden" id="from_where" value="<?php echo $_POST['from_where'];?>">
                        <?php
                        if($exa->get_lec_exa() == 1){
                            
                            echo '<input type="checkbox" class="col-sm-1" id="lec_exa" name="lec_exa" checked>';
                            
                        }else{
                            
                            echo '<input type="checkbox" class="col-sm-1" id="lec_exa" name="lec_exa">';
                            
                        }
                        ?>
                        <label for="lec_exa" class="col-sm-4 col-check-label">Données perso (R:)</label>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-6">
                        <?php
                        if($exa->get_salle_inf_exa() == 1){
                        
                            echo '<input type="checkbox" class="col-sm-1" id="salle_inf_exa" name="salle_inf_exa" checked>';
                        
                        }else{
                        
                            echo '<input type="checkbox" class="col-sm-1" id="salle_inf_exa" name="salle_inf_exa">';
                        
                        }
                        ?>
                        <label for="salle_inf_exa" class="col-sm-4 col-check-label">Salle d'informatique</label>
                    </div>

                </div>
                
                
                
                <div class="form-group row">
                    <div class="col-sm-6">
                        <?php
                        if($exa->get_reseau_exa() == 1){
                            
                            echo '<input type="checkbox" class="col-sm-1" id="reseau_exa" name="reseau_exa" checked>';
                        
                        }else{
                        
                            echo '<input type="checkbox" class="col-sm-1" id="reseau_exa" name="reseau_exa">';
                        
                        }
                        ?>
                        <label for="reseau_exa" class="col-sm-4 col-check-label">Réseau disponible</label>
                    </div>

                </div>
                
                <input type="hidden" id="id_exa" value="<?php echo $id_exa ?>">

                <div class="form-group row">
                    <div class="col-sm-offset-8 col-sm-2">
                        <input type="submit" class="form-control btn btn-primary submit" id="modify_conf" value="Modifier">
                    </div>
                    <div class="col-sm-2">
                        <input type="button" class="form-control btn btn-info" id="retour" value="Retour">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="./js/materiel_examen.js"></script>
</body>
</html>

<?php
session_start();
$aut = "ADM_USR;ADM_EXA;USR_EXA";
require("./../config/config.inc.php");
require(WAY . "./includes/secure.inc.php");
require_once(WAY . "/includes/head.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

?>
<div class="row">
    <div class="header">
        <h3>Suivis</h3>
    </div>
</div>

<div class="col-md-12">
    <div class="panel panel-primary ">
        <div class="panel-heading">
            Formulaires des suivis
        </div>
        <div class="panel-body">
            <form id="form_scan" action="" method="post">
                <div class="form-group">
                    <label for="code">Code :</label>
                    <input type="text" class="form-control" id="code" name="code" autocomplete="off">
                </div>
                <button type="submit" name='submit' class="btn btn-primary">
                    Submit
                </button>
                <button type="reset" class="btn btn-warning">
                    Reset
                </button>
            </form>
        </div>
    </div>
</div>

<?php

?>

<script src="./js/suivi.js"></script>
</body>
</html>
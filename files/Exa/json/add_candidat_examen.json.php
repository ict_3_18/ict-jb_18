<?php 
header('Content-Type: application/json');
session_start();
$aut = "ADM_USR"; 
require_once("./../../config/config.inc.php");
require_once(WAY."/includes/autoload.inc.php");
require_once(WAY . "./includes/secure.inc.php");

$cla = new Classe(); 
$can = new Candidat(); 
$grp = new Groupe(); 
$exa = new Examen();

$tab = array(); 

if(isset($_POST['ref_can']) && $_POST['ref_can'] != 0){
    $can = new Candidat($_POST['ref_can']);
    $tab_candidats[0]['id_can'] = $can->get_id();
}else if(isset($_POST['ref_cla']) && $_POST['ref_cla'] != 0){
    //Tableau des candidats associés à la classe 
    $tab_candidats = $cla->get_tab_can_one_cla($_POST['ref_cla']); 
}else if(isset($_POST['ref_grp']) && $_POST['ref_grp'] != 0){
    //Tableau des candidats associés au groupe 
    $tab_candidats = $grp->get_tab_can_one_grp($_POST['ref_grp']); 
}

//Ajoute les candidats à l'examen --> la méthode gère les doublons 
if($exa->add_candidats_examen($tab_candidats , $_POST['ref_exa'] )){
    $tab['reponse'] = true;
    $tab['message']['texte'] = "Les candidats ont ete ajoute avec succes a l'examen";
    $tab['message']['type'] = "success";
    $tab['ref_exa'] = $_POST['ref_exa']; 
}else{
    $tab['reponse'] = false;
    $tab['message']['texte'] = "Les candidats n'ont pas pu etre ajoutes a l'examen";
    $tab['message']['type'] = "danger";
}

echo json_encode($tab); 
?>
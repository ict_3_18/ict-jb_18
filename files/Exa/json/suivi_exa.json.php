<?php
header('Content-Type: application/json');
session_start();
$aut = "ADM_USR";
require("./../../config/config.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

$confirm_code = substr($_POST['code'], 0, 3);
$id_exam = substr($_POST['code'], 3, 4);
$id_arg = substr($_POST['code'], 9, 3);

print_r($_POST['code']);

$exa = new Examen($id_exam);

//$exa->scan_code($id_arg);

if($exa->check_code($confirm_code)){
    $tab['answer'] = true;
    $tab['message']['text'] = "Code securisé";
    $tab['message']['type'] = "success";
    $tab['id'] = $confirm_code;


}else{
    $tab['answer'] = false;
    $tab['message']['text'] = "Code non securisé";
    $tab['message']['type'] = "danger";
}

echo json_encode($tab);
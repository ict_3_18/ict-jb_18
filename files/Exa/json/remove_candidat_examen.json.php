<?php

header('Content-Type: application/json');
session_start();
$aut = "ADM_USR";
require_once("./../../config/config.inc.php");
require_once(WAY . "/includes/autoload.inc.php");
require_once(WAY . "./includes/secure.inc.php");

$can = new Candidat();
$exa = new Examen();

$tab = array();

if (isset($_POST['all_can_id'])) {
    //Supprime les candidats de l'examen si ils ont pas de note 
    if ($can->remove_cans_exa_if_not_note($_POST['all_can_id'], $_POST['ref_exa'])) {
        $tab['reponse'] = true;
        $tab['message']['texte'] = "Candidat(s) supprimé";
        $tab['message']['type'] = "success";
    } else {
        $tab['reponse'] = false;
        $tab['message']['texte'] = "Probleme lors de la suppression des candidats";
        $tab['message']['type'] = "danger";
    }
}else{//Aucun candidat à supprimer 
    $tab['reponse'] = false;
    $tab['message']['texte'] = "On ne peut pas supprimer des candidats avec une note";
    $tab['message']['type'] = "danger";
}

echo json_encode($tab);
?>
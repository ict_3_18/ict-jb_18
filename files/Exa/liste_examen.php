<?php
session_start();
$aut = "ADM_USR;ADM_EXA;USR_EXA";
require("./../config/config.inc.php");
require(WAY . "./includes/secure.inc.php");
require_once(WAY . "/includes/head.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

$exa = new Examen();
$mod = new Module();
$per = new Personne($_SESSION['id']);
?>
<div class="row">
    <div class="header">
        <h3>Les examens</h3>
    </div>
</div>

<!-- Parties ouvertes -->
<div class="col-md-12">
    <div class="panel panel-primary ">
        <div class="panel-heading">
            Liste des Examens
        </div>
        <div class="panel-body">

            <?php
            $tab_exa = $exa->list_exam();

            $data_lines = array();
            foreach ($tab_exa as $i => $examen) {





                //Division de la date et l'heure
                $date_debut = explode(" ", $examen['date_heure_exa'])[0];
                $date = date("d.m.y", strtotime($date_debut));

                $heure = explode(" ", $examen['date_heure_exa'])[1];
                if ($examen['duree_exa'] != null) {
                    $duree = $examen['duree_exa'];
                    $heure_debut = $exa->hrs_min_sec_to_min($heure);
                    $calc = $heure_debut + $duree;
                    $heure_fin = $exa->min_to_hrs_min_sec($calc);
                } else {
                    $heure_fin = "??:??:??";
                }

                $tab_salles = $exa->get_salles_exa($examen['id_exa']);
                $salles = "";



                foreach ($tab_salles as $sal) {
                    $salles .= $sal['nom_sle'] . " / ";
                }
                $salles = substr($salles, 0, -3);


                $profs = null;
                $tab_prof = $exa->get_profs_exam($examen['id_exa']);

                if ($tab_prof !== null) {
                    foreach ($tab_prof as $key => $prof) {
                        $profs[$key] = substr($prof['prenom_per'], 0, 1) . ". " . $prof['nom_per'];
                    }
                }


                $experts = null;
                $tab_expert = $exa->get_expert_exam($examen['id_exa']);

                if ($tab_expert !== null) {
                    foreach ($tab_expert as $key => $expert) {
                        $experts[$key] = substr($expert['prenom_per'], 0, 1) . ". " . $expert['nom_per'];
                    }
                }




                $color = $exa->get_couleur_examen($examen['id_exa']);


                //données simples afficher
                $data_lines[$i]['Module'] = $examen['no_ich_exa'] . " - " . $examen['version_ich_exa'];
                $data_lines[$i]['Date'] = $date;
                $data_lines[$i]['Hrs_db_fin'] = substr($heure, 0, 5) . " - " . substr($heure_fin, 0, 5);
                $data_lines[$i]['Candidats'] = $examen['Candidats'];
                $data_lines[$i]['Salle'] = $salles;
                $data_lines[$i]['Enseignants'] = $profs;
                $data_lines[$i]['Experts'] = $experts;

                //checkbox
                $data_lines[$i]['conv'] = $examen['convoc_exa'];

                //last one
                $data_lines[$i]['ref_exa'] = $examen['id_exa'];
                //colors
                $data_lines[$i]['couleur'] = $color;
                //background
                $data_lines[$i]['definitif_exa'] = $examen['definitif_exa'];
                $data_lines[$i]['aut'] = $per->check_aut("ADM_EXA");
            }

            /*
              echo '<pre>';
              print_r($data_lines);
              echo '</pre>';
             */
              echo build_table($data_lines);
            function build_data_rows($key2, $examen) {
                global $per;
                global $exa;
                $html = "";
                //background color
                if (!$examen['definitif_exa']) {
                    $html .= '<tr style="background-color:lightgrey; white-space:nowrap;">';
                } else {
                    $html .= '<tr style="background-color:white; white-space:nowrap;">';
                }

                //Module
                $html .= '<td>' . htmlspecialchars($examen['Module']) . '</td>';

                //Date
                $html .= '<td>' . htmlspecialchars($examen['Date']) . '</td>';

                //Hrs_db_fin
                $html .= '<td>' . htmlspecialchars($examen['Hrs_db_fin']) . '</td>';

                //Candidats
                $html .= '<td>';
                $nb_can = "";
                if (sizeof($examen['couleur']) > 1) {

                    foreach ($examen['couleur'] AS $couleur) {
                        $nb_can .= '<div class="color ';
                        $nb_can .= '" style="background-color:' . $couleur['color_grp'] . ';"'
                                . ' data-toggle="tooltip" title="' . $couleur['nb_can'] . ' candidats ' . $couleur['nom_grp'] . '" >'
                                . $couleur['nb_can'] . '</div>';
                        $nb_can .= " + ";
                    }
                    $nb_can = substr($nb_can, 0, -3);
                    $nb_can .= ' = <div class="color total" data-toggle="tooltip" title="' . $examen['Candidats'] . ' candidats" ><b>' . $examen['Candidats'] . '</b></div>';
                } else if (sizeof($examen['couleur']) == 1) {
                    $nb_can .= '<div class="color total';
                    $nb_can .= '" style="background-color:' . $examen['couleur'][0]['color_grp'] . ';"'
                            . ' data-toggle="tooltip" title="' . $examen['couleur'][0]['nb_can'] . ' candidats ' . $examen['couleur'][0]['nom_grp'] . '" >'
                            . $examen['couleur'][0]['nb_can'] . '</div>';
                }
                $html .= $nb_can;

                $html .= '</td>';

                //Salle
                $html .= '<td>' . htmlspecialchars($examen['Salle']) . '</td>';

                //Enseignants
                $html .= "<td>";
                if ($examen['Enseignants'] !== null) {
                    foreach ($examen['Enseignants'] as $prof) {
                        $html .= $prof;
                        $html .= "<br>";
                    }
                }
                $html .= "</td>";

                //Experts
                $html .= "<td>";
                if ($examen['Experts'] !== null) {
                    foreach ($examen['Experts'] as $expert) {
                        $html .= $expert;
                        $html .= "<br>";
                    }
                }
                $html .= "</td>";


                if ($examen['aut']) {

                    //Convocation
                    $html .= '<td style="text-align:center;"><button  type="" class="button_glyph">';
                    if ($examen['conv']) {
                        $html .= '<span id="icon_remove" id_exa="' . $examen['ref_exa'] . '" ';
                        $html .= 'data-toggle="tooltip" title="Retirer de la convocation" ';
                        $html .= 'class="glyphicon glyphicon-minus black remove_conv"></span>';
                    } else {
                        $html .= '<span id="icon_add" id_exa="' . $examen['ref_exa'] . '" ';
                        $html .= 'data-toggle="tooltip" title="Ajout à la convocation" ';
                        $html .= 'class="glyphicon glyphicon-plus black add_conv"></span>';
                    }
                    $html .= '</button></td>';


                    // ---- Icônes ----
                    //Page d'attributions des experts
                    $html .= '<td style="text-align:center;">';
                    $html .= '<form id="ajout_expert" action="' . URL . 'examens/experts_examen.php" method="post">';
                    //input the id
                    $html .= '<input type="hidden" name="ref_exa" id="hiddenField" value="' . $examen['ref_exa'] . '">';
                    //button to send id
                    $html .= '<button type="submit" class="button_glyph" data-toggle="tooltip" title="Ajout d\'experts ou d\'enseignants" >';
                    $html .= '<span class="glyphicon glyphicon-user black"></span>';
                    $html .= '</button>';
                    //end of form
                    $html .= '</form></td>';

                    //Page d'attributions des candidats
                    $html .= '<td style="text-align:center;">';
                    $html .= '<form id="ajout_can" action="' . URL . 'examens/add_candidat_examen.php" method="post">';
                    //input the id
                    $html .= '<input type="hidden" name="ref_exa" id="hiddenField" value="' . $examen['ref_exa'] . '">';
                    //button to send id
                    $html .= '<button type="submit" class="button_glyph" data-toggle="tooltip" title="Ajout de candidats">';
                    $html .= '<span class="glyphicon glyphicon-education black"></span>';
                    $html .= '</button>';
                    //end of form
                    $html .= '</form></td>';

                    //Page de Modification des examens
                    $html .= '<td style="text-align:center;">';
                    $html .= '<form id="modif_exa" action="' . URL . 'examens/modify_examen.php" method="post">';
                    //input the id
                    $html .= '<input type="hidden" name="ref_exa" id="hiddenField" value="' . $examen['ref_exa'] . '">';
                    //button to send id
                    $html .= '<button type="submit" class="button_glyph" data-toggle="tooltip" title="Modification de l\'examen">';
                    $html .= '<span class="glyphicon glyphicon-cog black"></span>';
                    $html .= '</button>';
                    //end of form
                    $html .= '</form></td>';

                    //Page Mat�riel dispo
                    $html .= '<td style="text-align:center;">';
                    $html .= '<form id="dispo_exa" action="' . URL . 'examens/materiel_examen.php" method="post">';
                    $exa = new Examen($examen['ref_exa']);

                    $title = "<h4>Matériel autorisé</h4>";
                    $title .= str_replace(chr(10), "<br>", $exa->get_materiel_exa());
                    if ($exa->get_salle_inf_exa() == 1) {
                        $title .= "<br><b>Salle info</b> : oui";
                    } else {
                        $title .= "<br><b>Salle info</b> : non";
                    }
                    if ($exa->get_salle_inf_exa() == 1) {
                        $title .= "<br><b>Données perso (R:)</b> : oui";
                    } else {
                        $title .= "<br><b>Données perso (R:)</b> : non";
                    }
                    if ($exa->get_reseau_exa() == 1) {
                        $title .= "<br><b>Réseau disponible</b> : oui";
                    } else {
                        $title .= "<br><b>Réseau disponible</b> : non";
                    }

                    if ($per->is_enseignant_exa($examen['ref_exa'])) {
                        $html .= '<input type="hidden" name="from_where" id="from_where" value="from_list">';
                        //input the id
                        $html .= '<input type="hidden" name="ref_exa" id="hiddenField" value="' . $examen['ref_exa'] . '">';
                        //button to send id
                        $html .= '<button type="submit" class="button_glyph" data-toggle="tooltip" data-html="true" title="' . $title . '">';

                        $html .= '<span class="glyphicon glyphicon-briefcase text-primary"></span>';
                        $html .= '</button>';
                    } else {

                        $html .= '<span class="glyphicon glyphicon-briefcase text-muted" data-toggle="tooltip" data-html="true" title="' . $title . '"></span>';
                    }

                    //Page d'ajout des notes
                     $html .= '<td style="text-align:center;">';
                     $html .= '<form id="ajout_note" action="' . URL . 'examens/add_note_candidat.php" method="post">';
                     //input the id
                     $html .= '<input type="hidden" name="id_exa" id="id_exa" value="' . $examen['ref_exa'] . '">';
                     //button to send id
                     $html .= '<button type="submit" class="button_glyph" data-toggle="tooltip" title="Ajout des notes">';
                     $html .= '<span class="glyphicon glyphicon-book black"></span>';
                     $html .= '</button>';
                     //end of form
                     $html .= '</form></td>';
                 }

                 $html .= '</tr>';
                 return $html;
             }

            /*
             * Méthode de construction du tableau
             * @return $html (String //code html pour la page)
             */

            function build_table($array) {
                // Start of the table
                $html = '<table class="table table-hover table-dark table-responsive">';

                // Header Row
                $html .= '<tr>';

                //Module
                $html .= '<th>Modules</th>';
                $html .= '<th>Dates</th>';
                $html .= '<th>Heures</th>';
                $html .= '<th>Nb Candidats</th>';
                $html .= '<th>Salles</th>';
                $html .= '<th>Enseignants</th>';
                $html .= '<th>Experts</th>';
                if ($array[0]['aut']) {
                    $html .= '<th> Conv </th>';
                    $html .= '<th> Exp </th>';
                    $html .= '<th> Can </th>';
                    $html .= '<th> Mod </th>';
                    $html .= '<th> Dispo </th>';
                     $html .= '<th> Notes </th>';
                }

                $html .= '</tr>';

                // Data Rows
               // print_r($array);
                foreach ($array as $key => $value) {
                    // foreach ($value as $key2 => $value2) {
                    //Divided cuz to many conditions
                    $html .= build_data_rows($key, $value);
                    //}

                    $html .= '</tr>';
                }

                // End of the table and returns it
                $html .= '</table>';
                return $html;
            }
            ?>


        </div>
    </div>    
</div>
</div>
<script src="./js/list_examen.js"></script>
</body>
</html>
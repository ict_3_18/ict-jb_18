<?php
session_start();
$aut = "ADM_USR";
require("./../config/config.inc.php");
require(WAY . "./includes/secure.inc.php");
require_once(WAY . "/includes/head.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

$exa = new Examen();

$mod = new Module();
$salle = new Salle();
$thisYear = date("y");

$id = $_POST['ref_exa'];

$arrExa = $exa->find_exam($id);
$tab_salles = $exa->get_salles_exa($id);

$select = ' selected="selected" ';

?>
            <div class="row">
                <div class="header">
                    <h3 id="first_title">Modification d'un examen</h3>
                </div>
            </div>
            <!-- Parties ouvertes -->
            <div class="col-md-12">
                <div class="panel panel-primary ">
                    <div class="panel-heading" id="second_title">
                        Modifier un examen
                    </div>
                    <div class="panel-body">
                        <form id="modify_examen_form">

                            <!-- Numero et nom de l'examen -->
                            <div class="form-group row">
                                <label for="num_nom_exa" class="col-sm-2 col-form-label">Numéro Examen</label>
                                <div class="col-sm-8">
                                    <select class="form-control" id="num_nom_exa" name="num_nom_exa">
                                        <option disabled value> Veuillez selectionner un numéro. </option>
                                        <?php 
                                        $options_mod = $mod->get_all("num_mod");
                                        foreach($options_mod as $opt){
                                            if($opt['num_mod'] == $arrExa['no_ich_exa']){
                                            echo "<option value=".$opt['num_mod'].$select.'>'.$opt['num_mod']." - ".$opt['nom_mod']."</option>";
                                            }
                                            echo "<option value=".$opt['num_mod'].">".$opt['num_mod']." - ".$opt['nom_mod']."</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <!-- Année -->
                            <div class="form-group row">
                                <label for="ann_exa" class="col-sm-2 col-form-label">Année de l'examen</label>
                                <div class="col-sm-4">
                                    <select class="form-control" id="ann_exa" name="ann_exa">
                                        <option disabled value> Veuillez selectionner une année. </option>
                                        <?php
                                        
                                        //Option pour l'année prochaine
                                        echo '<option value="';
                                        
                                        if($arrExa['version_ich_exa'] == $thisYear+1){
                                            echo ($thisYear+1).'"'.$select.'>';
                                        }else{
                                            echo ($thisYear+1).'">';
                                        }
                                        
                                        echo ($thisYear+1); 
                                        echo '</option>';
                                        
                                        //Option pour cette année
                                        echo '<option value="';
                                        
                                        if($arrExa['version_ich_exa'] == $thisYear){
                                            echo ($thisYear).'"'.$select.'>';
                                        }else{
                                            echo ($thisYear).'">';
                                        }
                                        
                                        echo ($thisYear); 
                                        echo '</option>';
                                        
                                        //Option pour l'année précédente
                                        echo '<option value="';
                                        
                                        if($arrExa['version_ich_exa'] == $thisYear-1){
                                            echo ($thisYear-1).'"'.$select.'>';
                                        }else{
                                            echo ($thisYear-1).'">';
                                        }
                                        
                                        echo ($thisYear-1); 
                                        echo '</option>';
                                        
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <!-- Version de l'examen -->
                            <div class="form-group row">
                                <label for="ver_exa" class="col-sm-2 col-form-label">Version de l'examen</label>
                                <div class="col-sm-4">
                                    <select class="form-control" id="ver_exa" name="ver_exa">
                                        <option disabled value> Veuillez selectionner une option. </option>
                                        <option value="" <?php if($arrExa['v_exam_ich_exa'] == ""){echo $select;}?>>Vide</option>
                                        <option value="A" <?php if($arrExa['v_exam_ich_exa'] == "A"){echo $select;}?>>Version A</option>
                                        <option value="B" <?php if($arrExa['v_exam_ich_exa'] == "B"){echo $select;}?>>Version B</option>
                                        <option value="C" <?php if($arrExa['v_exam_ich_exa'] == "C"){echo $select;}?>>Version C</option>
                                    </select>
                                </div>
                            </div>

                            <!-- Domaine de l'examen -->
                            <div class="form-group row">
                                <label for="id_dom" class="col-sm-2 col-form-label">Domaine de l'examen</label>
                                <div class="col-sm-4">
                                    <select class="form-control" id="id_dom" name="id_dom">
                                        <option disabled value> Veuillez selectionner un domaine. </option>
                                        <?php
                                        $options_dom = $exa->get_all_domaines();
                                        foreach ($options_dom as $opt) {
                                            echo '<option value="';
                                            
                                            if ($arrExa['id_dom'] == $opt['id_dom']){
                                                echo $opt["id_dom"].'"'.$select.'>';
                                            }else{
                                                echo $opt["id_dom"].'">';
                                            }
                                            
                                            echo $opt["nom_dom"];
                                            echo "</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <!-- Date et Heure -->
                            <div class="form-group row">
                                <label for="date_hrs_exa" class="col-sm-2 col-form-label">Date et heure de l'examen</label>
                                <div class="col-sm-4">
                                    <div class="input-group date">
                                        <?php
                                        $exp_date = explode(" ",$arrExa['date_heure_exa'])[0];
                                        $exp_time = explode(" ",$arrExa['date_heure_exa'])[1];
                                        ?>
                                        
                                        <input type="datetime-local" class="form-control" id="date_hrs_exa" name="date_hrs_exa" value="<?php
                                        echo $exp_date;
                                        echo 'T';
                                        echo $exp_time;
                                        ?>">
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <!-- Durée -->
                            <div class="form-group row">
                                <label for="dur_exa" class="col-sm-2 col-form-label">Durée de l'examen</label>
                                <div class="col-sm-4">
                                    <input type="number" class="form-control" id="dur_exa" name="dur_exa" placeholder="Durée en minutes" min="0" value="<?php echo $arrExa['duree_exa']?>">
                                </div>
                            </div>

                            <!-- Temps pour dyslexique -->
                            <div class="form-group row">
                                <label for="tmp_dys" class="col-sm-2 col-form-label">Temps supplémentaire pour dyslexique</label>
                                <div class="col-sm-4">
                                    <input type="number" class="form-control" id="tmp_dys" name="tmp_dys" placeholder="Durée en minutes" min="0" value="<?php echo $arrExa['temps_supp_dys_exa']?>">
                                </div>
                            </div>
                            
                            <!-- Sale -->
                            <div class="form-group row" id="div_sal">
                                <?php
                                
                                $cntSalles = count($tab_salles);
                                
                                ?>
                                <label for="sal_exa" class="col-sm-2 col-form-label">Salle de l'examen</label>
                                <div class="col-sm-4">
                                    <select class="form-control" id="sal_exa" name="sal_exa">
                                        <option disabled selected value>Veuillez selectionner une salle.</option>
                                        <?php
                                        $salles = $salle->get_nom_salles();

                                        foreach ($salles as $sal){
                                            if($sal['nom_sle'] == $tab_salles[0]['nom_sle']){
                                                echo '<option '.$select.'value="';
                                                echo $sal['id_sle'].'">';
                                                echo $sal['nom_sle'];
                                                echo "</option>";
                                            }else{
                                                echo '<option value="';
                                                echo $sal['id_sle'].'">';
                                                echo $sal['nom_sle'];
                                                echo "</option>";
                                            }
                                        }
                                        ?>   
                                    </select>
                                    <select <?php if($cntSalles < 2){echo 'style="display:none"';}?> class="form-control" id="sal_exa2" name="sal_exa">
                                        <option value="">Deuxième salle</option>
                                        <?php

                                        foreach ($salles as $sal){
                                            if($sal['nom_sle'] == $tab_salles[1]['nom_sle']){
                                                echo '<option '.$select.'value="';
                                                echo $sal['id_sle'].'">';
                                                echo $sal['nom_sle'];
                                                echo "</option>";
                                            }else{
                                                echo '<option value="';
                                                echo $sal['id_sle'].'">';
                                                echo $sal['nom_sle'];
                                                echo "</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                    <select <?php if($cntSalles < 3){echo 'style="display:none"';}?> class="form-control" id="sal_exa3" name="sal_exa">
                                        <option value="">Troisième salle</option>
                                        <?php

                                        foreach ($salles as $sal){
                                            if($sal['nom_sle'] == $tab_salles[2]['nom_sle']){
                                                echo '<option '.$select.'value="';
                                                echo $sal['id_sle'].'">';
                                                echo $sal['nom_sle'];
                                                echo "</option>";
                                            }else{
                                                echo '<option value="';
                                                echo $sal['id_sle'].'">';
                                                echo $sal['nom_sle'];
                                                echo "</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                    <select <?php if($cntSalles < 4){echo 'style="display:none"';}?> class="form-control" id="sal_exa4" name="sal_exa">
                                        <option value="">Quatrième salle</option>
                                        <?php

                                        foreach ($salles as $sal){
                                            if($sal['nom_sle'] == $tab_salles[3]['nom_sle']){
                                                echo '<option '.$select.'value="';
                                                echo $sal['id_sle'].'">';
                                                echo $sal['nom_sle'];
                                                echo "</option>";
                                            }else{
                                                echo '<option value="';
                                                echo $sal['id_sle'].'">';
                                                echo $sal['nom_sle'];
                                                echo "</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                    <select <?php if($cntSalles < 5){echo 'style="display:none"';}?> class="form-control" id="sal_exa5" name="sal_exa">
                                        <option value="">Cinquième salle</option>
                                        <?php

                                        foreach ($salles as $sal){
                                            if($sal['nom_sle'] == $tab_salles[4]['nom_sle']){
                                                echo '<option '.$select.'value="';
                                                echo $sal['id_sle'].'">';
                                                echo $sal['nom_sle'];
                                                echo "</option>";
                                            }else{
                                                echo '<option value="';
                                                echo $sal['id_sle'].'">';
                                                echo $sal['nom_sle'];
                                                echo "</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                    <select <?php if($cntSalles == 6){}else{echo 'style="display:none"';}?> class="form-control" id="sal_exa6" name="sal_exa">
                                        <option value="">Sixième salle</option>
                                        <?php

                                        foreach ($salles as $sal){
                                            if($sal['nom_sle'] == $tab_salles[5]['nom_sle']){
                                                echo '<option '.$select.'value="';
                                                echo $sal['id_sle'].'">';
                                                echo $sal['nom_sle'];
                                                echo "</option>";
                                            }else{
                                                echo '<option value="';
                                                echo $sal['id_sle'].'">';
                                                echo $sal['nom_sle'];
                                                echo "</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <a class="btn" id="remove_salle"><span class="glyphicon glyphicon-remove"></span></a>
                            </div>

                            <!-- ID pour la modification par la suite (après l'ajout) -->
                            <input type="hidden" id="id_exa" value="<?php echo $id ?>">

                            <!-- Bouton submit et reset -->
                            <div class="form-group row">
                                <div class="col-sm-offset-8 col-sm-2">
                                    <input type="submit" class="form-control btn btn-primary submit" id="submit_conf" value="Modifier">
                                </div>
                                <div class="col-sm-2">
                                    <input type="button" class="form-control btn btn-info" id="retour" value="Retour">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>    
            </div>
        </div>
        <script src="./js/modify_examen.js"></script>
    </body>
</html>
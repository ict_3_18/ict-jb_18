$(function () {


    $("#update_form").validate(
            {
                rules: {
                    nom_emp: {
                        required: true
                    },
                    adresse_emp: {
                        required: true
                    },
                    npa_emp: {
                        required: true
                    },
                    localite_emp: {
                        required: true
                    }
                },
                messages: {
                    nom_emp: {
                        required: "Veuillez saisir le nom de l'employeur"
                    },
                    adresse_emp: {
                        required: "Veuillez saisir l'adresse de l'employeur"
                    },
                    npa_emp: {
                        required: "Veuillez saisir le npa"
                    },
                    localite_emp: {
                        required: "Veuillez saisir l'adresse de l'employeur"
                    }
                },
                submitHandler: function (form) {
                    var conf_emp;
                    var payement_copies_emp;
                    var payement_envois_emp;

                    //vérifie quel bouton radio est sélectionné
                    if ($('#conf_emp').is(':checked')) {
                        conf_emp = 1;
                    } else {
                        conf_emp = 0;
                    }

                    if ($('#payement_copies_emp').is(':checked')) {
                        payement_copies_emp = 1;
                    } else {
                        payement_copies_emp = 0;
                    }

                    if ($('#payement_envois_emp').is(':checked')) {
                        payement_envois_emp = 1;
                    } else {
                        payement_envois_emp = 0;
                    }

                    $.post(
                            "./json/update.json.php?_=" + Date.now(),
                            {
                                id_emp: $("#id_emp").val(),
                                nom_emp: $("#nom_emp").val(),
                                adresse_emp: $("#adresse_emp").val(),
                                npa_emp: $("#npa_emp").val(),
                                localite_emp: $("#localite_emp").val(),
                                pers_contact_emp: $("#pers_contact_emp").val(),
                                conf_emp: conf_emp,
                                payement_copies_emp: payement_copies_emp,
                                payement_envois_emp: payement_envois_emp

                            },
                    function result(data, status) {
                        message(data.message.texte, data.message.type);
                    }
                    )

                }
            }
    );
});
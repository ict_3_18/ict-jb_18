-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Client :  od-93d2df
-- Généré le :  Lun 20 Août 2018 à 15:38
-- Version du serveur :  5.6.33-log
-- Version de PHP :  7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `glyg_ichjbnet`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_employeurs`
--

CREATE TABLE `t_employeurs` (
  `id_emp` int(10) UNSIGNED NOT NULL,
  `nom_emp` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `pers_contact_emp` int(100) NOT NULL,
  `adresse_emp` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `npa_emp` varchar(4) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `localite_emp` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `conf_emp` tinyint(4) NOT NULL DEFAULT '1',
  `payement_copies_emp` int(11) NOT NULL DEFAULT '1',
  `payement_envois_emp` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_employeurs`
--

INSERT INTO `t_employeurs` (`id_emp`, `nom_emp`, `pers_contact_emp`, `adresse_emp`, `npa_emp`, `localite_emp`, `conf_emp`, `payement_copies_emp`, `payement_envois_emp`) VALUES
(1, 'Ad-Networks', 41, 'Delémont', '2800', 'Delémont', 0, 1, 1),
(2, 'CIP Centre interrègional de perfectionnement', 58, 'Les Lovières 13', '2720', 'Tramelan', 1, 1, 1),
(3, 'Compagnie des Montres Francillon SA', 63, 'Rue de Francillon', '2610', 'Saint-Imier', 1, 1, 1),
(4, 'Compufa informatique Jäggi SA', 41, 'Viaduc 34', '2740', 'Moutier', 0, 1, 1),
(5, 'Institut fédéral des Hautes Etudes en form. prof.', 53, 'Kirchlindachstrasse 79', '3052', 'Zollikofen', 1, 1, 1),
(6, 'LAN Computer Systems AG', 46, 'Bahnhofplatz 6', '2501', 'Bienne', 1, 1, 1),
(7, 'Manufacture des Montres ROLEX SA', 7, 'Rue David-Moning 9', '2501', 'Bienne', 1, 1, 1),
(8, 'MPS Micro Precision Systems SA', 47, 'Chemin du Long Champ 95', '2500', 'Bienne', 0, 1, 1),
(9, 'Omega SA', 38, 'Rue Stämfpli 96', '2500', 'Bienne 4', 1, 1, 1),
(10, 'Pharmatic AG', 50, 'Könizstrasse 23', '3000', 'Berne ', 1, 1, 1),
(11, 'Posalux SA', 82, 'Rue Fritz Oppliger 18', '2500', 'Bienne', 1, 1, 1),
(12, 'Stevil Electronique', 22, 'Centre commercial', '2607', 'Cortébert', 1, 1, 1),
(13, 'Ultim Trading Top-D Computer AG', 39, 'Ch. du Longchamp 97', '2500', 'Bienne 8', 0, 1, 1),
(14, 'Ville de Bienne, Informatique et Logistique', 80, 'Rüschlistr. 14', '2503', 'Bienne', 1, 1, 1),
(15, 'ceff', 13, 'Baptiste-Savoye 26', '2610', 'St-Imier', 1, 0, 0),
(16, 'Adbin Sàrl', 51, 'Ch. du Croset 9', '1024', 'Ecublens', 0, 1, 1),
(17, 'Greatbatch Medical SA', 25, 'L\'Echelette 7', '2534', 'Orvin', 1, 1, 1),
(18, 'Energie Service Bienne', 23, 'Case postale 4263', '2504', 'Bienne', 1, 1, 1),
(19, 'Haute Ecole Arc', 59, 'Espace de l\'europe 11', '2000', 'Neuchâtel', 1, 1, 1),
(20, 'Energie Service Biel/Bienne ', 23, 'Gottstattstrasse 4 ', '2504', 'Bienne ', 1, 1, 1),
(21, 'Municipalité de Moutier Administration communale ', 61, 'Hôtel-de-Ville 1 ', '2740', 'Moutier ', 1, 1, 1),
(22, 'Compufa informatique Jäggi SA', 41, 'Viaduc 34 ', '2740', 'Moutier ', 0, 1, 1),
(23, 'Afa Organisation SA', 65, 'Route de Port 35', '2555', 'Brügg b. Biel', 0, 1, 1),
(24, '---', 28, '---', '---', '---', 1, 1, 1),
(25, 'The Swatch Group Services Ltd', 72, 'Längfeldweg 119', '2504', 'Bienne', 1, 1, 1),
(26, 'Astria informatique', 74, 'Rue Francillon 17', '2610', 'St-Imier', 1, 1, 1),
(27, 'Centre Hospitalier Bienne SA', 87, 'Chante-Merle 84', '2501', 'Bienne', 1, 1, 1),
(28, 'Fondation Battenberg', 86, 'Rue du Midi 55', '2504', 'Bienne', 1, 1, 1),
(29, 'MGI Luxury Group S.a.', 88, 'Bahnhofpl. 2B', '2502', 'Bienne', 1, 1, 1),
(30, 'FLUANCE AG', 98, 'Zuchwilerstrasse 27', '4500', 'Soleure', 1, 1, 1),
(31, 'Ceff, service informatique', 43, 'Baptise-Savoye 33', '2610', 'St-Imier', 1, 0, 0);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_employeurs`
--
ALTER TABLE `t_employeurs`
  ADD PRIMARY KEY (`id_emp`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_employeurs`
--
ALTER TABLE `t_employeurs`
  MODIFY `id_emp` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

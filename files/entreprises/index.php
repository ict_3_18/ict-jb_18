<?php
session_start();

$aut = "ADM_USR;USR_SIT";
require("./../config/config.inc.php");
require(WAY . "./includes/secure.inc.php");
require_once(WAY . "/includes/head.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

$emp = new Entreprise();
?>
<div class="container">
    <div class="row">
        <div class="header">
            <h3>Employeur</h3>
        </div>
    </div>
    <div class="panel panel-primary">
        <div class="panel-heading">
            Employeur
        </div>


        <div class="panel-body">
            <form id="employeur_form" action="<?php echo URL; ?>entreprises/entreprise.php" method="post">
                <div class="form-group row">
                    <div class="col-sm-12">
                        <!-- Bouton ajouter un employeur -->
                        <div class="form-group row">                            
                            <?php
                            if ($per->check_aut("USR_USR")) {
                                ?>
                                <div class="col-sm-offset-10">
                                    <button id="next" class="btn btn-primary" type="submit" >Ajouter un employeur</button>
                                </div>                       
                                <?php
                            }
                            ?>

                        </div>
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">Modifier</th>
                                    <th scope="col">Nom et localité</th>
                                    <th scope="col">E-mail</th>
                                    <th scope="col">Mobile</th>
                                    <th scope="col">Bureau</th>
                                </tr>                               
                            </thead>
                            <tbody>
                                <!-- Liste des employeurs -->
                                <?php
                                $employeur = $emp->get_emp_loc_mail_num();

                                foreach ($employeur as $emp) {
                                    echo "<tr>";
                                    //employeur à modifier
                                    echo "<td class=\"row\" value=\"";
                                    echo $emp['id_emp'] . "\">";
                                    echo "<button type=\"submit\" name=\"id_emp\" class=\"but_glyph\" value=\"";
                                    echo $emp['id_emp'] . "\">";
                                    echo "<span class=\"glyphicon glyphicon-cog\"></span>";
                                    echo "</button>";
                                    echo "</td>";
                                    //nom et localité
                                    echo "<td  value=\"";
                                    echo $emp['id_emp'] . "\">";
                                    echo $emp['nom_emp'] . "<br>" . $emp['localite_emp'];
                                    echo "</td>";
                                    //mail
                                    echo "<td  value=\"";
                                    echo $emp['id_emp'] . "\">"; 
                                    echo "<a href=\"mailto:" . $emp['email_per'] . "\">";
                                    echo $emp['nom_per'] . "&nbsp;" . $emp['prenom_per'];
                                    echo "</a>";
                                    echo "</td>";
                                    //téléphone mobile                                  
                                    echo "<td >";
                                    echo "<a href=\"tel:" . $emp['tel_mobile_per'] . "\">";
                                    echo $emp['tel_mobile_per'];
                                    echo "</a>";
                                    echo "</td>";
                                    //téléphone bureau
                                    echo "<td value=\"";
                                    echo $emp['id_emp'] . "\">";
                                    echo "<a href=\"tel:" . $emp['tel_bureau_per'] . "\">";
                                    echo $emp['tel_bureau_per'];
                                    echo "</a>";
                                    echo "</td>";
                                    echo "</tr>";
                                }
                                ?>  
                            </tbody>
                        </table>                      

                    </div>
                </div>

            </form>
        </div>

    </div>
</div>
<!--<script src="./js/index.js"></script>-->
<style>

    .but_glyph{
        background:none;
        border:none;
        padding:0;
        line-height: 2;
        position: relative;
        display: block;
        margin-left: auto;
        margin-right: auto;
    }
</style>
</body>
</html>
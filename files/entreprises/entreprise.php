<?php
session_start();
$aut = "ADM_USR";
require("./../config/config.inc.php");
require(WAY . "./includes/secure.inc.php");
require_once(WAY . "/includes/head.inc.php");
require_once(WAY . "/includes/autoload.inc.php");


if (isset($_POST['id_emp'])) {
    $ent = new Entreprise($_POST['id_emp']);
} else {
    $ent = new Entreprise();
}
$per = new Personne();
?>
<div class="container">
    <div class="row">
        <div class="header">
            <h3>Employeur</h3>
        </div>
    </div>
    <div class="panel panel-primary">
        <div class="panel-heading">
            Employeur
        </div>

        <?php
        //print_r($_POST);
        ?>
        <div class="panel-body">            
            <form id = "entreprise_form">
                <div class="form-group row">
                    <?php
                    if (isset($_POST['id_emp'])) {
                        echo '<input type="hidden" id="id_emp" value="' . $_POST['id_emp'] . '">';
                    }
                    ?>

                    <!-- Le nom de l'employeur -->
                    <label for="nom_emp" class="col-sm-2 col-form-label">Nom</label>
                    <div class="col-sm-5">                        
                        <input type="text" class="form-control" id="nom_emp" name="nom_emp" value="<?php echo $ent->get_nom(); ?>" placeholder="nom de l'employeur">
                    </div>
                </div>

                <!-- L'adresse de l'employeur -->
                <div class="form-group row">
                    <label for="adresse_emp" class="col-sm-2 col-form-label">Adresse</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="adresse_emp" name="adresse_emp" value="<?php echo $ent->get_adresse_emp(); ?>"placeholder="adresse de l'employeur">
                    </div>
                </div>

                <!-- Le NPA de l'employeur-->
                <div class="form-group row">
                    <label for="npa_emp" class="col-sm-2 col-form-label">NPA</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="npa_emp" name="npa_emp" value="<?php echo $ent->get_npa_emp(); ?>" placeholder="npa">
                    </div>
                </div>

                <!-- La localité de l'employeur -->
                <div class="form-group row">
                    <label for="localite_emp" class="col-sm-2 col-form-label">Localité</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="localite_emp" name="localite_emp" value="<?php echo $ent->get_localite_emp(); ?>" placeholder="localité">
                    </div>
                </div>

                <!-- La personne de contact -->
                <div class="form-group row">
                    <label for="pers_contact_emp" class="col-sm-2 col-form-label">Personne de contact</label>
                    <div class="col-sm-5">
                        <select name="pers_contact_emp" id="pers_contact_emp" class="form-control" >
                            <option disabled selected value>Sélectionner une personne de contact</option>
                            <?php
                            $tab_per = $per->get_all();

                            $id_contact = $ent->get_pers_contact_emp();

                            foreach ($tab_per as $contact) {
                                echo '<option value=';
                                echo $contact['id_per'] . "\"";

                                if ($id_contact == $contact['id_per']) {
                                    echo " selected ";
                                }
                                echo ">";
                                echo $contact['nom_per'] . ", " . $contact['prenom_per'];
                                echo "</option>";
                            }
                            ?>
                        </select>
                    </div>
                </div>
              
                <!-- Mode d'envoi -->
                <div class="form-group row">
                    <label for="conf_emp" class="col-sm-2 col-form-label" >Envoi en lettre signature</label>
                    <div class="col-sm-5">
                        <?php
                        $tab_conf_emp = $ent->get_conf_emp();
                        ?>
                        <input type="radio" name="conf_emp" id="conf_emp"<?php
                        if ($tab_conf_emp == 1) {
                            echo "checked";
                        }
                        ?>>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="conf_emp" class="col-sm-2 col-form-label">Envoi en courrier normal</label>
                    <div class="col-sm-5">
                        <?php
                        $tab_conf_emp = $ent->get_conf_emp();
                        ?>
                        <input type="radio" name="conf_emp" id="conf_emp"<?php
                        if ($tab_conf_emp == 0) {
                            echo "checked";
                        }
                        ?>>

                    </div>
                </div>

                <!-- Paiement -->
                <div class="form-group row">
                    <label for="payement_copies_emp" class="col-sm-2 col-form-label">Payement des copies</label>
                    <div class="col-sm-5">
                        <?php
                        $tab_payement_copies = $ent->get_payement_copies_emp();
                        ?>

                        <input type="radio" name="payement_copies_emp" id="payement_copies_emp"<?php
                        if ($tab_payement_copies == null || $tab_payement_copies) {
                            echo "checked";
                        }
                        ?>>
                        <label for="payement_copies_emp">OUI</label>
                        <br>
                        <input type="radio" name="payement_copies_emp" id="payement_copies_emp"<?php
                        if (!($tab_payement_copies == null || $tab_payement_copies)) {
                            echo "checked";
                        }
                        ?>>
                        <label for="payement_copies_emp">NON</label>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="payement_envois_emp" class="col-sm-2 col-form-label">Payement des envois</label>
                    <div class="col-sm-5">
                        <?php
                        $tab_payement_envois = $ent->get_payement_envois_emp();
                        ?>
                        <input type="radio" name="payement_envois_emp" id="payement_copies_emp"<?php
                        if ($tab_payement_envois == null || $tab_payement_envois) {
                            echo "checked";
                        }
                        ?>>
                        <label for="payement_envois_emp">OUI</label>
                        <br>
                        <input type="radio" name="payement_envois_emp" id="payement_copies_emp"<?php
                        if (!($tab_payement_envois == null || $tab_payement_envois)) {
                            echo "checked";
                        }
                        ?>>
                        <label for="payement_envois_emp">NON</label>
                    </div>
                </div>


                <div class="form-group row">
                    <div class="col-sm-offset-8 col-sm-2">
                        <!-- Bouton d'ajout ou de modification de l'employeur -->
                        <?php
                        if (isset($_POST['id_emp'])) {
                            echo '<input type="submit" class="form-control btn btn-primary submit" id="butt_update" value="Modifier"/>';
                        } else {
                            echo '<input type="submit" class="form-control btn btn-primary submit" id="butt_conf" value="Ajouter"/>';
                        }
                        ?>

                    </div> 
                    <div class="col-sm-2">
                        <input type="button" class="form-control btn btn-warning" id="retour_index" value="Retour">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="./js/entreprise.js"></script>
</body>
</html>

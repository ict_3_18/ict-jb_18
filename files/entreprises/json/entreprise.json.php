<?php
$aut="ADM_USR";
header('Content-Type: application/json');
session_start();
require("./../../config/config.inc.php");
require_once(WAY . "./includes/secure.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

//print_r($_POST);
$ent = new Entreprise();

if ($ent->check_no_doublon($_POST['nom_emp'])) {

    $id = $ent->add($_POST);
    $ent->set_id_emp($id);

    if ($ent->init()) {
        $tab['reponse'] = true;
        $tab['message']['texte'] = "";
        $tab['message']['type'] = "success";
    } else {
        $tab['reponse'] = false;
        $tab['message']['texte'] = "Un problème est survenu";
        $tab['message']['type'] = "danger";
    }
} else {
    $tab['reponse'] = false;
    $tab['message']['texte'] = "Existe déjà.";
    $tab['message']['type'] = "danger";
}


//print_r($_POST);

echo json_encode($tab);
?>
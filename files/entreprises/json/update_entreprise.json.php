<?php
$aut="ADM_USR";
header('Content-Type: application/json');
session_start();
require("./../../config/config.inc.php");
require_once(WAY . "./includes/secure.inc.php");
require_once(WAY . "/includes/autoload.inc.php");


$ent = new Entreprise();
$per = new Personne($_SESSION['id']);

if($per->check_aut($aut)){
    if ($ent->check_no_doublon($_POST['nom_emp'])) {

    $ent->update($_POST, $_POST['id_emp']);

    if ($ent->init()) {
        $tab['reponse'] = true;
        $tab['message']['texte'] = "L'employeur a été modifié";
        $tab['message']['type'] = "success";
    } else {
        $tab['reponse'] = false;
        $tab['message']['texte'] = "Un problème est survenu";
        $tab['message']['type'] = "danger";
    }
} else {
    $tab['reponse'] = false;
    $tab['message']['texte'] = "Existe déjà";
    $tab['message']['type'] = "danger";
}
    
}else{
    $tab['reponse'] = false;
    $tab['message']['texte'] = "Vous n'avez pas l'autorisation";
    $tab['message']['type'] = "danger";
}

echo json_encode($tab);
?>
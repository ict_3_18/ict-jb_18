<?php
$aut="ADM_USR";
header('Content-Type: application/json');
session_start();
require("./../../config/config.inc.php");
require_once(WAY . "./includes/secure.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

$ent = new Entreprise();

$ent->update($_POST, $_POST['id_emp']);

echo json_encode($_POST);
?>
<?php
session_start();
$aut = "ADM_USR;ADM_EXA;USR_EXA";
require("./../config/config.inc.php");
require(WAY . "./includes/secure.inc.php");
require_once(WAY . "/includes/head.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

$exa = new Examen();
$mod = new Module();
$per = new Personne($_SESSION['id']);
$cat = new Categorie(1);


echo "<h1>Initialisation catégorie id=1</h1>";
echo $cat;

echo "<h1>get_all</h1>";
$test = $cat->get_all();
echo '<pre>';
print_r($test);
echo '</pre>';

echo "<h1>get_all_with_rubriques</h1>";
$test = $cat->get_all_with_rubriques();
echo '<pre>';
print_r($test[0]);
echo '</pre>';

echo "<h1>get_categories_with_criteres</h1>";
$test = $cat->get_categories_with_criteres();
echo '<pre>';
print_r($test[0]);
echo '</pre>';



?>
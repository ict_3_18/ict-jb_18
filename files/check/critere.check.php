<?php
session_start();
$aut = "ADM_USR;ADM_EXA;USR_EXA";
require("./../config/config.inc.php");
require(WAY . "./includes/secure.inc.php");
require_once(WAY . "/includes/head.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

$exa = new Examen();
$mod = new Module();
$per = new Personne($_SESSION['id']);
$crt = new Critere(1);


echo "<h1>Initialisation critère id=1</h1>";
echo $crt;

echo "<h1>Initialisation critère by num 1.6</h1>";
$crt->init_by_num("1.6");
echo $crt;

echo "<h1>get_all_criteres</h1>";
$test = $crt->get_all_criteres();
echo '<pre>';
print_r($test);
echo '</pre>';

echo "<h1>get_all</h1>";
$test = $crt->get_all();
echo '<pre>';
print_r($test[2]);
echo '</pre>';

echo "<h1>get_all_with_details</h1>";
$test = $crt->get_all_with_details();
echo '<pre>';
print_r($test[3]);
echo '</pre>';

echo "<h1>get_all_specifiques</h1>";
$test = $crt->get_all_specifiques();
echo '<pre>';
print_r($test[1]);
echo '</pre>';

echo "<h1>get_all_specifiques_by_thm id_thm=1</h1>";
$test = $crt->get_all_specifiques_by_thm(1);
echo '<pre>';
print_r($test[1]);
echo '</pre>';

echo "<h1>get_next</h1>";
$test = $crt->get_next();
echo '<pre>';
print_r($test);
echo '</pre>';

echo "<h1>get_previous</h1>";
$test = $crt->get_previous();
echo '<pre>';
print_r($test);
echo '</pre>';

echo "<h1>get_tab_crt</h1>";
$test = $crt->get_tab_crt();
echo '<pre>';
print_r($test[1]);
echo '</pre>';

/*echo "<h1>get_tab_id_rubrique</h1>";
$test = $crt->get_tab_id_rubrique();
echo '<pre>';
print_r($test);
echo '</pre>';*/


?>
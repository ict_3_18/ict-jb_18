<?php
session_start();
require("./../config/config.inc.php");
require(WAY . "./includes/secure.inc.php");
require(WAY . "./includes/head.inc.php");


function check($classe){
   ?>
   <div class="panel panel-primary">
      <div class="panel-heading">
         <?php echo $classe;?>
      </div>
      <div class="panel-body">
      <?php 
         require(WAY . "./check/".$classe.".check.php");
      ?>
      </div>
   </div>      
   <?php 
}

check("personne");
check("jeu");
check("partie");
check("joueur");

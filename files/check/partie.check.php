<div class="col-md-6">
   <div class="panel panel-primary ">
      <div class="panel-heading">
         Tostring
      </div>
      <div class="panel-body">
      <?php 
         $partie = new Partie(1);
         echo $partie;
      ?>
      </div>
   </div>      
</div>
<div class="col-md-6">
   <div class="panel panel-primary ">
      <div class="panel-heading">
         get_all()
      </div>
      <div class="panel-body">
         <pre>
      <?php 
        print_r($partie->get_all());
      ?>
      </pre>
      </div>
   </div>      
</div>
<div class="col-md-6">
   <div class="panel panel-primary ">
      <div class="panel-heading">
         get_joueurs()
      </div>
      <div class="panel-body">
         <pre>
      <?php 
        print_r($partie->get_joueurs(1));
      ?>
      </pre>
      </div>
   </div>      
</div>
<div class="col-md-6">
   <div class="panel panel-primary ">
      <div class="panel-heading">
         get_open_parties()
      </div>
      <div class="panel-body">
         <pre>
      <?php 
        print_r($partie->get_open_parties(1));
      ?>
      </pre>
      </div>
   </div>      
</div>

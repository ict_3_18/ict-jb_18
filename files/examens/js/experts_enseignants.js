$(function () {

    /**
     * Retourne a la liste des examens
     */
    $("#retour").on("click", function () {
        location.assign("liste_examen.php");
    });

    /**
     * Envoie le formulair d'ajout de l'expert et reload la page
     */
    $("#submit_exp_conf").click(function (e) {
        e.preventDefault();
        $.post(
            "./json/add_expert_exam.json.php?_=" + Date.now(),
            {
                id_exa: $("#id_exa").val(),
                exp_exa: $("#exp_exa").val()
            }
        ).always( function () {
            window.location.reload();
        });
        
    });

    /**
     * Envoie le formulair d'ajout de l'expert et reload la page
     */
    $("#submit_ens_conf").click(function (e) {
        e.preventDefault();
        $.post(
            "./json/add_enseignant_exam.json.php?_=" + Date.now(),
            {
                id_exa: $("#id_exa").val(),
                ens_exa: $("#ens_exa").val()
            }
        ).always( function () {
            window.location.reload();
        });
    });

    /**
     * WIP!
     * Envoie le formulair de suppression et supprime l'expert
     */
    $(".btn_del_exp").click(function() {
        $.post(
                "./json/remove_exp_ens_exam.json.php?_=" + Date.now(),
                {
                    id_per: $(this).attr("id_per"),
                    id_exa: $("#id_exa").val(),
                    ref_fnc: 1
                }
        ).always( function () {
            window.location.reload();
        });
    });

    /**
     * WIP!
     * Envoie le formulair de suppression et supprime l'enseignant
     */
    $(".btn_del_ens").click(function() {
        $.post(
                "./json/remove_exp_ens_exam.json.php?_=" + Date.now(),
                {
                    id_per: $(this).attr("id_per"),
                    id_exa: $("#id_exa").val(),
                    ref_fnc: 3
                }
        ).always( function () {
            window.location.reload();
        });
    });
});
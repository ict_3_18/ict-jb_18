$(function () {


    $(".note").change(function () {
        console.log($(this).parentsUntil("#can_in_exa", "tr").attr("id_can"));

        $.post(
                "./json/add_note_candidat.json.php?_=" + Date.now(),
                {
                    note_can: $(this).val(),
                    id_can: $(this).parentsUntil("#can_in_can", "tr").attr("id_can"),
                    id_exa: $(this).parentsUntil("#can_in_exa", "tr").attr("id_exa")
                },
                function (data) {
                    // console.log(data.id_can);
                    //$(".conf_change").css("visibility", "visible")
                    $("#conf_change_" + data.id_can).css("visibility", "visible")
                },
                'json'
                );


    });

    $(".note").click(function (data) {
        $("#conf_change_" +$(this).parentsUntil("#can_in_can", "tr").attr("id_can")).css("visibility", "hidden")
        
    });

    $(".remedie").change(function (data) {
        console.log($(this).val());

        if ($(this).is(":checked")) {
            var remedie = 1;
        } else {
            var remedie = 0;
        }

        console.log(remedie);

        $.post(
                "./json/remedie_candidat.json.php?_=" + Date.now(),
                {
                    remedie: remedie,
                    id_can: $(this).parentsUntil("#can_in_can", "tr").attr("id_can"),
                    id_exa: $(this).parentsUntil("#can_in_exa", "tr").attr("id_exa")
                },
                'json'
                );
    });
}); 
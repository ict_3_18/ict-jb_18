$(function () {
    // Clic sur le bouton de retour -> retour à la dernière page visitée
    $("#retour").on("click", function () {
        history.back(-1);
    });

    $("#materiel_examen_form").validate(
            {
                submitHandler: function (form) {
                    if ($("#lec_exa").is(":checked")) {
                        var lec = 1;
                    } else {
                        var lec = 0;
                    }

                    if ($("#salle_inf_exa").is(":checked")) {
                        var salle = 1;
                    } else {
                        var salle = 0;
                    }

                    if ($("#reseau_exa").is(":checked")) {
                        var res = 1;
                    } else {
                        var res = 0;
                    }

                    $.post(
                            "./json/materiel_examen.json.php?_=" + Date.now(),
                            {
                                materiel_exa: $("#materiel_exa").val(),
                                lec_exa: lec,
                                salle_inf_exa: salle,
                                reseau_exa: res,
                                soft_exa : $("#soft_exa").val(),
                                infos_begin_exa : $("#infos_begin_exa").val(),
                                data_aut_exa : $("#data_aut_exa").val(),
                                id_exa: $("#id_exa").val()

                            },
                            function result(data, status) {
                                history.back(-1);
                            }
                    );
                }
            });
});


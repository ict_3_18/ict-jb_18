$(function(){
    $("#form_scan").validate({
        rules: {
            code: {
                required: true,
                minlength: 13,
                maxlength: 13
            }
        },
        messages: {
            code: {
                required: "Veuillez scanner le code",
                minlength: "Votre code est trop court",
                maxlength: "Votre code est trop long"
            }
        },
        submitHandler: function (form) {
            $('#message').html('');
            $.post(
                "./json/suivi_exa.json.php?_=" + Date.now(),
                {
                    code: $("#code").val()
                },
                function(data){
                    if(data.status){
                        $('[id_exa="'+data.id_exa+'"] [action="'+data.action+'"]').removeClass('btn-danger').addClass('btn-success');
                        $('#message').html(data.message.texte);
                        $('#message').removeClass("bg-success").removeClass("bg-danger").addClass("bg-"+data.message.type);
                        $('#code').val('');
                        if(($('[id_exa="'+data.id_exa+'"] .btn-danger').length)>0){
                            $('[id_exa="'+data.id_exa+'"] [action="active_archivage_exa"]').prop('disabled','disabled');
                        }else{
                            $('[id_exa="'+data.id_exa+'"] [action="active_archivage_exa"]').prop('disabled','');
                        }
                    }
                }
            );
        }
    });

    $('.check_suivi').click(function(){
        if($(this).is(':checked')){
            status = 1;
        }else{
            status = 0;
        }
        $.post(
            './json/suivi_exa.json.php?_='+Date.now(),
            {
                action:$(this).attr("action"),
                id_exa:$(this).attr("id_exa"),
                status:status
            }
        );
    });

    $('[data-toggle="tooltip"]').tooltip();
    $('.suivi').click(function() {
        $('#message').html('');
        var status;
        if($(this).hasClass("btn-danger")){
            status = 1;
        }else{
            status = 0;
        }
        $.post(
            './json/suivi_exa.json.php?_='+Date.now(),
            {
                action:$(this).attr("action"),
                id_exa:$(this).parent().attr("id_exa"),
                status: status
            },
            function(data){
                if(data.status){
                    $('#message').html(data.message.texte);
                    $('#message').removeClass("bg-success").removeClass("bg-danger").addClass("bg-"+data.message.type);

                    $('[id_exa="'+data.id_exa+'"] [action="'+data.action+'"]').toggleClass('btn-danger').toggleClass('btn-success');
                    if(($('[id_exa="'+data.id_exa+'"] .btn-danger').length)>0){
                        $('[id_exa="'+data.id_exa+'"] [action="active_archivage_exa"]').prop('disabled','disabled');
                        $('[id_exa="'+data.id_exa+'"] [action="active_archivage_exa"]').removeClass('btn-warning');
                    }else{
                        $('[id_exa="'+data.id_exa+'"] [action="active_archivage_exa"]').prop('disabled','');
                        $('[id_exa="'+data.id_exa+'"] [action="active_archivage_exa"]').addClass('btn-warning');
                    }
                }
            }
        );
    });
    $('.archivage').click(function(){
        $.post(
          './json/suivi_exa.json.php?_='+Date.now(),
            {
                action:"active_archivage_exa",
                id_exa:$(this).parent().attr("id_exa"),
                status:1
            },
            function(data){
                $('.examen_'+data.id_exa).remove();
            }
        );
    });
});


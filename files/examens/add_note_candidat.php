<?php

session_start();
$aut = "ADM_USR";
require_once("./../config/config.inc.php");
require_once(WAY . "./includes/secure.inc.php");
require_once( WAY . "/includes/autoload.inc.php");
require_once( WAY . "/includes/head.inc.php");


$id_exa = 0;
$candidat = new Candidat();

if (isset($_POST['id_exa'])) {
    $id_exa = $_POST['id_exa'];
    $exa = new Examen($id_exa);
    //echo $exa;
    $tab_can = $exa->get_can();
    $count_candidats = count($tab_can);

    /** ----------- DEBUT CODEBAR ------------ */
    /*echo 'En HTML';
    require_once '../class/php-barcode-generator/BarcodeGeneratorHTML.php';
    $generatorHTML = new Barcode\BarcodeGeneratorHTML();
    // widthFactor, totalHeight et color ne sont pas obligatoire
    echo $generatorHTML->getBarcode($exa->calcul_checksum(), $generatorHTML::TYPE_EAN_13, 3, 40, '#000');

    echo '<br>En PNG (aussi disponble en JPG et SVG)<br>';
    require_once '../class/php-barcode-generator/BarcodeGeneratorPNG.php';
    $generatorPNG = new Barcode\BarcodeGeneratorPNG();
    echo '<img src="data:image/png;base64,' . base64_encode($generatorPNG->getBarcode($exa->calcul_checksum(), $generatorPNG::TYPE_EAN_13)) . '">';*/
    /** ----------- FIN CODEBAR ----------- */

    //print_r($tab_can);
    //Affiche le numero et l'année de l'examen 
?>
    <div id="load_list_can_note" class="col-md-6 col-md-offset-3" id_exa="<?= $id_exa; ?>">
    <?php
    echo "<div class=\"row\">";
    echo "<div class=\"header\">";
    echo "<h3><strong>" . $exa->get_num_nom() . "  " . $exa->get_nom_mod($id_exa) . " - " . $exa->get_date_hrs() . "</strong></h3>";
    echo "</div>";
    echo "</div>";
    ?>
        <?php
        if ($count_candidats !== 0) {
            ?>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <span id="number_can_already_in_exa"><?php echo $count_candidats; ?></span> candidats
                    <div class="btn">
                        <form action="pdf/recap_note.pdf.php" method="post" >
                            <?php echo Form::hidden("id_exa",$_POST['id_exa']); ?>
                            <button type="submit" id="btn_export_to_pdf" name='export_to_pdf'
                                    value="exported" class="btn btn-info">
                                Export to PDF
                            </button>
                        </form>
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table table-striped table-hover table-condensed" id="can_in_exa">
                        <thead>
                            <tr>
                                <th>N°</th>
                                <th>Candidat</th>
                                <th>Note</th>
                                <th>Remédié</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($tab_can as $key => $can) {
                                echo "<tr class=\"line\" id_exa=\"" . $exa->get_id() . "\" id_can=\"" . $can['id_can'] . "\">";
                                echo "<td style=\"background:" . $candidat->get_color_from_id($can['id_can']) . "\">" . substr($can['no_candidat_can'], -3) . "</td>";
                                echo "<td>" . $can['nom_can'] . " " . $can['prenom_can'] . "</td>";
                                echo "<td>";
                                echo "<input class=\"note\" tabindex=\"".$key."\" type=\"number\" max=\"6\" min=\"1\" step=\"0.1\" value=\"" . $can['note_can'] . "\">";

                                echo "</td>";
                                echo "<td>";
                                echo "<input class=\"col-md-8 remedie\" id=\"remedie_can_" . $can['id_can'] . "\" type=\"checkbox\"";
                                if ($can['remedie_note']) {
                                    echo " checked=\"checked\" ";
                                }
                                echo ">";
                                echo "</td>";
                                echo "<td>";
                                echo "<span id=\"conf_change_" . $can['id_can'] . "\" style=\"visibility: hidden\" title=\"Note modifiée\" class=\"glyphicon glyphicon-ok black conf_change\" value=" . $can['id_can'] . " ></span>";
                                echo "</td>";

                                echo '</tr>';
                            }
                            ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <?php
    } else {
        echo "<div class=\"alert alert-info\" role=\"alert\">";
        echo "Aucune 'ref_exa' reçu en post";
        echo "</div>";
    }
}
?>

<script src="./js/add_note_candidat.js"></script> 

</body>
</html>
<?php
session_start();
$aut = "ADM_USR";
require("./../config/config.inc.php");
require(WAY . "./includes/secure.inc.php");
require_once(WAY . "/includes/head.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

$per = new Personne();
$id_exa = $_POST['ref_exa'];
$exa = new Examen($id_exa);
?>

<!-- Parties ouvertes -->
<div class="container">
    <div class="row">
        <div class="header">
            <h3>Ajout d'experts et d'enseignants</h3>
        </div>
    </div>
    <div class="panel panel-primary ">
        <div class="panel-heading">
            Ajouter dans ICT-<?=$exa->get_num_nom()."-".$exa->get_ann().$exa->get_ver()?>
        </div>
        <div class="panel-body">
            <form id="add_experts_examen">

                <!-- Numero et nom de l'examen -->
                <div class="form-group row">
                    <label for="exp_exa" class="col-sm-2 col-form-label">Expert</label>
                    <div class="col-sm-6">
                        <select class="form-control" id="exp_exa" name="exp_exa">
                            <option disabled selected> Veuillez selectionner un expert. </option>
                            <?php
                            $experts = $per->get_all_expert_actif();
                            foreach ($experts AS $exp) {
                                echo "<option value=" . $exp['id_per'] . ">" . $exp['nom_per'] . " " . $exp['prenom_per'] . "</option>";
                            }
                            ?>
                        </select>
                        <input type="hidden" id="id_exa" name="id_exa" value="<?php echo $id_exa ?>">

                    </div>
                    <div class="col-sm-2">
                        <input type="submit" class="form-control btn btn-primary submit" id="submit_exp_conf" value="Ajouter">
                    </div>
                </div>
            </form>
            <div class="row">
                <?php
                $list_per_exa = $exa->get_expert_exam($id_exa);
                //print_r($list_per_exa);
                foreach ($list_per_exa as $expert_exa) {
                    echo '<div class="col-sm-offset-2 col-sm-8">';
                    echo '<div class="col-md-8">';
                    echo $expert_exa['nom_per'] . " " . $expert_exa['prenom_per'];
                    echo '</div>';
                    echo "<button class=\"btn_del_exp col-sm-1 glyphicon glyphicon-remove btn btn-danger\" id_per=" . $expert_exa['id_per'] . "></button>";
                    echo "</div>";
                }
                ?>
            </div>
            <br>
            <form id="add_enseignant_examen">

                <!-- Numero et nom de l'examen -->
                <div class="form-group row">
                    <label for="ens_exa" class="col-sm-2 col-form-label">Enseigants</label>
                    <div class="col-sm-6">
                        <select class="form-control" id="ens_exa" name="ens_exa">
                            <option disabled selected> Veuillez selectionner un enseignant. </option>
                            <?php
                            $enseignant = $per->get_all_enseignant_actif();
                            foreach ($enseignant AS $ens) {
                                echo "<option value=" . $ens['id_per'] . ">" . $ens['nom_per'] . " " . $ens['prenom_per'] . "</option>";
                            }
                            ?>
                        </select>
                        <input type="hidden" name="id_exa"  id="id_exa" value="<?php echo $id_exa ?>">

                    </div>
                    <div class=" col-sm-2">
                        <input type="submit" class="form-control btn btn-primary submit" id="submit_ens_conf" value="Ajouter">
                    </div>
                </div>
            </form>
            <div class="row">
                <?php
                $list_per_exa = $exa->get_profs_exam($id_exa);
                //print_r($list_per_exa);
                foreach ($list_per_exa as $enseignant_exa) {
                    echo '<div class="col-sm-offset-2 col-sm-8">';
                    echo '<div class="col-md-8">';
                    echo $enseignant_exa['nom_per'] . " " . $enseignant_exa['prenom_per'];
                    echo '</div>';
                    echo '<button class="btn_del_ens col-sm-1 glyphicon glyphicon-remove btn btn-danger" id_per=' . $enseignant_exa['id_per'] . "></button>";
                    echo '</div>';
                }
                ?>
            </td>
        </div>
        <div class="form-group row">
            <div class="col-sm-offset-8 col-sm-2">
                <input type="button" class="form-control btn btn-info" id="retour" value="Retour">
            </div>
        </div>
    </div>    
</div>
</div>
<script src="./js/experts_enseignants.js"></script>
</body>
</html>
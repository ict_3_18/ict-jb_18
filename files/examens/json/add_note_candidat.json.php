<?php

header('Content-Type: application/json');
session_start();

$aut = "ADM_USR";

require_once("./../../config/config.inc.php");
require_once(WAY . "/includes/autoload.inc.php");
require_once(WAY . "./includes/secure.inc.php");

$examen = new Examen($_POST['id_exa']);
$id_can = $_POST['id_can'];
$note = $_POST['note_can'];
//$remedie = $_POST['remedie'];
//Modification des notes des Candidats --> la méthode gère les doublons
if ($examen->modify_note_can($id_can, $note)) {
    $tab['id_can'] = $id_can;
    $tab['reponse'] = true;
    $tab['message']['texte'] = "Les notes ont été modifiées";
    $tab['message']['type'] = "success";
    $tab['ref_exa'] = $_POST['id_exa'];
} else {
    $tab['reponse'] = false;
    $tab['message']['texte'] = "La remediation n'a pas été modifiées";
    $tab['message']['type'] = "danger";
}

echo json_encode($tab);
?>
<?php
header('Content-Type: application/json');
session_start();
$aut = "ADM_USR";
require("./../../config/config.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

$exa = new Examen($_POST['id_exa']);

if (isset($_POST)) {
    $materiel_exa = $exa->add_materiel_exam($_POST);
    
    if($materiel_exa) {
        $tab['reponse'] = true;
        $tab['message']['type'] = "Matériel bien mis a jour";
        $tab['message']['type'] = "success";
    }else{
        $tab['reponse'] = false;
        $tab['message']['texte'] = "Un problème est survenue";
        $tab['message']['type'] = "danger";
    }
}else{
    $tab['reponse'] = false;
    $tab['message']['texte'] = "Un problème est survenu";
    $tab['message']['type'] = "danger";
}

echo json_encode($tab);

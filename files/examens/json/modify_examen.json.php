<?php
header('Content-Type: application/json');
session_start();
$aut = "ADM_USR";
require("./../../config/config.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

$exa = new Examen();

if (isset($_POST)) {

    if($_POST['salles'] == 0){
        //la modifiation
        $modif_exa = $exa->modifier_exam($_POST['id_exa'], $_POST);

        if ($modif_exa) {
            $tab['reponse'] = true;
            $tab['message']['texte'] = "L'examen à bien été modifié.";
            $tab['message']['type'] = "success";
        }else{
            $tab['reponse'] = false;
            $tab['message']['texte'] = "Un problème est survenu";
            $tab['message']['type'] = "danger";
        }

    }else {
        //la suppréssion des salles déjà présentes pour l'examen
        $del_sal = $exa->del_salle($_POST['id_exa']);

        //la modifiation
        $modif_exa = $exa->modifier_exam($_POST['id_exa'], $_POST);
        $add_sal = $exa->add_salle($_POST['salles'], $_POST['id_exa']);

        if ($modif_exa && $add_sal && $del_sal) {
            $tab['reponse'] = true;
            $tab['message']['texte'] = "L'examen à bien été modifié.";
            $tab['message']['type'] = "success";
        }else {
            $tab['reponse'] = false;
            $tab['message']['texte'] = "Un problème est survenu";
            $tab['message']['type'] = "danger";
        }
    }
    
} else {
    $tab['reponse'] = false;
    $tab['message']['texte'] = "Un problème est survenu";
    $tab['message']['type'] = "danger";
}

echo json_encode($tab);
?>
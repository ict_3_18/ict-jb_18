<?php
header('Content-Type: application/json');
session_start();

$aut = "ADM_EXA";
require_once substr(__dir__, 0, strpos(__dir__, "files")+strlen("files")) . "/config/config.inc.php";
require(WAY . "./includes/secure.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

$tableau_title = array(
    1=>"Classeur des candidats",
    3=>"Notes saisies",
    4=>"Travaux des candidats",
    5=>"Rapport d'examens",
    6=>"CD travaux",
    7=>"CD données",
    8=>"Définitif"
);

$tableau = array(
    1=>"archiver_cla_exa",
    3=>"saisi_note_exa",
    4=>"archiver_dos_exa",
    5=>"valider_exa",
    6=>"archiver_cd_ep_exa",
    7=>"archiver_cd_exa",
    8=>"definitf_exa"
);

if(isset($_POST['code'])){


    $code = $_POST['code'];
    $code_split = str_split($code);

    $id_exa = $code_split[4].$code_split[5].$code_split[6];

    $exa = new Examen($id_exa);

    foreach($tableau AS $key => $value){
        if($key = $code_split[11]) {
            $index = $key;
        }
    }

    $tab_reponse = array();
    $tab_reponse['id_exa'] = $id_exa;
    $tab_reponse['status'] = 1;
    $tab_reponse['action'] = $tableau[$index];
    $tab_reponse['message']['texte'] = "Scan du code \"".$tableau_title[$index]."\" effectué avec succés";
    $tab_reponse['message']['type'] = "success";


    if($exa->set_type_archive($tableau[$index],1)){
        echo json_encode($tab_reponse);
    }
}

else if($_POST['action'] == "active_archivage_exa"){

    $tab_reponse = array();
    $tab_reponse['id_exa'] = $_POST['id_exa'];
    $tab_reponse['status'] = $_POST['status'];
    $tab_reponse['action'] = $_POST['action'];

    $exa = new Examen($tab_reponse['id_exa']);
    $exa->add_active_archivage_exa($tab_reponse['status']);

    echo json_encode($tab_reponse);
}

else{

    foreach($tableau AS $key => $value){
        if($value = $_POST['action']) {
            $index = $key;
        }
    }

    $tab_reponse = array();
    $tab_reponse['id_exa'] = $_POST['id_exa'];
    $tab_reponse['status'] = $_POST['status'];
    $tab_reponse['action'] = $_POST['action'];

    if($_POST['status']) {
        $tab_reponse['message']['texte'] = "Activation de \"" . $tableau_title[$index] . "\" effectué avec succés";
    }
    else{
        $tab_reponse['message']['texte'] = "Désactivation de \"" . $tableau_title[$index] . "\" effectué avec succés";
    }
    $tab_reponse['message']['type'] = "success";

    $exa = new Examen($_POST['id_exa']);
    if($exa->set_type_archive($_POST['action'],$_POST['status'])){
        echo json_encode($tab_reponse);
    }
}

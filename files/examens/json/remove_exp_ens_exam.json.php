<?php

header('Content-Type: application/json');
session_start();
$aut = "ADM_USR";
require_once substr(__dir__, 0, strpos(__dir__, "files")+strlen("files")) . "/config/config.inc.php";
require_once(WAY . "/includes/autoload.inc.php");

$exa = new Examen($_POST['id_exa']);

if (isset($_POST)) {
    $exp_exa = $exa->del_enseignant_expert_exam($_POST);

    if ($exp_exa) {
        $tab['reponse'] = true;
        $tab['message']['type'] = "Mat�riel bien mis a jour";
        $tab['message']['type'] = "success";
    } else {
        $tab['reponse'] = false;
        $tab['message']['texte'] = "Un probl�me est survenue";
        $tab['message']['type'] = "danger";
    }
} else {
    $tab['reponse'] = false;
    $tab['message']['texte'] = "Un probl�me est survenu";
    $tab['message']['type'] = "danger";
}

echo json_encode($_POST);
<?php

header('Content-Type: application/json');
session_start();
$aut = "ADM_USR";
require("./../../config/config.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

$exa = new Examen();

if ($exa->check_no_doublon($_POST['num_nom_exa'], $_POST['ann_exa'], $_POST['ver_exa'])) {
    $id_init = $exa->add($_POST);
    $exa->set_id($id_init);

    if ($exa->init()) {
        $tab['reponse'] = true;
        $tab['message']['texte'] = "L'examen à bien été ajoutée.";
        $tab['message']['type'] = "success";
        $tab['id'] = $id_init;
    }else{
         $tab['reponse'] = false;
         $tab['message']['texte'] = "Un problème est survenu";
         $tab['message']['type'] = "danger";
    }
    
} else {
    $tab['reponse'] = false;
    $tab['message']['texte'] = "Un problème est survenu";
    $tab['message']['type'] = "danger";
}


echo json_encode($tab);
?>
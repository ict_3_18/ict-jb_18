<?php
header('Content-Type: application/json');
session_start();
$aut = "ADM_USR";
require("./../../config/config.inc.php");
require_once(WAY . "/includes/autoload.inc.php");


if (isset($_POST)) {
    
    $exa = new Examen($_POST['id_exa']);
    
    if($_POST['conv_exa']){
        //delete cuz true
        $change_conv = $exa->change_convocation(0);
        $test = "it's now 0";
    }else{
        //add cuz false
        $change_conv = $exa->change_convocation(1);
        $test = "it's now 1";
    }

    if ($change_conv) {
        $tab['reponse'] = true;
        $tab['message']['texte'] = "L'examen à bien été modifié.";
        $tab['message']['type'] = "success";
        $tab['id_exa'] = $_POST['id_exa'];
    }else{
         $tab['reponse'] = false;
         $tab['message']['texte'] = "Un problème est survenu";
         $tab['message']['type'] = "danger";
    }
    
} else {
    $tab['reponse'] = false;
    $tab['message']['texte'] = "Un problème est survenu";
    $tab['message']['type'] = "danger";
}


echo json_encode($tab);
?>
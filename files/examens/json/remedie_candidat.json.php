<?php

header('Content-Type: application/json');
session_start();

$aut = "ADM_USR";

require_once("./../../config/config.inc.php");
require_once(WAY . "/includes/autoload.inc.php");
require_once(WAY . "./includes/secure.inc.php");

$examen = new Examen($_POST['id_exa']);
$id_can = $_POST['id_can'];
$remedie = $_POST['remedie'];

if ($examen->remedie_can($id_can, $remedie)) {
    $tab['id_can'] = $id_can;
    $tab['reponse'] = true;
    $tab['message']['texte'] = "La remediation a été modifiées";
    $tab['message']['type'] = "success";
    $tab['ref_exa'] = $_POST['id_exa'];
    $tab['remedie'] = $remedie;
    //echo '<span id="icon_add" title="Note modifiée" class="glyphicon glyphicon-plus black add_conv">aa</span>';
} else {
    $tab['reponse'] = false;
    $tab['message']['texte'] = "La remediation n'a pas été modifiées";
    $tab['message']['type'] = "danger";
}

echo json_encode($tab);
?>
<?php

header('Content-Type: application/json');
session_start();
$aut = "ADM_USR";
require_once("./../../config/config.inc.php");
require_once(WAY . "/includes/autoload.inc.php");
require_once(WAY . "./includes/secure.inc.php");

$can = new Candidat();
$exa = new Examen();

$tab = array();

if (isset($_POST['all_can_id'])) {
    $exa2 = new Examen($_POST['ref_exa']);
    //Test si l'examen est un TPI ou non
    if($exa2->get_num_nom() == 0) {
        $tpi = new Tpi();
        foreach ($_POST['all_can_id'] as $can_id) {
            if(!$tpi->has_per_in_tpi($tpi->get_id_tpi_by_ref_exa_and_ref_can($_POST['ref_exa'],$can_id))) {
                if ($can->remove_can_exa_if_not_note($can_id, $_POST['ref_exa'])) {
                    $tpi->remove_crt_tpi($tpi->get_id_tpi_by_ref_exa_and_ref_can($_POST['ref_exa'], $can_id));
                    $tpi->remove_tpi($_POST['ref_exa'], $can_id);

                    $tab['reponse'] = true;
                    $tab['message']['texte'] = "Candidat(s) supprimé";
                    $tab['message']['type'] = "success";
                } else {
                    $tab['reponse'] = false;
                    $tab['message']['texte'] = "Probleme lors de la suppression des candidats";
                    $tab['message']['type'] = "danger";
                }

            }
        }
    }else{
        //Supprime les candidats de l'examen si ils ont pas de note
        if ($can->remove_cans_exa_if_not_note($_POST['all_can_id'], $_POST['ref_exa'])) {
            $tab['reponse'] = true;
            $tab['message']['texte'] = "Candidat(s) supprimé";
            $tab['message']['type'] = "success";
        } else {
            $tab['reponse'] = false;
            $tab['message']['texte'] = "Probleme lors de la suppression des candidats";
            $tab['message']['type'] = "danger";
        }
    }
}else{//Aucun candidat à supprimer
    $exa2 = new Examen($_POST['ref_exa']);
    $tab['reponse'] = false;
    if($exa2->get_num_nom() == 0) {
        $tab['message']['texte'] = "On ne peut pas supprimer des candidats avec une note ou ayant des experts associés pour le TPI";
    }else{
        $tab['message']['texte'] = "On ne peut pas supprimer des candidats avec une note";
    }

    $tab['message']['type'] = "danger";
}


/////////////
/*
$exa2 = new Examen($_POST['ref_exa']);
if($exa2->get_num_nom() == 0) {
    $tpi = new Tpi();
    foreach ($_POST['all_can_id'] as $can_id) {
        if(!$tpi->has_per_in_tpi($tpi->get_id_tpi_by_ref_exa_and_ref_can($_POST['ref_exa'],$can_id))) {

            $tpi->remove_crt_tpi($tpi->get_id_tpi_by_ref_exa_and_ref_can($_POST['ref_exa'], $can_id));
            $tpi->remove_tpi($_POST['ref_exa'], $can_id);

        }
    }
}*/
////////////////

echo json_encode($tab);
?>
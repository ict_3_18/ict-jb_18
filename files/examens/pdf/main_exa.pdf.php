<?php
session_start();
if (!empty($_POST)) {
    $aut = "USR_EXA";
    require("./../../config/config.inc.php");
    require(WAY . "./includes/secure.inc.php");
    $exa = new Examen($_POST['id_exa']);

    $pdf = new PDFMain_exa();
    $pdf->gen_recap($exa->get_id());

    $pdf->Output();
}
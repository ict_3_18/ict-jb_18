<?php
session_start();
if (!empty($_POST)) {
    $aut = "USR_EXA";
    require("./../../config/config.inc.php");
    require(WAY . "./includes/secure.inc.php");
    $exa = new Examen($_POST['id_exa']);
    $tab_can = $exa->get_grp_cla_can();

    $pdf = new PDFRecap_note();
    $pdf->gen_recap($exa->get_id());
    
    $pdf->Output();
}
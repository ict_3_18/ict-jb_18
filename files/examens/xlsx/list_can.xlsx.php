<?php
/**
 * @author Nicolas Schwab
 * 
 */
session_start();

$aut = "USR_EXA";

require_once("./../../config/config.inc.php");
require_once(WAY . "/includes/autoload.inc.php");
require_once(WAY . "/vendor/autoload.php");

use PhpOffice\PhpSpreadsheet\Cell\DataValidation;
use PhpOffice\PhpSpreadsheet\NamedRange;
use PhpOffice\PhpSpreadsheet\Settings;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Conditional;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$can = new Candidat();
$cla = new Classe();
$ref_exa = $_POST['ref_exa'];
$exa = new Examen($ref_exa);
$liste_can = $can->get_tab_all_can_in_one_exa($ref_exa);
$count_can = count($liste_can);
$nom_complet_mod = "ICT ";

// Nom du module complet
if($exa->get_num_nom() != 0) {
    if(strlen($exa->get_ann()) > 2) {
        $version = substr($exa->get_ann(),-2);
    }
    else {
        $version = $exa->get_ann();
    }
    $nom_complet_mod .= $exa->get_num_nom()."-".$version.strtoupper($exa->get_ver());
}

// Header
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment; filename="'.date("Y-m-d")." - ".$nom_complet_mod.'.xlsx"');

// Formats des nombres
$formatStringThreeDecimals = "0.000";
$formatStringOneDecimal = "0.0";
$formatStringPercentage = "0.00%";

// Création du fichier Excel
$spreadsheet = new Spreadsheet();
$spreadsheet->getProperties()
    ->setCreator("ICT-JB.net")
    ->setTitle("Liste des candidats du module ".$nom_complet_mod)
    ->setSubject("Liste de candidats")
    ->setKeywords("cco ; examens ; liste ; candidats ; ict-jb.net");
$sheet = $spreadsheet->getActiveSheet();
$sheet->setTitle("Liste des candidats");

// Localisation (nécessaire pour l'interprétation des formules en français)
$locale = 'fr';
$validLocale = Settings::setLocale($locale);

// Styles de cellules
$styleTitre = [
    'font' => [
        'bold' => true,
        'size' => 16
    ]
];
$styleDate = [
    'font' => [
        'bold' => true
    ],
    'alignment' => [
        'horizontal' => Alignment::HORIZONTAL_RIGHT
    ]
];
$styleAlignRight = [
    'alignment' => [
        'horizontal' => Alignment::HORIZONTAL_RIGHT
    ]
];
$styleAlignCenter = [
    'alignment' => [
        'horizontal' => Alignment::HORIZONTAL_CENTER
    ]
];
$styleBoldCenter = [
    'font' => [
        'bold' => true
    ],
    'alignment' => [
        'horizontal' => Alignment::HORIZONTAL_CENTER
    ]
];
$styleHeader = [
    'alignment' => [
        'horizontal' => Alignment::HORIZONTAL_CENTER,
        'vertical' => Alignment::VERTICAL_CENTER,
        'wrapText' => true
    ],
    'font' => [
        'bold' => true
    ]
];

// Ecriture de "l'entête"
$sheet->setCellValue('A1',$nom_complet_mod);
$sheet->setCellValue('A2', $exa->get_nom_mod($ref_exa));

$sheet->setCellValue('G1', $exa->get_date_hrs_jour_date()." 20".$exa->get_ann());

$sheet->setCellValue('F3', "Nb de pts max : ");

$sheet->setCellValue('A5', "N°");
$sheet->setCellValue('B5', "Nom prénom");
$sheet->setCellValue('C5', "Classe");
$sheet->setCellValue('D5', "Nb pts");
$sheet->setCellValue('E5', "Note 1/1000");
$sheet->setCellValue('F5', "Note 1/2");
$sheet->setCellValue('G5', "Echec");

// Styles de "l'entête"
$sheet->getStyle('A1:A2')->applyFromArray($styleTitre);
$sheet->getStyle('A5:G5')->applyFromArray($styleHeader);
$sheet->getStyle('G1')->applyFromArray($styleDate);
$sheet->getStyle('F3')->applyFromArray($styleAlignRight);
$sheet->getStyle('G3')->applyFromArray($styleBoldCenter);

// Styles et écriture du "corps"
$start_can_row = 6;
$cell_row = $start_can_row;
if($count_can > 0) {
    foreach ($liste_can as $temp_can) {
        $candidat = new Candidat($temp_can["id_can"]);

        $styleClasse = [
            'fill' => [
                'fillType' => Fill::FILL_SOLID,
                'startColor' => [
                    'rgb' => hexa3_to_hexa6($can->get_color_from_id($candidat->get_id()))
                ]
            ]
        ];

        $cla_candidat = new Classe($cla->get_cla_with_id($candidat->get_classe())[0]["id_cla"]);
        $grp_candidat = new Groupe($cla_candidat->get_ref_grp());
        $sheet->getCell("F1")->setValue($cla->get_ref_grp());

        $sheet->getCell('A'.$cell_row)->setValue($candidat->get_no_candidat_minus());
        $sheet->getStyle('A'.$cell_row)->applyFromArray($styleBoldCenter);

        $sheet->getCell('B'.$cell_row)->setValue($candidat->get_nom()." ".$candidat->get_prenom());

        $sheet->getCell('C'.$cell_row)->setValue($grp_candidat->get_nom_grp()." - ".$cla_candidat->get_nom_cla());
        $sheet->getStyle("C".$cell_row)->applyFromArray($styleClasse);

        $sheet->getStyle('E'.$cell_row)->getNumberFormat()->setFormatCode($formatStringThreeDecimals);

        $sheet->getStyle('F'.$cell_row)->getNumberFormat()->setFormatCode($formatStringOneDecimal);
        $sheet->getStyle('F'.$cell_row)->applyFromArray($styleBoldCenter);

        $sheet->getStyle('G'.$cell_row)->applyFromArray($styleAlignCenter);

        $cell_row++;
    }
}

$sheet->getStyle('D5:F'.($cell_row-1))->applyFromArray($styleAlignCenter);

//Style et écriture du "pied de page"
$sheet->getCell("C".$cell_row)->setValue("Moyennes : ");
$sheet->getStyle("C".$cell_row)->applyFromArray($styleAlignRight);
$sheet->getStyle("D".$cell_row.":G".$cell_row)->applyFromArray($styleBoldCenter);
$sheet->getStyle("D".$cell_row.":F".$cell_row)->getNumberFormat()->setFormatCode($formatStringThreeDecimals);
$sheet->getCell("F".($cell_row+1))->setValue("Taux d'échec : ");
$sheet->getStyle("F".($cell_row+1))->applyFromArray($styleAlignRight);
$sheet->getStyle("G".($cell_row+1))->getNumberFormat()->setFormatCode($formatStringPercentage);

// Nommage des plages
$spreadsheet->addNamedRange(new NamedRange('PointsMax', $sheet, 'G3:G3'));
$spreadsheet->addNamedRange(new NamedRange('Points', $sheet, 'D6:D'.($cell_row-1)));
$spreadsheet->addNamedRange(new NamedRange('NotesMillieme', $sheet, 'E6:E'.($cell_row-1)));
$spreadsheet->addNamedRange(new NamedRange('NotesDemi', $sheet, 'F6:F'.($cell_row-1)));
$spreadsheet->addNamedRange(new NamedRange('Echecs', $sheet, 'G6:G'.($cell_row-1)));
$spreadsheet->addNamedRange(new NamedRange('MoyPts', $sheet, 'D'.($cell_row).':D'.($cell_row)));
$spreadsheet->addNamedRange(new NamedRange('MoyNotesMil', $sheet, 'E'.($cell_row).':E'.($cell_row)));
$spreadsheet->addNamedRange(new NamedRange('MoyNotesDemi', $sheet, 'F'.($cell_row).':F'.($cell_row)));
$spreadsheet->addNamedRange(new NamedRange('NbEchecs', $sheet, 'G'.($cell_row).':G'.($cell_row)));


// Formules
for($i = 0; $i < count($liste_can); $i++) {
    $row = $start_can_row + $i;
    /*  Calcule la note au millième du candidat
     *  =SI(
     *      [nombre de points obtenus] <> "",
     *      SIERREUR(
     *          ARRONDI.INF(
     *              5/PointsMax*[nombre de points obtenus]+1,
     *              2
     *          );
     *          ""
     *      ),
     *      ""
     *  )
     */
    $sheet->getCellByColumnAndRow(5, $row)->setValue('=IF(D'.$row.' <> "", IFERROR(MROUND(5/PointsMax*D'.$row.'+1,0.001), ""), "")');

    /*  Calcule la note au demi du candidat
     *  =SIERREUR(
     *      SI(
     *          [moyenne au millième] > 0;
     *          ARRONDI.AU.MULTIPLE(
     *              [moyenne au millième];
     *              0,5
     *          ),
     *          ""
     *      );
     *      ""
     *  )
     */
    $sheet->getCellByColumnAndRow(6, $row)->setValue('=IFERROR(IF(E'.$row.' > 0, MROUND(E'.$row.',0.5), ""), "")');

    /*  Calcule si le candidat est en situation d'échec
     *  =SIERREUR(
     *      SI(
     *          [moyenne au demi] > 0;
     *          SI(
     *              [moyenne au demi] >= 4;
     *              "";
     *              "X"
     *          );
     *          ""
     *      );
     *      ""
     *  )
     */
    $sheet->getCellByColumnAndRow(7, $row)->setValue('=IFERROR(IF(F'.$row.' > 0, IF(F'.$row.' >= 4 , "", "X"), ""), "")');

    /*  Fait la moyenne des points des candidats sur l'examen
     *  =SIERREUR(
     *      MOYENNE(
     *          Points
     *      );
     *      ""
     *  )
     */
    $sheet->getCell('D'.$cell_row)->setValue('=IFERROR(AVERAGE(Points), "")');

    /*  Fait la moyenne des notes au millième des candidats sur l'examen
     *  =SIERREUR(
     *      MOYENNE(
     *          NotesMillieme
     *      );
     *      ""
     *  )
     */
    $sheet->getCell('E'.$cell_row)->setValue('=IFERROR(AVERAGE(NotesMillieme), "")');

    /*  Fait la moyenne des notes au demi des candidats sur l'examen
     *  =SIERREUR
     *      MOYENNE(
     *          NotesDemi
     *      );
     *      ""
     *  )
     */
    $sheet->getCell('F'.$cell_row)->setValue('=IFERROR(AVERAGE(NotesDemi), "")');

    /*  Compte le nombre d'échecs sur l'examen
     *  =NB.SI(
     *      Echecs;
     *      "X"
     *  )
     */
    $sheet->getCell('G'.$cell_row)->setValue('=COUNTIF(Echecs,"X")');
}

/*  Calcule le taux d'échec des candidats sur l'examen
 *  =NbEchecs/COUNTA(Points)
 */
$sheet->getCell("G".($cell_row+1))->setValue('=NbEchecs/COUNTA(Points)');

// Styles de bordures
$bordersAll = [
    'borders' => [
        'allBorders' => [
            'borderStyle' => Border::BORDER_THIN,
            'color' => [
                'rgb' => '000000'
            ]
        ]
    ]
];
$bordersOutline = [
    'borders' => [
        'outline' => [
            'borderStyle' => Border::BORDER_THIN,
            'color' => [
                'rgb' => '000000'
            ]
        ]
    ]
];
$bordersOutlineThick = [
    'borders' => [
        'outline' => [
            'borderStyle' => Border::BORDER_THICK,
            'color' => [
                'rgb' => '000000'
            ]
        ]
    ]
];

// Styles de bordures
$sheet->getStyle('G3')->applyFromArray($bordersOutlineThick);
$sheet->getStyle('A5:G'.($cell_row-1))->applyFromArray($bordersAll);
$sheet->getStyle('C'.$cell_row.':F'.$cell_row)->applyFromArray($bordersOutline);
$sheet->getStyle('A5:G5')->applyFromArray($bordersOutlineThick);

// Mise en forme conditionnelle
$conditionalStyleEchec = [
    'fill' => [
        'fillType' => Fill::FILL_SOLID,
        'startColor' => [
            'rgb' => 'FF0000'
        ],
        'endColor' => [
            'rgb' => 'FF0000'
        ]
    ],
    'font' => [
        'bold' => true,
        'color' => [
            'rgb' => 'FFFFFF'
        ]
    ]
];
$conditionalEchec = new Conditional();
$conditionalEchec->setConditionType(Conditional::CONDITION_CONTAINSTEXT);
$conditionalEchec->setOperatorType(Conditional::OPERATOR_CONTAINSTEXT);
$conditionalEchec->setText("X");
$conditionalEchec->getStyle()->applyFromArray($conditionalStyleEchec);
$conditionalStylesEchec = array($conditionalEchec);
$sheet->getStyle('G5:G'.($cell_row-1))->setConditionalStyles($conditionalStylesEchec);

$conditionalStyleNote = [
    'font' => [
        'color' => [
            'rgb' => 'FF0000'
        ]
    ]
];
$conditionalNote = new Conditional();
$conditionalNote->setConditionType(Conditional::CONDITION_CELLIS);
$conditionalNote->setOperatorType(Conditional::OPERATOR_LESSTHAN);
$conditionalNote->addCondition('4');
$conditionalNote->getStyle()->applyFromArray($conditionalStyleNote);
$conditionalStylesNote = array($conditionalNote);
$sheet->getStyle('E6:F'.($cell_row-1))->setConditionalStyles($conditionalStylesNote);

// Validation des données
$validationPtsMax = $sheet->getCell('G3')->getDataValidation();
$validationPtsMax->setType(DataValidation::TYPE_WHOLE);
$validationPtsMax->setErrorStyle(DataValidation::STYLE_STOP);
$validationPtsMax->setShowErrorMessage(true);
$validationPtsMax->setAllowBlank(true);
$validationPtsMax->setShowInputMessage(true);
$validationPtsMax->setErrorTitle("Erreur d'entrée");
$validationPtsMax->setError("Seuls des nombres entiers positifs sont autorisés !");
$validationPtsMax->setPromptTitle("Entrées autorisées");
$validationPtsMax->setPrompt("Seuls des nombres entiers positifs sont autorisés.");
$validationPtsMax->setOperator(DataValidation::OPERATOR_GREATERTHANOREQUAL);
$validationPtsMax->setFormula1(0);

for($i = 0; $i < count($liste_can); $i++) {
    $row = $start_can_row + $i;
    $validationPts = $sheet->getCellByColumnAndRow(4, $row)->getDataValidation();
    $validationPts->setType(DataValidation::TYPE_DECIMAL);
    $validationPts->setErrorStyle(DataValidation::STYLE_STOP);
    $validationPts->setShowErrorMessage(true);
    $validationPts->setAllowBlank(true);
    $validationPts->setShowInputMessage(true);
    $validationPts->setErrorTitle("Erreur d'entrée");
    $validationPts->setError("Seuls des nombres positifs sont autorisés !");
    $validationPts->setPromptTitle("Entrées autorisées");
    $validationPts->setPrompt("Seuls des nombres positifs sont autorisés.");
    $validationPts->setOperator(DataValidation::OPERATOR_GREATERTHANOREQUAL);
    $validationPts->setFormula1(0);
}

// Filtres
//$sheet->setAutoFilter('C5:C5');
//$sheet->setAutoFilter('G5:G5');

// Largeur des colonnes
$sheet->getColumnDimension('A')->setWidth(6.5);
$sheet->getColumnDimension('B')->setAutoSize(true);
$sheet->getColumnDimension('C')->setAutoSize(true);
$sheet->getColumnDimension('D')->setWidth(7.0);
$sheet->getColumnDimension('E')->setWidth(7.0);
$sheet->getColumnDimension('F')->setWidth(7.0);

// Une page de largeur en impression
$sheet->getPageSetup()->setFitToWidth(1);
$sheet->getPageSetup()->setFitToHeight(0);

// Centré horizontalement en impression
$sheet->getPageSetup()->setHorizontalCentered(true);

$sheet->setSelectedCell('A1');

$writer = new Xlsx($spreadsheet);
$writer->save('php://output');

/**
 * Convertit une couleur en hexa à 3 digits en couleur en hexa à 6 digit
 * @param $hexadecimal : format => #RGB
 * @return array|string : format => #RRGGBB
 */
function hexa3_to_hexa6($hexadecimal) {
    $hexa = str_split($hexadecimal);
    $hexa =
        $hexa[1].
        $hexa[1].
        $hexa[2].
        $hexa[2].
        $hexa[3].
        $hexa[3];

    return $hexa;
}
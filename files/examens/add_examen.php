<?php
session_start();
$aut = "ADM_USR";
require("./../config/config.inc.php");
require(WAY . "./includes/secure.inc.php");
require_once(WAY . "/includes/head.inc.php");
require_once(WAY . "/includes/autoload.inc.php");


$exa = new Examen();

$mod = new Module();
$salle = new Salle();
$thisYear = date("y");
?>

<!-- Parties ouvertes -->
<div class="container"><div class="row">
        <div class="header">
            <h3 id="first_title">Ajout d'un examen</h3>
        </div>
    </div>
    <div class="panel panel-primary ">
        <div class="panel-heading" id="second_title">
            Ajouter un examen
        </div>
        <div class="panel-body">
            <form id="add_examen_form">
                
                <!-- Numero et nom de l'examen -->
                <div class="form-group row">
                    <label for="num_nom_exa" class="col-sm-2 col-form-label">Numéro Examen</label>
                    <div class="col-sm-8">
                        <select class="form-control" id="num_nom_exa" name="num_nom_exa">
                            <option disabled selected value> Veuillez selectionner un numéro. </option>
                            <?php 
                            $options = $mod->get_all("num_mod");
                            foreach($options AS $opt){
                                echo "<option value=".$opt['id_desc_mod'].">".$opt['num_mod']." - ".$opt['nom_mod']."</option>";
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <!-- Année -->
                <div class="form-group row">
                    <label for="ann_exa" class="col-sm-2 col-form-label">Année de l'examen</label>
                    <div class="col-sm-3">
                        <select class="form-control" id="ann_exa" name="ann_exa">
                            <option disabled selected value> Veuillez selectionner une année. </option>
                            <option value="<?php echo ($thisYear+1) ?>"><?php echo ($thisYear+1) ?></option>
                            <option value="<?php echo ($thisYear) ?>"><?php echo ($thisYear) ?></option>
                            <option value="<?php echo ($thisYear-1) ?>"><?php echo ($thisYear-1) ?></option>
                        </select>
                    </div>

                <!-- Version de l'examen -->
                    <label for="ver_exa" class="col-sm-2 col-form-label">Version de l'examen</label>
                    <div class="col-sm-3">
                        <select class="form-control" id="ver_exa" name="ver_exa">
                            <option disabled selected value> Veuillez selectionner une option. </option>
                            <option value="A">Version A</option>
                            <option value="B">Version B</option>
                            <option value="C">Version C</option>
                        </select>
                    </div>
                </div>
                
                <!-- Date et Heure -->
                <div class="form-group row">
                    <label for="date_hrs_exa" class="col-sm-2 col-form-label">Date et heure de l'examen</label>
                    <div class="col-sm-3">
                        <div class="input-group date">
                            <input type="datetime-local" class="form-control" id="date_hrs_exa" name="date_hrs_exa">
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>

                <!-- Durée -->
                    <label for="dur_exa" class="col-sm-2 col-form-label">Durée de l'examen</label>
                    <div class="col-sm-3">
                        <input type="number" class="form-control" id="dur_exa" name="dur_exa" placeholder="Durée en minutes">
                    </div>
                </div>


                <!-- Domaine de l'examen -->
                <div class="form-group row">
                    <label for="id_dom" class="col-sm-2 col-form-label">Domaine de l'examen</label>
                    <div class="col-sm-4">
                        <select class="form-control" id="id_dom" name="id_dom">
                            <option disabled selected value> Veuillez selectionner un domaine. </option>
                            <?php
                            $options_dom = $exa->get_all_domaines();
                            foreach ($options_dom as $opt) {
                                echo '<option value="';
                                echo $opt["id_dom"].'">';
                                echo $opt["nom_dom"];
                                echo "</option>";
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <!-- Sale -->
                <div class="form-group row" id="div_sal" style="display:none">
                    <label for="sal_exa" class="col-sm-2 col-form-label">Salle de l'examen</label>
                    <div class="col-sm-4">
                        <select class="form-control" id="sal_exa" name="sal_exa">
                            <option disabled selected value>Veuillez selectionner une salle.</option>
                            <?php
                            $salles = $salle->get_nom_salles();
                            
                            foreach ($salles as $sal){
                                echo '<option value="';
                                echo $sal['id_sle'].'">';
                                echo $sal['nom_sle'];
                                echo "</option>";
                            }
                            ?>   
                        </select>
                        <select style="display:none" class="form-control" id="sal_exa2" name="sal_exa">
                            <option value="">Deuxième salle</option>
                            <?php
                            
                            foreach ($salles as $sal){
                                echo '<option value="';
                                echo $sal['id_sle'].'">';
                                echo $sal['nom_sle'];
                                echo "</option>";
                            }
                            ?>
                        </select>
                        <select style="display:none" class="form-control" id="sal_exa3" name="sal_exa">
                            <option value="">Troisième salle</option>
                            <?php
                            
                            foreach ($salles as $sal){
                                echo '<option value="';
                                echo $sal['id_sle'].'">';
                                echo $sal['nom_sle'];
                                echo "</option>";
                            }
                            ?>
                        </select>
                        <select style="display:none" class="form-control" id="sal_exa4" name="sal_exa">
                            <option value="">Quatrième salle</option>
                            <?php
                            
                            foreach ($salles as $sal){
                                echo '<option value="';
                                echo $sal['id_sle'].'">';
                                echo $sal['nom_sle'];
                                echo "</option>";
                            }
                            ?>
                        </select>
                        <select style="display:none" class="form-control" id="sal_exa5" name="sal_exa">
                            <option value="">Cinquième salle</option>
                            <?php
                            
                            foreach ($salles as $sal){
                                echo '<option value="';
                                echo $sal['id_sle'].'">';
                                echo $sal['nom_sle'];
                                echo "</option>";
                            }
                            ?>
                        </select>
                        <select style="display:none" class="form-control" id="sal_exa6" name="sal_exa">
                            <option value="">Sixième salle</option>
                            <?php
                            
                            foreach ($salles as $sal){
                                echo '<option value="';
                                echo $sal['id_sle'].'">';
                                echo $sal['nom_sle'];
                                echo "</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <a class="btn" id="remove_salle"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                
                <!-- ID pour la modification par la suite (après l'ajout) -->
                <input type="hidden" id="id_exa" value="">
                
                <!-- Bouton submit et reset -->
                <div class="form-group row">
                    <div class="col-sm-offset-8 col-sm-2">
                        <input type="submit" class="form-control btn btn-primary submit" id="submit_conf" value="Ajouter">
                    </div>
                    <div class="col-sm-2">
                        <input type="reset" class="form-control btn btn-warning" id="reset_conf" value="Annuler">
                    </div>
                </div>

            </form>

        </div>
    </div>    
</div>

</div>
<script src="./js/add_examen.js"></script>
<?php
session_start();
$aut = "ADM_USR;ADM_EXA;USR_EXA";
require("./../config/config.inc.php");
require(WAY . "./includes/secure.inc.php");
require_once(WAY . "/includes/head.inc.php");
$exa = new Examen();
$tab_exa = $exa->get_future_exam();
$tab_profs = array();
$tab_exp = array();

$pers = new Personne();
$tab_per = $pers->get_all_expert_actif();
?>

<div class="col-md-8">
    <div class="panel panel-primary ">
        <div class="panel-heading">
            Liste des Examens à venir
        </div>
        <div class="panel-body" id="list_exa">
            <pre><?php
            /*echo "<pre>";
            print_r($tab_exa);
            echo "</pre>";*/

            foreach($tab_exa AS $examen){
                $exa = new Examen($examen["id_exa"]);
                $date_exa = strtoupper($exa->get_date_hrs_jour_date()." ").date("Y",strtotime($exa->get_date_hrs()));
                $heure_debut = $exa->get_heure_db_exa();
                $duree = $examen['duree_exa'];
                $heure_fin = date("H:i",strtotime($heure_debut) + $duree*60);
                $heures_exa = "De ".$heure_debut." à ".$heure_fin;

                echo "<b>".$date_exa."</b>";
                echo "<br>";
                echo $heures_exa;
                echo "<br>";
                echo "<b>ICT-".$exa->get_num_nom()."</b>";
                echo " - ";
                echo $exa->get_nom();
                echo "<br>";
                echo "Nombre de candidats : ".$examen['nb_can'];
                echo "<br>";

                $str = "Enseignants : ";
                $tab_profs = $exa->get_profs_exam($examen["id_exa"]);
                foreach($tab_profs AS $profs) {
                    $str .= $profs["prenom_per"]." ".$profs["nom_per"].", ";
                }
                $str = substr($str,0,-2);
                echo $str."<br>";

                $str = "Experts : ";
                $tab_exp = $exa->get_expert_exam($examen["id_exa"]);
                foreach($tab_exp AS $exp) {
                    $str .= $exp["prenom_per"]." ".$exp["nom_per"].", ";
                }
                $str = substr($str,0,-2);
                echo $str."<br>";
                echo "<br>";
            }
            ?>
                </pre>
        </div>
    </div>
</div>
<div class="col-md-4">
    <div class="panel panel-primary ">
        <div class="panel-heading">
            E-mails de tous les experts
        </div>
        <div class="panel-body" id="list_exa">
            <?php

            echo "<b>Experts : </b><br>";
            foreach ($tab_per AS $personnes){
                //$per = new Personne($personnes["id_per"]);

                echo $personnes["email_per"]."; ";
            }

            ?>
        </div>
    </div>
</div>
</body>
</html>
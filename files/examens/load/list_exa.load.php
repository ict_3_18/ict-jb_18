<table class="table table-hover table-dark table-responsive">
    <?php
    session_start();
    $aut = "ADM_USR;ADM_EXA;USR_EXA";
    require("./../../config/config.inc.php");
    require(WAY . "./includes/secure.inc.php");

    $exa = new Examen();
    $mod = new Module();
    $per = new Personne($_SESSION['id']);
    $tab_exa = $exa->list_exam();

    function row_head($per, $date)
    {
        echo "<tr class='head_jour'>"
            . "<th>" . ucfirst($date) . "</th>"
            . "<th>Heure</th>"
            . "<th>Candidats</th>"
            . "<th>Salles</th>"
            . "<th></th>"
            . "<th>Enseignants</th>";

        if ($per->check_aut("ADM_EXA")) {
            echo '<th></th>';
        }

        echo '<th>Experts</th>'
            . '<th style="text-align:center;">Matériel</th>';

        if ($per->check_aut("ADM_EXA")) {
            echo '<th style="text-align:center;"></th>';
        }
        echo "</tr>";
    }

    $date_old = 0;
    foreach ($tab_exa as $examen) {
        $exa = new Examen($examen['id_exa']);

        if ($per->is_enseignant_exa($exa->get_id()) || $per->is_expert_exa($exa->get_id()) || $per->check_aut("ADM_EXA") || $_POST["all_exa"]) {
            $date_debut = explode(" ", $examen['date_heure_exa'])[0];
            $date = date("d.m.y", strtotime($date_debut));
            $heure = explode(" ", $examen['date_heure_exa'])[1];
            if ($date != $date_old) {
                row_head($per, $exa->get_date_hrs_jour_date());
                $date_old = $date;
            }

            if ($examen['definitif_exa']) {
                echo '<tr class="exa_def">';
            } else {
                echo '<tr class="exa_no_def">';
            }
            ?>

            <!-- N° exa -->
            <td>
                <h4>
                    <b> <?php
                        $nom_exa = "ICT ";
                        if ($exa->get_num_nom() != 0) {
                            if(strlen($exa->get_ann()) > 2) {
                                $version = substr($exa->get_ann(),-2);
                            }
                            else {
                                $version = $exa->get_ann();
                            }

                            $nom_exa .= $exa->get_num_nom()."-".$version.strtoupper($exa->get_ver());

                        } else {
                            $nom_exa .= $exa->get_nom();
                        }
                        echo $nom_exa
                        ?>
                    </b>
                </h4>
            </td>

            <!--  Heure -->
            <td>
                <?php
                if ($examen['duree_exa'] != null) {
                    $duree = $examen['duree_exa'];
                    $heure_debut = $exa->hrs_min_sec_to_min($heure);
                    $calc = $heure_debut + $duree;
                    $heure_fin = $exa->min_to_hrs_min_sec($calc);
                } else {
                    $heure_fin = "??:??:??";
                }
                echo substr($heure, 0, 5) . " - " . substr($heure_fin, 0, 5);
                ?></td>

            <!-- Candidats -->
            <td>
                <form method="post" action="<?php echo URL . "examens/xlsx/list_can.xlsx.php"; ?>">
                    <input type="hidden" name="ref_exa" id="hiddenField" value="<?= $exa->get_id() ?>">
                    <button type="submit" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Obtenir la liste des candidats"><span class="glyphicon glyphicon-list"></span></button>
                </form>
                &nbsp;
                <?php
                $color = $exa->get_couleur_examen($examen['id_exa']);
                //print_r($color);
                $nb_can = "";
                ?>

                <?php

                if (sizeof($color) > 1) {
                    foreach ($color AS $couleur) {
                        $nb_can .= '<div class="color ';
                        $nb_can .= '" style="background-color:' . $couleur['color_grp'] . ';"'
                            . ' data-toggle="tooltip" title="' . $couleur['nb_can'] . ' candidats ' . $couleur['nom_grp'] . '" >'
                            . $couleur['nb_can'] . '</div>';
                        $nb_can .= " + ";
                    }


                    $nb_can = substr($nb_can, 0, -3);
                    $nb_can .= ' = <div class="color total" data-toggle="tooltip" title="' . $examen['Candidats'] . ' candidats" ><b>' . $examen['Candidats'] . '</b></div>';
                } else if (sizeof($color) == 1) {
                    $couleur = $color[0];
                    $nb_can .= '<div class="color';
                    $nb_can .= '" style="background-color:' . $couleur['color_grp'] . ';"'
                        . ' data-toggle="tooltip" title="' . $couleur['nb_can'] . ' candidats ' . $couleur['nom_grp'] . '" >'
                        . $couleur['nb_can'] . '</div>';

                }

                echo $nb_can;
                ?>
            </td>
            <!-- Salles -->
            <td>
                <?php
                $tab_salles = $exa->get_salles_exa();
                $salles = "";

                foreach ($tab_salles as $sal) {
                    $salles .= $sal['nom_sle'] . " / ";
                }
                $salles = substr($salles, 0, -3);
                echo $salles
                ?>
            </td>
            <!-- Rapport -->
            <td>
                <form method="post" action="<?php echo URL . "rapport_exa/rapport.php"; ?>"
                      id="TestForm">
                    <input type="hidden" name="id_exa" value="<?= $exa->get_id(); ?>">
                    <input class="btn btn-primary btn-sm" type="submit" value="Rapport">
                </form>
            </td>
            <!-- Enseigant affichage-->
            <td>
                <?php
                $profs = null;
                $str_mail_all = null;
                $tab_prof = $exa->get_profs_exam($examen['id_exa']);
                if ($tab_prof !== null) {
                    foreach ($tab_prof as $key => $prof) {
                        $profs = $per->get_initiales($prof['prenom_per']) . " " . $prof['nom_per'] . "<br>";
                        $str_mail_all .= $prof['email_per'] . ";";
                        ?>
                        <a href="mailto:<?= $prof['email_per']."?subject=".$nom_exa ?>"><?= $profs ?></a>
                        <?php
                    }
                }
                ?>
            </td>
            <!-- Expert/Enseigant btn + Mail broadcast btn -->
            <?php

            $experts = null;
            $nom = null;
            $tab_expert = $exa->get_expert_exam($examen['id_exa']);
            $str_experts = "";
            if ($tab_expert !== null) {
                foreach ($tab_expert as $key => $expert) {

                    $experts = $per->get_initiales($expert['prenom_per']) . " " . $expert['nom_per'] . "<br>";
                    $str_mail_all .= $expert['email_per'] . ";";
                    $str_experts .=  "<a href=\"mailto:".$expert['email_per'] ."?subject=".$nom_exa."\">".$experts."</a>";
                }
            }
            ?>

            <?php if ($per->check_aut("ADM_EXA")) { ?>
                <td style="text-align:center;">
                    <form id="ajout_expert" action="<?= URL ?>examens/experts_examen.php"
                          method="post">
                        <input type="hidden" name="ref_exa" id="hiddenField"
                               value="<?= $exa->get_id() ?>">
                        <button type="submit" class="button_glyph" data-toggle="tooltip"
                                title="Ajout d'experts ou d'enseignants">
                            <span class="glyphicon glyphicon-user black"></span>
                        </button>
                        <br>
                    </form>
                        <input type="hidden" name="ref_exa" id="hiddenField" value="<?= $exa->get_id() ?>">
                        <a href="mailto:<?= $str_mail_all ?>">
                        <button type="submit" class="button_glyph" data-toggle="tooltip"
                                title="Envoie un mail aux experts et aux enseignants">
                            <span class="glyphicon glyphicon-envelope black"></span>
                        </button>
                        </a>
                </td>
            <?php } ?>
            <!-- Experts affichage -->
            <td>
                <?php
                echo $str_experts;
                ?>
            </td>
            <!-- Materiel -->
            <td style="text-align:center;">
                <?php

                echo '<form id="dispo_exa" action="' . URL . 'examens/materiel_examen.php" method="post">';
                //$exa = new Examen($examen['ref_exa']);

                $title = "<h4>Matériel autorisé</h4>";
                $title .= str_replace(chr(10), "<br>", $exa->get_materiel_exa());

                if ($exa->get_salle_inf_exa() == 1) {
                    $title .= "<br><b>Salle info</b> : oui";
                } else {
                    $title .= "<br><b>Salle info</b> : non";
                }

                if ($exa->get_lec_exa() == 1) {
                    $title .= "<br><b>Données perso (R:)</b> : oui";
                } else {
                    $title .= "<br><b>Données perso (R:)</b> : non";
                }
                if ($exa->get_reseau_exa() == 1) {
                    $title .= "<br><b>Réseau disponible</b> : oui";
                } else {
                    $title .= "<br><b>Réseau disponible</b> : non";
                }

                $per = new Personne($_SESSION['id']);

                if ($per->is_enseignant_exa($exa->get_id()) || $per->check_aut("ADM_EXA")) {
                    echo '<input type="hidden" name="ref_exa" id="hiddenField" value="' . $exa->get_id() . '">';
                    echo '<button type="submit" class="button_glyph" data-toggle="tooltip" data-html="true" title="' . $title . '">';
                    echo '<span class="glyphicon glyphicon-briefcase text-primary"></span>';
                    echo '</button>';
                } else {

                    echo '<span class="glyphicon glyphicon-briefcase text-muted" data-toggle="tooltip" data-html="true" title="' . $title . '"></span>';
                }
                echo '</form>';
                ?>
            </td>
            <!-- Ajout candidat -->
            <?php
            if ($per->check_aut("ADM_EXA")) {
                echo '<td style="text-align:center;">';
                echo '<form target="_blank" id="code_barre_exa" action="'.URL.'examens/pdf/main_exa.pdf.php" method="post">';
                echo '<input type="hidden" name="id_exa" id="hiddenField" value="' . $exa->get_id() . '">';
                echo '<button type="submit" class="button_glyph" data-toggle="tooltip" title="Code barre">';
                echo '<span class="glyphicon glyphicon-barcode black"></span>';
                echo '</button>';
                echo '</form>';
                echo '&nbsp;&nbsp;';
                echo '<form id="ajout_can" action="' . URL . 'examens/add_candidat_examen.php" method="post">';
                echo '<input type="hidden" name="ref_exa" id="hiddenField" value="' . $exa->get_id() . '">';
                echo '<button type="submit" class="button_glyph" data-toggle="tooltip" title="Ajout de candidats">';
                echo '<span class="glyphicon glyphicon-education black"></span>';
                echo '</button>';
                echo '</form>';
                echo '&nbsp;&nbsp;';
                echo '<form id="ajout_note" action="' . URL . 'examens/add_note_candidat.php" method="post">';
                echo '<input type="hidden" name="id_exa" id="id_exa" value="' . $exa->get_id() . '">';
                echo '<button type="submit" class="button_glyph" data-toggle="tooltip" title="Ajout des notes">';
                echo '<span class="glyphicon glyphicon-book black"></span>';
                echo '</button>';
                echo '</form>';
                echo '&nbsp;&nbsp;';
                echo '<form id="modif_exa" action="' . URL . 'examens/modify_examen.php" method="post">';
                echo '<input type="hidden" name="ref_exa" id="hiddenField" value="' . $exa->get_id() . '">';
                echo '<button type="submit" class="button_glyph" data-toggle="tooltip" title="Modification de l\'examen">';
                echo '<span class="glyphicon glyphicon-cog black"></span>';
                echo '</button>';
                echo '</form>';
                echo '</td>';
            }
            ?>
            </tr>
            <?php
        }
    }
    ?>
</table>

<script>
    $('[data-toggle="tooltip"]').tooltip();
</script>
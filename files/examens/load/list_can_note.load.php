<?php
session_start();
$aut = "ADM_USR";
require_once("./../../config/config.inc.php");
require_once(WAY . "./includes/secure.inc.php");
require_once(WAY . "/includes/autoload.inc.php");
$can = new Candidat();
$id_exa = $_POST['ref_exa'];

$exa =  new Examen($id_exa);
$tab_can = $exa->get_can();
$count_candidats = count($tab_can); 

if ($count_candidats !== 0) {
    ?>
    <div class="panel panel-primary">
        <div class="panel-heading">
            <span id="number_can_already_in_exa"><?php echo $count_candidats ; ?></span> candidats 
        </div>
        <div class="panel-body">
            <table class="table" id="can_in_exa">
                <thead>
                    <tr>
                        <th>Candidat</th>
                        <th>Numéro</th>
                        <th>Note</th>
						<th>Remédié</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($tab_can as $can) {
                        echo "<tr class=\"can\">";
                            echo "<td class=\"nom_can no_hover\">" . $can['nom_can'] . " " . $can['prenom_can'] . "</td>";
                            echo "<td class=\"no_hover\">" . substr($can['no_candidat_can'], -3) . "</td>";
                            echo "<td class=\"col-md-2\" style=\"background-color:white;\">";
                            if ($can['note_can'] == 0) {
                                echo "<input type=\"number\" max=\"6\" min=\"1\" step=\"0.5\" id_exa=\"".$exa->get_id()."\" id_can=\"".$can['id_can']."\">";
                            }
                            echo "</td>";
							echo "<td class=\"col-md-2\" style=\"background-color:white;\">";
                            if ($can['note_can'] == 0) {
                                echo "<input type=\"checkbox\" >";
                            }
                            echo "</td>";
                        echo '</tr>';
                    }
                    ?>
					
                </tbody>
            </table>
        </div>
    </div>
    <?php
} else {
    echo "<div class=\"alert alert-info\" role=\"alert\">";
    echo "Aucun candidat dans l'examen";
    echo "</div>";
}
?>
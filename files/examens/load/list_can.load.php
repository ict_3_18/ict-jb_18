<?php
session_start();
$aut = "ADM_USR";
require_once("./../../config/config.inc.php");
require_once(WAY . "./includes/secure.inc.php");
require_once(WAY . "/includes/autoload.inc.php");
$can = new Candidat();
$tpi = new Tpi();
$ref_exa = $_POST['ref_exa'];
$count_candidats = count($can->get_tab_all_can_in_one_exa($ref_exa));
if ($count_candidats !== 0) {
    ?>
    <div class="panel panel-primary">
        <div class="panel-heading">
            <span id="number_can_already_in_exa"><?php echo $count_candidats ; ?></span> candidats
        </div>
        <div class="panel-body">
            <table class="table" id="can_in_exa">
                <thead>
                <tr>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Numéro</th>
                    <th><?php echo "<button class=\"btn btn-danger btn-xs remove_all_can_exa center-block\">Supprimer tout</button>"; ?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($can->get_tab_all_can_in_one_exa($ref_exa) as $row_can) {
                    echo "<tr class=\"row_can\" style=\"background-color:" . $can->get_color_from_id($row_can['id_can']) . ";\">";
                    echo "<td class=\"nom_can no_hover\">" . $row_can['nom_can'] . "</td>";
                    echo "<td class=\"no_hover\">" . $row_can['prenom_can'] . "</td>";
                    echo "<td class=\"no_hover\">" . substr($row_can['no_candidat_can'], -3) . "</td>";
                    echo "<td class=\"col-md-2\" style=\"background-color:white;\">";

                    if ($row_can['note_can'] == 0) {
                        if(!$tpi->has_per_in_tpi($tpi->get_id_tpi_by_ref_exa_and_ref_can($ref_exa,$row_can['id_can']))) {
                            echo "<button class=\"btn button_glyph btn-xs remove_can_exa center-block\" id_can=\"" . $row_can['id_can'] . "\" ><span class=\"glyphicon glyphicon-remove red\"></span></button>";
                        }
                    }
                    echo "</td>";
                    echo '</tr>';
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
    <?php
} else {
    echo "<div class=\"alert alert-info\" role=\"alert\">";
    echo "Aucun candidat dans l'examen";
    echo "</div>";
}
?>
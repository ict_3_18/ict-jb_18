<?php
    session_start();
    $aut = "ADM_USR"; 
    require_once("./../config/config.inc.php");
    require_once(WAY . "./includes/secure.inc.php");
    require_once( WAY ."/includes/autoload.inc.php");
    require_once( WAY ."/includes/head.inc.php");
    
    $grp = new Groupe(); 
    $cla = new Classe();
    $can = new Candidat(); 

    $ref_exa = 0; 
    if(isset($_POST['ref_exa'])){
        $ref_exa = $_POST['ref_exa']; 
        //Affiche le numero et l'année de l'examen 
        $exa = new Examen($ref_exa);
        ?>
    <div class="container">
        <?php
        echo "<div class=\"row\">";
            echo "<div class=\"header\">";
                echo "<h3><strong>".$exa->get_num_nom()."  ".$exa->get_nom_mod($ref_exa)." - ".$exa->get_date_hrs()."</strong></h3>";
                echo "</div>";
            echo "</div>";
        ?>
          <div class="panel panel-primary">
             <div class="panel-heading">
                Ajout d'un candidat(s) à un examen
             </div>
          
             <div class="panel-body">

                   <!--  Groupe -->
                   <div class="form-group row">
                      <label for="ref_grp" class="col-sm-1 col-form-label">Groupe</label>
                      <div class="col-sm-5">
                        <select name="grp_exa" id="ref_grp" class="form-control select_can">
                            <option value="" name="">Sélectionne qqch ...</option>
                            <?php 
                                foreach ($grp->get_all_no_termine() as $g){
                                    echo "<option value=\"".$g["id_grp"]."\" >".$g["nom_grp"]."</option>"; 
                                }
                            ?>
                        </select>
                      </div>
                   </div>
                      
                    <!--  Classe  -->
                   <div class="form-group row">
                      <label for="ref_cla" class="col-sm-1 col-form-label">Classe</label>
                      <div class="col-sm-5">
                        <select name="cla_exa" id="ref_cla" class="form-control select_can">
                            <option value="" name="">Sélectionne qqch ...</option>
                            <?php 
                                foreach ($cla->get_all_actifs('nom_cla') as $c){
                                    echo "<option value=\"".$c["id_cla"]."\" id_grp=\"".$c["ref_grp"]."\">".$c["nom_cla"]."</option>"; 
                                }
                            ?>
                        </select>
                      </div>
                   </div>
                      
                   <!--  Candidat -->
                   <div class="form-group row">
                      <label for="ref_can" class="col-sm-1 col-form-label">Candidat</label>
                      <div class="col-sm-5">
                        <select name="can_exa" id="ref_can" class="form-control select_can">
                            <option value="" name="">Sélectionne qqch ...</option>
                            <?php 
                                foreach ($can->get_tab_can_by_grp_no_archive_no_termine() as $c){
                                    echo "<option value=\"".$c["id_can"]."\" id_cla=\"".$c["ref_classe"]."\">".substr($c["no_candidat_can"],-3)." - ".$c['nom_can']." ".$c['prenom_can']."</option>"; 
                                }
                            ?>
                        </select>
                      </div>
                   </div>
                    
                   <!-- Bouton submit et reset -->
                  <div class="form-group row">
                     <div class="col-sm-offset-8 col-sm-2">
                         <a href="#" class="form-control btn btn-primary" id="submit_conf">Ajouter</a>
                     </div>
                     <div class="col-sm-2"> 
                        <?php
                            echo "<a href=\"".URL."examens/liste_examen.php\" class=\"form-control btn btn-warning\">Retour</a>";    
                        ?>
                     </div>
                  </div> 
                   
            </div>
              
         </div>

        <div id="load_list_can" ref_exa="<?php echo $ref_exa; ?>">
            <!-- <table> en load </table> -->
        </div>

        <?php
            }else{
                echo "<div class=\"alert alert-info\" role=\"alert\">"; 
                    echo "Aucune 'ref_exa' reçu en post"; 
                echo "</div>";
            }
         ?>

    </div>
          
        <script src="./js/add_candidat_examen.js"></script> 
        
    </body>
</html>
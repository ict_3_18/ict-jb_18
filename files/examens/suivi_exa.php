<?php
session_start();
$aut = "ADM_USR;ADM_EXA;USR_EXA";
require("./../config/config.inc.php");
require(WAY . "./includes/secure.inc.php");
require_once(WAY . "/includes/head.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

$exa = new Examen();
$mod = new Module();
$per = new Personne($_SESSION['id']);
$tab_exa = $exa->list_exam();
?>

            <div class="col-md-12">
                <div class="panel panel-primary ">
                    <div class="panel-heading">
                        Liste des Examens
                    </div>
                    <div class="panel-body">

                        <table class="table table-hover table-dark table-responsive">
                            <tr>
                                <th>Date</th>
                                <th>Heures</th>
                                <th>Module</th>
                                <th>Nb Candidats</th>
                                <th>Rapports</th>
                                <th>Enseignants / Experts</th>
                                <th style="text-align:center;">Matériel</th>
                                <th>Définitif</th>

                                <th>CD travaux</th>
                                <th>CD données</th>
                                <th>Travaux des candidats</th>

                                <th>Classeur des candidats</th>
                                <th>Rapport d'examens</th>
                                <th>Notes saisies</th>
                                <th>Archivé</th>

                            </tr>
                            <?php
                            foreach ($tab_exa as $examen) {
                                $exa = new Examen($examen['id_exa']);
                                //print_r($examen);
                                if($examen['definitif_exa']){
                                    echo '<tr style="background-color:white; white-space:nowrap;">';
                                }else{
                                    echo '<tr style="background-color:lightgrey; white-space:nowrap;">';
                                }
                                ?>
                                    <!-- Date -->
                                    <td><b><?= $exa->get_date_hrs_jour_date(); ?></b>
                                    </td>
                                    <td>
                                        <?php

                                        $date_debut = explode(" ", $examen['date_heure_exa'])[0];
                                        $date = date("d.m.y", strtotime($date_debut));

                                        $heure = explode(" ", $examen['date_heure_exa'])[1];
                                        if ($examen['duree_exa'] != null) {
                                            $duree = $examen['duree_exa'];
                                            $heure_debut = $exa->hrs_min_sec_to_min($heure);
                                            $calc = $heure_debut + $duree;
                                            $heure_fin = $exa->min_to_hrs_min_sec($calc);
                                        } else {
                                            $heure_fin = "??:??:??";
                                        }
                                        echo substr($heure, 0, 5) . " - " . substr($heure_fin, 0, 5);
                                        ?></td>
                                    <!-- N° exa -->
                                    <td><?= $exa->get_num_nom()."-".$exa->get_ver() ?></td>
                                    <!-- Candidats -->
                                    <td>
                                        <?php
                                        $color =$exa->get_couleur_examen($examen['id_exa']);
                                        //print_r($color);
                                        $nb_can = "";
                                        if(sizeof($color) > 1) {
                                            foreach ($color AS $couleur) {
                                                $nb_can .= '<div class="color ';
                                                $nb_can .= '" style="background-color:' . $couleur['color_grp'] . ';"'
                                                    . ' data-toggle="tooltip" title="' . $couleur['nb_can'] . ' candidats ' . $couleur['nom_grp'] . '" >'
                                                    . $couleur['nb_can'] .'</div>';
                                                $nb_can .= " + ";
                                            }
                                            $nb_can .= ' = <div class="color total" data-toggle="tooltip" title="' . $examen['Candidats'] . ' candidats" ><b>' . $examen['Candidats'] . '</b></div>';
                                        }else if(sizeof($color) == 1) {
                                            $couleur = $color[0];
                                            $nb_can .= '<div class="color';
                                            $nb_can .= '" style="background-color:' . $couleur['color_grp'] . ';"'
                                                . ' data-toggle="tooltip" title="' . $couleur['nb_can'] . ' candidats ' . $couleur['nom_grp'] . '" >'
                                                . $couleur['nb_can'] . '</div>';

                                            $nb_can = substr($nb_can, 0, -3);

                                        }
                                        echo $nb_can;
                                        ?>
                                    </td>

                                    <!-- Rapport -->
                                    <td>
                                    <form method="post" action ="<?php echo URL . "rapport_exa/rapport.php"; ?>" id="TestForm">
                                        <input type="hidden" name="id_exa" value="<?= $exa->get_id(); ?>">
                                        <input class="btn btn-primary btn-sm" type="submit" value="Rapport">
                                    </form>
                                    </td>

                                    <td>
                                    <?php
                                        $profs = null;
                                        $tab_prof = $exa->get_profs_exam($examen['id_exa']);

                                        if ($tab_prof !== null) {
                                            foreach ($tab_prof as $key => $prof) {
                                                $profs .= substr($prof['prenom_per'], 0 ,1) . ". " . $prof['nom_per']."<br>";
                                            }
                                        }
                                        echo $profs;
                                        $experts = null;
                                        $tab_expert = $exa->get_expert_exam($examen['id_exa']);

                                        if ($tab_expert !== null) {
                                            foreach ($tab_expert as $key => $expert) {
                                                $experts .= substr($expert['prenom_per'], 0, 1) . ". " . $expert['nom_per']."<br>";
                                            }
                                        }
                                        echo $experts;
                                    ?>
                                    </td>

                                    <td style="text-align:center;">
                                    <?php

                                        echo '<form id="dispo_exa" action="' . URL . 'examens/materiel_examen.php" method="post">';
                                            //$exa = new Examen($examen['ref_exa']);

                                        $title = "<h4>Matériel autorisé</h4>";
                                        $title .= str_replace(chr(10), "<br>", $exa->get_materiel_exa());

                                        if ($exa->get_salle_inf_exa() == 1) {
                                            $title .= "<br><b>Salle info</b> : oui";
                                        }else{
                                            $title .= "<br><b>Salle info</b> : non";
                                        }

                                        if ($exa->get_lec_exa() == 1) {
                                            $title .= "<br><b>Données perso (R:)</b> : oui";
                                        }else{
                                            $title .= "<br><b>Données perso (R:)</b> : non";
                                        }
                                        if ($exa->get_reseau_exa() == 1) {
                                            $title .= "<br><b>Réseau disponible</b> : oui";
                                        }else{
                                            $title .= "<br><b>Réseau disponible</b> : non";
                                        }

                                            echo '<input type="hidden" name="from_where" id="from_where" value="from_list">';
                                            echo '<input type="hidden" name="ref_exa" id="hiddenField" value="' . $exa->get_id() . '">';
                                            echo '<button type="submit" class="button_glyph" data-toggle="tooltip" data-html="true" title="' . $title . '">';
                                            echo  '<span class="glyphicon glyphicon-briefcase text-primary"></span>';
                                            echo  '</button>';

                                        echo '</form>';
                                    ?>
                                    </td>
                                    <!--Définitif-->
                                    <td class="text-center">
                                    <input type="checkbox" class="form-control check_suivi" action="definitif_exa" id_exa="<?= $exa->get_id()?>"
                                        <?php if($exa->get_definitif()):?>
                                            checked
                                        <?php endif ?>
                                    ></td>




                                <!-- CD avec travaux des candidats-->
                                <td class="text-center">
                                    <input type="checkbox" class="form-control check_suivi" action="archiver_cd_ep_exa"  id_exa="<?= $exa->get_id()?>"
                                        <?php if($exa->get_cd_can()):?>
                                            checked
                                        <?php endif ?>
                                    >
                                </td>

                                    <!-- CD avec données d'exa-->
                                    <td class="text-center">
                                    <input type="checkbox" class="form-control check_suivi" action="archiver_cd_exa" id_exa="<?= $exa->get_id()?>"
                                        <?php if($exa->get_cd_exa()):?>
                                            checked
                                        <?php endif ?>
                                    ></td>

                                    <!-- Archivage des travaux des candidats -->
                                    <td class="text-center">
                                    <input type="checkbox" class="form-control check_suivi" action="archiver_dos_exa" id_exa="<?= $exa->get_id()?>"
                                        <?php if($exa->get_epr_can()):?>
                                            checked
                                        <?php endif ?>
                                    ></td>

                                    <!-- Archiver l'épreuve dans le classeur des candidats -->
                                    <td class="text-center">
                                    <input type="checkbox" class="form-control check_suivi" action="archiver_cla_exa" id_exa="<?= $exa->get_id()?>"
                                        <?php if($exa->get_epr_cla()):?>
                                            checked
                                        <?php endif ?>
                                    ></td>

                                <!--Rapport d'examens-->
                                <td class="text-center">
                                    <input type="checkbox" class="form-control check_suivi" action="valider_exa"  id_exa="<?= $exa->get_id()?>"
                                        <?php if($exa->get_validation()):?>
                                            checked
                                        <?php endif ?>
                                    >
                                </td>

                                <!--Notes saisies-->
                                <td class="text-center">
                                    <input type="checkbox" class="form-control check_suivi" action="saisi_note_exa"  id_exa="<?= $exa->get_id()?>"
                                        <?php if($exa->get_notes()):?>
                                            checked
                                        <?php endif ?>
                                    >
                                </td>

                                <!--Archivé-->
                                <td class="text-center">
                                    <input type="checkbox" class="form-control check_suivi" action="active_archivage_exa"  id_exa="<?= $exa->get_id()?>"
                                        <?php if($exa->get_archive()):?>
                                            checked
                                        <?php endif ?>
                                    ></td>

                                </tr>
                            <?php
                            }
                            ?>
                        </table>
                    </div>
                </div>    
            </div>
        </div>
        <script src="./js/suivi.js"></script>
    </body>
</html>
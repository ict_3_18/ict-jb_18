<?php
session_start();
$aut = "ADM_USR;ADM_EXA;USR_EXA";
require("./../config/config.inc.php");
require(WAY . "./includes/secure.inc.php");
require_once(WAY . "/includes/head.inc.php");
require_once(WAY . "/includes/autoload.inc.php");
$id_exa = $_POST['ref_exa'];
$exa = new Examen($id_exa);
?>

<div class="container">
    <div class="panel panel-primary">
        <div class="panel-heading" id="second-title">
            Matériel disponible pendant l'examen
        </div>
        <div class="panel-body">
            <form id="materiel_examen_form" href="">
                <table class="table table-striped">
                <tr>
                    <td colspan="2">
                    <label for="soft_exa" class="col-sm-4 col-form-label">Programmes nécessaires :</label>
                        <div class="col-sm-8">
                            <textarea type="text" id="soft_exa" name="soft_exa" class="form-control" ><?= $exa->get_soft_exa()?></textarea>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <label for="infos_begin_exa" class="col-sm-4 col-form-label">Remarques de début d'examen :</label>
                        <div class="col-sm-8">
                            <textarea type="text" id="infos_begin_exa" name="infos_begin_exa" class="form-control" ><?= $exa->get_infos_begin_exa()?></textarea>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <label for="aut_exa" class="col-sm-4 col-form-label">Matériel disponible physiquement :</label>
                        <div class="col-sm-8">
                            <textarea type="text" id="materiel_exa" name="materiel_exa" class="form-control" ><?= $exa->get_materiel_exa()?></textarea>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="checkbox" class="col-sm-1" id="salle_inf_exa" name="salle_inf_exa"
                        <?php if($exa->get_salle_inf_exa() == 1) {echo ' checked ';} ?>">
                        <label for="salle_inf_exa" class="col-sm-11 col-check-label">Salle d'informatique nécessaire</label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="checkbox" class="col-sm-1" id="lec_exa" name="lec_exa" <?php if($exa->get_lec_exa() == 1){ echo ' checked '; } ?>>
                        <label for="lec_exa" class="col-sm-3 col-check-label">Données perso (R:\cco_ict_<?= $exa->get_num_nom()?>)</label>
                        <div class="col-sm-8">
                            <textarea type="text" id="data_aut_exa" name="data_aut_exa" class="form-control" ><?= $exa->get_data_exa()?></textarea>
                        </div>
                    </td>

                </tr>
                <tr>
                    <td colspan="2">
                        <input type="checkbox" class="col-sm-1" id="reseau_exa" name="reseau_exa"
                        <?php if($exa->get_reseau_exa() == 1){ echo ' checked ';}?>
                        >
                        <label for="reseau_exa" class="col-sm-11 col-check-label">Réseau informatique disponible pendant l'examen</label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="hidden" id="id_exa" value="<?php echo $id_exa ?>">

                        <div class="col-sm-offset-8 col-sm-2">
                            <input type="submit" class="form-control btn btn-primary submit" id="modify_conf" value="Enregistrer">
                        </div>
                        <div class="col-sm-2">
                            <input type="button" class="form-control btn btn-info" id="retour" value="Retour">
                        </div>
                    </td>
                </tr>
				<tr>
                    <td colspan="2">
                        <label class="col-sm-11 col-check-label">Indications aux enseignants :</label>
						<p class="col-sm-11 ">Le dossier ou déposer vos fichier source est P:\partage personnel-personnel\jhi\pj\sources\<?= $exa->get_num_nom() ?></p>
						<p class="col-sm-11 ">Les sources seront copiées sur les PC des candidats dans le C:\ICT-<?= $exa->get_num_nom() ?>\sources\</p>
						<p class="col-sm-11 ">Les candidats déposeront leurs fichiers d'examens dans C:\ICT-<?= $exa->get_num_nom() ?>\candidat_<i>n° candidat</i>\</p>
                    </td>
                </tr>
                
            </form>
            </table>
        </div>
    </div>
</div>
<script src="./js/materiel_examen.js"></script>
</body>
</html>

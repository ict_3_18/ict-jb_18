<?php
session_start();
$aut = "ADM_USR;ADM_EXA;USR_EXA";
require("./../config/config.inc.php");
require(WAY . "./includes/secure.inc.php");
require_once(WAY . "/includes/head.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

?>
<html>
<body>
    <div class="row">
        <div class="header">
            <h3>Suivis</h3>
        </div>
    </div>

    <div class="col-md-12">
        <div class="panel panel-primary ">
            <div class="panel-heading">
                Formulaires des suivis
            </div>
            <div class="panel-body">
                <div class="col-md-3">
                    <form id="form_scan" action="" method="post">
                        <div class="form-group">
                            <label for="code">Code :</label>
                            <input type="text" class="form-control" id="code" name="code" autocomplete="off">
                        </div>
                        <button type="submit"  name='submit' class="btn btn-primary">
                            Submit
                        </button>
                        <button type="reset" class="btn btn-warning">
                            Reset
                        </button>
                    </form>
                </div>
                <div id="message" class="col-md-9">
                </div>
            </div>
        </div>
    </div>

    <?php

    ini_set('error_reporting',~E_NOTICE);
    $exa = new Examen($id_exa);

    $tab_examens = array();
    $tab_examens['archiver_cd_ep_exa']['title'] = "CD travaux des candidats";
    $tab_examens['archiver_cd_ep_exa']['glyph'] = "cd";

    $tab_examens['archiver_cd_exa']['title'] = "CD des données";
    $tab_examens['archiver_cd_exa']['glyph'] = "cd";

    $tab_examens['archiver_dos_exa']['title'] = "Travaux des candidats";
    $tab_examens['archiver_dos_exa']['glyph'] = "folder-open";

    $tab_examens['archiver_cla_exa']['title'] = "Classeur des candidats";
    $tab_examens['archiver_cla_exa']['glyph'] = "book";

    $tab_examens['saisi_note_exa']['title'] = "Notes saisies";
    $tab_examens['saisi_note_exa']['glyph'] = "eye-open";

    $tab_examens['valider_exa']['title'] = "Rapport d'examens";
    $tab_examens['valider_exa']['glyph'] = "list-alt";

    $tab_exa = $exa->list_exam();
    $color_exa = "";

    foreach($tab_exa AS $examen): ?>
        <?php
        $tab_color = $exa->get_couleur_examen($examen['id_exa']);
        foreach($tab_color AS $color){
            if($color['nb_can'] > $examen['nb_can_max']){
                $examen['nb_can_max'] = $color['nb_can'];
            }
            if($color['nb_can'] == $examen['nb_can_max']){
                $color_exa = $color['color_grp'];
            }
        }

        if(strlen($examen['version_ich_exa']) > 2) {
            $version = substr($examen['version_ich_exa'],-2);
            }
        else {
            $version = $examen['version_ich_exa'];
        }
        ?>
        <div class="examen_<?=$examen['id_exa']?> col-md-2">
            <div class="thumbnail">
                <div class="caption">
                    <h3 style="background-color:<?=$color_exa?>">
                        ICT <?= $examen['no_ich_exa']."-".$version.strtoupper($examen['v_exam_ich_exa'])?>
                    </h3>
                    <h5><?= date('d.m.Y',strtotime($examen['date_heure_exa']))?></h5>
                    <p id_exa="<?=$examen['id_exa']?>">
                        <?php
                        $archivage_ok = true;
                        foreach ($tab_examens AS $key => $exam) :
                            if($examen[$key] == 1){
                                $type_class = "success";
                            }else{
                                $type_class = "danger";
                                $archivage_ok = false;
                            }
                                ?>
                            <button data-toggle="tooltip" title="<?= $exam['title'] ?>" action="<?= $key ?>" class="suivi btn btn-<?= $type_class ?> btn-xs">
                                <span class="glyphicon glyphicon-<?= $exam['glyph'] ?>"></span>
                            </button>&nbsp;&nbsp;
                        <?php endforeach; ?>
                        <button <?= ($archivage_ok) ? "" : "disabled=disabled" ?> action="active_archivage_exa" class="btn <?= ($archivage_ok) ? "btn-warning" : "" ?> archivage btn-xs">
                            <span data-toggle="tooltip" title="Archivage examens" class="glyphicon glyphicon-ok"></span>
                        </button>
                    </p>
                </div>
            </div>
        </div>
    <?php endforeach;
    ?>

<script>
    document.getElementById("code").focus();

    $('html').click(function(evt){
        if(evt.target.id != "search") {
            document.getElementById("code").focus();
        }
    });
</script>
</body>
<script src="./js/suivi.js"></script>
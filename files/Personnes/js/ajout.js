$(function () {
    $("#cancel").click(function () {
        window.location = "./index.php"
    });

    $("#ajout_per_form").validate({
        rules: {
            nom_per: {
                required: true
            },
            adresse_per: {
                required: true
            },
            npa_per: {
                number: true
            },
            tel_mobile_per: {
                number: true
            },
            tel_bureau_per: {
                number: true
            },
            tel_prive_per: {
                number: true
            },
            email_per: {
                email: true,
                required: true
            },
            password_conf_per: {
                equalTo: "#password_new_per"
            }
        },
        messages: {
            nom_per: {
                required: "Veuillez saisir le nom de la personne &agrave ajouter"
            },
            adresse_per: {
                required: "Veuillez saisir l'adresse de la personne &agrave ajouter"
            },
            npa_per: {
                number: "Veuillez saisir le NPA de la personne &agrave ajouter"
            },
            tel_mobile_per: {
                number: "Veuillez saisir le num&eacutero de t&eacutel&eacutephone mobile de la personne &agrave ajouter"
            },
            tel_bureau_per: {
                number: "Veuillez saisir le num&eacutero de t&eacutel&eacutephone du bureau de la personne &agrave ajouter"
            },
            tel_prive_per: {
                number: "Veuillez saisir le num&eacutero de t&eacutel&eacutephone priv&eacute de la personne &agrave ajouter"
            },
            email_per: {
                required: "Veuillez entrer l'adresse mail de la personne &agrave ajouter",
                email: "Veuillez saisir l'adresse mail de la personne &agrave ajouter"
            },
            password_conf_per: {
                equalTo: "Le mot de passe ne correspond pas au mot de passe indiqu&eacute&eacute"
            }
        },

        submitHandler: function (form) {
            // console.log("formulaire envoy&eacute");


            if ($("#enseignant_per").is(":checked")) {
                var enseignant = 1;
            } else {
                var enseignant = 0;
            }
            if ($("#expert_per").is(":checked")) {
                var expert = 1;
            } else {
                var expert = 0;
            }
            if ($("#supp_prof_per").is(":checked")) {
                var supp_prof = 1;
            } else {
                var supp_prof = 0;
            }
            if ($("#actif_per").is(":checked")) {
                var actif = 1;
            } else {
                var actif = 0;
            }
            if ($("#admin_per").is(":checked")) {
                var admin = 1;
            } else {
                var admin = 0;
            }
            if ($("#admin_tpi_per").is(":checked")) {
                var admin_tpi = 1;
            } else {
                var admin_tpi = 0;
            }

            var password = $("#password_new_per").val();
            if(password === ""){
                password = "$seuwAS" + Date.now() + "DW556as$$"
            }




            $("#loading").css("display", "block");
            $.post(
                "./json/ajout.json.php?_=" + Date.now(),
                {
                    sexe_per: $("#sexe_per").val(),
                    nom_per: $("#nom_per").val(),
                    prenom_per: $("#prenom_per").val(),
                    adresse_per: $("#adresse_per").val(),
                    localite_per: $("#localite_per").val(),
                    npa_per: $("#npa_per").val(),
                    entreprise_per: $("#entreprise_per>option:selected").text(),
                    ref_emp: $("#entreprise_per").val(),
                    tel_mobile_per: $("#tel_mobile_per").val(),
                    tel_bureau_per: $("#tel_bureau_per").val(),
                    tel_prive_per: $("#tel_prive_per").val(),
                    email_per: $("#email_per").val(),
                    password_new_per: password,
                    enseignant_per: enseignant,
                    expert_per: expert,
                    supp_prof_per: supp_prof,
                    actif_per: actif,
                    admin_per: admin,
                    admin_tpi_per: admin_tpi
                },
                function result(data, status) {
                    if (data.reponse === true) {
                        message("La personne a bien &eacutet&eacute cr&eacute&eacutee ! <br><a href='./index.php'>Retour &agrave la liste des personnes !</a>", "success");
                        $("#ajout_per_form").trigger("reset");
                    }
                    if (data.reponse === false) {
                        message("L'adresse mail existe d&eacutej&agrave !", "danger");
                    }

                }
            ).always(function () {
                $("#loading").css("display", "none");
            });
        }
    });
});




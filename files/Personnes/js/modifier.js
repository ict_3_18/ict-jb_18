$(function () {
    $("#modifier_per_form").validate({

        rules: {
            nom_per: {
                required: true
            },
            adresse_per: {
                required: true
            },
            npa_per: {
                number: true
            },
            tel_mobile_per: {
                number: true
            },
            tel_bureau_per: {
                number: true
            },
            tel_prive_per: {
                number: true
            },
            email_per: {
                email: true
            },
            /*password_conf_per: {
                equalTo: "#password_per"
            }*/
        },
        messages: {
            nom_per: {
                required: "Veuillez saisir le nom de la personne à modifier"
            },
            adresse_per: {
                required: "Veuillez saisir l'adresse de la personne à modifer"
            },
            npa_per: {
                number: "Le NPA doit être un numéro"
            },
            tel_mobile_per: {
                number: "Le numéro de téléphone doit être en chiffre"
            },
            tel_bureau_per: {
                number: "Le numéro de téléphone doit être en chiffre"
            },
            tel_prive_per: {
                number: "Le numéro de téléphone doit être en chiffre"
            },
            email_per: {
                email: "Ce n'est pas une adresse mail"
            },
            /*password_conf_per: {
                equalTo: "Le mot de passe ne correspond pas au mot de passe indiqué"
            }*/
        },
        submitHandler: function (form) {
            console.log("formulaire envoyé");

            if ($("#enseignant_per").is(":checked")) {
                var enseignant = 1;
            } else {
                var enseignant = 0;
            }
            if ($("#expert_per").is(":checked")) {
                var expert = 1;
            } else {
                var expert = 0;
            }
            if ($("#supp_prof_per").is(":checked")) {
                var supp_prof = 1;
            } else {
                var supp_prof = 0;
            }
            if ($("#actif_per").is(":checked")) {
                var actif = 1;
            } else {
                var actif = 0;
            }
            if ($("#admin_per").is(":checked")) {
                var admin = 1;
            } else {
                var admin = 0;
            }
            if ($("#admin_tpi_per").is(":checked")) {
                var admin_tpi = 1;
            } else {
                var admin_tpi = 0;
            }

            $.post(
                    "./json/modification.json.php?_=" + Date.now(),
                    {
                        id_per: $("#id_per").val(),
                        sexe_per: $("#sexe_per").val(),
                        nom_per: $("#nom_per").val(),
                        prenom_per: $("#prenom_per").val(),
                        adresse_per: $("#adresse_per").val(),
                        localite_per: $("#localite_per").val(),
                        npa_per: $("#npa_per").val(),
                        entreprise_per: $("#entreprise_per").val(),
                        tel_mobile_per: $("#tel_mobile_per").val(),
                        tel_bureau_per: $("#tel_bureau_per").val(),
                        tel_prive_per: $("#tel_prive_per").val(),
                        email_per: $("#email_per").val(),
                        password_per: $("#password_per").val(),
                        enseignant_per: enseignant,
                        expert_per: expert,
                        supp_prof_per: supp_prof,
                        actif_per: actif,
                        admin_per: admin,
                        admin_tpi_per: admin_tpi
                    }
            )
        }
    });
    $("#delete").click(function () {
        $.post(
                "./json/modification.json.php?_=" + Date.now(),
                {
                    id_per: $("#id_per").val(),
                    delete:1
                })
    });
});




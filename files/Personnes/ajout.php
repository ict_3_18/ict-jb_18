<?php
session_start();
$aut = "ADM_USR";
require("./../config/config.inc.php");
require(WAY . "/includes/secure.inc.php");
require_once(WAY . "/includes/head.inc.php");
require_once(WAY . "/includes/autoload.inc.php");
?>
<div class="container">
    <div class="row">
        <div class="header">
            <h3>Personne</h3>
        </div>
    </div>
    <div class="panel panel-primary">
        <div class="panel-heading">
            Ajout d'une personne
        </div>

        <div class="panel-body">
            <form id="ajout_per_form" action="check.php" method="post">
                <!-- Coordonn�es -->
                <h4>Coordonn&eacute;es :</h4>
                <div class="form-group row">
                    <label for='sexe_per' class="col-sm-2 col-form-label">Genre :</label>
                    <div class='col-sm-10'>
                        <select id='sexe_per' name="sexe_per" class="form-control input-sm">
                            <option value="M">Monsieur</option>
                            <option value="F">Madame</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label for='nom_per' class="col-sm-2 col-form-label">Nom* :</label>
                    <div class='col-sm-10'>
                        <input type="text" class="form-control input-sm" id="nom_per" name="nom_per">
                    </div>
                </div>
                <div class="form-group row">
                    <label for='prenom_per' class="col-sm-2 col-form-label">Pr&eacute;nom :</label>
                    <div class='col-sm-10'>
                        <input type="text" class="form-control input-sm" id="prenom_per" name="prenom_per">
                    </div>
                </div>

                <div class="form-group row">
                    <label for='adresse_per' class="col-sm-2 col-form-label">Adresse* :</label>
                    <div class='col-sm-10'>
                        <input type="text" class="form-control input-sm" id="adresse_per" name="adresse_per">
                    </div>
                </div>

                <div class="form-group row">
                    <label for='localite_per' class="col-sm-2 col-form-label">Localit&eacute; :</label>
                    <div class='col-sm-10'>
                        <input type="text" class="form-control input-sm" id="localite_per" name="localite_per">
                    </div>
                </div>

                <div class="form-group row">
                    <label for='npa_per' class="col-sm-2 col-form-label">NPA :</label>
                    <div class='col-sm-10'>
                        <input type="text" class="form-control input-sm" id="npa_per" name="npa_per">
                    </div>
                </div>

                <div class="form-group row">
                    <label for='tel_mobile_per' class="col-sm-2 col-form-label">Tel mobile :</label>
                    <div class='col-sm-10'>
                        <input type="tel" class="form-control input-sm" maxlength="10" id="tel_mobile_per" name="tel_mobile_per">
                    </div>
                </div>

                <div class="form-group row">
                    <label for='tel_bureau_per' class="col-sm-2 col-form-label">Tel Bureau :</label>
                    <div class='col-sm-10'>
                        <input type="tel" class="form-control input-sm" maxlength="10" id="tel_bureau_per" name="tel_bureau_per">
                    </div>
                </div>

                <div class="form-group row">
                    <label for='tel_prive_per' class="col-sm-2 col-form-label">Tel priv&eacute; :</label>
                    <div class='col-sm-10'>
                        <input type="tel" class="form-control input-sm" maxlength="10" id="tel_prive_per" name="tel_prive_per">
                    </div>
                </div>

                <div class="form-group row">
                    <label for='email_per' class="col-sm-2 col-form-label">E-mail* :</label>
                    <div class='col-sm-10'>
                        <input type="email" class="form-control input-sm" id="email_per" name="email_per">
                    </div>
                </div>

                <!--Coordonn�es professionnelles-->
                <h4>Coordonn&eacute;es professionnelles:</h4>
                <div class="form-group row">
                    <label for='entreprise_per' class="col-sm-2 col-form-label">Entreprise :</label>
                    <div class='col-sm-10'>
                        <select class="form-control input-sm" id="entreprise_per" name="entreprise_per">
                            <?php
                            $entreprise = new Entreprise();
                            $tab_entr = $entreprise->get_all();
                            foreach ($tab_entr as $entr) {
                                if ($entr['id_emp'] == 24) {
                                    echo "<option value=" . $entr['id_emp'] . " selected=true>" . $entr['nom_emp'] . "</option>";
                                } else {
                                    echo "<option value=" . $entr['id_emp'] . ">" . $entr['nom_emp'] . "</option>";
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <!--Donn�es d'acc�es-->
                <h4>Donn&eacute;es d'acc&eacute;es:</h4>
                <div class="form-group row">
                    <label for='password_new_per' class="col-sm-2 col-form-label">Mot de passe :</label>
                    <div class='col-sm-10'>
                        <input type="password" class="form-control input-sm" id="password_new_per" name="password_new_per">
                    </div>
                </div>

                <div class="form-group row">
                    <label for='password_conf_per' class="col-sm-2 col-form-label">Confirmation :</label>
                    <div class='col-sm-10'>
                        <input type="password" class="form-control input-sm" id="password_conf_per" name="password_conf_per">
                    </div>
                </div>

                <div class="form-group row">
                    <label for='enseignant_per' class="col-sm-2 col-form-label">Enseignant :</label>
                    <div class='col-sm-10'>
                        <input type="checkbox" class="form-check-input" id="enseignant_per" name="enseignant_per">
                    </div>
                </div>

                <div class="form-group row">
                    <label for='expert_per' class="col-sm-2 col-form-label">Expert :</label>
                    <div class='col-sm-10'>
                        <input type="checkbox" class="form-check-input" id="expert_per" name="expert_per">
                    </div>
                </div>

                <div class="form-group row">
                    <label for='supp_prof_per' class="col-sm-2 col-form-label">Supp&eacute;rieur prof. :</label>
                    <div class='col-sm-10'>
                        <input type="checkbox" class="form-check-input" id="supp_prof_per" name="supp_prof_per">
                    </div>
                </div>

                <div class="form-group row">
                    <label for='actif_per' class="col-sm-2 col-form-label">Actif :</label>
                    <div class='col-sm-10'>
                        <input type="checkbox" class="form-check-input" id="actif_per" name="actif_per" checked="checked">
                    </div>
                </div>

                <div class="btn-group">
                    <input type='submit' class="btn btn-success" id='submit_conf' name='submit_conf' value='Ajouter'>
                    <input type='button' class="btn btn-warning" id='cancel' name='cancel' value='Annuler' >
                </div>
            </form>
        </div>
        <div class="panel-footer">
            Les champs marqu&eacute;s * sont obligatoire
        </div>
    </div>
    <script src="./js/ajout.js"></script>
    
</div>
</body>
</html>

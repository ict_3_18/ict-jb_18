<?php

header("Content-Type: application/json");
session_start();
require './../../config/config.inc.php';
require_once(WAY . "/includes/autoload.inc.php");

$per = new Personne();
if (isset($_POST['delete'])) {
   $per->delete($_POST['id_per']);
} else {
    $id = $per->update($_POST);
    $per->setId_per($id);
}

echo json_encode($_POST);

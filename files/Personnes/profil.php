<?php
session_start();
$aut = "USR_SIT";
require "./../config/config.inc.php";
require(WAY . "/includes/secure.inc.php");
require_once WAY . "/includes/head.inc.php";
require_once(WAY . "/includes/autoload.inc.php");
$per = new Personne($_SESSION['id']);
?>
<div class="container">
    <div class="row">
        <div class="header">
            <h3>Profil</h3>
        </div>
    </div>
    <div class="panel panel-primary">
        <div class="panel-heading">
            <?php
            if ($per->get_sexe() == "M") {
                echo "<b>Mr. ";
            } else {
                echo "<b>Mme ";
            }
            echo $per->get_prenom() . " " . $per->get_nom();
            ?>
        </div>

        <div class="panel-body">
            <form action="modif.php" method="post">
                <?php
                echo " " . $per->get_prenom() . " " . $per->get_nom() . "</b><br>";
                echo $per->get_entreprise() . "<br>";
                echo "<br>";

                if ($per->get_tel_bureau() != NULL) {
                    echo "<a href=tel:" . $per->get_tel_bureau() . ">" . $per->tel($per->get_tel_bureau()) . "</a> priv&eacute;<br>";
                } else {
                    echo "";
                }
                if ($per->get_tel_prive() != NULL) {
                    echo "<a href=tel:" . $per->get_tel_prive() . ">" . $per->tel($per->get_tel_prive()) . "</a> priv&eacute;<br>";
                } else {
                    echo "";
                }
                if ($per->get_tel_mobile() != NULL) {
                    echo "<a href=tel:" . $per->get_tel_mobile() . ">" . $per->tel($per->get_tel_mobile()) . "</a> mobile<br>";
                } else {
                    echo "";
                }
                echo "<a href=mailto:" . $per->get_email() . ">" . $per->get_email() . "</a><br>";
                echo $per->get_adresse() . "<br>";
                echo $per->get_localite() . " " . $per->get_npa() . "<br><br>";
                ?>
                <button type='button' class='btn btn-primary'><label id=modif for="<?php echo $per->get_id(); ?>">Modifier</label></button>
                <input type=submit id="<?php echo $per->get_id(); ?>" name=modifier value="<?php echo $per->get_id(); ?>" class='btn btn-primary' style="display: none">
                <button type="button" class ="btn btn-warning" onclick="window.history.back()"> Cancel </button>
            </form>
        </div>

        <div class="panel-footer">
        </div>
    </div>
    <script src="./js/modif.js"></script>
</div>
</body>
</html>

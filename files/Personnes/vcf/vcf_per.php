<?php 
 
session_start(); 
$aut = "USR_SIT"; 
require("./../../config/config.inc.php"); 
require(WAY . "/includes/secure.inc.php"); 
require_once(WAY . "/includes/autoload.inc.php"); 
 
$per = new Personne($_GET['id_per']); 
header("Content-Disposition: attachment; filename=\"" . basename($per->get_nom() . " " . $per->get_prenom() . ".vcf") . "\""); 
header("Content-Type: application/force-download"); 
header("Connection: close"); 
 
echo $per->vcard(); 
?> 
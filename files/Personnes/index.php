<?php
session_start();
$aut = "USR_SIT";
require("./../config/config.inc.php");
require(WAY . "/includes/secure.inc.php");
require_once(WAY . "/includes/head.inc.php");
require_once(WAY . "/includes/autoload.inc.php");
$per = new Personne($_SESSION['id']);
?>
<div class="row">
    <div class="header">
        <h3>Personnes
            <?php
            $is_admin = false;
            if ($per->check_aut("ADM_USR")) {
                echo "<input type=\"button\" value=\"Ajouter une personne\" class=\"btn btn-success\" onclick=\"window.open('ajout.php', '_self')\"></h3>";

                $is_admin = true;
            }
            ?>

    </div>
</div>
<?php
//$per = new Personne();
if (empty($_GET)) {
    $tab_per = $per->get_all_actif();
} elseif ($_GET['type'] == "ens") {
    $tab_per = $per->get_all_enseignant_actif();
} elseif ($_GET['type'] == "exp") {
    $tab_per = $per->get_all_expert_actif();
} elseif ($_GET['type'] == "sup") {
    $tab_per = $per->get_all_supp_prof_actif();
} elseif ($_GET['type'] == "ina") {
    $tab_per = $per->get_all_inactif();
}
?>

<table class="table table-striped table-bordered" id="liste_per">

    <th>Nom, pr&eacute;nom</th>
    <th>Adresse</th>
    <th>Priv&eacute;</th>
    <th>Bureau</th>
    <th>Mobile</th>

    <?php

    if($per->check_aut("ADM_USR")){
        echo "<th></th>";
    }
    foreach ($tab_per as $p) {
        $usr = new Personne($p['id_per']);

        echo "<tr><td><a href=\"" . URL . "Personnes/vcf/vcf_per.php?id_per=" . $usr->get_id() . "\" target=_blank><span class='glyphicon glyphicon-download' /></a>&nbsp;&nbsp;<b><a target=\"_blank\" href=mailto:" . $usr->get_email() . ">" . $usr->get_nom() . " " . $usr->get_prenom() . "</a></td>";
        if ($usr->get_adresse() == NULL) {
            echo "<td></td>";
        } else {
            echo "<td>" . $usr->get_adresse() . ", " . $usr->get_npa() . ", <b>" . $usr->get_localite() . "</b></td>";
        }
        echo "<td><a href=tel: '" . $usr->get_tel_prive() . "'>" . $per->tel($usr->get_tel_prive()) . "</a></td>";
        echo "<td><a href=tel: '" . $usr->get_tel_bureau() . "'>" . $per->tel($usr->get_tel_bureau()) . "</a></td>";
        echo "<td><a href=tel: '" . $usr->get_tel_mobile() . "'>" . $per->tel($usr->get_tel_mobile()) . "</a></td>";
        if($per->check_aut("ADM_USR")){

            echo "<td>";
            echo "<form action=\"modif.php\" method=\"post\">";
            echo "<input type=\"submit\" id=\"" . $usr->get_id() . "\" value=\" Modifier \" class=\"btn btn-primary btn-xs\" />";
            echo "<input type=\"hidden\" value=\"" . $usr->get_id() . "\" name=\"modifier\" >";
            echo "</form>";
            echo "</td>";
        }
    }
    ?>
</table>
</div>
</body>
</html>
<?php
    session_start();
    $aut="USR_CAN";
    header('Content-type: text/json');
    header('Content-type: application/json; charset=utf-8');
    
    require '../config/config.inc.php';
    require(WAY."/includes/secure.inc.php");
    require_once WAY.'/includes/autoload.inc.php';

    $can = new Candidat();
    $tab_can = $can->get_search($_GET['term']."%");    
    echo json_encode($tab_can);
?>
<?php
session_start();
$aut = 'ADM_GRP';
require '../config/config.inc.php';
require(WAY."/includes/secure.inc.php");
require WAY . "/includes/head.inc.php";
require_once(WAY."/includes/autoload.inc.php");


$grp = new Groupe();
$tab_grp = $grp->get_all_no_archive();

$cla = new Classe();
$tab_cla = $cla->get_all();
$dom = new Domaine();
$tab_dom = $dom->get_all();
$tab_id = "";
$nom_cla = "";
$id_cla = "";
$duree_min_tpi_cla = "";
$duree_max_tpi_cla = "";
$stop_form_cla = "";
$termine_cla = "";
$ref_grp = "";
$Titre = "Création d'une Classe";
if (!empty($_GET['id_cla'])) {
    $get_tab_id = $cla->get_select_id_classe($_GET['id_cla']);
    $tab_links = $dom->get_all_cla_dom_with_id($_GET['id_cla']);
    $tab_id = $get_tab_id[0];
    $nom_cla = $tab_id['nom_cla'];
    $id_cla = $tab_id['id_cla'];
    $duree_min_tpi_cla = $tab_id['duree_min_tpi_cla'];
    $duree_max_tpi_cla = $tab_id['duree_max_tpi_cla'];
    $stop_form_cla = $tab_id['stop_form_cla'];
    $termine_cla = $tab_id['termine_cla'];
    $ref_grp = $tab_id['ref_grp'];
    $Titre = "Modification d'une Classe";
}
?>
    <div class="panel panel-primary">
        <div class="panel-heading">
            <?php echo $Titre; ?>
        </div>
        <div class="panel-body">
            <form class="form-horizontal" id="classe_form" method="post">
                <?php
                $new_tab_grp = array();

                //Input text -> Nom de la classe
                echo '<div class="form-group">';
                echo '<div class="col-md-3">';
                echo Form::label("nom_cla_lb", "Nom :", "nom_cla", "");
                echo '</div><div class="col-md-9">';
                echo Form::text("nom_cla", $nom_cla, "col-md-6", "", "");
                echo '</div></div>';

                //Input number -> les heures des modules
                echo '<div class="form-group">';
                echo '<div class="col-md-3">';
                echo Form::label("duree_tpi_cla", "Durée Min/Max TPI :", "duree_min_tpi_cla", "");
                echo '</div><div class="col-md-9">';
                echo Form::number("duree_min_tpi_cla", $duree_min_tpi_cla, "0", "127", "col-md-2", "", "1", "");
                //            echo Form::text("duree_min_tpi_cla", $duree_min_tpi_cla, "col-md-2", "", "", "", "", "");
                echo Form::label("", " / ", "", "col-md-1");
                echo Form::number("duree_max_tpi_cla", $duree_max_tpi_cla, "0", "127", "col-md-2", "", "1", "");
                //            echo Form::text("duree_max_tpi_cla", $duree_max_tpi_cla, "col-md-2", "", "", "", "", "");
                echo Form::label("", " Heures", "", "col-md-2");
                echo '</div></div>';

                //Input checkbox -> formation stoppée ou non
                echo '<div class="form-group">';
                echo '<div class="col-md-3">';
                echo Form::label("stop_form_cla_lb", "Formation interrompue avant terme :", "stop_form_cla", "");
                echo '</div><div class="col-md-9">';
                echo Form::checkbox("stop_form_cla", "", $stop_form_cla);
                echo '</div></div>';

                //Input select -> Etat de formation de la classe
                echo '<div class="form-group">';
                echo '<div class="col-md-3">';
                echo Form::label("termine_cla_lb", "Etat de la formation :", "termine_cla", "");
                echo '</div><div class="col-md-9">';
                $insert = array(0 => array("0", "En Cours"), 1 => array("1", "Terminée"));
                echo Form::select("termine_cla", $insert, 0, 1, '', $termine_cla, '');
                echo '</div></div>';

                //Input select -> reference au groupe actif
                echo '<div class="form-group">';
                echo '<div class="col-md-3">';
                echo Form::label("ref_grp_lb", "Groupe :", "ref_grp", "");
                echo '</div><div class="col-md-9">';
                echo Form::select("ref_grp", $tab_grp, 'id_grp', 'nom_grp', '', $ref_grp, "input-form");
                echo '</div></div>';

                if (!empty($_GET['id_cla'])) {
                    echo Form::submit("submit", "Modifier", "btn btn-primary", "action=modifier id_cla=" . $id_cla);
                } else {
                    echo Form::submit("submit", "Créer", "btn btn-primary", "action=creer id_cla=" . $id_cla);
                }
                echo '</div></form>';


                if (isset($_GET['edit'])) {
                    echo '<hr><div class="panel-body">'
                        . '<form class="form-horizontal" id="classe_dom_form" method="post">';

                    if (!empty($tab_links)) {
                        foreach ($tab_dom as $dom) {
                            foreach ($tab_links as $links) {
                                if ($dom['id_dom'] == $links['id_dom']) {
                                    echo '<div class="form-group">';
                                    echo Form::label("lbl" . $dom['id_dom'], $dom['nom_dom'], "dom_" . $dom['id_dom'], "col-md-3");
                                    echo '<div class="col-md-9">';
                                    echo Form::number("dom_" . $dom['id_dom'], $links['nb_mod'], "0", "127", "domain-area col-md-3", "", "1", "id_dom=" . $dom['id_dom'] . " id_cla=" . $id_cla);
                                    del_button($dom, $id_cla);
                                    echo '</div></div>';
                                    break;
                                } else if ($links == end($tab_links)) {
                                    echo '<div class="form-group">';
                                    echo Form::label("lbl" . $dom['id_dom'], $dom['nom_dom'], "dom_" . $dom['id_dom'], "col-md-3");
                                    echo '<div class="col-md-9">';
                                    echo Form::number("dom_" . $dom['id_dom'], "", "0", "127", "domain-area col-md-3", "", "", "id_dom=" . $dom['id_dom'] . " id_cla=" . $id_cla);
                                    del_button($dom, $id_cla, "none");
                                    echo '</div></div>';
                                    break;
                                }
                            }
                        }
                    } else {
                        foreach ($tab_dom as $dom) {
                            echo '<div class="form-group ">';
                            echo Form::label("lbl" . $dom['id_dom'], $dom['nom_dom'], "dom_" . $dom['id_dom'], "col-md-3");
                            echo '<div class="col-md-9">';
                            echo Form::number("dom_" . $dom['id_dom'], "", "0", "127", "domain-area col-md-3", "", "", "id_dom=" . $dom['id_dom'] . " id_cla=" . $id_cla);
                            del_button($dom, $id_cla, "none");
                            echo '</div></div>';
                        }
                    }


//                echo '<div class=" col-md-3 ">';
//                echo Form::submit("submit", "Modifier", "btn btn-primary", "action=modifier id_cla=" . $id_cla);
//                echo '</div>';
                    echo '</form></div>';
                }
                ?>
        </div>
    </div>
    <script src="./js/edit_classe.js"></script>
    <style>
        input[type=number]::-webkit-inner-spin-button,
        input[type=number]::-webkit-outer-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }
    </style>
    </body>
    </html>
<?php

function del_button($dom, $id_cla, $display = "inline") {
    $display = 'style="display:' . $display . '"';
    $form = new Form();
    echo Form::button("btn_" . $dom['id_dom'], "", "X", "btn btn-danger delete-btn", "", $display . " action=delete  id_dom=" . $dom['id_dom'] . " id_cla=" . $id_cla);
}

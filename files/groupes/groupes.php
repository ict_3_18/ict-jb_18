<?php
session_start();
require("../config/config.inc.php");
require_once(WAY . "/includes/autoload.inc.php");
$aut = 'ADM_GRP;USR_GRP';
require(WAY . "./includes/secure.inc.php");
require_once(WAY . "/includes/head.inc.php");

$grp = new Groupe();
$tab_grp = $grp->get_all_order_by_archive();
$cla = new Classe();
$tab_cla = $cla->get_all();
$tab_n = $cla->get_all_actifs();
$test_archive = 0;
//$tab_color = $cla->get_Select_GroupColor();
?>
<div style="padding:0;margin-left:10%;margin-right: 10%"class="panel panel-primary col-md-10">
    <div class="panel-heading">
        Groupes
    </div>

    <table class="table table-bordered index " style="border:2px solid lightgray;">
        <tr>
            <td>
                <?php
                echo '<div class="col-md-8">';
                echo '<form method="post" action="edit_groupe.php">';
                echo Form::submit("creer", "Créer un groupe", "btn btn-primary");
                echo '</form>';
                echo '</div>';


                if(empty($_POST) OR $_POST['affichage'] != "archivé") {
                    echo '<div class="col-md-4">';
                    echo '<form method="post" action="groupes.php">';
                    echo Form::hidden("affichage", "archivé");
                    echo Form::submit("btn_affichage", "Afficher groupes archivés", "btn btn-primary");
                    echo '</form>';
                    echo '</div>';
                }else{
                    echo '<div class="col-md-4">';
                    echo '<form method="post" action="groupes.php">';
                    echo Form::hidden("affichage", "actif");
                    echo Form::submit("btn_affichage", "Afficher groupes actifs", "btn btn-primary");
                    echo '</form>';
                    echo '</div>';
                }
                ?>
            </td>
        </tr>
    </table>
    <div class="panel-body">
        <?php
        if(!empty($_POST)){
            if($_POST['affichage'] == "archivé") {
                $test_archive = 1;
            }

            if($_POST['affichage'] == "actif") {
                $test_archive = 0;
            }
        }
        if($test_archive == 0){
            echo '<h3 class="text-center">Groupes actifs</h3><hr>';
        }else{
            echo '<h3 class="text-center">Groupes archivés</h3><hr>';
        }

        foreach ($tab_grp as $g) {
            if($g['archive_grp'] == $test_archive){
                $tab_all_cla = $grp->get_tab_all_cla_by_grp($g['id_grp']);
                ?>
<!-- border:3px solid <?= $g['color_grp'] ?>; -->
                <div class="col-md-6 groupe" style="background:<?=$g['color_grp']?>;">
                    <div class="col-md-4">
                        <form method="get" action="edit_groupe.php">
                            <?= Form::hidden("id_grp", $g['id_grp'])?>
                            <?= Form::submit("modification", $g['nom_grp'], "btn btn-primary col-md-12", 'style="border-color:black"')?>
                        </form>

            <form method="post" action="etat_notes_grp.php">
            <?=Form::hidden("id_grp", $g['id_grp'])?>
            <?=Form::submit("Notes", "Notes", "btn btn-primary col-md-12", 'style="border-color:black"')?>
            </form>


                        <form method="post" action="./pdf/bulletin_grp.pdf.php">
                            <input type="hidden" name="id_grp" id="id_grp" value="<?= $g['id_grp'] ?>">
                            <input type="submit" name="Notes" id="Notes" value="Bulletin Dual" class="btn btn-primary btn-xs col-md-6" style="border-color:black">
                            <input type="submit" name="Notes" id="Notes" value="Bulletin Classic" class="btn btn-primary btn-xs col-md-6" style="border-color:black">
                        </form>
                    </div>
                    <div class="col-md-8">
                        <?php
                        foreach ($tab_all_cla as $cla) {
                            echo '<a href="edit_classe.php?id_cla=' . $cla['id_cla'] . '&edit=Editer" class="grp-href col-md-6"><button class=" btn btn-xs col-md-12">' . $cla['nom_cla'] . "</button></a>";
                        }
                        ?>
                    </div>
                </div>

                <?php
                /*
                echo '<div class="col-md-6">';
                echo '<table class="table " style="border:3px solid ' . $g['color_grp'] . '; background:' . $g['color_grp'] . ';">';
                echo '<tr>';
                echo '<td class="col-md-2">';
                echo '<form method="get" action="edit_groupe.php">';
                echo Form::hidden("id_grp", $g['id_grp']);
                if ($g['archive_grp']) {
                    echo Form::submit("modification", $g['nom_grp'], "btn btn-default col-md-12", 'style="border-color:black"');
                } else {
                    echo Form::submit("modification", $g['nom_grp'], "btn btn-primary col-md-12", 'style="border-color:black"');
                }
                echo '</form>';

                echo '<form method="post" action="./xlsx/etat_notes_classe.xlsx.php">';
                echo Form::hidden("id_grp", $g['id_grp']);
                echo Form::submit("Notes", "Notes", "btn btn-primary col-md-12", 'style="border-color:black"');
                echo '</form>';
                echo '<form method="post" action="./pdf/bulletin_grp.pdf.php">';

                echo Form::hidden("id_grp", $g['id_grp']);
                echo Form::submit("Notes", "Bulletin", "btn btn-primary col-md-12", 'style="border-color:black"');
                echo '</form>';
                echo '</td>';
                echo '<td class="col-md-7">';

                echo '<div class="grp-list-cla" style="background:white; padding-left:5px;  padding-top:5px;  padding-bottom:5px; height:auto;">';
                foreach ($tab_all_cla as $cla) {
                    echo '<a href="edit_classe.php?id_cla=' . $cla['id_cla'] . '&edit=Editer" class="grp-href col-md-4">' . $cla['nom_cla'] . "</a>";
                }
                echo '</div></td>';
                echo '</tr>';
                echo '</table></div>';*/

            }
        }

        ?>
        </table>
    </div>
    <style>
        .checkbox-inline, .nom-grp{
            font-size: 15px;
            padding: 0;
        }
        .checkbox-inline:hover{
            font-size: 16px;
            font-weight: bold;
        }
    </style>
    <script src="./js/groupes.js"></script>
</div>
</body>
</html>
<?php
session_start();
$aut = 'ADM_GRP';
require '../config/config.inc.php';
require(WAY."/includes/secure.inc.php");
require WAY . "/includes/head.inc.php";
require_once(WAY."/includes/autoload.inc.php");

$grp = new Groupe();
$tab_grp = $grp->get_all();

$id_grp = "";
$nom_grp = "";
$color_grp = "";
$archive_grp = "";
if (!empty($_GET['id_grp'])) {
    $get_tab_id = $grp->get_grp_with_id($_GET['id_grp']);
    $tab_id = $get_tab_id[0];
    $id_grp = $tab_id['id_grp'];
    $nom_grp = $tab_id['nom_grp'];
    $color_grp = $tab_id['color_grp'];
    $archive_grp = $tab_id['archive_grp'];
}

$tab_couleurs = array("#F66" => "Rouge - 4xx 9xx","#0AF" => "Bleu - 2xx 7xx","#6F0" => "Vert - 3xx 8xx","#FF0" => "Jaune - 1xx 6xx","#FFF" => "Blanc - 0xx 1xx");

/*echo '<pre>';
print_r($tab_couleurs);
echo '</pre>';*/

//print_r(array_keys($tab_couleurs,"Rouge - 4xx 9xx"));

?>
<div class="panel panel-primary">
    <div class="panel-heading">
        Modification d'un groupe
    </div>
    <div class="panel-body">
        <form class="form-horizontal" id="groupe_form" method="post">
            <?php
            //Nom_grp, Envoie le nom du groupe
            echo '<div class="form-group">';
            echo Form::label("nom_grp_lb", "Nom :", "nom_grp", "col-md-3");
            echo '<div class="col-md-9">';
            echo Form::text("nom_grp", $nom_grp, "", "", "");
            echo '</div></div>';

            //Color_grp, Envoie la couleur du groupe
            echo '<div class="form-group">';
            echo Form::label("color_grp_lb", "Couleur :", "color_grp", "col-md-3");
            echo '<div class="col-md-9">';
            /*
            echo Form::text("color_grp", $color_grp, "", "", "");
            */
            echo '<select class="" id="color_grp" name="color_grp">';

            if($color_grp == "#FFF"){
                echo '<option selected value="#FFF">Blanc - 0xx 5xx</option>';
            }else{
                echo '<option value="#FFF">Blanc - 0xx 5xx</option>';
            }

            if($color_grp == "#FF0"){
                echo '<option selected value="#FF0">Jaune - 1xx 6xx</option>';
            }else{
                echo '<option value="#FF0">Jaune - 1xx 6xx</option>';
            }

            if($color_grp == "#0AF"){
                echo '<option selected value="#0AF">Bleu - 2xx 7xx</option>';
            }else{
                echo '<option value="#0AF">Bleu - 2xx 7xx</option>';
            }

            if($color_grp == "#6F0"){
                echo '<option selected value="#6F0">Vert - 3xx 8xx</option>';
            }else{
                echo '<option value="#6F0">Vert - 3xx 8xx</option>';
            }

            if($color_grp == "#F66"){
                echo '<option selected value="#F66">Rouge - 4xx 9xx</option>';
            }else{
                echo '<option value="#F66">Rouge - 4xx 9xx</option>';
            }

            echo '</select>';
            echo '</div></div>';

            //Archive_grp, Envoie si le groupe est archivé 
            echo '<div class="form-group">';
            echo Form::label("archive_grp_lb","Archivé :", "archive_grp", "col-md-3");
            echo '<div class="col-md-9">';
            echo Form::checkbox("archive_grp", "", $archive_grp);
            echo '</div></div>';

            echo'<div class="form-group">';
            echo'<div class=" col-md-3 ">';
            if (!empty($_GET['id_grp'])) {
                echo Form::submit("submit", "Modifier", "btn btn-primary", "action=modifier id_grp=" . $id_grp);
            } else {
                echo Form::submit("submit", "Créer", "btn btn-primary", "action=creer id_grp=" . $id_grp);
            }
            echo '</div></div>';
            ?>
        </form>
    </div>
</div>
<script src="./js/edit_groupe.js"></script>
</body>
</html>

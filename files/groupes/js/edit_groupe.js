$(function () {

    $("#groupe_form").validate({
        rules: {
            nom_grp: {
                required: true
            },
//            color_grp: {
//                required: true
//            }
        },

        messages: {
            nom_grp: {
                required: "requis"
            },
//            color_grp: {
//                required: "requis"
//            }
        },
        submitHandler: function (form) {

            if ($('#archive_grp').is(':checked')) {
                var archive_grp = 1;
            } else {
                var archive_grp = 0;
            }


            $("#loading").css("display", "block");
            $.post(
                    "./json/add_groupe.json.php?_=" + Date.now(),
                    {
                        nom_grp: $("#nom_grp").val(),
                        color_grp: $("#color_grp").val(),
                        archive_grp: archive_grp,
                        id_grp: $("#submit").attr("id_grp"),
                        action: $("#submit").attr("action")

                    },
                    function result(data, status) {
                        //Ajoute le message
                        $("#loading").css("display", "none");
                        if (data.action == "creer") {
                            $("#ajout_per_form").trigger("reset");
                        }
                        message(data.message.texte, data.message.type);

                    }
            );
        }
    });

});



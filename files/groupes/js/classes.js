$(function(){
    $("table").on("click",".lock_notes",function(){
        $.post(
            './json/lock_cla.json.php?_='+Date.now(),
            {
                id_cla: $(this).attr("id_cla")
            }
        ).done(function (data) {
            $("#lock_"+data.id_cla).removeClass("btn-primary").removeClass("lock_notes").addClass("btn-danger").addClass("unlock_notes");
            $("#span_"+data.id_cla).removeClass("glyphicon-eye-open").addClass("glyphicon-eye-close");
        });
    });

    $("table").on("click",".unlock_notes",function(){
        $.post(
            './json/unlock_cla.json.php?_='+Date.now(),
            {
                id_cla: $(this).attr("id_cla")
            }
        ).done(function (data) {
            $("#lock_"+data.id_cla).removeClass("btn-danger").removeClass("unlock_notes").addClass("btn-primary").addClass("lock_notes");
            $("#span_"+data.id_cla).removeClass("glyphicon-eye-close").addClass("glyphicon-eye-open");
        });
    });
});


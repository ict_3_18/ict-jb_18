$(function () {
    $("#classe_form").validate({
        rules: {
            nom_cla: {
                required: true
            },
            duree_min_tpi_cla: {
                required: true
            },
            duree_max_tpi_cla: {
                required: true
            }
        },

        messages: {
            nom_cla: {
                required: "requis"
            },
            duree_min_tpi_cla: {
                required: "requis"
            },
            duree_max_tpi_cla: {
                required: "requis"
            }
        },
        submitHandler: function (form) {

            if ($('#stop_form_cla').is(':checked')) {
                var stop_form = 1;
            } else {
                var stop_form = 0;
            }

            console.log($("ref_grp").val());
            $("#loading").css("display", "block");
            $.post(
                    "./json/add_classe.json.php?_=" + Date.now(),
                    {
                        nom_cla: $("#nom_cla").val(),
                        duree_min_tpi_cla: $("#duree_min_tpi_cla").val(),
                        duree_max_tpi_cla: $("#duree_max_tpi_cla").val(),
                        termine_cla: $("#termine_cla").val(),
                        stop_form_cla: stop_form,
                        ref_grp: $("#ref_grp").val(),
                        id_cla: $("#submit").attr("id_cla"),
                        action: $("#submit").attr("action")

                    },
                    function result(data, status) {
                        //Ajoute le message
                        $("#loading").css("display", "none");
                        if (data.action == "creer") {
                            document.location.href = "edit_classe.php?id_cla=" + data.id_cla + "&edit=Editer";
                        }
                        message(data.message.texte, data.message.type);
                    }
            );
        }
    });

    $('.domain-area').change(function () {
        $.post(
                "./json/add_cla_dom.json.php?_=" + Date.now(),
                {
                    id_cla: $(this).attr("id_cla"),
                    id_dom: $(this).attr("id_dom"),
                    nb_mod: $(this).val()
                },
                function (data, status) {
                    message(data.message.texte, data.message.type);
                    if (data.action == "add") {
                        $('#btn_' + data.id_dom).css("display", "inline-block");
                    } else if (data.action == "delete") {
                        $('input#dom_' + data.id_dom).val("");
                        $('#btn_' + data.id_dom).css("display", "none");
                    }
                }
        );
    });

    $('.delete-btn').click(function () {
        $.post(
                "./json/add_cla_dom.json.php?_=" + Date.now(),
                {
                    id_cla: $(this).attr("id_cla"),
                    id_dom: $(this).attr("id_dom"),
                    nb_mod: ""
                },
                function (data, status) {
                    message(data.message.texte, data.message.type);
                    $('input#dom_' + data.id_dom).val("");
                    $('#btn_' + data.id_dom).css("display", "none");
                }
        );
    }
    );
}
);



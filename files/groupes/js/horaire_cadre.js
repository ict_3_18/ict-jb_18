$(function () {

    $(".tranche-horaire").click(function () {

        var valChx = $("input[type=radio]:checked").val();

        if ($(this).attr("id_horaire") == 0) {

            $(this).attr("id_horaire", valChx);

        } else if (valChx == $(this).attr("id_horaire")) {

            $(this).attr("id_horaire", "0");

        } else {

            $(this).attr("id_horaire", valChx);

        }

        change(this);

        check_btn_del_da($(this).attr("id_j"));

    });

 

    $(".pressed-day").click(function () {

        var valChx = $("input[type=radio]:checked").val();

 

        for (var i = 0, max = 20; i < max; i++) {

            var str = "#" + i.toString() + $(this).attr("id");

 

            if ($(str).hasClass("Pause")) {

                continue;

            } else {

                if ($("#bPratique").is(":checked")) {

                    if ($(str).attr("pause_pr") == "true") {

                    } else {

                        change(str, valChx);

                    }

                } else {

                    if ($(str).attr("pause_th") == "true") {

                    } else {

                        change(str, valChx);

                    }

                }

            }

        }

        check_btn_del_da($(this).attr("id"));

    });

 

    $(".btn-delete").click(function () {

        var id_j = $(this).attr("id_j");

        for (var i = 0, max = 20; i <= max; i++) {

            var str = "#" + i.toString() + id_j;

            change(str, 0);

 

            $(this).parent().slideUp(500, function () {

                $(this).css("display", "none");

            });

        }

    });

 

    $(".delete-tab-btn").click(function () {

        var jour;

        for (var i = 0, max = 5; i < max; i++) {

            if (i == 0) {

                jour = "lu";

            } else if (i == 1) {

                jour = "ma";

            } else if (i == 2) {

                jour = "me";

            } else if (i == 3) {

                jour = "je";

            } else if (i == 4) {

                jour = "ve";

            }

            for (var y = 0, max = 20; y <= max; y++) {

                var str = "#" + y.toString() + jour;

                change(str, 0);

            }

        }

        $.post(

                "./../groupes/json/update_cla_horaire.json.php?_=" + Date.now(),

                {

                    id_cla: $(this).attr("id_cla"),

                    action: $(this).attr("action")

                },

                function (data, status) {

                    message(data.message.texte, data.message.type);

 

                    $(".btn-delete").parent().slideUp(500, function () {

                        $(this).css("display", "none");

                    });

 

                }

        );

    });

 

 

 

    $(".ajout-btn").click(function () {

        var jour;

        var id_jour;

        var tab_horaire = "";

        for (var i = 0, maxi = 5; i < maxi; i++) {

            id_jour = i;

            if (i == 0) {

                jour = "lu";

            } else if (i == 1) {

                jour = "ma";

            } else if (i == 2) {

                jour = "me";

            } else if (i == 3) {

                jour = "je";

            } else if (i == 4) {

                jour = "ve";

            }

            for (var y = 0, max = 20; y <= max; y++) {

                var str = "#" + y.toString() + jour;

                if ($(str).attr("id_horaire") > 0) {

                    var id_horaire = $(str).attr("id_horaire");

                    var heure_debut = $(str).attr("h_tr");

                    var heure_fin = "";

                    for (var x = y, max = 20; x <= max; x++) {

                        var str2 = "#" + x.toString() + jour;

                        heure_fin = $(str2).attr("h_tr");

                        if ($(str2).attr("id_horaire") == id_horaire) {

                            continue;

                        } else {

 

                            y = x - 1;

                            break;

                        }

                    }

                    tab_horaire += id_jour + ";" + heure_debut + ";" + heure_fin + ";" + id_horaire + "|";

                }

            }

        }

        $.post(

                "./../groupes/json/update_cla_horaire.json.php?_=" + Date.now(),

                {

                    id_cla: $(this).attr("id_cla"),

                    action: $(this).attr("action"),

                    tab_horaire: tab_horaire

                },

                function (data, status) {

                    message(data.message.texte, data.message.type);

                }

        );

    });

 

    $('input[type="radio"]').change(function () {

        $('.glyphicon-triangle-right').removeClass("glyphicon glyphicon-triangle-right");

        $(this).parent().children("span").addClass("glyphicon glyphicon-triangle-right");

    });

 

    $(".hover-day").hover(function () {

        if ($(this).is(":hover")) {

            $(this).css("background", "lightsteelblue");

            $(this).parent().children(".td-del").css("background", "lightsteelblue");

        } else {

            $(this).css("background", "transparent");

            $(this).parent().children(".td-del").css("background", "transparent");

        }

    });

 

    function change(str, valChx) {

        $(str).attr("id_horaire", valChx);

 

        if ($(str).attr("id_horaire") == 0) {

            $(str).css("background", "");

        } else if ($(str).attr("id_horaire") == 1) {

            $(str).css("background", "yellow");

        } else if ($(str).attr("id_horaire") == 2) {

            $(str).css("background", "red");

        } else if ($(str).attr("id_horaire") == 3) {

            $(str).css("background", "blue");

        } else if ($(str).attr("id_horaire") == 4) {

            $(str).css("background", "orange");

        }

    }

 

    function check_btn_del_da(id_j) {

        var rempli = false;

        for (var i = 0, max = 20; i <= max; i++) {

            var str = "#" + i.toString() + id_j;

            if ($(str).attr("id_horaire") > 0) {

//                $("#" + id_j).children(".champ-del").css("display", "block").slideDown(100);

                $("#" + id_j).children(".champ-del").slideDown(500, function () {

                    $(this).css("display", "block");

                });

                rempli = true;

                break;

            } else {

//                $("#" + id_j).children(".champ-del").css("display", "none").slideUp(100);

                rempli = false;

            }

        }

        if (rempli == false) {

            $("#" + id_j).children(".champ-del").slideUp(500, function () {

                $(this).css("display", "none");

            });

        }

 

    }

});
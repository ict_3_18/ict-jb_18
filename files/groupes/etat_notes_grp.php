<?php

require_once substr(__dir__,0,strpos(__dir__,"files")+6)."config/config.inc.php";
//require("../../config/config.inc.php");

//include(WAY . "/class/class.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

session_start();
$aut = 'ADM_GRP';
require(WAY . "./includes/secure.inc.php");
require_once(WAY . "/includes/head.inc.php");

$grp = new Groupe();
//require_once(WAY . "/plugins/PhpSpreadsheet/vendor/autoload.php");

/*use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;*/

//print_r($_POST);

$grp =  new Groupe($_POST['id_grp']);
$tab_can = $grp->get_tab_can_by_grp_no_archive_no_termine($grp->get_id_grp());

$tab_dom = array();
//echo "<pre>";
foreach($tab_can AS $candidat) {
    $can = new Candidat($candidat['id_can']);

    $tab_notes_can = $can->get_all_notes_by_dom();
    $tab_notes[$can->get_id()] = $tab_notes_can;
    $tab_keys_dom = array_keys($tab_notes_can);
    ksort($tab_keys_dom);
    foreach($tab_keys_dom AS $dom){
        if($dom == 5 OR $dom == 6) {
            foreach ($tab_notes_can[$dom] AS $key => $exa) {
                $tab_dom[$dom][$key] = $key;
            }
            ksort($tab_dom[$dom]);
        }
    }
}
//print_r($tab_);
//print_r($tab_dom);
//print_r($tab_notes);
//echo "</pre>";
?>
<table class="table table-bordered table-condensed table-striped table-hover">
    <tr>
        <th><?= $grp->get_nom_grp() ?></th>
<?php

foreach($tab_dom AS $key => $dom){
    foreach ($tab_dom[$key] AS $exa) {
        echo "<th  class=\"text-center\">" . $exa . "</th>";
    }
    echo "<th class=\"text-center\">Moyenne</th>";
}
echo "<th class=\"text-center\">Comp. info.</th>";

echo "</tr>";


/*echo "<pre>";
print_r($tab_can);
echo "</pre>";*/

foreach($tab_can AS $can){
    echo "<tr>";
    echo "<th>".$can['nom_can']." ".$can['prenom_can']."</th>";
    foreach($tab_dom AS $key => $dom){
        $nb_notes = 0;
        $somme_notes = 0;
        ini_set('error_reporting',~E_NOTICE);
        foreach ($dom AS $exa) {
            $note = $tab_notes[$can['id_can']][$key][$exa];
            echo "<td class=\"text-center\">";
            if($note){
                $nb_notes++;
                $somme_notes += $note;
                echo $note;
            }
            echo "</td>";
        }
        $moyenne = round($somme_notes/$nb_notes,2);
        $moyenne_demi = round(($moyenne*2),0)/2;
        $tab_moyennes[$key][10] = $moyenne;
        $tab_moyennes[$key][2] = $moyenne_demi;
        $color = "";
        if($moyenne < 3.75){
            $color = "bg-danger";
        }elseif($moyenne < 4){
            $color = "bg-warning";
        }
        echo "<th class=\"text-center ".$color."\">". $moyenne . " / ". $moyenne_demi . "</th>";
    }
    $moyenne = ($tab_moyennes[5][2]+4*$tab_moyennes[6][2])/5;
    if($moyenne < 3.75){
        $color = "bg-danger";
    }elseif($moyenne < 4){
        $color = "bg-warning";
    }
    $moyenne_demi = round(($moyenne*2),0)/2;
    echo "<th class=\"text-center ".$color."\">". $moyenne . "</th>";
    echo "</tr>";
}
?>
</table>

<div class="btn">
    <form action="xlsx/etat_notes_grp.xlsx.php" method="post" >
        <?php echo Form::hidden("id_grp",$_POST['id_grp']); ?>
        <button type="submit" id="btnExport" name='export'
                value="Export to Excel" class="btn btn-success">
            Export to Excel
        </button>
    </form>
</div>


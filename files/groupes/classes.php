<?php
session_start();
require("../config/config.inc.php");
require_once(WAY . "/includes/autoload.inc.php");
$aut = 'ADM_GRP;USR_GRP';
require(WAY . "./includes/secure.inc.php");
require_once(WAY . "/includes/head.inc.php");
//$form = new Form();
$grp = new Groupe();
$tab_grp = $grp->get_all();
$cla = new Classe();
$tab_cla = $cla->get_all();
$tab_n = $cla->get_all_actifs();
$tab_order_t = $cla->get_all_order_by_termine();

/*echo '<pre>';
print_r($tab_order_t);
echo'</pre>';*/
?>
<div style="padding:0;margin-left:10%;margin-right: 10%"class="panel panel-primary col-md-10">
    <div class="panel-heading">
        Classes
    </div>

    <table class="table table-bordered index " style="border:2px solid lightgray;">
        <tr>
            <td>
                <?php
                echo '<div class="col-md-5">';
                echo '<form method="post" action="edit_classe.php">';
                echo Form::submit("creer", "Créer une classe", "btn btn-primary");
                echo '</form>';
                echo '</div>';
                ?>  
            </td>
        </tr>
    </table>
    <div class="panel-body">   

        <?php
        $button = array(
            "editer" => array("name" => "Editer", "class" => "edit", "action"=> "edit_classe.php"),
            "bulletin" => array("name" => "Bulletin", "class" => "bulletin", "action"=> ""),
            "validation" => array("name" => "Validation", "class" => "valide", "action"=> ""),
            "recapCB" => array("name" => "Récap CB", "class" => "recapcb", "action"=> ""),
            "details" => array("name" => "Détails", "class" => "details", "action"=> ""),
            "horaire" => array("name" => "Horaire cadre", "class" => "horaire", "action"=> "Horaire cadre.php")
        );

        echo '<table class="table table-bordered text-center">';
        foreach ($tab_order_t as $cla) {
            echo '<tr style="background-color:' . $cla['color_grp'] . '" >';
            echo '<td>';
            //echo substr($cla['nom_cla'], -7) . " (" . $cla['nom_grp'] . ")";
            //echo $cla['nom_cla'] . " (" . $cla['nom_grp'] . ")";
            echo $cla['nom_grp'] . " - " . $cla['nom_cla'];
            echo '</td>';
            echo '<td>';
            if($cla['notes_open']){
                echo '<button type="button" id="lock_'.$cla['id_cla'].'" class="btn btn-primary btn-sm lock_notes" id_cla="'.$cla['id_cla'].'"><span id="span_'.$cla['id_cla'].'" class="glyphicon glyphicon-eye-open"></span></button>';
            }else{
                echo '<button type="button" id="lock_'.$cla['id_cla'].'" class="btn btn-danger btn-sm unlock_notes" id_cla="'.$cla['id_cla'].'"><span id="span_'.$cla['id_cla'].'" class="glyphicon glyphicon-eye-close"></span></button>';
            }

            echo '</td>';
            foreach ($button as $b) {
                echo '<td>';
                echo '<form method="get" action="'.$b['action'].'">';
                echo Form::hidden("id_cla", $cla['id_cla']);
                echo Form::submit($b['class'], $b['name'], "btn btn-inverse " . $b['class'] . "\"", 'style="width:90%;border-color:black; background-color:' . $cla['color_grp'] . '"');
                echo '</form>';
                echo '</td>';
            }
            echo '</tr>';
        }
        echo '</table>';
        ?>
        </table>     
    </div>
    <script src="./js/classes.js"></script>
</div>
</body>
</html>
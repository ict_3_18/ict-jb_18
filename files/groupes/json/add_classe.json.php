<?php
header("Content-Type: application/json");
require("../../config/config.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

session_start();
$aut = 'ADM_GRP';

require(WAY . "./includes/secure.inc.php");

$cla = new Classe();
$dom = new Domaine();
$grp = new Groupe($_POST['ref_grp']);

$tab = $_POST;

$color = $grp->get_color_grp();

$tab['couleur_cla'] = $color;
sleep(1);

if ($tab['action'] == "creer") {
    array_splice($tab, -3, 2);
    $id = $cla->add($tab);
    $return['reponse'] = true;
    $return['action'] = "creer";
    $return['id_cla'] = $id;
    $return['message']['texte'] = "La Classe a été ajoutée !";
    $return['message']['type'] = "success";
} else {
    array_splice($tab, -2, 1);
    $id = $cla->update($tab);
    $return['reponse'] = true;
    $return['action'] = "edit";
    $return['id_cla'] = $tab['id_cla'];
    $return['message']['texte'] = "La Classe a été Modifiée !";
    $return['message']['type'] = "success";
}

echo json_encode($return);

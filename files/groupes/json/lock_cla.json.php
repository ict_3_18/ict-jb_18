<?php
header("Content-Type: application/json");
session_start();

require("./../../config/config.inc.php");
$aut = 'ADM_GRP';
require(WAY . "./includes/secure.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

$cla = new Classe($_POST['id_cla']);
//$cla = new Classe(52);
$return =  array();
$return['reponse']['dd'] = false;
//echo $cla->unlock();
if ($cla->lock()) {
    $return['reponse'] = true;
    $return['action'] = "creer";
    $return['id_cla'] = $cla->get_id_cla();
    $return['message'] = array();
    $return['message']['texte'] = "La Classe a été vérouillée !";
    $return['message']['type'] = "success";
}

echo json_encode($return);
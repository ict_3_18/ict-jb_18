<?php

header("Content-Type: application/json");
require("../../config/config.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

session_start();
$aut = 'ADM_GRP';

require(WAY . "./includes/secure.inc.php");

$h_cad = new Horaire();

$tab_horaire = array();

if ($_POST['action'] == "ajout") {
    $Array_horaire = explode("|", $_POST['tab_horaire']);
    foreach ($Array_horaire as $horaire) {
        $horaire = explode(";", $horaire);
        array_push($tab_horaire, $horaire);
    }
    array_pop($tab_horaire);

    $h_cad->delete_horaire_by_cla($_POST['id_cla']);

    $tab = array();
    foreach ($tab_horaire as $index => $horaire) {
        $tranche_horaire['jour_cad'] = $horaire[0];
        $tranche_horaire['debut_cad'] = $horaire[1];
        $tranche_horaire['fin_cad'] = $horaire[2];
        $tranche_horaire['pause_cad'] = "";
        $tranche_horaire['ref_cla'] = $_POST["id_cla"];
        $tranche_horaire['ref_bra'] = $horaire[3];

        $return = $h_cad->add($tranche_horaire);

        array_push($tab, $tranche_horaire);
    }
}else if($_POST['action'] == "supprimer"){
    $h_cad->delete_horaire_by_cla($_POST['id_cla']);
}

$tab['reponse'] = true;
$tab['message']['texte'] = "L'horaire a été mis à jour !";
$tab['message']['type'] = "success";

echo json_encode($tab);



<?php
header("Content-Type: application/json");
require("../../config/config.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

session_start();
$aut = 'ADM_GRP';

require(WAY . "./includes/secure.inc.php");

$dom = new Domaine($_POST['id_dom']);

$nb_mod = $dom->get_nb_mod_by_cla($_POST['id_cla']);

if ($nb_mod) {
    if ($_POST['nb_mod'] > 0) {
        $tab['reponse'] = $dom->update_cla_dom($_POST);
        if ($tab['reponse']) {
            $tab['message']['type'] = "success";
            $tab['message']['texte'] = "Le nombre de module pour cette classe a été mis à jour";
            $tab['action'] = "update";
            $tab['id_dom'] = $dom->get_id_dom();
        } else {
            $tab['message']['type'] = "danger";
            $tab['message']['texte'] = "La modification n'a pas pu se faire";
        }
    } else {
        $tab['reponse'] = $dom->delete_cla_dom($_POST['id_cla']);
        if ($tab['reponse']) {
            $tab['message']['type'] = "success";
            $tab['message']['texte'] = "Le nombre de module pour cette classe a été mis à jour";
            $tab['action'] = "delete";
            $tab['id_dom'] = $dom->get_id_dom();
        } else {
            $tab['message']['type'] = "danger";
            $tab['message']['texte'] = "La modification n'a pas pu se faire";
        }
    }
} else {
    $tab['reponse'] = $dom->add_cla_dom($_POST);
    if ($tab['reponse']) {
        $tab['message']['type'] = "success";
        $tab['message']['texte'] = "Le nombre de module pour cette classe a été mis à jour";
        $tab['action'] = "add";
        $tab['id_dom'] = $dom->get_id_dom();
    } else {
        $tab['message']['type'] = "danger";
        $tab['message']['texte'] = "La modification n'a pas pu se faire";
    }
}
echo json_encode($tab);

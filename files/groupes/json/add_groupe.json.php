<?php
header("Content-Type: application/json");
require("../../config/config.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

session_start();
$aut = 'ADM_GRP';

require(WAY . "./includes/secure.inc.php");

$grp = new Groupe();
$tab = $_POST;

sleep(1);
//print_r($tab);
if ($tab['action'] == "creer") {
    array_splice($tab, -2, 2);
    $id = $grp->add($tab);
    $return['reponse'] = true;
    $return['action'] = "creer";
    $return['id_grp'] = $id;
    $return['message']['texte'] = "Le Groupe a été ajouté !";
    $return['message']['type'] = "success";
} else {
    array_splice($tab, -1, 1);
    $tab = $grp->update($tab);
    $return['reponse'] = true;
    $return['action'] = "edit";
    $return['id_grp'] = $tab['id_grp'];
    $return['message']['texte'] = "Le Groupe a été Modifié !";
    $return['message']['type'] = "success";
}

echo json_encode($return);


<?php
require_once substr(__dir__,0,strpos(__dir__,"files")+6)."config/config.inc.php";

require_once(WAY . "/includes/autoload.inc.php");
error_reporting(E_ALL);
ini_set('display_errors', 1);
session_start();
$aut = 'ADM_GRP';
require(WAY . "/includes/secure.inc.php");

require WAY .'/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();


$grp = new Groupe($_POST['id_grp']);

header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$grp->get_nom_grp().'.xlsx"');
header('Cache-Control: max-age=0');

$tab_can = $grp->get_tab_can_by_grp_no_archive_no_termine($grp->get_id_grp());

$tab_dom = array();

//Tableau de style pour le Titre
$styleTitleArray = [
    'font' => [
        'bold' => true,
        'size' => '14',
    ],
    'borders' => [
        'allBorders'=>[
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
        ],
    ],
];

//Tableau de style pour les numéros de modules
$styleNoModuleArray = [
    'font' => [
        'bold' => true,
        'size' => '12',
    ],
    'alignment' => [
        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
    ],
    'borders' => [
        'allBorders'=>[
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
        ],
    ],
];

//Tableau de style pour les noms
$styleNameArray = [
    'alignment' => [
        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
    ],
    'borders' => [
        'allBorders'=>[
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
        ],
    ],
];


//Tableau de style pour les notes)
$styleNoteArray = [
    'alignment' => [
        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
    ],
    'borders' => [
        'allBorders'=>[
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
        ],
    ],
];

//Tableau de style pour les moyennes
$styleMoyenneArray = [
    'font' => [
        'bold' => true,
        'size' => '14',
    ],
    'borders' => [
        'allBorders'=>[
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
        ],
    ],
    'alignment' => [
        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
    ],
];

//Affichage du titre dans la première cellule et appliquer le style
$sheet->setCellValue('A1', $grp->get_nom_grp());
$sheet->getStyle('A1')->applyFromArray($styleTitleArray);

//Préparation des différents tableaux
foreach($tab_can AS $candidat) {
    $can = new Candidat($candidat['id_can']);

    $tab_notes_can = $can->get_all_notes_by_dom();
    $tab_notes[$can->get_id()] = $tab_notes_can;
    $tab_keys_dom = array_keys($tab_notes_can);
    ksort($tab_keys_dom);
    foreach($tab_keys_dom AS $dom){
        if($dom == 5 OR $dom == 6) {
            foreach ($tab_notes_can[$dom] AS $key => $exa) {
                $tab_dom[$dom][$key] = $key;
            }
            ksort($tab_dom[$dom]);
        }
    }
}

//Création d'un tableau remplis des lettres qui partent de A à AAA
for($col = 'A'; $col != 'AAA'; $col++) {
    $tab_alph[] = $col;
}

//Création d'un tableau qui permet de déterminer les différentes positions des notes/moyennes
$tab_pos_dom = array();
$tab_pos_dom[6]['debut'] = 'B';

$index_dom = 0;
$offset=1;
for($i=6;$i>=5;$i--){
//foreach($tab_dom AS $key_dom => $dom){
    $index_dom ++;
	$tab_exa = $tab_dom[$i];
	$first = true;
	$cpt = 0;
	foreach($tab_exa AS $key => $value) {
		if($first){
			$tab_pos_dom[$i]['debut'] = $tab_alph[$cpt + $offset];
			$first = false;
		}
		$cpt++;
		//Affichage des numéros de modules
		$sheet->setCellValue($tab_alph[$offset + $cpt - 1] . '1', $value);
		//Déterminer la position des notes de compétence informatique
		
		//Appliquer le style des numéros de module
		$sheet->getStyle($tab_alph[$offset + $cpt - 1] . '1')->applyFromArray($styleNoModuleArray);
	}
	$pos = $cpt + $offset - 1;
	$tab_pos_dom[$i]['fin'] = $tab_alph[$pos];
	$pos++;
	$offset++;
	$sheet->setCellValue($tab_alph[$pos] . '1', ".01");
	$tab_pos_dom[$i]['moyenne_01'] = $tab_alph[$pos];
	$sheet->getStyle($tab_alph[$pos] . '1')->applyFromArray($styleNoModuleArray);
	$pos++;
	$offset++;
	$sheet->setCellValue($tab_alph[$pos] . '1', ".5");
	$sheet->getStyle($tab_alph[$pos] . '1')->applyFromArray($styleNoModuleArray);
	$tab_pos_dom[$i]['moyenne_5'] = $tab_alph[$pos];
	
	
	$offset = $cpt + $offset;

}
$comp_info_pos = $pos+1;

//Afficher 'info' dans la première cellule à l'emplacement des notes de compétences informatique et appliquer le style
$sheet->setCellValue($tab_alph[$comp_info_pos] . '1', 'info');
$sheet->getStyle($tab_alph[$comp_info_pos] . '1')->applyFromArray($styleTitleArray);

//Supprimer les notices
ini_set('error_reporting',~E_NOTICE);
/*echo "<pre>";
print_r($tab_dom);
print_r($tab_notes[534]);
echo "</pre>";*/

foreach($tab_can AS $key_can => $can) {
    //Afficher les noms des élèves et appliquer le style
    $sheet->setCellValue('A'.($key_can+2), $can['nom_can']." ".$can['prenom_can']);
    $sheet->getStyle('A'.($key_can+2))->applyFromArray($styleNameArray);
	$cpt = 1;
	
	
	$tab_exa_6_unique = array_unique($tab_dom[6]);
	foreach($tab_exa_6_unique AS $key => $exa_6){
        //Placer les différents modules théoriques dans un tableau qui part de 0
//        $tab_exa_6 = $exa_6;
        //Filtrer les doublons
//		print_r($tab_exa_6);
      //  $tab_exa_6_unique = array_unique($tab_exa_6);
//		print_r($tab_exa_6_unique);
  //      foreach ($tab_exa_6_unique AS $key => $value) {
            //Notes CCO
			//echo "<br>key : ".$exa_6;
			$note_6 = $tab_notes[$can['id_can']][6][$key];
			//echo "<br>key = ".$key;
			//echo "note_6 = ".$note_6;
			//echo "<br>note : ".$note_6;
            //Déterminer l'emplacement des notes de CIE
            $key_apres_moyenne = $key+1;
            //Afficher les notes de CCO, changer le format et ajouter une mise en forme conditionnelle
            if($note_6){
				//echo "note_6 = ".$note_6;
                $sheet->setCellValue($tab_alph[$cpt] . ($key_can + 2), $note_6);
                $sheet->getStyle($tab_alph[$cpt] . ($key_can + 2))->getNumberFormat()->setFormatCode('0.00');
                AddColors($tab_alph[$cpt] . ($key_can + 3),$sheet,true,false,false);
            }
			$cpt++;

       // }
    }
$cpt+=2;
$tab_exa_5_unique = array_unique($tab_dom[5]);
	foreach($tab_exa_5_unique AS $key => $exa_5){
        //Placer les différents modules pratiques dans un tableau qui part de 0
        //$tab_exa_5[] = $exa_5;
        //Filtrer les doublons
        //$tab_exa_5_unique = array_unique($tab_exa_5);
       // foreach ($tab_exa_5_unique AS $key => $value) {
            //Notes CIE
            $note_5 = $tab_notes[$can['id_can']][5][$key];
			
            //Afficher les notes de CIE, changer le format et ajouter une mise en forme conditionnelle
            if($note_5){
                $sheet->setCellValue($tab_alph[$cpt] . ($key_can + 2), $note_5);
                $sheet->getStyle($tab_alph[$cpt] . ($key_can + 2))->getNumberFormat()->setFormatCode('0.00');
                AddColors($tab_alph[$key + $cpt] . ($key_can + 3),$sheet,true,false,false);
            }
       // }
	   $cpt++;
    }


/*	$cpt=1;
	
	$cpt = $key_apres_moyenne;
   */
}

foreach($tab_can AS $key_can => $can) {

    //Création d'un tableau regroupant toutes les moyennes
    $tab_moyennes = array(
        $tab_pos_dom[6]['moyenne_01'] . ($key_can + 3),
        $tab_pos_dom[5]['moyenne_01'] . ($key_can + 3),
        $tab_pos_dom[6]['moyenne_5'] . ($key_can + 3),
        $tab_pos_dom[5]['moyenne_5'] . ($key_can + 3),
        $tab_moyennes = $tab_alph[$comp_info_pos] . ($key_can + 3),
    );

    //Ajouter une mise en forme conditionnelle aux moyennes en utilsant la fonction AddColors
    foreach ($tab_moyennes as $moyennes) {
        AddColors($moyennes,$sheet,true,true,true);
    }
}

function AddColors($pos,$sheet,$red,$green,$bold){
    //Création de la mise en forme conditionnel
    $conditional1 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
    $conditional1->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
    //La condition s'applique si le contenu de la cellule est plus petit que 3.75
    $conditional1->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_LESSTHAN);
    $conditional1->addCondition('3.75');

    //Applique les couleurs des notes en dessous de la moyenne si le paramètre 'red' est à TRUE
    if($red) {
        $conditional1->getStyle($pos)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR)->getStartColor()->setARGB('FF8787');
        $conditional1->getStyle($pos)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR)->getEndColor()->setARGB('FF8787');
    }
    //Met le texte en gras si le paramètre 'bold' est à TRUE
    if($bold)
        $conditional1->getStyle()->getFont()->setBold(true);

    $conditional2 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
    $conditional2->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
    //La condition s'applique si le contenu cellule est plus grand ou égal à 4
    $conditional2->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_GREATERTHANOREQUAL);
    $conditional2->addCondition('4');

    //Applique les couleurs des notes au dessus de la moyenne si le paramètre 'green' est à TRUE
    if($green) {
        $conditional2->getStyle($pos)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR)->getStartColor()->setARGB('87FF93');
        $conditional2->getStyle($pos)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR)->getEndColor()->setARGB('87FF93');
    }
    else if(!$green){
        $conditional2->getStyle($pos)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR)->getStartColor()->setARGB('FFFFFFF');
        $conditional2->getStyle($pos)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR)->getEndColor()->setARGB('FFFFFFF');
    }
    //Met le texte en gras si le paramètre 'bold' est à TRUE
    if($bold)
        $conditional2->getStyle()->getFont()->setBold(true);


    //Création d'un tableau de conditions
    $conditionalStyles = $sheet->getStyle($pos)->getConditionalStyles();

    //Remplissage du tableau de conditions
    $conditionalStyles[] = $conditional2;
    $conditionalStyles[] = $conditional1;

    //Applique le tableau de conditions qui prend comme emplacement le paramètre 'pos'
    $sheet->getStyle($pos)->setConditionalStyles($conditionalStyles);

    $conditional3 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
    $conditional3->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
    //La condition s'applique si le contenu de la cellule est plus petit que 4
    $conditional3->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_LESSTHAN);
    $conditional3->addCondition('4');

    //Applique les couleurs des notes en dessous de la moyenne si le paramètre 'red' est à TRUE
    if($red) {
        $conditional3->getStyle($pos)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR)->getStartColor()->setARGB('FFAC4D');
        $conditional3->getStyle($pos)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR)->getEndColor()->setARGB('FFAC4D');
    }
    //Met le texte en gras si le paramètre 'bold' est à TRUE
    if($bold)
        $conditional3->getStyle()->getFont()->setBold(true);

    $conditional4 = new \PhpOffice\PhpSpreadsheet\Style\Conditional();
    $conditional4->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
    $conditional4->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_GREATERTHANOREQUAL);
    $conditional4->addCondition('3.75');

    //Applique les couleurs des notes en dessous de la moyenne si le paramètre 'red' est à TRUE
    if($red) {
        $conditional4->getStyle($pos)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR)->getStartColor()->setARGB('FFAC4D');
        $conditional4->getStyle($pos)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR)->getEndColor()->setARGB('FFAC4D');
    }
    //Met le texte en gras si le paramètre 'bold' est à TRUE
    if($bold)
        $conditional4->getStyle()->getFont()->setBold(true);

    //Création d'un tableau de conditions
    $conditionalStyles = $sheet->getStyle($pos)->getConditionalStyles();

    //Remplissage du tableau de conditions
    $conditionalStyles[] = $conditional4;
    $conditionalStyles[] = $conditional3;

    //Applique le tableau de conditions qui prend comme emplacement le paramètre 'pos'
    $sheet->getStyle($pos)->setConditionalStyles($conditionalStyles);
}

//Insetion d'une nouvelle ligne
$sheet->insertNewRowBefore(1);


foreach($tab_can AS $key_can => $can) {
    //Remplissage des cellules des moyennes de CCO en utilisant la fonction AVERAGE d'excel, changer le format et appliquer le style
    $sheet->setCellValue($tab_pos_dom[6]['moyenne_01'] . ($key_can + 3), '=AVERAGE(' . $tab_pos_dom[6]['debut'] . ($key_can + 3) . ':' . $tab_pos_dom[6]['fin'] . ($key_can + 3) . ')');
    $sheet->getStyle($tab_pos_dom[6]['moyenne_01'] . ($key_can + 3))->applyFromArray($styleNoteArray);
    $sheet->getStyle($tab_pos_dom[6]['moyenne_01'] . ($key_can + 3))->getNumberFormat()->setFormatCode('0.00');

    //Remplissage des cellules des moyennes de CIE en utilisant la fonction AVERAGE d'excel, changer le format et appliquer le style
    $sheet->setCellValue($tab_pos_dom[5]['moyenne_01'] . ($key_can + 3), '=AVERAGE(' . $tab_pos_dom[5]['debut'] . ($key_can + 3) . ':' . $tab_pos_dom[5]['fin'] . ($key_can + 3) . ')');
    $sheet->getStyle($tab_pos_dom[5]['moyenne_01'] . ($key_can + 3))->applyFromArray($styleNoteArray);
    $sheet->getStyle($tab_pos_dom[5]['moyenne_01'] . ($key_can + 3))->getNumberFormat()->setFormatCode('0.00');

    //Remplissage des cellules des moyennes de CCO arrondi à la demi en utilisant la fonction MROUND d'excel, changer le format et appliquer le style
    $sheet->setCellValue($tab_pos_dom[6]['moyenne_5'] . ($key_can + 3), '=MROUND(' . $tab_pos_dom[6]['moyenne_01'] . ($key_can + 3) . ',0.5)');
    $sheet->getStyle($tab_pos_dom[6]['moyenne_5'] . ($key_can + 3))->applyFromArray($styleNoteArray);
    $sheet->getStyle($tab_pos_dom[6]['moyenne_5'] . ($key_can + 3))->getNumberFormat()->setFormatCode('0.0');

    //Remplissage des cellules des moyennes de CIE arrondi à la demi en utilisant la fonction MROUND d'excel, changer le format et appliquer le style
    $sheet->setCellValue($tab_pos_dom[5]['moyenne_5'] . ($key_can + 3), '=MROUND(' . $tab_pos_dom[5]['moyenne_01'] . ($key_can + 3) . ',0.5)');
    $sheet->getStyle($tab_pos_dom[5]['moyenne_5'] . ($key_can + 3))->applyFromArray($styleNoteArray);
    $sheet->getStyle($tab_pos_dom[5]['moyenne_5'] . ($key_can + 3))->getNumberFormat()->setFormatCode('0.0');

    //Remplissage des cellules des compétences informatique en faisant la moyenne des moyennes des CIE/CCO : (CIE + CCO*4)/5, changer le format et appliquer le style
    $sheet->setCellValue($tab_alph[$comp_info_pos] . ($key_can + 3), '=((' . $tab_pos_dom[6]['moyenne_5'] . ($key_can + 3) . '*4)+' . $tab_pos_dom[5]['moyenne_5'] . ($key_can + 3) . ')/5');
    $sheet->getStyle($tab_alph[$comp_info_pos] . ($key_can + 3))->applyFromArray($styleNoteArray);
    $sheet->getStyle($tab_alph[$comp_info_pos] . ($key_can + 3))->getNumberFormat()->setFormatCode('0.00');

    //Afficher 'Comp.' dans la nouvelle première cellule à l'emplacement des notes de compétences informatique et appliquer le style
    $sheet->setCellValue($tab_alph[$comp_info_pos] . '1', 'Comp.');
    $sheet->getStyle($tab_alph[$comp_info_pos] . '1')->applyFromArray($styleTitleArray);

    //Récupérer les notes de compétences informatique dans une variable
    $note = $sheet->getCell($tab_alph[$comp_info_pos] . ($key_can + 3))->getCalculatedValue();


    //Appliquer les couleurs sur les noms des élèves en fonction de leur notes de compétences informatique
    if($note >= 4){
        $sheet->getStyle('A'.($key_can+3))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR)->getStartColor()->setARGB('87FF93');
        $sheet->getStyle('A'.($key_can+3))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR)->getEndColor()->setARGB('87FF93');
    }
    else if($note < 4 && $note >=3.75){
        $sheet->getStyle('A'.($key_can+3))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR)->getStartColor()->setARGB('FFAC4D');
        $sheet->getStyle('A'.($key_can+3))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR)->getEndColor()->setARGB('FFAC4D');
    }
    else if($note < 3.75){
        $sheet->getStyle('A'.($key_can+3))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR)->getStartColor()->setARGB('FF8787');
        $sheet->getStyle('A'.($key_can+3))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR)->getEndColor()->setARGB('FF8787');
    }

    //Récupérer la dernipre ligne dans une variable
    $last_row = $key_can + 3;
}

//Affichage au dessus des moyennes en 0.01 et 0.5 'Moyennes' dans une cellule fusionné et centré et appliquer le style
foreach($tab_dom AS $key_dom => $dom) {
    $sheet->setCellValue($tab_pos_dom[$key_dom]['moyenne_01'] . '1','Moyenne');
    $sheet->getStyle($tab_pos_dom[$key_dom]['moyenne_01'] . '1')->applyFromArray($styleMoyenneArray);
    $sheet->getStyle($tab_pos_dom[$key_dom]['moyenne_5'] . '1')->applyFromArray($styleMoyenneArray);
    $sheet->mergeCells($tab_pos_dom[$key_dom]['moyenne_01'].'1'.':'.$tab_pos_dom[$key_dom]['moyenne_5'].'1');
}

//Mettre des bordures sur toutes les notes / moyennes en comptant les cellules vides
$sheet->getStyle('B3:'.$tab_alph[$comp_info_pos].$last_row )->applyFromArray($styleNoteArray);

//Mettre la taille des cellules contenant les noms des élèves en automatique
$sheet->getColumnDimension('A')->setAutoSize(true);


//Création du fichier Excel
$writer = new Xlsx($spreadsheet);
$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
$writer->save("php://output");

?>
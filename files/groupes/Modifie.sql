ALTER TABLE `t_groupes` ADD `archive_grp` BOOLEAN NOT NULL AFTER `color_grp`;
ALTER TABLE `t_classes` CHANGE `id_cla` `id_cla` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT;




ALTER TABLE `t_domaines` ADD `actif_dom` TINYINT NOT NULL DEFAULT '1' AFTER `nom_dom`;


-- *************************************
Passer la table t_domaines en innoDB !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
-- *************************************




-- *************************************
Ajout t_cla_dom
-- *************************************

-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  mer. 22 août 2018 à 08:53
-- Version du serveur :  10.1.28-MariaDB
-- Version de PHP :  5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `ict-jb`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_cla_dom`
--

CREATE TABLE `t_cla_dom` (
  `id_cla` int(10) UNSIGNED NOT NULL,
  `id_dom` int(10) UNSIGNED NOT NULL,
  `nb_mod` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `t_cla_dom`
--
ALTER TABLE `t_cla_dom`
  ADD UNIQUE KEY `id_cla_2` (`id_cla`,`id_dom`),
  ADD KEY `id_cla` (`id_cla`),
  ADD KEY `id_dom` (`id_dom`);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `t_cla_dom`
--
ALTER TABLE `t_cla_dom`
  ADD CONSTRAINT `t_cla_dom_ibfk_1` FOREIGN KEY (`id_cla`) REFERENCES `t_classes` (`id_cla`),
  ADD CONSTRAINT `t_cla_dom_ibfk_2` FOREIGN KEY (`id_dom`) REFERENCES `t_domaines` (`id_dom`);
COMMIT;

ALTER TABLE `t_cla_dom` CHANGE `nb_mod` `nb_mod` TINYINT(4) NULL;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- *************************************
FIN Ajout t_cla_dom
-- *************************************

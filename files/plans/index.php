<?php
/**
 * @author Alan Pena - cp-15alp
 * @version Mai 2019
 */

session_start();
require("./../config/config.inc.php");
$aut = "ADM_TPI";
require(WAY . "/includes/secure.inc.php");
require_once(WAY . "/includes/autoload.inc.php");
require_once(WAY . "/includes/head.inc.php");

// Type d'erreur à afficher
error_reporting(E_ALL);

$pls = new Plan();

$tab_pls = $pls->get_all_pls();
?>
<div class="container">
    <div class="row" style="display: none">
        <div class="col-lg-4">
            <div class="list-group">
                <?php foreach ($tab_pls as $pls): ?>
                    <?php
                    $plan = new Plan($pls['id_pls']);
                    $tab_exa = $plan->get_all_examens();
                    ?>
                    <a href="<?= URL . 'plans/attribution.php?id_pls=' . $pls['id_pls'] ?>" class="list-group-item">
                        <?= $pls['nom_pls'] ?>
                        <?= implode(' / ', $tab_exa['num_mod']) ?>
                    </a>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-12">
                    <p class="lead">Liste des plans existant</p>
                    <ul class="list-group">
                        <?php foreach ($tab_pls as $pls): ?>
                            <li class="list-group-item" data-id_pls="<?= $pls['id_pls'] ?>">
                                <div class="row">
                                    <div class="col-lg-9">
                                        <h4 class="list-group-item-heading"><a href="<?= URL . 'plans/attribution.php?id_pls=' . $pls['id_pls'] ?>"><?= $pls['nom_pls'] ?></a></h4>
                                    </div>
                                    <div class="col-lg-3" style="text-align: right">
                            <span>
                                <a class="btn btn-primary btn-sm" title="Editer ce plan" href="<?= URL . 'plans/attribution.php?id_pls=' . $pls['id_pls'] ?>"><span class="glyphicon glyphicon-pencil"></span></a>
                                <button data-id_pls="<?= $pls['id_pls'] ?>" class="btn btn-danger btn-sm btn-del-pls" title="Supprimer ce plan"><span class="glyphicon glyphicon-trash"></span></button>
                            </span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <p class="list-group-item-text">
                                            <i class="text-muted">Comporte les examens suivants :</i><br>
                                            <span>
                                <?php
                                $plan = new Plan($pls['id_pls']);
                                foreach ($plan->get_all_examens() as $key => $exa) {
                                    if ($key !== 0) {
                                        echo ' - ';
                                    }
                                    echo "<b>$exa</b>";
                                }
                                ?>
                                </span>
                                        </p>
                                    </div>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-12">
                    <p class="lead">Créer un nouveau plan</p>
                    <p>Séléctionnez les examens que vous souhaitez pour votre plan</p>
                    <?php $examen = new Examen() ?>
                    <?php foreach ($examen->get_exam_for_plan() as $exa): ?>
                        <div class="checkbox">
                            <label>
                                <input class="exam_selected" type="checkbox" value="<?= $exa['id_exa'] ?>">
                                <?php $time = strtotime($exa['date_heure_exa']); ?>
                                <b><?= $exa['num_mod'] ?></b>-<?= $exa['v_exam_ich_exa'] ?> le <b><?= date('d.m.Y', $time) ?></b> à <b><?= date('H:i', $time) ?></b> en <b><?= $exa['nom_sle'] ?></b>
                            </label>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="col-md-12">
                    <div class="input-group">
                        <input id="nom-plan" type="text" class="form-control" placeholder="Nom du plan">
                        <div class="input-group-btn">
                            <button id="create-plan" class="btn btn-primary">Créer</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $('.btn-del-pls').on('click', function () {
            let id_pls = $(this).attr('data-id_pls');
            $.ajax({
                type: 'POST',
                url: './json/delete_plan.json.php?_=' + Date.now(),
                data: {
                    id_pls: id_pls
                },
                success: function (results) {
                    if (results) {
                        $('.list-group-item[data-id_pls=' + id_pls + ']');
                        location.reload();
                    }
                }
            });
        });
        $('#create-plan').on('click', function () {
            let nom_plan = $('#nom-plan').val();
            let tab_exa = [];
            $('.exam_selected:checkbox:checked').each(function (i, el) {
                tab_exa.push(el.value)
            });
            $.ajax({
                type: 'POST',
                url: './json/create_plan.json.php?_=' + Date.now(),
                data: {
                    nom_pls: nom_plan,
                    tab_exa: tab_exa
                },
                success: function (results) {
                    location.reload();
                }
            });
        });
    });
</script>
<?php
/**
 * @author Alan Pena - cp-15alp
 * @version Mai 2019
 */

session_start();
require("./../config/config.inc.php");
$aut = "ADM_SIT";
require(WAY . "/includes/secure.inc.php");
require_once(WAY . "/includes/autoload.inc.php");
require_once(WAY . "/includes/head.inc.php");

// Type d'erreur à afficher
//error_reporting(E_ALL);

if (empty($_GET['id_pls']) || $_GET['id_pls'] == null) {
    echo '<p><b>Erreur</b>: Aucun ID de plan donné en POST !</p>';
    exit();
} else {
    $id_pls = $_GET['id_pls'];
    $pls = new Plan($id_pls);
}

$tab_can = $pls->get_all_candidats();
if (empty($tab_can) || $tab_can == null) {
    echo '<p><b>Erreur</b> : Aucun candidat pour ce plan !</p>';
    exit();
}

$tab_salles = $pls->get_all_salles();
if (empty($tab_salles) || $tab_salles == null) {
    echo '<p><b>Erreur</b> : Aucune salle pour ce plan !</p>';
    exit();
}

$tab_can = $pls->get_all_candidats();
$tab_salles = $pls->get_all_salles();

?>

<div id="plans-container" class="plans-container" data-id_pls="<?= $id_pls ?>">
    <ul class="nav nav-tabs" role="tablist">
        <?php foreach ($tab_salles as $index => $salle): ?>
            <li class="onglet <?= $index === 0 ? 'active' : '' ?>" data-id_sle="<?= $salle['id_sle'] ?>"
                role="presentation">
                <a href="#cla-<?= $salle['id_sle'] ?>" role="tab"
                   data-toggle="tab"><?= strtoupper($salle['nom_sle']) ?></a>
            </li>
        <?php endforeach; ?>
    </ul>
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="tab-content">
                <?php foreach ($tab_salles as $index => $salle): ?>
                    <?php $sle = new Salle($salle['id_sle']); ?>
                    <div role="tabpanel" class="onglet tab-pane <?= $index === 0 ? 'active' : '' ?>"
                         id="cla-<?= $salle['id_sle'] ?>" data-id_sle="<?= $salle['id_sle'] ?>">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="panel panel-primary">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Salle de classes</h3>
                                            </div>
                                            <div class="panel-body plan" data-id_sle="<?= $salle['id_sle'] ?>">
                                                <?= $sle->gen_plan(1000, 500); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="panel panel-primary">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Ordre des attributions</h3>
                                            </div>
                                            <div class="panel-body pcname-container sortable-used">
                                                <?php foreach ($sle->get_tables() as $table): ?>
                                                    <?php if ($table['ordre_tab'] !== null) : ?>
                                                        <span class="pcname used" data-id_tab="<?= $table['id_tab'] ?>"
                                                              data-id_sle="<?= $table['ref_sle'] ?>"><?= $table['pcname_tab'] ?></span>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="panel panel-danger">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Ne pas utiliser</h3>
                                            </div>
                                            <div class="panel-body pcname-container sortable-not-used">
                                                <?php foreach ($sle->get_tables() as $table): ?>
                                                    <?php if ($table['ordre_tab'] === null) : ?>
                                                        <span class="pcname not-used"
                                                              data-id_tab="<?= $table['id_tab'] ?>"
                                                              data-id_sle="<?= $table['ref_sle'] ?>"><?= $table['pcname_tab'] ?></span>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-8">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= count($tab_can); ?> Candidat<?= count($tab_can) > 1 ? 's' : '' ?> à
                        placer</h3>
                </div>
                <div class="panel-body candidats-container">
                    <?php foreach ($tab_can as $can): ?>

                    <?php
                    $can2 = new Candidat($can['id_can']);
                    $metier = $can2->get_lettre_met();
                    $lettre_metier = $metier['lettre_met'];

                        ?>

                        <span class="candidat" style="background-color: <?= $can['color_grp'] ?>;"
                              data-id="<?= $can['id_can'] ?>"><?= substr($can['no_candidat_can'], 1) . "-" . $lettre_metier ?></span>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="action-container">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Actions</h3>
                    </div>
                    <div class="panel-body">
                        <button class="btn btn-primary placer">Placer</button>
                        <a href="gsc/get_zip.php?id_pls=<?= $id_pls ?>" id="action-get-files" class="btn btn-default">Fichiers .bat / .lsc</a>
                        <a href="backgrounds/create_backgrounds.php?id_pls=<?= $id_pls ?>" id="action-get-backgrounds" class="btn btn-default ">Backgrounds examens</a>
                        <a href="pdf/plan.pdf.php?id_pls=<?= $id_pls ?>" class="btn btn-default ">Feuille de préparation</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<script src="js/plans.js"></script>
<script>

</script>
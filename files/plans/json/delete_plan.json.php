<?php
/**
 * @author Alan Pena - cp-15alp
 * @version Mai 2019
 */

header("Content-Type: application/json");

session_start();

require("./../../config/config.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

$aut = "ADM_TPI";
require(WAY . "/includes/secure.inc.php");

$pls = new Plan($_POST['id_pls']);

$pls->delete();

echo json_encode(true);
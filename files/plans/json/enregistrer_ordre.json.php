<?php
/**
 * @author Alan Pena - cp-15alp
 * @version Mai 2019
 */

header("Content-Type: application/json");

session_start();

require("./../../config/config.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

$aut = "ADM_TPI";
require(WAY . "/includes/secure.inc.php");

$tab = new Table();

foreach ($_POST['pcs'] as $pc) {
    $tab->change_order(!isset($pc['ordre']) ? null : $pc['ordre'], $pc['id_tab']);
}

echo json_encode(true);
<?php
/**
 * @author Alan Pena - cp-15alp
 * @version Mai 2019
 */

header("Content-Type: application/json");

session_start();

require("./../../config/config.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

$aut = "ADM_TPI";
require(WAY . "/includes/secure.inc.php");

$prj = new Projet();
if (!isset($_POST['ref_pls'])) {
    echo json_encode('Il n\'y a pas d\'ID plan !');
    exit();
} else {
    $pls = new Plan($_POST['ref_pls']);
}

/**
 * Si load = true cela veut dire que c'est au chargement de la page et donc qu'il faut récupérer les placements
 */
if ($_POST['load'] === 'true') {
    $placements = $pls->get_placements();
    if (!empty($placements) || $placements !== false) {
        echo json_encode($placements);
        exit();
    } else {
        echo json_encode(false);
        exit();
    }
}

/**
 *  Tableau des tables par salles
 *  Sous cette forme :
 *
 *  [0] => [
 *      ['id_tab'] => 172,
 *      ['id_sle'] => 13,
 *      ['ordre'] => 1
 *  ]
 */
if (empty($_POST['tables'])) {
    echo json_encode('Il n\'y a pas de table pour ce plan !');
    exit();
} else {
    $tab_tables = $_POST['tables'];
}


/**
 *  Tableau d'examen par élève par année
 *  Sous cette forme :
 *
 *  [0] => [
 *      ['annee'] => 2018
 *      ['candidats'] => [
 *          [0] => [
 *              ['id_can'] => 435
 *              ['examens'] => [
 *                  345,
 *                  465,
 *                  213
 *              ]
 *          ],
 *          [1] => [
 *              etc...
 *          ]
 *      ]
 *  ],
 *  [1] => [
 *      etc...
 *  ]
 */
$tab_candidats_avec_examens_par_annee = [];
$index_principal = 0;
$index_candidat = 0;

try {
    $tab_annees = $pls->get_all_annees();
    foreach ($tab_annees as $annee) {
        $tab_candidats_avec_examens_par_annee[$index_principal]['annee'] = $annee;
        try {
            $tab_candidats = $pls->get_all_can_by_annee($annee);
            $index_candidat = 0;
            foreach ($tab_candidats as $candidat) {
                $tab_candidats_avec_examens_par_annee[$index_principal]['candidats'][$index_candidat]['id_can'] = $candidat;
                try {
                    $tab_examens = $pls->get_exa_by_can($candidat);
                    if (count($tab_examens) >= 1) {
                        $tab_candidats_avec_examens_par_annee[$index_principal]['candidats'][$index_candidat]['examens'] = $tab_examens;
                    }
                } catch (Exception $e) {
                    echo json_encode($e->getMessage());
                    exit();
                }
                $index_candidat++;
            }
        } catch (Exception $e) {
            echo json_encode($e->getMessage());
            exit();
        }
        $index_principal++;
    }
} catch (Exception $e) {
    echo json_encode($e->getMessage());
    exit();
}

/**
 *  Maintenant qu'ils sont mélanger mais trier par annee on enleve l'annee pour les avoir tous dans le même niveau
 */
$tab_candidats_avec_examens = [];

foreach ($tab_candidats_avec_examens_par_annee as $candidats_avec_examens_par_annee) {
    for ($i = 0; $i < count($candidats_avec_examens_par_annee['candidats']); $i++) {
        $tab_candidats_avec_examens[] = $candidats_avec_examens_par_annee['candidats'][$i];
    }
}

/**
 * Génération du dernier tableau final
 */
$tab_candidats_par_tables = [];

foreach ($tab_tables as $table) {
    $tab_candidats_par_tables[]['table'] = [
        'id_tab' => $table['id_tab'],
        'id_sle' => $table['id_sle']
    ];
}

foreach ($tab_candidats_avec_examens as $key => $candidat) {
    for ($i = 0; $i < count($tab_candidats_par_tables); $i++) {
        if (empty($tab_candidats_par_tables[$i]['candidats'])) {
            $tab_candidats_par_tables[$i]['candidats'][] = $candidat;
            break;
        } else {
            $peut_placer = true;
            for ($j = 0; $j < count($tab_candidats_par_tables[$i]['candidats']); $j++) {
                if (!empty(array_intersect($tab_candidats_par_tables[$i]['candidats'][$j]['examens'], $candidat['examens']))) {
                    $peut_placer = false;
                }
            }
            if ($peut_placer) {
                $tab_candidats_par_tables[$i]['candidats'][] = $candidat;
                break;
            }
        }
    }
}

/**
 * Suppression des anciens placements pour ce plan puis
 * insertion des informations dans la base de donnée
 */
$pls->reset();

/**
 * Insertion des nouveaux placements
 */
foreach ($tab_candidats_par_tables as $candidats_par_table) {
    if (isset($candidats_par_table['candidats'])) {
        foreach ($candidats_par_table['candidats'] as $candidats) {
            try {
                $pls->add_placement($candidats['id_can'], $candidats_par_table['table']['id_tab']);
            } catch (Exception $e) {
                echo json_encode($e->getMessage());
                exit();
            }
        }
    }
}

/**
 * Envoie des données
 */
echo json_encode($tab_candidats_par_tables);
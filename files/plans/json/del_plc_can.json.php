<?php
/**
 * @author CP-16RDM
 * @version Janvier 2020
 */

header("Content-Type: application/json");

session_start();

require("./../../config/config.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

$aut = "ADM_TPI";
require(WAY . "/includes/secure.inc.php");

$pls = new Plan($_POST['ref_pls']);
$pls->del_plc_can($_POST['id_can']);

echo json_encode($_POST);
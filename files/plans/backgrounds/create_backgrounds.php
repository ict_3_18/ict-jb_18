<?php
session_start();
require("./../../config/config.inc.php");
$aut = "ADM_SIT";
require(WAY . "/includes/secure.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

// Type d'erreur à afficher
error_reporting(E_ALL);

if (isset($_GET['id_pls']) && !empty($_GET['id_pls']) && $_GET['id_pls'] != '') {
    $id_pls = $_GET['id_pls'];
} else {
    echo "Pas d'ID de plan référencé";
    exit();
}

/**
 * Transforme une couleur au format HEX vers un format RGB
 * Marche avec #17F3BA
 * Marche avec #44422CC mais aussi avec le format court #42C
 * Retourne un tableau ['r' => 45, 'g' => 124, 'b' => 71]
 * Retourn false si ce n'est pas un
 * @param $hex
 * @return array|bool
 */
function hex_to_rgb($hex)
{
    $hex = preg_replace("/[^0-9A-Fa-f]/", '', $hex);
    $rgbArray = [];
    if (strlen($hex) == 6) {
        $colorVal = hexdec($hex);
        $rgbArray['r'] = 0xFF & ($colorVal >> 0x10);
        $rgbArray['g'] = 0xFF & ($colorVal >> 0x8);
        $rgbArray['b'] = 0xFF & $colorVal;
    } elseif (strlen($hex) == 3) {
        $rgbArray['r'] = hexdec(str_repeat(substr($hex, 0, 1), 2));
        $rgbArray['g'] = hexdec(str_repeat(substr($hex, 1, 1), 2));
        $rgbArray['b'] = hexdec(str_repeat(substr($hex, 2, 1), 2));
    } else {
        return false;
    }
    return $rgbArray;
}

/**
 * Retourne la cordonnée X pour écrire le texte au milieu
 * @param $image_width
 * @param $font
 * @param $txt
 * @return float|int
 */
function center_text($image_width, $font, $txt) {
    return ($image_width / 2) - (imagefontwidth($font) * strlen($txt) / 2);
}

$prj = new Projet();

try {
    $query = 'SELECT DSC_MOD.num_mod, TAB.pcname_tab, EXA.date_heure_exa, EXA.duree_exa, CAN.no_candidat_can, CAN.dys_can, GRP.color_grp, TAB.resolutionX_tab, TAB.resolutionY_tab
                FROM t_candidats CAN
                JOIN t_classes CLA ON CLA.id_cla = CAN.ref_classe
                JOIN t_groupes GRP ON GRP.id_grp = CLA.ref_grp
                JOIN t_ref_can_exa CAN_EXA ON CAN_EXA.ref_can = CAN.id_can
                JOIN t_examens EXA ON EXA.id_exa = CAN_EXA.ref_exa AND EXA.ref_pls = :id_pls
                JOIN t_desc_modules DSC_MOD ON DSC_MOD.id_desc_mod = EXA.no_ich_exa
                JOIN t_plans PLS ON PLS.id_pls = EXA.ref_pls
                JOIN t_places PLC ON PLC.id_can = CAN.id_can AND PLC.ref_pls = :id_pls
                JOIN t_table TAB ON TAB.id_tab = PLC.ref_tab
                ORDER BY CAN.no_candidat_can DESC ';
    $stmt = $prj->pdo->prepare($query);
    $stmt->execute([
        'id_pls' => $id_pls
    ]);
    $tab_can = $stmt->fetchAll(PDO::FETCH_ASSOC);
} catch (Exception $e) {
    echo $e->getMessage();
    return false;
}

require '../zip.lib.php';
$zip_filename = "Backgrounds_$id_pls.zip";
$zip = new zipfile($zip_filename);

/**
 * Fonts
 * Elles est déclarer ici pour ne pas load la font plusieurs fois
 */
$font_50 = imageloadfont('./fonts/verdana_50.gdf');
$font_144 = imageloadfont('./fonts/verdana_144.gdf');
$font_200 = imageloadfont('./fonts/verdana_200.gdf');

foreach ($tab_can as $can) {

    /**
     * Image size
     */
    $image_width = $can['resolutionX_tab'];
    $image_height = $can['resolutionY_tab'];

    /**
     * Image
     */
    $image = imagecreatetruecolor($image_width, $image_height);

    /**
     * Couleurs (la première est celle du background)
     */
    $color_bg = imagecolorallocate($image, 0, 0, 0);
    $tab_color_rgb = hex_to_rgb($can['color_grp']);
    $color_txt = imagecolorallocate($image, $tab_color_rgb['r'], $tab_color_rgb['g'], $tab_color_rgb['b']);

    /**
     * Textes à afficher
     */
    $txt_nom_exa = 'CCO ICT ' . $can['num_mod'];
    $txt_date_and_temps = date('d F Y H:i', strtotime($can['date_heure_exa'])) . ' - ' . date('H:i', strtotime('+' . $can['duree_exa'] . ' minutes', strtotime(date('H:i', strtotime($can['date_heure_exa'])))));
    $txt_candidat = 'Candidat';
    $txt_no_candidat = substr($can['no_candidat_can'], 1);
    $txt_dys = '*';
    $txt_place = '[' . substr($can['pcname_tab'], 3) . ']';
    $txt_message_en_bas_1 = 'Il est interdit de laisser des fichiers';
    $txt_message_en_bas_2 = utf8_decode('à l\'extérieur du dossier C:\ICT' . $can['num_mod']);

    // le nom du CCO
    imagestring($image, $font_50, center_text($image_width, $font_50, $txt_nom_exa), imagefontheight($font_50), $txt_nom_exa, $color_txt);

    // La date du CCO avec l'heure du début et de fin
    imagestring($image, $font_50, center_text($image_width, $font_50, $txt_date_and_temps), imagefontheight($font_50) * 2, $txt_date_and_temps, $color_txt);

    // Le texte "Candidat"
    imagestring($image, $font_144, center_text($image_width, $font_144, $txt_candidat), $image_height / 4, $txt_candidat, $color_txt);

    // Le numéro du candidat
    imagestring($image, $font_200, center_text($image_width, $font_200, $txt_no_candidat), $image_height / 2 - imagefontheight($font_200) / 2, $txt_no_candidat, $color_txt);

    // Astérisque pour les candidats dyslexiques
    if (!empty($can['dys_can'])) {
        $espace = imagefontwidth($font_200) * strlen($txt_no_candidat);
        imagestring($image, $font_144, center_text($image_width, $font_200, $txt_no_candidat) + $espace, $image_height / 2 - imagefontheight($font_200) / 2, $txt_dys, $color_txt);
    }

    // Numéro de la place
    imagestring($image, $font_50, center_text($image_width, $font_50, $txt_place), $image_height / 4 * 3 - imagefontheight($font_50), $txt_place, $color_txt);

    // Message en bas
    imagestring($image, $font_50, center_text($image_width, $font_50, $txt_message_en_bas_1), $image_height - imagefontheight($font_50) * 2 - imagefontheight($font_50), $txt_message_en_bas_1, $color_txt);
    imagestring($image, $font_50, center_text($image_width, $font_50, $txt_message_en_bas_2), $image_height - imagefontheight($font_50) * 2, $txt_message_en_bas_2, $color_txt);

    // Nom du fichier "NO-CAN_NO_CCO.jpg"
    $filename = $txt_no_candidat . '_' . $can['num_mod'] . '.jpg';

    // Créer l'image et l'enregistre dans le dossier ./tmp/
    imagejpeg($image, './tmp/' . $filename);

    // Ouvre l'image qui vient d'être enregistrer
    $handle = fopen('./tmp/' . $filename, "rb");

    // Récupère le contenu de l'image
    $content = stream_get_contents($handle);

    // Ferme le fichiers
    fclose($handle);

    // Ajoute un fichier au ZIP avec le contenu de l'image
    $zip->addfile($content, $filename);

    // Supprime l'image du dossier ./tmp/
    unlink('./tmp/' . $filename);

    // Supprime la mémoire aloué pour l'image
    imagedestroy($image);
}

// Envoie le fichier ZIP
echo $zip->file();
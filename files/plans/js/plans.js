/**
 * @author Alan Pena
 * @description Scripts pour la page attribution.php
 */

$(function () {

    // ---------------------------------------- Initialisation ----------------------------------------

    init();


    // ---------------------------------------- jQuery UI ----------------------------------------

    // Initialisation de la boolean qui permet de savoir si l'item déplacé est ré-ordré ou changé d'emplacement
    let differentList;

    $('.sortable-used').sortable({
        connectWith: '.sortable-not-used',
        dropOnEmpty: true
    }).disableSelection();

    $('.sortable-not-used').sortable({
        connectWith: '.sortable-used',
        dropOnEmpty: true
    }).disableSelection();

    $('.sortable-used, .sortable-not-used').bind('sortreceive', function (event, ui) {
        differentList = true;
    });

    $('.sortable-used, .sortable-not-used').bind('sortchange', function (event, ui) {
        differentList = false;
    });

    $('.sortable-used, .sortable-not-used').bind('sortstop', function (event, ui) {
        if (differentList) {
            if ($('.pcname[data-id_tab=' + ui.item[0].dataset.id_tab + ']').hasClass('used')) {
                $('.pcname[data-id_tab=' + ui.item[0].dataset.id_tab + ']').removeClass('used').addClass('not-used');
                $('.table[data-id_tab=' + ui.item[0].dataset.id_tab + ']').addClass('not-used');
            } else {
                $('.pcname[data-id_tab=' + ui.item[0].dataset.id_tab + ']').removeClass('not-used').addClass('used');
                $('.table[data-id_tab=' + ui.item[0].dataset.id_tab + ']').removeClass('not-used');
            }
        }
        differentList = false;
        ordrer_tables(get_salles());
        enregistrer_ordre(get_salles());
    });

    // ---------------------------------------- Fonctions ----------------------------------------

    /**
     * Fonction appeler au début du script pour l'initialisation
     */
    function init() {
        actualise_couleur_tables();
        ordrer_tables(get_salles());
        placer_candidats('true');
    }

    /**
     * Met la couleur de la case du plan pour les tables non utilisés
     * (Pour l'initialisation)
     */
    function actualise_couleur_tables() {
        $('.pcname.not-used').each(function (i, el) {
            $(`.table[data-id_tab=${el.dataset.id_tab}]`).addClass('not-used');
        });
    }

    /**
     * Retourne l'id des classes présent pour ce plan
     * @returns {Array}
     */
    function get_salles() {
        let salles = [];
        $('.pcname').each(function () {
            if (!salles.includes(this.dataset.id_sle)) salles.push(this.dataset.id_sle);
        });
        return salles;
    }

    /**
     * Retourne la liste des pcs a l'aide d'un tableau d'id des salles
     * @param salles
     * @returns {Array}
     */
    function get_pcs(salles) {
        let pcs = [];
        salles.forEach(function (i) {
            pcs.push($(`.pcname[data-id_sle=${i}]`));
        });
        return pcs;
    }

    /**
     * Définie l'ordre de d'attribution (data-order)
     * @param {Array} salles
     * @returns {Array}
     */
    function ordrer_tables(salles) {
        let pcs = [];
        salles.forEach(function (i) {
            $(`.pcname[data-id_sle=${i}]`).each(function (i, el) {
                if ($(this).hasClass('not-used')) $(this).removeAttr('data-order');
                else $(this).attr('data-order', i + 1);
            });
            pcs.push($(`.pcname[data-id_sle=${i}]`).not('.not-used'));
        });
        return pcs;
    }

    /**
     * Fonction qui va enregistrer l'ordre des pc dans la base
     */
    function enregistrer_ordre() {
        let pcs = [];
        get_pcs(get_salles()).forEach(function (el, i) {
            for (let i = 0; i < el.length; i++) {
                pcs.push({
                    id_tab: el[i].dataset.id_tab,
                    ordre: el[i].dataset.order
                });
            }
        });
        $.ajax({
            type: 'POST',
            url: './json/enregistrer_ordre.json.php?_=' + Date.now(),
            data: {
                pcs: pcs
            },
            success: function (results) {
            }
        });
    }

    function recuperer_placement() {

    }

    /**
     * Place les candidats
     * @param load
     */
    function placer_candidats(load = 'false') {
        let ref_pls = $('#plans-container').data('id_pls');
        let tables = gen_table_alterner();
        if (load === 'true') {
            $('.placer').text('Mélanger');
        }
        $.ajax({
            type: 'POST',
            url: './json/placer_candidats.json.php?_=' + Date.now(),
            data: {
                load: load,
                ref_pls: ref_pls,
                tables: tables
            },
            success: function (results) {
                results.forEach(function (el) {
                    if (el.candidats != null) {
                        switch_onglet_salle(el.table.id_sle);
                        let pos = $(`.plan[data-id_sle=${el.table.id_sle}] .table[data-id_tab=${el.table.id_tab}]`);
                        el.candidats.forEach(function (can, i) {
                            $(`.candidat[data-id=${can.id_can}]`).appendTo(`.plan[data-id_sle=${el.table.id_sle}] > .table-container > .table[data-id_tab=${el.table.id_tab}] .table-candidats-container`);
                        });
                    }
                });
                $('.nav-tabs a:first').tab('show');
            }
        });
    }

    function switch_onglet_salle(id_sle) {
        $('.onglet.active').removeClass('active');
        $(`.onglet[data-id_sle=${id_sle}]`).addClass('active');
    }

    /**
     * Retourne la taille de la plus grande salle (seulement place utiliser)
     * (Si une salle a 100 places mais que 80 sont dans la zone non utilisé ça taille ne vaut que 20)
     * @param {Array} pcs
     * @returns {number}
     */
    function plus_grande_salle(pcs) {
        let taille = 0;
        pcs.forEach(function (el, i) {
            if (taille < el.length) taille = el.length;
        });
        return taille;
    }

    /**
     * Fonction qui retourne un tableau avec les places dans l'ordre d'attribution en alternant les salles
     * @returns {Array}
     */
    function gen_table_alterner() {
        let pcs_alterner = [];
        let pcs = [];
        for (let i = 0; i < plus_grande_salle(ordrer_tables(get_salles())); i++) {
            get_salles().forEach(function (el) {
                if ($(`.pcname[data-id_sle=${el}][data-order=${i + 1}]`)[0] !== undefined)
                    pcs_alterner.push($(`.pcname[data-id_sle=${el}][data-order=${i + 1}]`)[0]);
            })
        }
        pcs_alterner.forEach(function (el, i) {
            pcs.push({
                id_tab: el.dataset.id_tab,
                id_sle: el.dataset.id_sle,
                ordre: el.dataset.order
            });
        });
        return pcs;
    }


    // ---------------------------------------- Événement jQuery ----------------------------------------

    /**
     * Quand on clique sur le bouton #placer
     */
    $('.placer').on('click', function () {
        placer_candidats();
        $(this).text('Mélanger');
    });


    /** TEST DRAG AND DROP **/

    $( function() {
        $(".candidat").draggable();
    } );

    $(".table-candidat").droppable({
        drop: function( event, ui ) {
            var id_can = ui.draggable.attr("data-id");
            var ref_pls = $("#plans-container").attr("data-id_pls");
            var ref_tab = $(this).attr("data-id_tab");

            ui.draggable.position({
                my: "center",
                at: "center",
                of: $(this).find(".table-candidats-container"),
                using: function(pos) {
                    $(this).animate(pos, 300, "linear");
                }
            });

            $.ajax({
                type: 'POST',
                url: './json/change_plc_can.json.php?_=' + Date.now(),
                data: {
                    id_can: id_can,
                    ref_pls: ref_pls,
                    ref_tab: ref_tab
                },
                success: function (results) {
                }
            });
        }
    });

    $(".candidats-container").droppable({
        drop: function( event, ui ) {
            var id_can = ui.draggable.attr("data-id");
            var ref_pls = $("#plans-container").attr("data-id_pls");

            $.ajax({
                type: 'POST',
                url: './json/del_plc_can.json.php?_=' + Date.now(),
                data: {
                    id_can: id_can,
                    ref_pls: ref_pls
                },
                success: function (results) {
                }
            });
        }
    });

});
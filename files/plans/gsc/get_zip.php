<?php
session_start();
require("./../../config/config.inc.php");
$aut = "ADM_SIT";
require(WAY . "/includes/secure.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

// Type d'erreur à afficher
//error_reporting(E_ALL);

if (isset($_GET['id_pls']) && !empty($_GET['id_pls']) && $_GET['id_pls'] != '') {
    $id_pls = $_GET['id_pls'];
} else {
    echo "Pas d'ID de plan référencé";
    exit();
}

/**
 * Retourne le nom d'un module, si c'est un module xxxA la lettre est enlever, sinon le nom est laissé tel quel
 * @example 226A = 226  // 226B = 226B
 * @param $nom_module
 * @return string
 */
function nom_module($nom_module)
{
    return (strcasecmp(substr($nom_module, -1), 'A') == 0 ? substr($nom_module, 0, -1) : $nom_module);
}

$prj = new Projet();

try {
    $query = 'SELECT EXA.no_ich_exa
                FROM t_examens EXA
                WHERE EXA.ref_pls = :id_pls';
    $stmt = $prj->pdo->prepare($query);
    $stmt->execute([
        'id_pls' => $id_pls
    ]);
    $tab_no_ich_exa = $stmt->fetchAll(PDO::FETCH_COLUMN);
} catch (Exception $e) {
    echo $e->getMessage();
    return false;
}

$tab_data_plan = [];

foreach ($tab_no_ich_exa as $no_ich_exa) {
    try {
        $query = 'SELECT TAB.pcname_tab, DESC_MOD.num_mod, CAN.no_candidat_can, CAN.login_can
                    FROM t_examens EXA
                    JOIN t_ref_can_exa REF_CAN_EXA ON REF_CAN_EXA.ref_exa = EXA.id_exa
                    JOIN t_candidats CAN ON CAN.id_can = REF_CAN_EXA.ref_can
                    JOIN t_desc_modules DESC_MOD ON DESC_MOD.id_desc_mod = EXA.no_ich_exa
                    JOIN t_places PLC ON PLC.id_can = CAN.id_can AND PLC.ref_pls = EXA.ref_pls 
                    JOIN t_table TAB ON TAB.id_tab = PLC.ref_tab
                    WHERE EXA.no_ich_exa = :no_ich_exa
                    AND EXA.ref_pls = :id_pls
                    ORDER BY TAB.pcname_tab';
        $stmt = $prj->pdo->prepare($query);
        $stmt->execute([
            'id_pls' => $id_pls,
            'no_ich_exa' => $no_ich_exa
        ]);
        $tab_data_plan[] = $stmt->fetchAll();
    } catch (Exception $e) {
        echo $e->getMessage();
        return false;
    }
}

require '../zip.lib.php';
$zip_filename = "Plan_$id_pls.zip";
$zip = new zipfile($zip_filename);

for ($i = 0; $i < count($tab_data_plan); $i++) {
    $bat_file_content = 'rem Nom du module (en entier) : ' . $tab_data_plan[$i][0]['num_mod'] . "\n\n";
    $lcs_file_content = "[LanSchool Class List]\n";

    foreach ($tab_data_plan[$i] as $key => $data_plan) {
        $bat_info_array = [
            ($data_plan['pcname_tab']),
            nom_module($data_plan['num_mod']),
            substr($data_plan['no_candidat_can'], 1),
            $data_plan['login_can']
        ];
        $bat_file_content .= 'call op/%1.bat ' . implode(' ', $bat_info_array) . "\n";
        $lcs_file_content .= "Student_$key=" . $data_plan['pcname_tab'] . "\n";
    }

    $lcs_file_content .= "Type=1\n";

    $zip->addFile($bat_file_content, 'bat/' . nom_module($tab_data_plan[$i][0]['num_mod']) . '.bat');
    $zip->addFile($lcs_file_content, 'lsc/' . nom_module($tab_data_plan[$i][0]['num_mod']) . '.lsc');
}

echo $zip->file();
<?php
/**
 * @author Alan Pena - cp-15alp
 * @version Mai 2019
 */

session_start();
require("./../../config/config.inc.php");
$aut = "ADM_SIT";
require(WAY . "/includes/secure.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

/**
 * Transforme une couleur au format HEX vers un format RGB
 * Marche avec #17F3BA
 * Marche avec #44422CC mais aussi avec le format court #42C
 * Retourne un tableau ['r' => 45, 'g' => 124, 'b' => 71]
 * Retourn false si ce n'est pas un
 * @param $hex
 * @return array|bool
 */
function hex_to_rgb($hex)
{
    $hex = preg_replace("/[^0-9A-Fa-f]/", '', $hex);
    $rgbArray = [];
    if (strlen($hex) == 6) {
        $colorVal = hexdec($hex);
        $rgbArray['r'] = 0xFF & ($colorVal >> 0x10);
        $rgbArray['g'] = 0xFF & ($colorVal >> 0x8);
        $rgbArray['b'] = 0xFF & $colorVal;
    } elseif (strlen($hex) == 3) {
        $rgbArray['r'] = hexdec(str_repeat(substr($hex, 0, 1), 2));
        $rgbArray['g'] = hexdec(str_repeat(substr($hex, 1, 1), 2));
        $rgbArray['b'] = hexdec(str_repeat(substr($hex, 2, 1), 2));
    } else {
        return false;
    }
    return $rgbArray;
}

$prj = new Projet();
$pdf = new PDFPlan();

$id_pls = $_GET['id_pls'];

try {
    $query = 'SELECT DESC_MOD.num_mod, EXA.v_exam_ich_exa, EXA.date_heure_exa, EXA.duree_exa, GROUP_CONCAT(PER.nom_per, " ", PER.prenom_per, " (", FCT.nom_fnc, ")" ORDER BY DESC_MOD.num_mod SEPARATOR ", ") AS experts
                FROM t_examens EXA
                JOIN t_desc_modules DESC_MOD ON DESC_MOD.id_desc_mod = EXA.no_ich_exa
                JOIN t_ref_per_exa PER_EXA ON PER_EXA.ref_exa = EXA.id_exa
                JOIN t_personnes PER ON PER.id_per = PER_EXA.ref_per
                JOIN t_fonctions FCT ON FCT.id_fnc = PER_EXA.ref_fnc
                WHERE EXA.ref_pls = :id_pls
                GROUP BY DESC_MOD.num_mod';
    $stmt = $prj->pdo->prepare($query);
    $stmt->execute([
        'id_pls' => $id_pls
    ]);
    $tab_info_exa = $stmt->fetchAll(PDO::FETCH_ASSOC);
} catch (Exception $e) {
    echo $e->getMessage();
    return false;
}

try {
    $query = 'SELECT DISTINCT SLE.id_sle, SLE.nom_sle 
                FROM t_plans PLS
                JOIN t_examens EXA ON EXA.ref_pls = PLS.id_pls
                JOIN t_ref_examens_salles REF_EXA_SLE ON REF_EXA_SLE.ref_exa = EXA.id_exa
                JOIN t_salles SLE ON SLE.id_sle = REF_EXA_SLE.ref_sle
                WHERE PLS.id_pls = :id_pls';
    $stmt = $prj->pdo->prepare($query);
    $stmt->execute([
        'id_pls' => $id_pls
    ]);
    $tab_sle = $stmt->fetchAll(PDO::FETCH_ASSOC);
} catch (Exception $e) {
    echo $e->getMessage();
    return false;
}

foreach ($tab_sle as $sle) {
    $pdf->AddPage();
    $pdf->SetFont('Arial', 'B', 24);
    $pdf->Cell(40, 10, $sle['nom_sle']);
    $pdf->Ln(15);
    $pdf->SetFont('Arial', 'B', 16);
    $pdf->Cell(40, 10, 'Candidats :');
    $pdf->Ln();

    $query = 'SELECT CAN.no_candidat_can, GRP.id_grp, GRP.color_grp
                FROM t_candidats CAN
                JOIN t_classes CLA ON CLA.id_cla = CAN.ref_classe
                JOIN t_groupes GRP ON GRP.id_grp = CLA.ref_grp
                JOIN t_places PLC ON PLC.id_can = CAN.id_can
                JOIN t_plans PLS ON PLS.id_pls = PLC.ref_pls
                JOIN t_table TAB ON TAB.ref_sle = :id_sle AND PLC.ref_tab = TAB.id_tab
                WHERE PLS.id_pls = :id_pls
                ORDER BY GRP.id_grp DESC, CAN.no_candidat_can';
    $stmt = $prj->pdo->prepare($query);
    $stmt->execute([
        'id_sle' => $sle['id_sle'],
        'id_pls' => $id_pls
    ]);
    $tab_can = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $pdf->SetFont('Arial', 'B', 12);

    /*********************************/

    $key = 0;
    if($tab_can){
        $id_grp = $tab_can[0]['id_grp'];
        foreach ($tab_can as $can) {
            if ($can['id_grp'] !== $id_grp) {
                $pdf->Ln(15);
                $id_grp = $can['id_grp'];
                $key = 0;
            }
            $key++;
            $tab_color_rgb = hex_to_rgb($can['color_grp']);
            $pdf->SetFillColor($tab_color_rgb['r'], $tab_color_rgb['g'], $tab_color_rgb['b']);
            $cell_with = 16;
            $padding = 2;
            $pdf->Cell($cell_with, 10, substr($can['no_candidat_can'], 1), 1, 0, 'C', 1);
            $pdf->Cell($padding, 10);
            if ($key % floor((210 - (2 * 8)) / ($cell_with + $padding)) == 0) {
                $pdf->Ln(10 + $padding);
            }
        }
    }

    /*********************************/

//    $id_grp = $tab_can[0]['id_grp'];
//    $key = 0;
//
//    foreach ($tab_can as $can) {
//        if ($can['id_grp'] !== $id_grp) {
//            $pdf->Ln(15);
//            $id_grp = $can['id_grp'];
//            $key = 0;
//        }
//        $key++;
//        $tab_color_rgb = hex_to_rgb($can['color_grp']);
//        $pdf->SetFillColor($tab_color_rgb['r'], $tab_color_rgb['g'], $tab_color_rgb['b']);
//        $cell_with = 16;
//        $padding = 2;
//        $pdf->Cell($cell_with, 10, substr($can['no_candidat_can'], 1), 1, 0, 'C', 1);
//        $pdf->Cell($padding, 10);
//        if ($key % floor((210 - (2 * 8)) / ($cell_with + $padding)) == 0) {
//            $pdf->Ln(10 + $padding);
//        }
//    }

    $pdf->Ln(20);
    $pdf->SetFont('Arial', 'B', 12);

    $header = [
        [
            'size' => 20,
            'txt' => 'N° ICT'
        ],
        [
            'size' => 30,
            'txt' => 'Date'
        ],
        [
            'size' => 20,
            'txt' => 'Début'
        ],
        [
            'size' => 20,
            'txt' => 'Fin'
        ],
        [
            'size' => 100,
            'txt' => 'Experts'
        ]
    ];

    foreach ($header as $col) {
        $pdf->SetFillColor(230);
        $pdf->Cell($col['size'], 7, utf8_decode($col['txt']), '', '', '', 1);
    }

    $pdf->Ln();
    $pdf->SetFillColor(255);

    foreach ($tab_info_exa as $info_exa) {
        $pdf->SetFont('Arial', '', 12);
        $pdf->Cell($header[0]['size'], 10, $info_exa['num_mod'] . '-' . $info_exa['v_exam_ich_exa'], 'T');
        $pdf->Cell($header[1]['size'], 10, date('d.m.Y', strtotime($info_exa['date_heure_exa'])), 'T');
        $pdf->Cell($header[2]['size'], 10, date('H:i', strtotime($info_exa['date_heure_exa'])), 'T');
        $pdf->Cell($header[3]['size'], 10, date('H:i', strtotime($info_exa['date_heure_exa'] . ' +' . $info_exa['duree_exa'] . ' minutes')), 'T');
        $pdf->SetFont('Arial', '', 10);
        $pdf->MultiCell($header[4]['size'], 6, utf8_decode($info_exa['experts']), 'T', 'L');
		$pdf->Ln();
    }
}

$pdf->Output();
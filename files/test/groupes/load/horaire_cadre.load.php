<?php
/**
 * @author Beruwalage Julian
 * @description Ce fichier s'occupe de load l'horaire TPI
 */

session_start();
$aut = 'ADM_GRP;USR_GRP';
require_once("./../../config/config.inc.php");
require_once(WAY . "./includes/secure.inc.php");
require_once(WAY . "/includes/autoload.inc.php");


$h_cad = new Horaire();
$cla = new Classe($_POST['id_cla']);
$grp = new Groupe($cla->get_ref_grp());

$nom_cla = $cla->get_nom_cla();
$nom_grp = $grp->get_nom_grp();


?>
<div style="padding:0;margin-left:10%;margin-right: 10%"class="panel panel-primary col-md-10">
    <div class="panel-heading">
        Horaire
    </div>
    <div class="panel-body">
        <?php
        echo '<div class="col-md-8 titre">';
        echo "Classe " . $nom_cla . " (" . $nom_grp . ")";
        echo '</div>';

        $tab_horaire = array(0 => array("h_tr" => "7:30"),

            1 => array("h_tr" => "8:15"),

            2 => array("h_tr" => "9:00", "Pause" => "Pause"),

            3 => array("h_tr" => "9:15"),

            4 => array("h_tr" => "10:00"),

            5 => array("h_tr" => "10:45", "type-heure" => "tranche"),

            6 => array("h_tr" => "10:50"),

            7 => array("h_tr" => "11:30", "type-heure" => "tranche"),

            8 => array("h_tr" => "11:35", "pause_th" => "true", "type-heure" => "tranche"),

            9 => array("h_tr" => "11:45", "pause_pr" => "true", "pause_th" => "true"),

            10 => array("h_tr" => "12:25", "pause_pr" => "true", "type-heure" => "tranche"),

            11 => array("h_tr" => "12:45", "type-heure" => "tranche"),

            12 => array("h_tr" => "13:10"),

            13 => array("h_tr" => "13:55", "Pause" => "Pause"),

            14 => array("h_tr" => "14:05"),

            15 => array("h_tr" => "14:50"),

            16 => array("h_tr" => "15:30", "type-heure" => "tranche"),

            17 => array("h_tr" => "15:35", "type-heure" => "tranche"),

            18 => array("h_tr" => "15:40"),

            19 => array("h_tr" => "16:25"),

            20 => array("h_tr" => "16:30", "type-heure" => "tranche")

        );

        $tab_Jour = array(0 => array("j" => "Lundi", "id" => "lu", "nb" => 0),
            1 => array("j" => "Mardi", "id" => "ma", "nb" => 1),
            2 => array("j" => "Mercredi", "id" => "me", "nb" => 2),
            3 => array("j" => "Jeudi", "id" => "je", "nb" => 3),
            4 => array("j" => "Vendredi", "id" => "ve", "nb" => 4)
        );
        echo '<div class="col-md-8 div-horaire">';
        echo '<table class="tab-horaire">';
        echo '<tr>';
        echo '<td class="col-md-2"></td>';
        foreach ($tab_Jour as $tj) {
            $return = $h_cad->get_jour_in_horaire($_POST['id_cla'], $tj['nb']);

            echo '<td class="text-center col-md-2 hover-day" id="' . $tj['id'] . '">'
                . '<div class="col-md-12  pressed-day" id="' . $tj['id'] . '">' . $tj['j'] . '</div>';

            $display = "none";
            if($return){
                $display = "block";
            }

            echo '<div class="text-center col-md-12 champ-del" style="padding:5px;display:'.$display.'">' .
                Form::button("del-" . $tj['id'], "delete", "", "btn-danger glyphicon glyphicon-remove btn-delete", "", "id_j='" . $tj['id'] . "'") . '</div></td>';
        }
        echo '</tr>';



        foreach ($tab_horaire as $index => $th) {
            $h = "";
            $cl_tr = "";
            $cl_td = "";
            $Pause = "";
            $h .= "padding:0px;text-align:center;";
            if (isset($th['Pause'])) {
                $cl_td .= "Pause";
                $h .= "background-color:#E0E0E0;";
            }

            if (isset($th['type-heure']) && $th['type-heure'] == "tranche") {
                $cl_tr .= $cl_td . " tranche";
            } else {
                $cl_tr .= $cl_td . "basic";
            }

            echo '<tr class="' . $cl_tr . '" style="' . $h . '">';
            echo '<td class="' . $cl_td . '"style="' . $h . '">';
            echo $th["h_tr"];
            echo '</td>';
            foreach ($tab_Jour as $tj) {
                $style = "";
                $get_tranche = $h_cad->get_tranche_index_cla($_POST['id_cla'], $tj['nb'], $th['h_tr']);

                $str = "";
                $str .= "<td";
                $str .= " id=" . $index . $tj['id'];
                $str .= " class='tranche-horaire " . $cl_td . "'";
                $str .= " id_j='" . $tj['id'] . "'";
                $str .= " id_heure='" . $index . "'";
                $str .= " h_tr='" . $th['h_tr'] . "'";

                if (isset($th['pause_pr']) && $th['pause_pr'] == "true") {
                    $str .= " pause_pr='" . $th['pause_pr'] . "'";
                }
                if (isset($th['pause_th']) && $th['pause_th'] == "true") {
                    $str .= " pause_th='" . $th['pause_th'] . "'";
                }
                if (isset($get_tranche['pause_cad']) && isset($th['Pause'])) {

                } else {
                    $str .= " id_horaire='" . $get_tranche['ref_bra'] . "'";
                    if ($get_tranche['ref_bra'] == 1) {
                        $style .= " background:yellow;";
                    } else if ($get_tranche['ref_bra'] == 2) {
                        $style .= " background:red;";
                    } else if ($get_tranche['ref_bra'] == 3) {
                        $style .= " background:blue;";
                    } else if ($get_tranche['ref_bra'] == 4) {
                        $style .= " background:orange;";
                    }
                }
                $str .= " style=" . $style;
                $str .= " ></td>";

                echo $str;
            }
            echo '</tr>';
        }

        echo '</table>';

        echo '</div>';
        echo '<div class="col-md-offset-1 col-md-3" >';

        echo '<table class="table" style="margin-top:50px">';
        echo '<tr>';
        echo '<td class="col-md-7 "><span class="glyphicon glyphicon-triangle-right"></span>' . Form::radio("bPratique", "chxBranche", "1", "", "checked ") . Form::label("lbPratique", "Pratique", "bPratique", "sel-br") . '</td>';
        echo '<td class="col-md-3" style="background:yellow;"></td>';
        echo '</tr>';
        echo '<tr>';
        echo '<td class="col-md-8"><span></span>' . Form::radio("bMPT", "chxBranche", "2", "", "") . Form::label("lbMPT", "MPT", "bMPT", "sel-br") . '</td>';
        echo '<td class="col-md-3" style="background:red;"></td>';
        echo '</tr>';
        echo '<tr>';
        echo '<td class="col-md-8"><span ></span>' . Form::radio("bTheorie", "chxBranche", "3", "", "") . Form::label("lbTheorie", "Théorie", "bTheorie", "sel-br") . '</td>';
        echo '<td class="col-md-3" style="background:Blue;"></td>';
        echo '</tr>';
        echo '<tr>';
        echo '<td class="col-md-8"><span></span>' . Form::radio("bCFC", "chxBranche", "4", "", "") . Form::label("lbCFC", 'CFC', "bCFC", "sel-br", "chx") . '</td>';
        echo '<td class="col-md-3" style="background:orange;"></td>';
        echo '</tr>';
        echo '</table>';

        echo '<table class="table" style="margin-top:50px">';
        echo '<tr style="z-index:5">';
        echo '<td class="col-md-8">' .
            Form::button("btn_del", "", "Supprimer le tableau", "btn col-md-12 btn-danger delete-tab-btn", "", "action='supprimer' id_cla=" . $_POST['id_cla'])
            . '</td>';
        echo '</tr>';
        echo '<tr style="z-index=5">';
        echo '<td class="col-md-8">' .
            Form::button("btn_" . $_POST['id_cla'], "", "Ajouter le tableau", "btn col-md-12 btn-primary ajout-btn", "", "action='ajout' id_cla=" . $_POST['id_cla'])
            . '</td>';
        echo '</tr>';
        echo '</table>';
        echo '</div>';
        ?>
    </div>
</div>
<script src="./../groupes/js/horaire_cadre.js"></script>
<?php

Class Personne extends Projet {

    private $id_per;
    private $sexe;
    private $nom;
    private $prenom;
    private $adresse;
    private $localite;
    private $npa;
    private $tel_mobile;
    private $tel_bureau;
    private $tel_prive;
    private $email;
    private $entreprise;
    private $ref_emp;
    private $password;
    private $enseignant;
    private $expert;
    private $supp_prof;
    private $actif;
    private $nom_emp;

    public function __construct($id_per = null) {

        parent::__construct();

        if ($id_per) {
            $this->set_id_per($id_per);
            $this->init();
        }
    }

    public function init() {
        $query = "SELECT PER.*, EMP.nom_emp FROM t_personnes PER LEFT JOIN t_employeurs EMP on EMP.id_emp = PER.ref_emp WHERE id_per=:id_per";
        
        try {
            $stmt = $this->pdo->prepare($query);
            $args[':id_per'] = $this->get_id();
            $stmt->execute($args);
            $tab = $stmt->fetch();
            $this->set_sexe($tab['sexe_per']);
            $this->set_nom($tab['nom_per']);
            $this->set_prenom($tab['prenom_per']);
            $this->set_email($tab['email_per']);
            $this->set_password($tab['password_new_per']);
            $this->set_adresse($tab['adresse_per']);
            $this->set_localite($tab['localite_per']);
            $this->set_npa($tab['npa_per']);
            $this->set_entreprise($tab['entreprise_per']);
            $this->set_ref_emp($tab['ref_emp']);
            $this->set_tel_prive($tab['tel_prive_per']);
            $this->set_tel_mobile($tab['tel_mobile_per']);
            $this->set_tel_bureau($tab['tel_bureau_per']);
            $this->set_enseignant($tab['enseignant_per']);
            $this->set_expert($tab['expert_per']);
            $this->set_supp_prof($tab['supp_prof_per']);
            $this->set_actif($tab['actif_per']);
            $this->set_nom_emp($tab['nom_emp']);
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

	public function init_by_mail($email){
        $query = "SELECT * FROM t_personnes WHERE email_per =:email";

        try {
            $stmt = $this->pdo->prepare($query);
            $args['email'] = strtolower($email);
            $stmt->execute($args);
            $tab = $stmt->fetch();
            $this->set_id_per($tab['id_per']);
            $this->init();
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

    public function __toString() {
        $str = "\n<pre>\n";
        foreach ($this AS $key => $val) {
            if ($key != "pdo") {
                $str .= "\t" . $key;
                $lengh_key = strlen($key);
                for ($i = strlen($key); $i < 20; $i++) {
                    $str .= "&nbsp;";
                }
                $str .= "=>&nbsp;&nbsp;&nbsp;" . $val . "\n";
            }
        }
        $str .= "\n</pre>";
        $str .= parent::__toString();
        return $str;
    }

    //Ajoute une personne
    public function add($tab) {
        $this->gen_password($tab['password_new_per']);
        //tableau d'arguments
        $args['sexe_per'] = $tab['sexe_per'];
        $args['nom_per'] = $tab['nom_per'];
        $args['prenom_per'] = $tab['prenom_per'];
        $args['password_new_per'] = $this->get_password();
        $args['email_per'] = strtolower($tab['email_per']);
        $args['adresse_per'] = $tab['adresse_per'];
        $args['localite_per'] = $tab['localite_per'];
        $args['npa_per'] = $tab['npa_per'];
        $args['entreprise_per'] = $tab['entreprise_per'];
        $args['ref_emp'] = $tab['ref_emp'];
        $args['tel_prive_per'] = $tab['tel_prive_per'];
        $args['tel_mobile_per'] = $tab['tel_mobile_per'];
        $args['tel_bureau_per'] = $tab['tel_bureau_per'];
        $args['enseignant_per'] = $tab['enseignant_per'];
        $args['expert_per'] = $tab['expert_per'];
        $args['supp_prof_per'] = $tab['supp_prof_per'];
        $args['actif_per'] = $tab['actif_per'];

        $query = "INSERT INTO t_personnes SET "
                . "sexe_per = :sexe_per,"
                . "nom_per = :nom_per, "
                . "prenom_per = :prenom_per, "
                . "email_per = :email_per, "
                . "password_new_per = :password_new_per, "
                . "adresse_per = :adresse_per, "
                . "localite_per = :localite_per, "
                . "npa_per = :npa_per, "
                . "entreprise_per = :entreprise_per, "
                . "ref_emp = :ref_emp, "
                . "tel_prive_per = :tel_prive_per, "
                . "tel_mobile_per = :tel_mobile_per, "
                . "tel_bureau_per = :tel_bureau_per, "
                . "enseignant_per = :enseignant_per, "
                . "expert_per = :expert_per, "
                . "supp_prof_per = :supp_prof_per, "
                . "actif_per = :actif_per;"
                //Selection du dernier id_per entr�
                . "SELECT @lastid := id_per FROM t_personnes ORDER BY id_per DESC LIMIT 1;"
                //Attribution de l'autorisation 'Utilisateur' � la derni�re personne ajout�
                . "INSERT INTO t_fnc_per SET t_fnc_per.id_per = @lastid,"
                . "t_fnc_per.id_fnc = 3;";
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $stmt = $this->pdo->lastInsertId();
        } catch (Exception $ex) {
            return false;
        }
    }

	//Ajoute une personne
    public function add_debug($tab) {
        $this->gen_password($tab['password_new_per']);
        //tableau d'arguments
        $args['nom_per'] = $tab['nom_per'];
        $args['prenom_per'] = $tab['prenom_per'];
        $args['password_new_per'] = $this->get_password();
        $args['email_per'] = strtolower($tab['email_per']);
        
        $query = "INSERT INTO t_personnes SET "
                . "nom_per = :nom_per, "
                . "prenom_per = :prenom_per, "
                . "email_per = :email_per, "
                . "password_new_per = :password_new_per ";
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $stmt = $this->pdo->lastInsertId();
        } catch (Exception $ex) {
            return false;
        }
    }
	
	
    //Met � jour les personnes
    public function update($tab) {

        $this->gen_password($tab['password_new_per']);

        //tableau d'arguments
        $args['id_per'] = $tab['id_per'];
        $args['sexe_per'] = $tab['sexe_per'];
        $args['nom_per'] = $tab['nom_per'];
        $args['prenom_per'] = $tab['prenom_per'];
        $args['password_new_per'] = $this->get_password();
        $args['email_per'] = strtolower($tab['email_per']);
        $args['adresse_per'] = $tab['adresse_per'];
        $args['localite_per'] = $tab['localite_per'];
        $args['npa_per'] = $tab['npa_per'];
        $args['entreprise_per'] = $tab['entreprise_per'];
        $args['ref_emp'] = $tab['ref_emp'];
        $args['tel_prive_per'] = $tab['tel_prive_per'];
        $args['tel_mobile_per'] = $tab['tel_mobile_per'];
        $args['tel_bureau_per'] = $tab['tel_bureau_per'];
        $args['enseignant_per'] = $tab['enseignant_per'];
        $args['expert_per'] = $tab['expert_per'];
        $args['supp_prof_per'] = $tab['supp_prof_per'];
        $args['actif_per'] = $tab['actif_per'];

        $query = "UPDATE t_personnes SET "
                . "sexe_per = :sexe_per,"
                . "nom_per = :nom_per, "
                . "prenom_per = :prenom_per, "
                . "email_per = :email_per, "
                . "password_new_per = :password_new_per, "
                . "adresse_per = :adresse_per, "
                . "localite_per = :localite_per, "
                . "npa_per = :npa_per, "
                . "entreprise_per = :entreprise_per, "
                . "ref_emp = :ref_emp, "
                . "tel_prive_per = :tel_prive_per, "
                . "tel_mobile_per = :tel_mobile_per, "
                . "tel_bureau_per = :tel_bureau_per, "
                . "enseignant_per = :enseignant_per, "
                . "expert_per = :expert_per, "
                . "supp_prof_per = :supp_prof_per, "
                . "actif_per = :actif_per "
                . "WHERE t_personnes.id_per =:id_per";

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
        } catch (Exception $ex) {
            return false;
        }
        return $this->pdo->lastInsertId();
    }

    //R�cup�re toutes les addresses mail
    public function get_mail() {
        $query = "SELECT email_per FROM t_personnes";

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $exc) {
            return false;
        }
    }
   
    //Sélectione id, nom, prenom de la classe Personne 
    public function get_nom_prenom_id() {
        $query = "SELECT nom_per, prenom_per, id_per FROM t_personnes ORDER BY nom_per ASC";
   
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $e) {
            return false;
        }
    }
   
   
    
    public function get_all_fnc(){
       
        $query = "SELECT abr_fnc,nom_fnc FROM t_fonctions FNC "
                ."JOIN t_fnc_per FNP ON FNC.id_fnc=FNP.id_fnc AND FNP.id_per=:id_per ";
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $exc) {
            return false;
        }
    }

    //R�cup�re tout les experts
    public function get_all_expert_actif() {
        $query = "SELECT * FROM t_personnes WHERE actif_per = 1 AND expert_per=1 ORDER BY nom_per, prenom_per ASC ";


        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $exc) {
            return false;
        }
    }

    //R�cup�re tout les enseignants
    public function get_all_enseignant_actif() {
        $query = "SELECT * FROM t_personnes WHERE actif_per = 1 AND enseignant_per=1 ORDER BY nom_per, prenom_per ASC ";


        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $exc) {
            return false;
        }
    }

    //R�cup�re toutes les personnes
    public function get_all() {
        $query = "SELECT * FROM t_personnes ORDER BY nom_per, prenom_per ASC ";
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $exc) {
            return false;
        }
    }

    //R�cup�re toutes les personnes inactifs
    public function get_all_actif() {
        $query = "SELECT * FROM t_personnes WHERE actif_per = 1 ORDER BY nom_per, prenom_per ASC ";


        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $exc) {
            return false;
        }
    }

    //R�cup�re toutes les personnes inactifs
    public function get_all_inactif() {
        $query = "SELECT * FROM t_personnes WHERE actif_per = 0 ORDER BY nom_per, prenom_per ASC ";


        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $exc) {
            return false;
        }
    }

    //R�cup�re tout les supp�rieurs professionels
    public function get_all_supp_prof_actif() {
        $query = "SELECT * FROM t_personnes WHERE actif_per = 1 AND supp_prof_per=1 ORDER BY nom_per, prenom_per ASC ";


        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $exc) {
            return false;
        }
    }

    //R�cup�re l'id le nom et le pr�nom de l'expert actif choisi
    public function get_expert_actif_id($id) {
        $query = "SELECT id_per, nom_per, prenom_per FROM t_personnes WHERE actif_per = 1 AND expert_per=1 AND id_per=:id";

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(array(":id" => $id));
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $exc) {
            return false;
        }
    }

    //Formater num�ro de t�l�phone
    function tel($str) {
        if (strlen($str) == 10) {
            $res = substr($str, 0, 3) . '&nbsp;';
            $res .= substr($str, 3, 3) . '&nbsp;';
            $res .= substr($str, 6, 2) . '&nbsp;';
            $res .= substr($str, 8, 2) . '&nbsp;';
            return $res;
        }
    }

    //Cr�ation de Vcard
    public function vcard() {
        if ($this->get_sexe() == 'F') {
            $this->set_sexe("Mme.");
        } else {
            $this->set_sexe("M.");
        }

        $str = "BEGIN:VCARD\n";
        $str .= "VERSION:3.0\n";
        //Représentation structurée du nom de la personne
        $str .= "N;CHARSET=utf-8;ENCODING=8BIT:" . $this->get_nom() . ";" . $this->get_prenom() . ";;" . $this->get_sexe() . "; \n";
        //Chaine formatée représentant le nom associé à la vCard
        $str .= "FN;CHARSET=utf-8;ENCODING=8BIT:" . $this->get_prenom() . " " . $this->get_nom() . "; \n";
        //Représentation structurée de l'adresse de la personne
        $str .= "ADR;HOME;PREF;CHARSET=utf-8;ENCODING=8BIT:" . $this->get_adresse() . ";" . $this->get_localite() . ";" . $this->get_npa() . ";Suisse \n";
        //Libellé de l'adresse de la personne
        $str .= "LABEL;HOME;PREF;CHARSET=utf-8;ENCODING=QUOTED-PRINTABLE:" . $this->get_adresse() . "=0D=0A=\n" . $this->get_localite() . " " . $this->get_npa() . "=0D=0A=;\n";
        //Adresse mail de la personne
        $str .= "EMAIL;CHARSET=utf-8;ENCODING=8BIT:" . $this->get_email() . ";\n";
        //Nom de l'entreprise
        if ($this->get_entreprise() == NULL) {
            if($this->get_nom_emp() == "---"){
                $this->set_nom_emp("");
            }
            $str .= "ORG;CHARSET=utf-8;ENCODING=8BIT:" . $this->get_nom_emp() . ";\n";
        } else {
            $str .= "ORG;CHARSET=utf-8;ENCODING=8BIT:" . $this->get_entreprise() . ";\n";
        }
        //Numéro de téléphone privé
        $str .= "TEL;HOME;VOICE:" . $this->get_tel_prive() . ";\n";
        //Numéro de téléphone du bureau
        $str .= "TEL;WORK;VOICE:" . $this->get_tel_bureau() . ";\n";
        //Numéro de téléphone mobile
        $str .= "TEL;CELL;VOICE:" . $this->get_tel_mobile() . ";\n";
        $str .= "END:VCARD\n";

        return $str;
    }

    public function check_email($email) {
        $query = "SELECT email_per FROM t_personnes WHERE email_per = :email LIMIT 1";
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(array(":email" => $email));
            $tab = $stmt->fetch();
            if ($tab["email_per"] == strtolower($email)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $ex) {
            return false;
        }
    }

    public function check_login($email, $password) {
        $query = "SELECT id_per, password_new_per, nom_per FROM t_personnes WHERE email_per = :email";

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(array(":email" => $email));
            $tab = $stmt->fetch();
            if (password_verify($password, $tab['password_new_per'])) {
                $_SESSION['id'] = $tab['id_per'];
//Cr�ation de la login_string "navigateur + adresse IP"
                $user_browser_ip = $_SERVER['HTTP_USER_AGENT'] . $_SERVER['REMOTE_ADDR'];
// hash du mot de passe + login_string
                $_SESSION['login_string'] = password_hash($tab['password_new_per'] . $user_browser_ip, PASSWORD_DEFAULT);
                $_SESSION['email'] = strtolower($email);
                $_SESSION['name'] = $tab['nom_per'];
                return true;
            }
        } catch (Exception $ex) {
            return false;
        }
    }

    public function check_connect() {
// On s'assure que les trois variables de session existent bien
        if (isset($_SESSION['id'], $_SESSION['email'], $_SESSION['login_string'])) {
// Cr�ation de la login_string
            $user_browser_ip = $_SERVER['HTTP_USER_AGENT'] . $_SERVER['REMOTE_ADDR'];

// V�rification de la login_string(correspond � la login_string en session)
            if (password_verify($this->get_password() . $user_browser_ip, $_SESSION['login_string'])) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

	/**
     * 2019-01-18 JHI
     * @param $length
     * @param $characters
     * @return string mot de passe
     */
    function gen_random_password($length, $characters) {


    // $length - the length of the generated password
    // $characters - types of characters to be used in the password

    // define variables used within the function
        $symbols = array();
        $used_symbols = '';

    // an array of different character types
        $symbols["lower_case"] = 'abcdefghijklmnopqrstuvwxyz';
        $symbols["upper_case"] = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $symbols["numbers"] = '1234567890';
        $symbols["special_symbols"] = "!?~@#-_+<>[]{}";

        $characters = explode(",",$characters); // get characters types to be used for the passsword
        //print_r($characters);
        foreach ($characters as $key=>$value) {
            $used_symbols .= $symbols[$value]; // build a string with all characters
        }
        $symbols_length = strlen($used_symbols) - 1; //strlen starts from 0 so to get number of characters deduct 1


        $pass = '';
        for ($i = 0; $i < $length; $i++) {
            $n = rand(0, $symbols_length); // get a random character from the string with all characters
            $pass .= $used_symbols[$n]; // add the character to the password string
            //echo $pass;
        }
		
        return $pass; // return the generated password
    }


	public function new_pwd($password)
    {

        $headers = 'MIME-Version: 1.0' . "\r\n";

        $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
        $headers .= 'From: ICT-JB.net <webmaster@ict-jb.net>' . "\r\n";
        $headers .= 'Reply-To: webmaster@ict-jb.net' . "\r\n";
        $headers .= "X-Sender: <www.ict-b.net>" . "\r\n";;
        $headers .= "X-Mailer: PHP" . "\r\n";;
        $headers .= "X-auth-smtp-user: webmaster@ict-jb.net" . "\r\n";;
        $headers .= "X-abuse-contact: webmaster@ict-jb.net" . "\r\n";;
        $expe = "webmaster@ict-jb.net";

        $date_time = date("D, d M Y H:i:s O");
        $headers .= 'DATE: ' . $date_time . "\r\n";


        $sujet_mail = "Nouveau mot de passe pour " . $this->get_prenom() . " " . $this->get_nom();
        $mess_mail = utf8_decode("Votre nouveau mot de passe pour l'accès à vos notes est : " . $password . "\n Prenez en soins !");
        $destinataire_mail = $this->get_email() . ", jacques@hirtzel.ch";
        //$destinataire_mail = "jacques@hirtzel.ch";
        mail($destinataire_mail, $sujet_mail, $mess_mail, $headers);


        /*$mail = new mail();
        $destinataire_mail = array( $this->get_email());*/


        //Envoie de l'email
        /*$mail->send_email($destinataire_mail, $sujet_mail, $mess_mail);
        $mail->send_email(array('jacques.hirtzel@ceff.ch'), $sujet_mail, $mess_mail);
*/

       // mail($destinataire_mail, $sujet_mail, $mess_mail, $headers);
  //      mail("jacques@hirtzel.ch", $sujet_mail, $mess_mail, $headers);
    }

    public function check_aut($aut_list) {
        $tab_aut = explode(";", $aut_list);

        $tab_aut_per = $this->get_all_aut();

        foreach ($tab_aut AS $aut) {
            if (in_array($aut, $tab_aut_per)) {
                return true;
            }
        }
        return false;
    }

    public function get_all_aut() {

        $query = "SELECT DISTINCT code_aut FROM t_autorisations AUT "
                . "JOIN t_aut_fnc AUF ON AUT.id_aut=AUF.id_aut "
                . "JOIN t_fnc_per FNP ON AUF.id_fnc =FNP.id_fnc "
                . "WHERE FNP.id_per = :id_per";

        $args[":id_per"] = $this->get_id();

        $stmt = $this->pdo->prepare($query);
        $stmt->execute($args);
        $tab = $stmt->fetchAll();
        $new_tab = array();
        foreach ($tab as $aut) {
            $new_tab[] = $aut['code_aut'];
        }
        return ($new_tab);
//print_r($new_tab);
    }

    public function add_fnc($id_fnc) {
        $query = "INSERT INTO t_fnc_per SET "
                . "id_per = :id_per, "
                . "id_fnc = :id_fnc";

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(array(":id_per" => $this->get_id(), ":id_fnc" => $id_fnc));
            return true;
        } catch (Exception $exc) {
            return false;
        }
    }

    public function del_fnc($id_fnc) {
        $query = "DELETE FROM t_fnc_per WHERE "
                . "id_per = :id_per AND "
                . "id_fnc = :id_fnc";

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(array(":id_per" => $this->get_id(), ":id_fnc" => $id_fnc));
            return true;
        } catch (Exception $exc) {
            return false;
        }
    }
    
    /**
     * contrôle si l'utilisateur connecter est un enseignant et si il a un examen a son nom
     * @param type $id_exa
     * @return boolean
     */
    public function is_enseignant_exa($id_exa){
        $args[":id_per"] = $this->get_id();
        $args[":id_exa"] = $id_exa;
        $query = "SELECT * FROM t_ref_per_exa WHERE ref_per =:id_per AND ref_exa=:id_exa AND ref_fnc=3 "; 
        
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $tab = $stmt->fetch();
            if(isset($tab['ref_exa'])){
                return true;
            }else{
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
        
    }
    
    /**
     * contrôle si l'utilisateur connecter est un expert et si il a un examen a son nom
     * @param type $id_exa
     * @return boolean
     */
    public function is_expert_exa($id_exa){
        $args[":id_per"] = $this->get_id();
        $args[":id_exa"] = $id_exa;
        $query = "SELECT * FROM t_ref_per_exa WHERE ref_per =:id_per AND ref_exa=:id_exa AND ref_fnc=1 "; 
        
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $tab = $stmt->fetch();
            if(isset($tab['ref_exa'])){
                return true;
            }else{
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
        
    }

    public function gen_password($password) {
        $this->set_password(password_hash($password, PASSWORD_DEFAULT));
    }

	public function update_password() {
        $query = 'UPDATE t_personnes '
                . 'SET password_new_per=:password '
                . 'WHERE email_per=:email';

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(array(":password"=>$this->get_password(), ":email"=>$this->get_email()));
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

    function get_initiales($prenom = "")
    {
        if($prenom == ""){
            $prenom = $this->get_prenom();
        }
        $string = str_split($prenom);
        $initales = $string[0] . ".";
        foreach ($string as $key => $char){
            if($char == " " || $char == "-") {
                $initales .= $char . $string[$key + 1] . ".";
            }
        }
        return $initales;
    }

    function get_entreprise() {
        return $this->entreprise;
    }

    function set_entreprise($entreprise) {
        $this->entreprise = $entreprise;
    }

    function get_enseignant() {
        return $this->enseignant;
    }

    function set_enseignant($enseignant) {
        $this->enseignant = $enseignant;
    }

    function get_id() {
        return $this->id_per;
    }

    function get_sexe() {
        return $this->sexe;
    }

    function get_nom() {
        return $this->nom;
    }

    public function get_prenom() {
        return $this->prenom;
    }

    function get_adresse() {
        return $this->adresse;
    }

    function get_localite() {
        return $this->localite;
    }

    function get_npa() {
        return $this->npa;
    }

    function get_tel_mobile() {
        return $this->tel_mobile;
    }

    function get_tel_bureau() {
        return $this->tel_bureau;
    }

    function get_tel_prive() {
        return $this->tel_prive;
    }

    function get_email() {
        return $this->email;
    }

    function get_password() {
        return $this->password;
    }

    function get_expert() {
        return $this->expert;
    }

    function get_supp_prof() {
        return $this->supp_prof;
    }

    function get_actif() {
        return $this->actif;
    }

    function set_id_per($id_per) {
        $this->id_per = $id_per;
    }

    function set_sexe($sexe) {
        $this->sexe = $sexe;
    }

    function set_nom($nom) {
        $this->nom = $nom;
    }

    function set_prenom($prenom) {
        $this->prenom = $prenom;
    }

    function set_adresse($adresse) {
        $this->adresse = $adresse;
    }

    function set_localite($localite) {
        $this->localite = $localite;
    }

    function set_npa($npa) {
        $this->npa = $npa;
    }

    function set_tel_mobile($tel_mobile) {
        $this->tel_mobile = $tel_mobile;
    }

    function set_tel_bureau($tel_bureau) {
        $this->tel_bureau = $tel_bureau;
    }

    function set_tel_prive($tel_prive) {
        $this->tel_prive = $tel_prive;
    }

    function set_email($email) {
        $this->email = $email;
    }

    function set_password($password) {
        $this->password = $password;
    }

    function set_expert($expert) {
        $this->expert = $expert;
    }

    function set_supp_prof($supp_prof) {
        $this->supp_prof = $supp_prof;
    }

    function set_actif($actif) {
        $this->actif = $actif;
    }

    function get_ref_emp() {
        return $this->ref_emp;
    }

    function set_ref_emp($ref_emp) {
        $this->ref_emp = $ref_emp;
    }

    function get_nom_emp() {
        return $this->nom_emp;
    }

    function set_nom_emp($nom_emp) {
        $this->nom_emp = $nom_emp;
    }

}

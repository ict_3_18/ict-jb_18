<?php
Class Module extends Projet{

    private $id_mod;
    private $nom;
    private $num;
    private $description;
    private $lien_desc; 
    private $competence; 
    private $objectifs = array(); 
    
    public function __construct($id = null){
        parent::__construct();
        
        if($id){ //Si on a initialisé l'id en argument 
            $this->set_id_mod($id);
            $this->init();
        }
    }
     
    /**
     * Initialisation de l'objet (l'id doit être setté)
     * @return boolean
     */
    public function init() {
        $query = "SELECT * FROM t_desc_modules WHERE id_desc_mod=:id_desc_mod";
        try {
            $stmt = $this->pdo->prepare($query);
            $args[':id_desc_mod'] = $this->get_id_mod();
            $stmt->execute($args);
            $tab = $stmt->fetch();
            
            $this->set_nom($tab['nom_mod']);
            $this->set_num($tab['num_mod']);
            $this->set_description($tab['description_mod']);
            $this->set_lien_desc($tab['lien_desc']);
            $this->set_competence($tab['competence_mod']);
            
            $tab_objectifs = array(); 
            for($obj_num = 1 ; $obj_num <= 7 ; $obj_num++){
                //Push les objectifs dans le tableau
                $tab_objectifs[$obj_num] = $tab["obj_".$obj_num]; 
            } 
            $this->set_objectifs($tab_objectifs); 
            
           
        } catch (Exception $e) {
            return false;
        }
        return true;
    }
    
    
    /**
    * Ajoute un module dans la base de données
    * @param array tableau avec les propriétés du module
    * @return int id du module ajoutée
    */
    public function add($tab){
        // Tableau d'arguments
        $args['nom_mod'] = $tab['nom_mod'];
        $args['num_mod'] = $tab['num_mod'];
        $args['description_mod'] = $tab['description_mod'];
        $args['lien_desc'] = $tab['lien_desc'];
        $args['competence_mod'] = $tab['competence_mod'];
        
        //Requete
        $query = "INSERT INTO t_desc_modules SET "
                . "nom_mod = :nom_mod, "
                . "num_mod = :num_mod, "
                . "description_mod = :description_mod, "
                . "competence_mod = :competence_mod, ";
        
        for($obj_num = 1 ; $obj_num <= 7 ; $obj_num++){
            //Complète la requete et le tableau d'arguments 
            $args["obj_".$obj_num] = $tab["obj_".$obj_num]; 
            $query .= "obj_".$obj_num." = :obj_".$obj_num.", ";
        }
        
        $query .= "lien_desc = :lien_desc ";
        
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            
        } catch (Exception $e) {
            return false;
        }
        
        // retourne le dernier id inséré
        return $this->pdo->lastInsertId();
       
    }
    
    
    /**
    * Met à jour un module dans la base de données
    * @param array tableau avec les propriétés du module contenant l'id 
    * @return boolean
    */
    public function update($tab){
        // Tableau d'arguments
        $args['id_desc_mod'] = $tab['id_desc_mod'];
        $args['nom_mod'] = $tab['nom_mod'];
        $args['num_mod'] = $tab['num_mod'];
        $args['description_mod'] = $tab['description_mod'];
        $args['lien_desc'] = $tab['lien_desc'];
        $args['competence_mod'] = $tab['competence_mod'];
        
        //Requete
        $query = "UPDATE t_desc_modules SET "
                . "nom_mod = :nom_mod, "
                . "num_mod = :num_mod, "
                . "description_mod = :description_mod, "
                . "competence_mod = :competence_mod, ";
        
        for($obj_num = 1 ; $obj_num <= 7 ; $obj_num++){
            //Complète la requete et le tableau d'arguments 
            $args["obj_".$obj_num] = $tab["obj_".$obj_num]; 
            $query .= "obj_".$obj_num." = :obj_".$obj_num.", ";
        }
        
        $query .= "lien_desc = :lien_desc WHERE id_desc_mod = :id_desc_mod ";
        
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            
        } catch (Exception $e) {
            return false;
        }
        
        return true ; 
    }
    

    /**
     * Récupère la totalité de enregistrements de la table t_desc_modules dans l'ordre fourni 
     * @param string $order ordre à utiliser (avec le num par défaut)
     * @return array tableau des personnes
     */
    function get_all($order = "num_mod"){
         $query = "SELECT * FROM t_desc_modules order by ".$order;
         
         if($order == "num_mod"){ //Order special : 1,2,3,4,5A,5B ....
             $query = "SELECT * FROM t_desc_modules ORDER BY CAST(num_mod AS UNSIGNED), num_mod"; 
         }
         
         try {
             $stmt = $this->pdo->prepare($query);
             $stmt->execute();
             $tab = $stmt->fetchAll();
             return $tab;
         } catch (Exception $e) {
             return false;
         }
    }
    
    
    /**
     * Regarde si un joueur est en double 
     * @param numero du module 
     * @return true si il n'y a pas de doublon
     */
    public function check_doublon($num){
         $query = "SELECT * FROM t_desc_modules WHERE num_mod = :num_mod";
         $args[':num_mod'] = $num;
         try {
             $stmt = $this->pdo->prepare($query);
             $stmt->execute($args);
             
             if(!empty($stmt->fetchAll())){//Si le numero est déja dans la base 
                 return false ; 
             }
             
             return true ; 
            
         } catch (Exception $e) {
             return false;
         }
    }
    
     /**
     * Retourne un tableau avec les chaines option pour chaque module  
     * @return $tab
     */
    public function get_option_mod(){
        $tab_options = array(); 
        foreach($this->get_all("num_mod") as $module){
            $str = "<option value=".$module['num_mod'].">".$module['num_mod']."-".$module['nom_mod']."</option>"; 
            array_push($tab_options, $str);
        } 
        return $tab_options; 
    }
    
    /**
     * 
     * @return string Liste formatée du contenu de l'objet
     */
    public function __toString(){
        $str = "\n<pre>\n";
        foreach($this AS $key => $val){
            if($key != "pdo"){
                $str .= "\t".$key;
                $lengh_key = strlen($key);
                for($i=strlen($key);$i<20;$i++){
                    $str .= "&nbsp;";
                }
                $str .= "=>&nbsp;&nbsp;&nbsp;".$val."\n";
            }
        }
        $str .= "\n</pre>";
        return $str;
    }
   
    
    //Getter & Setter $id_mod
    function set_id_mod($id_mod) {
        $this->id_mod = $id_mod;
    }
    function get_id_mod() {
        return $this->id_mod;
    }

    //Getter & Setter $description
    function set_description($description) {
        $this->description = $description;
    }
    function get_description() {
        return $this->description;
    }
    
    //Getter & Setter $nom
    function set_nom($nom) {
        $this->nom = $nom;
    }
    function get_nom() {
        return $this->nom;
    }
      
    //Getter & Setter $num
    function set_num($num) {
        $this->num = $num;
    }
    function get_num() {
        return $this->num;
    }
    
    //Getter & Setter $lien_desc
    function set_lien_desc($lien_desc) {
        $this->lien_desc = $lien_desc;
    }
    function get_lien_desc() {
        return $this->lien_desc;
    }
    
    //Getter & Setter $competence
    function set_competence($competence) {
        $this->competence = $competence;
    }
    function get_competence() {
        return $this->competence;
    }
    
    //Getter & Setter $objectifs
    function set_objectifs($objectifs) {
        $this->objectifs = $objectifs;
    }
    function get_objectifs() {
        return $this->objectifs;
    }
    
}
?>

<?php
class PdfCriteresList extends Mypdf {
    


    function Header()
    {
        // Police Arial gras 15
        $this->SetFont('Arial','B',15);
        // D�calage
        $this->Cell(80);
        // Titre encadr�
        $this->Cell(30,10,utf8_decode('TPI / Crit�res sp�cifiques'),0,0,'C');
        // Saut de ligne
        $this->Ln(20);
    }
    
    function Footer()
    {
        // Positionnement � 1,5 cm du bas
        $this->SetY(-15);
        // Police Arial italique 8
        $this->SetFont('Arial','I',8);
        // Num�ro de page centr�
        $this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
    }
    
    
    function gen_crt($crt){
        
        $h_ln_title = 8;
        $h_ln_desc = 7;
        $h_ln_pt = 6;
        
        /* Zone 3 points */
        $this->SetDrawColor(0,255,157);
        $this->SetFillColor(200,200,200);
        $this->SetTextColor(0,0,0);
        
        $this->SetFont('arial', 'B', 12);
        $this->cell(10,$h_ln_title,$crt->get_code(),0,0,"C",true);
        $this->cell(170,$h_ln_title,utf8_decode($crt->get_nom()),0,1,"L","true");
        $this->SetFont('arial', 'i', 12);
        $this->Multicell(180,$h_ln_desc,utf8_decode($crt->get_description()),0,1);
        $this->SetFont('arial', '', 10);
        
        /* Zone 3 points */
        $this->SetDrawColor(0,255,157);
        $this->SetFillColor(0,255,157);
       
        
        $this->cell(20,$h_ln_pt,utf8_decode("3 points"),0,0,"C",true);
        
        $this->SetTextColor(0,0,0);
        $this->Multicell(160,$h_ln_pt,utf8_decode($crt->get_pt_3()),1,1);
        
        /* Zone 2 points */
        $this->SetDrawColor(0,148,255);
        $this->SetFillColor(0,148,255);
        
        $this->cell(20,$h_ln_pt,utf8_decode("2 points"),0,0,"C",true);
        $this->Multicell(160,$h_ln_pt,utf8_decode($crt->get_pt_2()),1,1);
        
        
        /* Zone 1 points */
        $this->SetDrawColor(255,133,76);
        $this->SetFillColor(255,133,76);
        
        $this->cell(20,$h_ln_pt,utf8_decode("1 point"),0,0,"C",true);
        $this->Multicell(160,$h_ln_pt,utf8_decode($crt->get_pt_1()),1,1);
        
        
        /* Zone 0 points */
        $this->SetDrawColor(255,0,0); // Bordure rouge
        $this->SetFillColor(255,0,0); // fond rouge
        $this->SetTextColor(255,255,255); // texte blanc
        
        $this->cell(20,$h_ln_pt,utf8_decode("0 point"),0,0,"C",true);
        
        $this->SetFillColor(255,255,255); // fond blanc
        $this->SetTextColor(0,0,0);  // texte noir
        $this->Multicell(160,$h_ln_pt,utf8_decode($crt->get_pt_0()),1,1);
    }
    
}

<?php
Class Joueur EXTENDS Personne{
    
    private $id_prt;
    private $score;
    private $status;
    private $id_jou;
    
    
    /**
     * constructeur 
     * @param int id [optionel]
     */
    function __construct($id = null) {
        parent::__construct();
        if($id){
            $this->set_id_jou($id);
            $this->init();
        }
    }
    
    /**
     * 
     * @return string Liste formatée du contenu de l'objet
     */
    public function __toString(){
        $str = "\n<pre>\n";
        foreach($this AS $key => $val){
            if($key != "pdo"){
                $str .= "\t".$key;
                $lengh_key = strlen($key);
                for($i=strlen($key);$i<20;$i++){
                    $str .= "&nbsp;";
                }
                $str .= "=>&nbsp;&nbsp;&nbsp;".$val."\n";
            }
        }
        $str .= "\n</pre>";
        $str .= parent::__toString();
        return $str;
    }
    
    /**
     * Initialisation de l'objet (l'id doit être setté)
     * @return boolean
     */
    public function init() {
        parent::init();
        $query = "SELECT * FROM t_joueurs WHERE id_jou=:id_jou";
        try {
            $stmt = $this->pdo->prepare($query);
            $args[':id_jou'] = $this->get_id_jou();
            $stmt->execute($args);
            $tab = $stmt->fetch();
            //print_r($tab);
            $this->set_id_jou($tab['id_jou']);
            $this->set_id_prt($tab['id_prt']);
            $this->set_status($tab['status_jou']);
            $this->set_score($tab['score_jou']);
            $this->set_rang($tab['rang_jou']);
            $this->set_id_per($tab['id_per']);
            parent::init();
        } catch (Exception $e) {
            return false;
        }
        return true;
    }
    
    /**
     * Renvoie la liste de tous les jeux disponibles
     * @param string $order ordre de tri
     * @return tableau des jeux disponibles ou false
     */
    public function get_all($order = "id_jou"){
        
        $query = "SELECT * FROM t_joueurs JOU JOIN t_personnes PER ON JOU.id_per=PER.id_per ORDER BY :order";
        try {
            $stmt = $this->pdo->prepare($query);
            $args[':order'] = $order;
            $stmt->execute($args);
            $tab = $stmt->fetchAll();
            //print_r($tab);
            
        } catch (Exception $e) {
            return false;
        }
        return $tab;
    }
    
    
    public function set_id_jou($id_prt) {
        $this->id_jou = $id_prt;
    }
    
    public function get_id_jou() {
        return $this->id_jou;
    }
    
    public function set_id_jeu($id_jeu) {
        $this->id_jeu = $id_jeu;
    }
    
    public function get_id_jeu() {
        return $this->id_jeu;
    }
    
    public function set_id_prt($id_prt) {
        $this->id_prt = $id_prt;
    }
    
    public function get_id_prt() {
        return $this->id_prt;
    }
    
    public function set_status($status) {
        $this->status = $status;
    }
    
    public function get_status() {
        return $this->status;
    }
    
    public function set_score($score) {
        $this->score = $score;
    }

    public function get_score() {
        return $this->score;
    }
    
    public function set_rang($rang) {
        $this->rang = $rang;
    }

    public function get_rang() {
        return $this->rang;
    }


}
<?php

class PDFHoraireTPI extends Mypdf {

    private $tab_horaire_tpi;

    private $marge_x = 0;

    private $joursTPI = array("Dates", "Périodes", "Durée totale");

    public function __construct($tab_horaire_tpi, $orientation = 'P', $unit = 'mm', $size = 'A4') {
        parent::__construct($orientation, $unit, $size);

        $this->marge_x = (210 - 160) / 2;
        $this->tab_horaire_tpi = $tab_horaire_tpi;

        $this->global_settings();
        $this->global_horaire();
        $this->info_complementaire();
        $this->table_header();
        $this->table_body();
    }

    public function Header() {
        $this->title();
    }

    public function Footer() {
        $this->y += 40;
        $this->Cell(0, 0, 'Page ' . $this->PageNo(), 0, 0, 'C');
    }

    private function title() {
        $can = new Candidat($this->tab_horaire_tpi[0]['ref_can']);
        $this->SetFont("Arial", "B", "18");
        $this->Cell(40, 10, "Horaire TPI", 0, 0, "L");
        $this->Cell("0", "10", utf8_decode($can->get_nom() . " " . $can->get_prenom() . " (" . $can->get_nom_classe() . " - " . $can->get_nom_group()) . ")", "", 0, "R");
    }

    private function global_settings() {
        $this->SetAutoPageBreak(1, 0);
        $this->AddPage();
        $this->SetFont("courier", 'B', 12);
    }

    private function global_horaire() {
        $this->SetFont("Arial", "", 12);
        $this->x = $this->marge_x;
        $this->y += 10;
        $this->Cell(80, 15, utf8_decode("Période du TPI : du " . date("d.m.y", strtotime($this->tab_horaire_tpi[0]['date_hor']))) . " au " . date("d.m.y", strtotime($this->tab_horaire_tpi[sizeof($this->tab_horaire_tpi) - 1]['date_hor'])), 0, 0, "L");
        $this->y += 15;
    }

    private function info_complementaire() {
        $tpi = new Tpi($this->tab_horaire_tpi[0]['id_tpi']);
        $superieur = new Personne($tpi->get_id_m_app());
        $expert1 = new Personne($tpi->get_id_exp_1());
        $expert2 = new Personne($tpi->get_id_exp_2());

        $this->SetFont("Arial", "", 11);
        $this->x = $this->marge_x;
        $this->Cell(25, 8, "Sujet : ");
        $this->Cell(25, 8, $tpi->get_nom());
        $this->x = $this->marge_x;
        $this->y += 8;
        $this->Cell(25, 8, utf8_decode("Supérieur : "));
        $this->Cell(25, 8, utf8_decode($superieur->get_nom() . " " . $superieur->get_prenom()));
        $this->x = $this->marge_x;
        $this->y += 8;
        $this->Cell(25, 8, utf8_decode("Expert resp : "));
        $this->Cell(25, 8, utf8_decode($expert1->get_nom() . " " . $expert1->get_prenom()));
        $this->x = $this->marge_x;
        $this->y += 8;
        $this->Cell(25, 8, utf8_decode("Expert acc : "));
        $this->Cell(25, 8, utf8_decode($expert2->get_nom() . " " . $expert2->get_prenom()));
        $this->y += 16;
    }

    private function table_header() {
        $this->SetFont("Arial", "B", 10);
        $this->x = $this->marge_x;
        foreach ($this->joursTPI as $item) {
            if ($item == $this->joursTPI[1]) {
                $this->Cell(80, 10, utf8_decode($item), 1, 0, "C");
            } else {
                $this->Cell(40, 10, utf8_decode($item), 1, 0, "C");
            }
        }
        $this->SetY($this->y + 10);
    }

    private function table_body() {
        $tab_jour = array('1' => "Lundi", '2' => "Mardi", '3' => "Mercredi", '4' => "Jeudi", '5' => "Vendredi", '6' => "Samedi", '7' => "Dimanche");

        $this->SetFont("Arial", "", 9);

        $this->x = $this->marge_x;
        $duree_total = 0;
        $height = 7;

        // Boucle principale (Creation du tableau pour chaque jours)
        foreach ($this->tab_horaire_tpi as $horaire_tpi) {
            $tab_heures = explode(",", $horaire_tpi['full_day_hor']);
            $timestamp = strtotime($horaire_tpi['date_hor']);
            $date_jour = date("N", strtotime($horaire_tpi['date_hor']));
            $day_hor = explode(",", $horaire_tpi['total_full_day_hor']);
            $maxCell = sizeof($tab_heures) <= 2 ? 2 : sizeof($tab_heures) == 4 ? 2 : sizeof($tab_heures); // Pour un jour donné (exemple : Mardi) : Le nombre max de cellules utilisées
            $this->x = $this->marge_x;

            // Dates
            $this->Cell(40, $height, $tab_jour[$date_jour] . " " . date("d", $timestamp) . " du " . date("m", $timestamp) . "." . date("y", $timestamp), "LB", 0, "C");
           // $this->y += $height;

            // Périodes
            //$this->y -= ($maxCell - 1) * $height;

            // Calcule duree total
            for ($i = 0; $i < sizeof($day_hor); $i += 2) {
                if ($i != sizeof($day_hor) - 1) {
                    $hor_value = explode('.', $day_hor[$i])[1];
                    $hor_value2 = explode('.', $day_hor[$i + 1])[1];
                    $duree_total += $hor_value + $hor_value2;
                }
            }

            for ($i = 0; $i < sizeof($tab_heures); $i += 2) { // Tab heure vaut soit 4, soit 2
                $heure = $tab_heures[$i];
                $tab_detailled_heure = explode("-", $heure);

                if ($i != sizeof($tab_heures) - 1) {
                    $heure2 = $tab_heures[$i + 1];
                    $tab_detailled_heure2 = explode("-", $heure2);
                    try {
                        $date1 = new DateTime($tab_detailled_heure[0]);
                        $date2 = new DateTime($tab_detailled_heure2[1]);
                    } catch (Exception $e) {
                        $e->getMessage();
                    }
                }

                $this->Cell(18, $height, $date1->format("H:i") . "", "LB", 0, "C");
                $this->Cell(4, $height, "-", "B", 0, "C");
                $this->Cell(18, $height, $date2->format("H:i"), "B", 0, "C");

                if ($i == sizeof($tab_heures) - 2) { // DERNIER tour de la boucle
                    if ($i == 2) {
                        $this->Cell(40, $height, $this->hours_minutes($duree_total), "LBR", 0, "C");
                    } else if ($i == 0) {
                        $this->Cell(40, $height, "-", "LB", 0, "C");
                        $this->Cell(40, $height, $this->hours_minutes($duree_total), "BLR", 0, "C");
                    }
                }
            }
            $this->y += $height;
            //$this->ln();
        }

        $this->y = (297 - 50);
    }

    /**
     * @param $time
     * @param string $format
     * @return string|void
     */
    function hours_minutes($time, $format = '%02d:%02d') {
        if ($time < 1) {
            return;
        }
        $hours = floor($time / 60);
        $minutes = ($time % 60);
        return sprintf($format, $hours, $minutes);
    }

}
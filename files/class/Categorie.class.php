<?php
class Categorie extends Projet {
    private $id;
    private $nom;
    private $id_rub;
    private $ordre;
    
    function __construct($id = 0) {
        
        $this->table_name = "t_categories";
        $this->suffix = "_cat";
        
        if($id != 0){
            $this->set_id($id);
            parent::__construct($id);
                        
        }else{
            parent::__construct();
        }
        
    }
    
    public function init() {
        $query = "SELECT * FROM t_categories WHERE id_cat=:id_cat";
        try {
            $stmt = $this->pdo->prepare($query);
            $args[':id_cat'] = $this->get_id();

            $stmt->execute($args);
            $tab = $stmt->fetch();
            
            $this->set_id($tab['id_cat']);
            $this->set_nom($tab['nom_cat']);
            $this->set_id_rub($tab['id_rub']);
            $this->set_ordre($tab['ordre_cat']);          
        } catch (Exception $e) {
            return false;
        }
        return true;
    }
    
    public function get_all(){
        $query = "SELECT * FROM t_categories "
                . "ORDER BY ordre_cat";
        $tab = $this->pdo->query($query)->fetchAll(PDO::FETCH_ASSOC);
        //print_r($tab_crt);
        return $tab;
    }
    
    public function get_all_with_rubriques() {
        $query = "SELECT * FROM `t_categories` CAT "
                . "JOIN t_rubriques RUB ON CAT.id_rub = RUB.id_rub "
                . "ORDER BY num_rub";
        $tab = $this->pdo->query($query)->fetchAll(PDO::FETCH_ASSOC);
        //print_r($tab_crt);
        return $tab;
    }
    
    public function get_categories_with_criteres() {
        $query = "SELECT nom_crt, nom_cat, CRT.id_cat, code_crt FROM `t_categories` CAT "
                . "JOIN t_criteres CRT ON CAT.id_cat = CRT.id_cat "
                . "ORDER BY CRT.id_cat ASC, CRT.code_crt ASC";
        $tab = $this->pdo->query($query)->fetchAll(PDO::FETCH_ASSOC);
        //print_r($tab_crt);
        return $tab;
    }
    
    public function get_all_criteres($id_cat = 0){
        if(!$id_cat) {
            $id_cat = $this->get_id();
        }
        $query = "SELECT * FROM t_criteres CRT WHERE CRT.id_cat = ".$id_cat." ORDER BY code_crt ";
        $tab = $this->pdo->query($query)->fetchAll(PDO::FETCH_ASSOC);
        //print_r($tab_crt);
        return $tab;
    }
    
    public function __toString() {
        $str = "\n<pre>\n";
        foreach ($this AS $key => $val) {
            if ($key != "pdo") {
                $str .= "\t" . $key;
                $lengh_key = strlen($key);
                for ($i = strlen($key); $i < 20; $i++) {
                    $str .= "&nbsp;";
                }
                $str .= "=>&nbsp;&nbsp;&nbsp;" . $val . "\n";
            }
        }
        $str .= "\n</pre>";
        return $str;
    }
    
    function get_id() {
        return $this->id;
    }

    function get_nom() {
        return $this->nom;
    }

    function get_id_rub() {
        return $this->id_rub;
    }

    function get_ordre() {
        return $this->ordre;
    }

    function set_id($id) {
        $this->id = $id;
    }

    function set_nom($nom) {
        $this->nom = $nom;
    }

    function set_id_rub($id_rub) {
        $this->id_rub = $id_rub;
    }

    function set_ordre($ordre) {
        $this->ordre = $ordre;
    }
  
    
}

?>


<?php
Class Jeu EXTENDS Projet{
    
    private $nom;
    private $code;
    private $description;
    private $nb_min_joueurs;
    private $nb_max_joueurs;
    private $age_min;
    private $age_max;
    private $id_jeu;
    
    /**
     * constructeur 
     * @param int id [optionel]
     */
    function __construct($id = null) {
       // echo "Jeu Init";
        if($id){
            $this->set_id_jeu($id);
        }
        Projet::__construct($id);
    }
    
    /**
     * 
     * @return string Liste formatée du contenu de l'objet
     */
    public function __toString(){
        $str = "\n<pre>\n";
        foreach($this AS $key => $val){
            if($key != "pdo"){
                $str .= "\t".$key;
                $lengh_key = strlen($key);
                for($i=strlen($key);$i<20;$i++){
                    $str .= "&nbsp;";
                }
                $str .= "=>&nbsp;&nbsp;&nbsp;".$val."\n";
            }
        }
        $str .= "\n</pre>";
        return $str;
    }
    
    /**
     * Initialisation de l'objet (l'id doit être setté)
     * @return boolean
     */
    public function init() {
        $query = "SELECT * FROM t_jeux WHERE id_jeu=:id_jeu";
        try {
            $stmt = $this->pdo->prepare($query);
            $args[':id_jeu'] = $this->get_id_jeu();
            $stmt->execute($args);
            $tab = $stmt->fetch();
            //print_r($tab);
            $this->set_nom($tab['nom_jeu']);
            $this->set_code($tab['code_jeu']);
            $this->set_description($tab['description_jeu']);
            $this->set_nb_joueurs_min($tab['nb_joueurs_min_jeu']);
            $this->set_nb_joueurs_max($tab['nb_joueurs_max_jeu']);
            $this->set_age_min($tab['age_min_jeu']);
            $this->set_age_max($tab['age_max_jeu']);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    
    /**
     * Renvoie la liste de tous les jeux disponibles
     * @param string $order ordre de tri
     * @return tableau des jeux disponibles ou false
     */
    public function get_all($order = "nom_jeu"){
        
        $query = "SELECT * FROM t_jeux ORDER BY :order";
        try {
            $stmt = $this->pdo->prepare($query);
            $args[':order'] = $order;
            $stmt->execute($args);
            $tab = $stmt->fetchAll();
            //print_r($tab);
            return $tab;
        } catch (Exception $e) {
            return false;
        }
    }
    
    /**
     * Renvoie la liste de tous les parties ouvertes par ce jeux
     * @return tableau des parties ouvertes ou false
     */
    public function get_open_parties($id_jeu = NULL, $from_time = NULL){
       return parent::get_open_parties($id_jeu,$from_time);
        
    }
   
    public function get_nom() {
        return $this->nom;
    }

    public function get_node() {
        return $this->code;
    }

    public function get_description() {
        return $this->description;
    }

    public function get_nb_joueurs_min() {
        return $this->nb_min_joueurs;
    }
    
    public function get_nb_joueurs_max() {
        return $this->nb_max_joueurs;
    }

    public function get_age_min() {
        return $this->age_min;
    }

    public function get_age_max() {
        return $this->age_max;
    }

   
    public function set_nom($nom) {
        $this->nom = $nom;
    }

    public function set_code($code) {
        $this->code = $code;
    }

    public function set_description($description) {
        $this->description = $description;
    }

    public function set_nb_joueurs_min($nb_min_joueurs) {
        $this->nb_min_joueurs = $nb_min_joueurs;
    }
    
    public function set_nb_joueurs_max($nb_max_joueurs) {
        $this->nb_max_joueurs = $nb_max_joueurs;
    }

    public function set_age_min($age_min) {
        $this->age_min = $age_min;
    }

    public function set_age_max($age_max) {
        $this->age_max = $age_max;
    }

    public function set_id_jeu($id_jeu) {
        $this->id_jeu = $id_jeu;
    }
    
    public function get_id_jeu() {
        return $this->id_jeu;
    }
    
}
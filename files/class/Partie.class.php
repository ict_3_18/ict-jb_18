<?php
Class Partie EXTENDS Jeu{
    
    private $date_ouverture;
    private $debut;
    private $fin;
    private $id_prt;

    /**
     * constructeur 
     * @param int id [optionel]
     */
    function __construct($id = null) {
        parent::__construct();
        if($id){
            $this->set_id_prt($id);
            
            $this->init();
        }
    }
    
    /**
     * 
     * @return string Liste formatée du contenu de l'objet
     */
    public function __toString(){
        $str = "\n<pre>\n";
        foreach($this AS $key => $val){
            if($key != "pdo"){
                $str .= "\t".$key;
                $lengh_key = strlen($key);
                for($i=strlen($key);$i<20;$i++){
                    $str .= "&nbsp;";
                }
                $str .= "=>&nbsp;&nbsp;&nbsp;".$val."\n";
            }
        }
        $str .= "\n</pre>";
        $str .= parent::__toString();
        return $str;
    }
    
    /**
     * Initialisation de l'objet (l'id doit être setté)
     * @return boolean
     */
    public function init() {
        $query = "SELECT * FROM t_parties PRT JOIN t_jeux JEU ON PRT.id_jeu = JEU.id_jeu WHERE PRT.id_prt=:id_prt";
        try {
            $stmt = $this->pdo->prepare($query);
            $args[':id_prt'] = $this->get_id_prt();
            $stmt->execute($args);
            $tab = $stmt->fetch();
           // print_r($tab);
            $this->set_date_ouverture($tab['date_ouverture_prt']);
            $this->set_debut($tab['db_prt']);
            $this->set_fin($tab['fin_prt']);
            $this->set_fin($tab['id_prt']);
            $this->set_id_jeu($tab['id_jeu']);
            parent::init();
        } catch (Exception $e) {
            return false;
        }
        return true;
    }
    
    public function get_nb_joueurs(){
        $query = "SELECT COUNT(id_jou) AS nb_jou, UNIX_TIMESTAMP(MAX(time_last_action_jou)) AS last_modif FROM t_joueurs JOU WHERE JOU.id_prt=:id_prt AND status_jou=1";
        try {
            $stmt = $this->pdo->prepare($query);
            $args[':id_prt'] = $this->get_id_prt();
            $stmt->execute($args);
            $tab = $stmt->fetch();
        } catch (Exception $e) {
            return false;
        }
        return $tab;
    }
    
    /**
     * Renvoie la liste de tous les jeux disponibles
     * @param string $order ordre de tri
     * @return tableau des jeux disponibles ou false
     */
    public function get_all($order = "date_ouverture_prt"){
        
        $query = "SELECT * FROM t_parties PRT JOIN t_jeux JEU ON PRT.id_jeu=JEU.id_jeu ORDER BY :order";
        try {
            $stmt = $this->pdo->prepare($query);
            $args[':order'] = $order;
            $stmt->execute($args);
            $tab = $stmt->fetchAll();
            //print_r($tab);
            return $tab;
        } catch (Exception $e) {
            return false;
        }
    }
    
    
    /**
     * Renvoie la liste de tous les jeux disponibles
     * @param string $order ordre de tri
     * @return tableau des jeux disponibles ou false
     */
    public function get_joueurs(){
        
        $query = "SELECT * FROM t_joueurs WHERE id_prt=:id_prt";
        try {
            $stmt = $this->pdo->prepare($query);
            $args[':id_prt'] = $this->get_id_prt();
            $stmt->execute($args);
            $tab = $stmt->fetchAll();
            //print_r($tab);
            return $tab;
        } catch (Exception $e) {
            return false;
        }
    }
    
    public function get_date_ouverture() {
        return $this->date_ouverture;
    }
    
    public function get_debut() {
        return $this->debut;
    }

    public function get_fin() {
        return $this->fin;
    }

    public function set_date_ouverture($date_ouverture) {
        $this->date_ouverture = $date_ouverture;
    }

    public function set_debut($debut) {
        $this->debut = $debut;
    }
    
    public function set_id_prt($id_prt) {
        $this->id_prt = $id_prt;
    }
    
    public function get_id_prt() {
        return $this->id_prt;
    }

    public function set_fin($fin) {
        $this->fin = $fin;
    }

}
<?php
Class PDFMain_exa extends Mypdf{

    public function gen_recap($id_exa)
    {
        $this->SetTextColor(0, 0, 0);
        $exa = new Examen($id_exa);
        $tab_exa = $exa->list_exam();
        $tab_can = $exa->get_grp_cla_can();
        $exa->get_date_hrs();

        $tab_day = array(
            "Dimanche",
            "Lundi",
            "Mardi",
            "Mercredi",
            "Jeudi",
            "Vendredi",
            "Samedi"
        );

        $tab_month['01'] = "Janvier";
        $tab_month['02'] = "Février";
        $tab_month['03'] = "Mars";
        $tab_month['04'] = "Avril";
        $tab_month['05'] = "Mai";
        $tab_month['06'] = "Juin";
        $tab_month['07'] = "Juillet";
        $tab_month['08'] = "Août";
        $tab_month['09'] = "Septembre";
        $tab_month['10'] = "Octobre";
        $tab_month['11'] = "Novembre";
        $tab_month['12'] = "Décembre";

        $this->SetAutoPageBreak(1, 10);
        $this->AddPage();

        $count_candidats = count($tab_can);

        foreach ($tab_can AS $candidat) {
            $tab_grp[] = $candidat['nom_grp'];
        }

        $tab_grp_unique = array_unique($tab_grp);
        $nbMax = 0;

        foreach ($tab_grp_unique AS $groupe) {
            foreach ($tab_can AS $value) {
                if ($value['nom_grp'] == $groupe) {
                    $tab_value[$groupe]['nom'][] = $value['nom_grp'];
                    $tab_value[$groupe]['color'][] = $value['color_grp'];
                    if (count($tab_value[$groupe]['nom']) > $nbMax) {
                        $nbMax = count($tab_value[$groupe]['nom']);
                        $color = $tab_value[$groupe]['color'][0];
                    }
                }
            }
        }
        if(strlen($exa->get_ann()) > 2) {
            $version = substr($exa->get_ann(),-2);
        }
        else {
            $version = $exa->get_ann();
        }

        $tab_couleur_classe = $this->convert_hexa_to_RGB($color);
        $this->SetFillColor($tab_couleur_classe['red'], $tab_couleur_classe['green'], $tab_couleur_classe['blue']);
        $this->SetFont('Arial', 'B', 35);
        $this->Cell(190, 18, utf8_decode("ICT " . $exa->get_num_nom())."-".$version.strtoupper($exa->get_ver()), 0, 1, "C", true);

        $this->SetFont('Arial', '', 23);
        $this->SetXY(10, 35);
        $this->MultiCell(180, 12, utf8_decode("" . $exa->get_nom_mod($exa->get_id()) . ""), 0, "C");

        $month = strftime("%m", strtotime($exa->get_date_hrs()));
        $dayOfWeek = strftime("%w", strtotime($exa->get_date_hrs()));
        $newDate = strftime("%d", strtotime($exa->get_date_hrs()));
        $year = strftime("%G", strtotime($exa->get_date_hrs()));

        $this->SetFont('Arial', 'B', 23);
        $this->Cell(180, 20, utf8_decode($tab_day[$dayOfWeek] . ", " . $newDate . " " . $tab_month[$month] . " " . $year), 0, 1, "C");

        foreach ($tab_exa as $examen) {
            if ($examen['id_exa'] == $exa->get_id()) {
                $heure = explode(" ", $examen['date_heure_exa'])[1];
                if ($examen['duree_exa'] != null) {
                    $duree = $examen['duree_exa'];
                    $heure_debut = $exa->hrs_min_sec_to_min($heure);
                    $calc = $heure_debut + $duree;
                    $heure_fin = $exa->min_to_hrs_min_sec($calc);
                } else {
                    $heure_fin = "??:??:??";
                }
                $experts = null;
                $tab_expert = $exa->get_expert_exam($examen['id_exa']);

                if ($tab_expert !== null) {
                    foreach ($tab_expert as $key => $expert) {
                        if ($key != (count($tab_expert) - 1))
                            $experts .= substr($expert['prenom_per'], 0, 1) . ". " . $expert['nom_per'] . ", ";
                        else
                            $experts .= substr($expert['prenom_per'], 0, 1) . ". " . $expert['nom_per'];
                    }
                }

                if($experts == null)
                    $experts = "Aucun expert";

                $this->Cell(180, 0, utf8_decode(substr($heure, 0, 5) . " - " . substr($heure_fin, 0, 5)), 0, 1, "C");

                $this->SetFont('Arial', '', 23);
                $this->Cell(180, 50, utf8_decode($count_candidats . " candidats"), 0, 1, "C");

                $this->SetFont('Arial', '', 23);
                $this->Cell(180, -25, utf8_decode($experts), 0, 1, "C");
            }
        }
        $this->Line(10, 130, 200, 130);

        $this->AddPage();

        $tab_title = array(
            "2 séries d'examen vierges",
            "Récapitulatif des notes",
            "Travaux des candidats",
            "Rapports d'examen"
        );

        $tab_little_title = array(
            "Travaux candidats",
            "Documents"
        );

        $tab_code = array(1, 3, 4, 5, 6, 7);

        $this->Cell(10, 20, "", 0, 1);

        $this->SetFillColor($tab_couleur_classe['red'], $tab_couleur_classe['green'], $tab_couleur_classe['blue']);

        foreach ($tab_title AS $key => $value) {
            $this->SetXY(10, 45 * ($key) + 10);
            $this->SetFont('Arial', 'B', 16);
            $this->Cell(190, 10, utf8_decode("ICT " . $exa->get_num_nom())."-".$version.strtoupper($exa->get_ver()), 0, 1, "C", true);

            $this->SetFont('Arial', 'B', 16);
            $this->Cell(190, 15, utf8_decode($value), 0, 1, "C");

            $this->SetFont('Arial', "", 10);
            $this->Cell(190, 10, utf8_decode($tab_day[$dayOfWeek] . ", " . $newDate . " " . $tab_month[$month] . " " . $year) . " " . utf8_decode(substr($heure, 0, 5) . " - " . substr($heure_fin, 0, 5)), 0, 1, "L");
            $this->Cell(190, 0, utf8_decode($experts) . " / " . utf8_decode($count_candidats . " candidats"), 0, 1, "L");

            $this->Line(10, 45 * ($key) + 51, 200, 45 * ($key) + 51);

            $code = "9990" . $exa->get_id() . "01" . "00" . $tab_code[$key];

            require_once 'php-barcode-generator/BarcodeGeneratorPNG.php';
            $generatorPNG = new Barcode\BarcodeGeneratorPNG();

            $fp = fopen('temp/data' . $key . '.png', 'w');
            fwrite($fp, $generatorPNG->getBarcode($code, $generatorPNG::TYPE_EAN_13) . "");
            fclose($fp);

            $this->Image('temp/data' . $key . '.png', 165, 45 * ($key) + 25, 35, 12, 'PNG');
            /*$this->SetFont('Arial', '', 8);
            $this->SetXY(165, 45 * ($key) + 40);
            $this->Cell(35, 0, $code, 0, 0, 'C');*/
        }

        foreach($tab_little_title AS $key => $value) {
            $this->SetXY(10+($key*95), 190);
            $this->SetFont('Arial', 'B', 16);
            $this->Cell(95, 10, utf8_decode("ICT " . $exa->get_num_nom())."-".$version.strtoupper($exa->get_ver()), 0, 1, "C", true);

            $this->SetXY(10+($key*95), 200);
            $this->SetFont('Arial', 'B', 14);
            $this->Cell(95, 8, utf8_decode($value), 0, 1, "L");

            $this->SetXY(10+($key*95), 208);
            $this->SetFont('Arial', '', 12);
            $this->MultiCell(60, 4, utf8_decode($experts), 0, 1);

            $this->SetXY(50, 200);

            $code = "9990" . $exa->get_id() . "01" . "00" . $tab_code[$key+4];

            require_once 'php-barcode-generator/BarcodeGeneratorPNG.php';
            $generatorPNG = new Barcode\BarcodeGeneratorPNG();

            $fp = fopen('temp/data' . ($key+4) . '.png', 'w');
            fwrite($fp, $generatorPNG->getBarcode($code, $generatorPNG::TYPE_EAN_13) . "");
            fclose($fp);

            $this->Image('temp/data' . ($key+4) . '.png', 66+($key*96), 202, 35, 12, 'PNG');
            /*$this->SetFont('Arial', '', 8);
            $this->SetXY(66+($key*96), 217);
            $this->Cell(35, 0, $code, 0, 0, 'C');*/
        }

        $this->Line(10,190,105,190);
        $this->Line(10,190,10,216);
        $this->Line(10,216,60,216);
        $this->Line(105,200,105,216);
    }
}

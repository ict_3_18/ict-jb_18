<?php
class PdfBulletin extends Mypdf {

    function header_bulletin($can){
        $this->SetFont('Arial','B',15);				// Dfinit la police
        $this->Image(WAY. "/images/pdf/ours.png",10,10);
        $this->SetXY(30,11);

        $classe = new Classe($can->get_classe());
        $groupe = new Groupe($classe->get_ref_grp());

        $tab_infos_met = $groupe->get_all_infos_met_by_id($groupe->get_ref_met());

        if($can->get_sexe() == "M"){
            $this->Cell(0,7,utf8_decode("Formation d'" . $tab_infos_met['nom_m_met']),0,1,'');	// Contenu
        }else{
            $this->Cell(0,7,utf8_decode("Formation d'" . $tab_infos_met['nom_f_met']),0,1,'');	// Contenu
        }

        $this->SetX(30);

        if($can->get_termine_classe()){
            if($can->get_stop_form_classe()){
                $etat = utf8_decode("suite à l'interruption de la formation");
            }else{
                $etat = "final";
            }
        }else{
            $etat = 'provisoire - Etat au '.date("d.m.Y");
        }

        $this->Cell(0,7,'Bulletin de notes '.$etat.'',0,1,'');	// Contenu
        $this->SetX(30);
        $this->SetFont('Arial','',10);				// Dfinit la police

        $this->Cell(0,6,utf8_decode($tab_infos_met['reglement_met']),0,0,'');	// Contenu

        $this->Ln();
    }


    function gen_bulletin($can)
    {

        //$this->can = new candidat($this->sql,$id_can);

        $this->AddPage();
        $this->header_bulletin($can);


        //$this->Ln(0);
        $this->SetXY(10, 35);
        $this->SetFont('Arial', 'B', 12);
        if ($can->get_sexe() == "F") {
            $this->cell(0, 5, utf8_decode("Candidate : " . $can->get_nom() . " " . $can->get_prenom() . " (" . substr($can->get_no_candidat(),1,3) . ")"), 0, 1);
        }else{
            $this->cell(0, 5, utf8_decode("Candidat : " . $can->get_nom() . " " . $can->get_prenom() . " (" . substr($can->get_no_candidat(),1,3) . ")"), 0, 1);
        }
        $this->SetFont('Arial','B',10);



        // $this->cell(0,5,utf8_decode(,0,1);
        $this->Ln(5);
        $this->SetAutoPageBreak(0,1);
        //$nb_exa = sizeof($this->can->assign_exa);

        $note_exa = new Notes($can->get_id());

        $domaines = $note_exa->get_domaines();
        //print_r($domaines);

        foreach($domaines as $domaine){
            //echo $domaine.sizeof($note_domaine[$domaine]);
            $nb_note=0;
            $notes = $note_exa->get_notes_valides_to_dom($domaine['id_dom']);
            //print_r($notes);
            if(sizeof($notes)) {
                $this->SetFont('Arial', 'B', 10);
                $str = "";
                if ($domaine == "TPI") {
                    $str = "Domaine de qualification \"Travail final\"";
                } elseif ($domaine == "CIE") {
                    $str .= "Cours interentreprises";
                } elseif ($domaine == "Formation scolaire") {
                    $str .= "Formation scolaire";
                } elseif ($domaine == "Compétences élargies") {
                    //$str .= utf8_decode("Compétences élargies");
                }
                if ($str != '') {
                    $this->cell(0, 8, $str, 0, 1);
                }
                $somme_note = 0;
                if ($domaine['nom_dom'] == "Compétences élargies") {
                    /*

                    $str = "Semestres";
                    foreach ($notes as $exa) {
                        //echo $exa['id_exa'].$domaine;
                        $note = $exa['note_can'] ;

                       // if (($note != "") && ($note != 0) && ($this->can->exa_remedie($exa['id_exa']) != 1)) {
                            $this->SetFont('Arial', '', 10);
                            $this->SetX(20);

                            $str .= "   " . sprintf("%.01f", $note) . "";
                            //$this->cell(0,5,,0,1);
                            $somme_note += $note;
                            $nb_note++;
                       // }
                    }
                     $this->cell(160,4,utf8_decode($str),0,1);

                    $str = "Semestres";
                    foreach ($notes as $exa) {
                        //echo $exa['id_exa'].$domaine;
                        $note = $exa['note_can'] ;

                       // if(($note != "") && ($note != 0) && ($this->can->exa_remedie($exa['id_exa']) != 1)){
                            $this->SetFont('Arial','',10);
                            $this->SetX(20);

                            $str .= "   ".sprintf("%.01f",$note)."";
                            //$this->cell(0,5,,0,1);
                            $somme_note += $note;
                            $nb_note++;
                        //}
                    }
                    // $this->cell(160,4,utf8_decode($str),0,1);
               */
                }else{
                    foreach ($notes as $exa) {
                        //echo $exa['id_exa'].$domaine;
                        if(!$exa['ltsi_exa']){ // Examen uniquement LTSI
                            $note = $exa['note_can'] ;

                            //if(($note != "") && ($note != 0) && ($this->can->exa_remedie($exa['id_exa']) != 1)){
                            $this->SetFont('Arial','',10);
                            $this->SetX(20);
                            if($exa['num_mod'] < 100){
                                $this->cell(160,5,utf8_decode($exa['nom_mod']),0,0);
                            }else{
                                if($exa['sans_note_exa']) {
                                    $this->cell(155,5,utf8_decode("ICT ".$exa['num_mod']." ".$exa['nom_mod']),0,0);
                                }else{
                                    $this->cell(160,5,utf8_decode("ICT ".$exa['num_mod']." ".$exa['nom_mod']),0,0);
                                }

                            }
                            if($exa['sans_note_exa']) {
                                $this->cell(0, 4, "Cours suivi", 0, 1);
                            }else{
                                $this->cell(0, 4, sprintf("%.01f", $note), 0, 1);
                                $somme_note += $note;
                                $nb_note++;
                            }
                            // }
                        }
                    }
                }
            }

            $this->SetFont('Arial','B',10);
            //$this->Ln();


            if($domaine['nom_dom'] == "TPI"){
                $article = utf8_decode("Domaine de qualification \"Travail final\"");
            }
            if($domaine['nom_dom'] == "CIE"){
                $article = utf8_decode("Moyenne Cours Interentreprises");
            }
            if($domaine['nom_dom'] == "Formation scolaire"){
                $article = utf8_decode("Moyenne formation scolaire");
            }
            if($domaine['nom_dom'] == "Compétences élargies"){
                $article = utf8_decode("Compétences élargies");
            }

            if($nb_note){
                if($domaine['nom_dom'] == "TPI"){
                    $this->SetX(10);
                    $this->cell(100,8," ".$article." ","B",0);
                }else {
                    $this->cell(100,8," ".$article." ","B",0);
                }

                if($domaine['nom_dom'] == "Formation scolaire" || $domaine['nom_dom'] == "CIE" ){
                    if($somme_note/$nb_note < 4 && !$can->get_termine_classe()){
                        $this->cell(70,8,"( Moyenne non arrondie : ".sprintf("%.02f",$somme_note/$nb_note)." )","B",0, "R");
                    }else{
                        $this->cell(70,8,"","B",0);
                    }
                    $this->cell(0,8,sprintf("%.01f",round(($somme_note/$nb_note)/5,1)*5),"B",1);
                    $tab_moyennes[$domaine['nom_dom']] = round(($somme_note/$nb_note)/5,1)*5;
                    $this->cell(0,3,"",0,1);
                }else{
                    $this->cell(70,8,"","B",0);
                    $this->cell(0,8,sprintf("%.01f",round($somme_note/$nb_note,1)),"B",1);
                    $tab_moyennes[$domaine['nom_dom']] = round($somme_note/$nb_note,1);
                    $this->cell(0,3,"",0,1);
                    //$this->Ln(0.5);
                }
            }
        }

        //}

        $nb_note=0;

        if(!$can->get_termine_classe()) {
            if(isset($tab_moyennes["Formation scolaire"]) &&  isset($tab_moyennes["CIE"])){
                $this->SetFont('Arial','B',10);
                $this->cell(190,8,utf8_decode("Composantes de la note globale"),"LTR",1);
                $this->cell(10,5,"","L");
                $this->cell(180,5,utf8_decode("Compétences en informatique"),"R",1);
                $this->SetFont('Arial','',10);
                $this->cell(20,5,"","L");
                $this->cell(130,5,utf8_decode("Note d'expérience des compétences en informatique (80%)\" "),0,0);
                $this->cell(0,5,sprintf("%.01f",$tab_moyennes["Formation scolaire"]),"R",1);
                $this->cell(20,5,"","L");
                $this->cell(130,5,utf8_decode("Note d'expérience des cours interentreprises (20%)\" "),0,0);
                $this->cell(0,5,sprintf("%.01f",$tab_moyennes["CIE"]),"R",1);
                $this->SetFont('Arial','B',10);
                $this->cell(10,5,"","L");
                $comp = ($tab_moyennes["Formation scolaire"]*4+$tab_moyennes["CIE"])/5;
                $this->cell(165,5,utf8_decode("Moyenne domaine de compétences \"Compétences informatiques\" "),0,0);
                $this->cell(0,5,sprintf("%.01f",round($comp,1)),"R",1);
                $comp = ($tab_moyennes["Formation scolaire"]*4+$tab_moyennes["CIE"])/5;

                if(isset($tab_moyennes['Compétences élargies'])){
                    $this->cell(10,10,"","L");
                    $this->cell(165,10,utf8_decode("Moyenne Domaine de compétences \"Compétences élargies\" "),0,0);
                    $this->cell(0,10,sprintf("%.01f",$tab_moyennes["Compétences élargies"]),"R",1);
                }
                if(isset($tab_moyennes['TPI'])) {
                    $this->cell(10, 10, "", "L");
                    $this->cell(165, 5, utf8_decode("Domaine de qualification \"Travail final\""), 0, 0);
                    $this->cell(0, 5, sprintf("%.01f", $tab_moyennes["TPI"], 1), "R", 1);
                    //$this->cell(0,5,round(,1),"R",1);
                }

                $this->cell(190,5,"","LBR");
            }
        }else{
            if (!$can->get_stop_form_classe()) {
                if(isset($tab_moyennes["Formation scolaire"]) &&  isset($tab_moyennes["CIE"])){
                    $this->SetFont('Arial','B',10);
                    $this->cell(190,8,utf8_decode("Composantes de la note globale"),"LTR",1);
                    $this->cell(10,5,"","L");
                    $this->cell(180,5,utf8_decode("Compétences en informatique"),"R",1);
                    $this->SetFont('Arial','',10);
                    $this->cell(20,5,"","L");
                    $this->cell(130,5,utf8_decode("Note d'expérience des compétences en informatique (80%)\" "),0,0);
                    $this->cell(0,5,sprintf("%.01f",$tab_moyennes["Formation scolaire"]),"R",1);
                    $this->cell(20,5,"","L");
                    $this->cell(130,5,utf8_decode("Note d'expérience des cours interentreprises (20%)\" "),0,0);
                    $this->cell(0,5,sprintf("%.01f",$tab_moyennes["CIE"]),"R",1);
                    $this->SetFont('Arial','B',10);
                    $this->cell(10,5,"","L");
                    $comp = ($tab_moyennes["Formation scolaire"]*4+$tab_moyennes["CIE"])/5;
                    $this->cell(165,5,utf8_decode("Moyenne domaine de compétences \"Compétences informatiques\" "),0,0);
                    $this->cell(0,5,sprintf("%.01f",round($comp,1)),"R",1);
                    $comp = ($tab_moyennes["Formation scolaire"]*4+$tab_moyennes["CIE"])/5;

                    if(isset($tab_moyennes['Compétences élargies'])){
                        $this->cell(10,10,"","L");
                        $this->cell(165,10,utf8_decode("Moyenne Domaine de compétences \"Compétences élargies\" "),0,0);
                        $this->cell(0,10,sprintf("%.01f",$tab_moyennes["Compétences élargies"]),"R",1);
                    }
                    if(isset($tab_moyennes['TPI'])) {
                        $this->cell(10, 10, "", "L");
                        $this->cell(165, 5, utf8_decode("Domaine de qualification \"Travail final\""), 0, 0);
                        $this->cell(0, 5, sprintf("%.01f", $tab_moyennes["TPI"], 1), "R", 1);
                        //$this->cell(0,5,round(,1),"R",1);
                    }

                    $this->cell(190,5,"","LBR");
                }
            }
        }




        if(!$can->get_termine_classe()){
            $this->SetFont('Arial','i',10);
            $this->ln(5);
            $this->cell(70,5,utf8_decode("Ce bulletin est remis à titre informatif, seul le bulletin final fait foi."),0,1);
            if($can->get_systeme() != "Dual"){
                $this->ln(2);
                $this->cell(70,5,utf8_decode("Signature du/de la représentant-e légal-e : "),0,0);
                $this->cell(80,5,"__________________________",0,1);
            }
        }else{

            $this->SetFont('Arial','i',10);
            $this->ln(5);
            $date = ucfirst(utf8_encode(strftime("%A %d %B %Y")));
            $this->cell(80,20,utf8_decode("Cormoret, le ".$date),0,0);
            $this->cell(40,20,utf8_decode("Expert en chef : J.Hirtzel"),0,0);
            $this->cell(50,20,utf8_decode("__________________________________"),0,1);
            //$this->cell(170,5,"",0,1);
        }
    }
}

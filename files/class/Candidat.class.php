<?php

Class Candidat extends Projet {

    private $id;
    private $no_candidat;
    private $sexe;
    private $nom;
    private $prenom;
    private $email;
    private $annee;
    private $systeme;
    private $employeur;
    private $classe;
    private $adresse;
    private $password;
    private $login;
    private $reglement;
	private $dys;
	private $moyens_supp;

    public function __construct($id_candidat = null) {
        parent::__construct();

        if ($id_candidat) {
            $this->set_id($id_candidat);
            $this->init();
        }
    }

    /**
     * @return bool
     */
    public function init() {
        $query = "SELECT * FROM t_candidats WHERE id_can =:id_can";
        try {
            $stmt = $this->pdo->prepare($query);
            $args['id_can'] = $this->get_id();
            $stmt->execute($args);
            $tab = $stmt->fetch();

            $this->set_no_candidat($tab['no_candidat_can']);
            $this->set_sexe($tab['sexe_can']);
            $this->set_nom($tab['nom_can']);
            $this->set_prenom($tab['prenom_can']);
            $this->set_email($tab['email_can']);
            $this->set_annee($tab['annee_can']);
            $this->set_systeme($tab['systeme_can']);
            $this->set_employeur($tab['ref_emp']);
            $this->set_classe($tab['ref_classe']);
            $this->set_adresse($tab['adresse_can']);
            $this->set_password($tab['password_can']);
            $this->set_login($tab['login_can']);
            $this->set_reglement($tab['reglement_can']);
			$this->set_dys($tab['dys_can']);
			$this->set_moyens_supp($tab['moyens_supp_can']);

            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * @param $email
     * @return bool
     */
    public function init_by_mail($email){
        $query = "SELECT * FROM t_candidats WHERE email_can =:email";
        try {
            $stmt = $this->pdo->prepare($query);
            $args['email'] = $email;
            $stmt->execute($args);
            $tab = $stmt->fetch();
            $this->set_id($tab['id_can']);
            $this->init();
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * @param $action
     * @return bool
     */
    public function log_log($action){
        $query = "INSERT into t_logs SET action_log=\"".$action."\", type_usr_log=\"CAN\", ref_usr_log=".$this->get_id().", ip_log=\"".$_SERVER['REMOTE_ADDR']."\"";
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            return true;
        } catch (Exception $ex) {
            return false;
        }

    }

    /**
     * Teste si le candidat est dyslexique
     * @param $id_can
     * @return bool
     */
    public function get_if_dys($id_can){
        $query = "SELECT dys_can FROM `t_candidats` WHERE id_can = ".$id_can;

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetch();
            if($tab['dys_can'] == 1){
                return true;
            }else{
                return false;
            }
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * @return array|bool
     */
    public function get_convoc($type = "all") {
        switch($type){
            case "all" :
                $condition = "";
                break;
            case "old" :
                $condition = " AND RCE.conf_convoc = 1 ";
                break;
            case "new" :
                $condition = " AND RCE.conf_convoc = 0 ";
                break;
        }
        //. "JOIN t_ref_examens_salles RES ON RES.ref_exa=EXA.id_exa "
        //. "JOIN t_salles SLE ON SLE.id_sle=RES.ref_sle "
        $query = "SELECT CAN.nom_can, CAN.prenom_can, CAN.no_candidat_can, DMO.num_mod, DMO.nom_mod, EXA.date_heure_exa, EXA.duree_exa, EXA.id_exa 
                  FROM t_examens EXA 
                  JOIN t_ref_can_exa RCE ON RCE.ref_exa=EXA.id_exa 
                  JOIN t_candidats CAN ON CAN.id_can=RCE.ref_can 
                  JOIN t_desc_modules DMO ON DMO.id_desc_mod=EXA.no_ich_exa 
                  WHERE CAN.id_can=:can AND EXA.convoc_exa=1 AND EXA.definitif_exa=1 AND EXA.date_heure_exa>=NOW() " . $condition . "
                  ORDER BY CAN.nom_can, CAN.prenom_can, EXA.date_heure_exa ";

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(array(":can" => $this->get_id()));
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * @param $id_exa
     * @return bool|mixed
     */
    public function get_sle_exa($id_exa){
        $query = "SELECT * FROM t_examens EXA 
                  JOIN t_plans PLS ON EXA.ref_pls=PLS.id_pls 
                  JOIN `t_places` PLC ON PLS.id_pls=PLC.ref_pls 
                  JOIN t_table TAB ON ref_tab=TAB.id_tab 
                  JOIN t_salles SLE ON TAB.ref_sle=SLE.id_sle 
                  WHERE `id_can` =:id_can AND EXA.id_exa=:id_exa";
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(array(":id_can" => $this->get_id(),":id_exa" => $id_exa));
            $tab = $stmt->fetch();
            return $tab;
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * @param $tab
     * @return bool|string
     */
    public function add_can($tab) {
        $query = "INSERT INTO t_candidats SET 
                  no_candidat_can = :no_candidat_can, 
                  sexe_can = :sexe_can, 
                  nom_can = :nom_can, 
                  prenom_can = :prenom_can, 
                  email_can = :email_can, 
                  annee_can = :annee_can, 
                  systeme_can = :systeme_can, 
                  ref_emp = :employeur_can, 
                  ref_classe = :classe_can,
                  dys_can = :dys_can,
                  login_can = :login_can ";

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($tab);

            return $this->pdo->lastInsertId();
        } catch (Exception $exc) {
            return false;
        }
    }

    /**
     * sort un tableau de tous les candidats
     */
    public function get_all($order = "nom_can, prenom_can") {

        $query = "SELECT * FROM t_candidats ORDER BY " . $order;

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * @return bool
     */
    public function get_all_notes_by_dom(){
        $query = "SELECT * FROM t_ref_can_exa CAX 
                  JOIN t_examens EXA ON CAX.ref_exa = EXA.id_exa 
                  WHERE CAX.ref_can = ".$this->get_id()." 
                  AND CAX.remedie_note = 0 AND CAX.note_can IS NOT NULL";
        try {
            $stmt = $this->pdo->prepare($query);
            if ($stmt->execute()) {
                $tab = $stmt->fetchAll();

                foreach($tab AS $note){
                    $tab_dom[$note['id_dom']][$note['no_ich_exa']] = $note['note_can'];
                    //  echo "<pre>";
                    //print_r($note);
                    // echo "</pre>";
                }
                return $tab_dom;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @return array|bool
     */
    public function get_tab_can_by_grp_no_archive_no_termine() {
        $query = "SELECT t_candidats.* FROM t_groupes LEFT JOIN 
                  t_classes ON t_classes.ref_grp = t_groupes.id_grp LEFT JOIN 
                  t_candidats ON t_classes.id_cla = t_candidats.ref_classe 
                  WHERE t_classes.termine_cla=0 AND t_groupes.archive_grp=0 AND t_classes.nom_cla <> ''";
        try {
            $stmt = $this->pdo->prepare($query);
            if ($stmt->execute()) {
                $tab = $stmt->fetchAll();
                return $tab;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * sort un tableau de tous les candidats avec les d�tails de leur classe et employeur
     */
    public function get_all_with_detail() {

        $query = "SELECT * FROM t_candidats CAN
                  JOIN t_classes CLA ON CAN.ref_classe = CLA.id_cla
                  JOIN t_employeurs EMP ON CAN.ref_emp = EMP.id_emp";

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * Redonne un tableau de touts les groupes apartenant au candidats
     */
    public function get_tab_grp_all_can() {

        $query = "SELECT t_candidats.id_can , t_groupes.id_grp, t_groupes.nom_grp FROM t_candidats 
                  LEFT JOIN t_classes ON t_classes.id_cla = t_candidats.ref_classe 
                  LEFT JOIN t_groupes ON t_groupes.id_grp = t_classes.ref_grp";

        try {
            $stmt = $this->pdo->prepare($query);
            if ($stmt->execute()) {
                $tab = $stmt->fetchAll();
                $tab_can_grp = Array();
                foreach ($tab as $row) {
                    $tab_grp = array();
                    array_push($tab_grp, $row['id_grp'], $row["nom_grp"]);
                    $tab_can_grp[$row['id_can']][] = $tab_grp;
                }
                return $tab_can_grp;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @return string
     */
    public function __toString() {
        $str = "\n<pre>\n";
        foreach ($this AS $key => $val) {
            if ($key != "pdo") {
                $str .= "\t" . $key;
                $lengh_key = strlen($key);
                for ($i = strlen($key); $i < 20; $i++) {
                    $str .= "&nbsp;";
                }
                $str .= "=>&nbsp;&nbsp;&nbsp;" . $val . "\n";
            }
        }
        $str .= "\n</pre>";
        return $str;
    }

    /**
     * Redonne un tableau de toutes les classes apartenant au candidats
     */
    public function get_tab_cla_all_can() {

        $query = "SELECT t_candidats.id_can , t_classes.id_cla, t_classes.nom_cla FROM t_candidats 
                  LEFT JOIN t_classes ON t_classes.id_cla = t_candidats.ref_classe";

        try {
            $stmt = $this->pdo->prepare($query);
            if ($stmt->execute()) {
                $tab = $stmt->fetchAll();
                $tab_can_cla = Array();
                foreach ($tab as $row) {
                    $tab_cla = array();
                    array_push($tab_cla, $row['id_cla'], $row["nom_cla"]);
                    $tab_can_cla[$row['id_can']][] = $tab_cla;
                }
                return $tab_can_cla;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Redonne une liste de tous les candidats pr�sents dans un examen
     * @return array
     */
    public function get_tab_all_can_in_one_exa($ref_exa) {

        $query = "SELECT CAN.id_can, CAN.no_candidat_can, CAN.nom_can, CAN.prenom_can, REF.note_can FROM t_examens AS EXA 
                  JOIN t_ref_can_exa AS REF ON EXA.id_exa = REF.ref_exa 
                  JOIN t_candidats AS CAN ON REF.ref_can = CAN.id_can 
                  WHERE EXA.id_exa = :ref_exa ORDER BY `CAN`.`no_candidat_can` ASC";

        try {
            $stmt = $this->pdo->prepare($query);
            $args[':ref_exa'] = $ref_exa;
            $stmt->execute($args);
            return $stmt->fetchAll();
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * récupération le nom, prenom d'un candidat sélectionnée avec un id
     */
    public function get_nom_prenom_can_with_id($id) {

        $query = "SELECT nom_can, prenom_can FROM t_candidats WHERE id_can = :id";

        $args[':id'] = $id;

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $tab = $stmt->fetch();
            return $tab['nom_can'] . " " . $tab['prenom_can'];
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * récupération le numero du candidat sélectionnée avec un id
     */
    public function get_no_can_with_id($id) {

        $query = "SELECT no_candidat_can FROM t_candidats WHERE id_can = :id";

        $args[':id'] = $id;

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $tab = $stmt->fetch();
            return $tab['no_candidat_can'];
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * supprime des candidat associÃ© Ã  un examen si ils n'ont pas de note 
     */
    public function remove_cans_exa_if_not_note($tab_ref_can, $ref_exa) {
        foreach ($tab_ref_can as $ref_can) {//parcours tout les candidats et les supprime 
            if (!$this->has_note_exa($ref_exa, $ref_can)) {
                //Supprime le candidat de l'examen si la note est null
                $query = "DELETE  FROM t_ref_can_exa WHERE ref_can = :ref_can AND ref_exa = :ref_exa";
                $args[':ref_exa'] = $ref_exa;
                $args[':ref_can'] = $ref_can;
                try {
                    $stmt = $this->pdo->prepare($query);
                    if (!$stmt->execute($args)) {
                        //Si il y a un problème lors de l'execution
                        return false;
                    }
                } catch (Exception $ex) {
                    return false;
                }
            }
        }
        return true; //retourne true si il n'y a pas eu d'erreur lors de la suppression d'un candiat 
    }

    /**
     * supprime le candidat associÃ© Ã  un examen si ils n'ont pas de note
     */
    public function remove_can_exa_if_not_note($ref_can, $ref_exa) {
        if (!$this->has_note_exa($ref_exa, $ref_can)) {
            //Supprime le candidat de l'examen si la note est null
            $query = "DELETE  FROM t_ref_can_exa WHERE ref_can = :ref_can AND ref_exa = :ref_exa";
            $args[':ref_exa'] = $ref_exa;
            $args[':ref_can'] = $ref_can;
            try {
                $stmt = $this->pdo->prepare($query);
                if (!$stmt->execute($args)) {
                    //Si il y a un problème lors de l'execution
                    return false;
                }
            } catch (Exception $ex) {
                return false;
            }

        }
        return true; //retourne true si il n'y a pas eu d'erreur lors de la suppression d'un candiat
    }

    /**
     * retourne true si le candidat a une note sur l'examen et que elle n'est pas null
     * Si on ne donne pas ref_can en argument il va prendre l'id de la classe
     */
    public function has_note_exa($ref_exa, $ref_can = null) {
        //Redonne la note du candidat Ã  l'examen si elle n'est pas null
        $query = "SELECT note_can FROM t_ref_can_exa WHERE (ref_can = :ref_can AND ref_exa = :ref_exa) AND (note_can != '')";
        $args[':ref_exa'] = $ref_exa;
        if ($ref_can) { //Si on ne donne pas ref_can en argument il va prendre l'id de la classe 
            $args[':ref_can'] = $ref_can;
        } else {
            $args[':ref_can'] = $this->get_id();
        }
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            if (!empty($stmt->fetchAll())) { // Si on a recu une note
                return true;
            }
        } catch (Exception $ex) {
            return false;
        }
    }

    public function get_conf_convoc_in_ref_can_exa($id,$id_exa){
        $query = "SELECT * FROM t_ref_can_exa WHERE ref_can = :id AND ref_exa = :id_exa";

        $args[':id'] = $id;
        $args[':id_exa'] = $id_exa;

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $tab = $stmt->fetch();
            return $tab;
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * permet de chercher pour une liste de candidats selon nom, prenom ou num�ro
     */
    public function get_search($term) {

        $args['term'] = $term;
        //numb permet de prendre les candidats 1*** avec juste ***
        $args['numb'] = "%" . $term;

        $query = "SELECT id_can AS id, CONCAT(prenom_can,' ',nom_can,' (',no_candidat_can,')') 
                  AS value FROM t_candidats
                  WHERE lower(nom_can) 
                  LIKE lower(:term) OR lower(prenom_can) 
                  LIKE lower(:term) OR no_candidat_can 
                  LIKE :numb 
                  ORDER by no_candidat_can,nom_can,prenom_can";

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $tab = $stmt->fetchall(PDO::FETCH_ASSOC);
            return $tab;
        } catch (Exception $exc) {
            return false;
        }
    }

    /**
     * Modifie le candidat
     * @param $tab
     * @return bool
     */
    public function update($tab) {

        $query = "UPDATE t_candidats SET 
                  no_candidat_can = :no_candidat_can, sexe_can = :sexe_can, 
                  nom_can = :nom_can, prenom_can = :prenom_can, 
                  email_can = :email_can, annee_can = :annee_can, 
                  systeme_can = :systeme_can, ref_emp = :employeur_can, 
                  ref_classe = :classe_can, dys_can = :dys_can, login_can = :login_can 
                  WHERE t_candidats.id_can = :id_can";

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($tab);
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * Sort la couleur du groupe du candidat
     * @param $id
     * @return mixed
     */
    function get_color_from_id($id) {

        $query = "SELECT t_groupes.color_grp FROM t_candidats 
                  JOIN t_classes ON t_classes.id_cla = t_candidats.ref_classe 
                  JOIN t_groupes ON t_groupes.id_grp = t_classes.ref_grp 
                  WHERE t_candidats.id_can = :id";

        try {
            $tab['id'] = $id;
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($tab);
            $color = $stmt->fetchall(PDO::FETCH_ASSOC);
            return $color[0]['color_grp'];
        } catch (Exception $ex) {

        }
    }

    /**
     * Sort la couleur du groupe du candidat moins le #
     * @param $id
     * @return bool|string
     */
    function get_color_grp_from_id($id) {

        $query = "SELECT t_groupes.color_grp FROM t_candidats 
                  JOIN t_classes ON t_classes.id_cla = t_candidats.ref_classe 
                  JOIN t_groupes ON t_groupes.id_grp = t_classes.ref_grp 
                  WHERE t_candidats.id_can = :id";

        try {
            $tab['id'] = $id;
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($tab);
            $color = $stmt->fetchall();
            return substr($color[0]['color_grp'], 1, 3);
        } catch (Exception $ex) {

        }
    }

    /**
     * @param $email
     * @return bool
     */
    public function check_email($email) {
        $query = "SELECT email_can FROM t_candidats WHERE email_can = :email LIMIT 1";
        try {
            $stmt = $this->pdo->prepare($query);
            $args[':email'] = strtolower($email);
            $stmt->execute($args);
            $tab = $stmt->fetch();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param $email
     * @param $password
     * @return bool
     */
    public function check_login($email, $password) {
        $query = "SELECT id_can,password_can FROM t_candidats WHERE email_can=:email LIMIT 1";
        try {
            $stmt = $this->pdo->prepare($query);
            $args[':email'] = strtolower($email);
            $stmt->execute($args);
            $tab = $stmt->fetch();
            if (password_verify($password, $tab['password_can'])) {//Compare le mot de passe avec celui dans la base crypté
                $_SESSION['id'] = $tab['id_can']; //Mets l'id de la personne en session 
                $user_browser_ip = $_SERVER['HTTP_USER_AGENT'] . $_SERVER['REMOTE_ADDR'];
                $_SESSION['login_string'] = password_hash($tab['password_can'] . $user_browser_ip, PASSWORD_DEFAULT);
                $_SESSION['email'] = strtolower($email);
                //print_r($_SESSION);
                //Met l'email, id en session avec l'ip du browser + mot de passe crypté
                $this->init();
                if($this->get_termine_classe()){
                    return false;
                }
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @return bool
     */
    public function check_connect() {
        if (isset($_SESSION['id'], $_SESSION['email'], $_SESSION['login_string'])) {//Si toutes les infos sont en session 
            $user_browser_ip = $_SERVER['HTTP_USER_AGENT'] . $_SERVER['REMOTE_ADDR']; //Prends l'ip du browser 
            if (password_verify($this->get_password() . $user_browser_ip, $_SESSION['login_string']) && !$this->get_termine_classe()) {
                //Si la login_string cryptée en session correspond au mot de passe de la personne actuelle et à l'ip du browser 
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Permet de set la convocation des examens à convoquer à 1 par rapport à l'id du candidat
     * @Author Remi Lopes
     * @Date 22.10.2019
     * @param $id
     * @return bool
     */
    public function set_conf_convoc_can_by_id_can($id){
        $query = "UPDATE t_ref_can_exa RCE1 
                    SET RCE1.conf_convoc=1 
                    WHERE RCE1.ref_exa IN 
                    (SELECT ref_exa FROM 
                    (SELECT ref_exa 
                    FROM `t_ref_can_exa`) AS RCE2 
                    WHERE (ref_exa) IN 
                    (SELECT id_exa 
                    FROM t_examens AS EXA 
                    WHERE ref_can=:id AND EXA.convoc_exa=1))";

        $args[':id'] = $id;

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $tab = $stmt->fetch();
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * Permet de set la convocation des examens à convoquer et à venir à 0 par rapport à l'id du candidat
     * @Author Remi Lopes
     * @Date 23.10.2019
     * @param $id
     * @return bool
     */

    public function set_conf_convoc_can_by_id_can_reset($id){
        $query = "UPDATE t_ref_can_exa RCE1 
                    SET RCE1.conf_convoc=0 
                    WHERE RCE1.ref_exa IN 
                    (SELECT ref_exa FROM 
                    (SELECT ref_exa 
                    FROM `t_ref_can_exa`) AS RCE2 
                    WHERE (ref_exa) IN 
                    (SELECT id_exa 
                    FROM t_examens AS EXA 
                    WHERE ref_can=:id AND EXA.convoc_exa=1 AND EXA.date_heure_exa>=NOW()))";

        $args[':id'] = $id;

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $tab = $stmt->fetch();
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }


    public function gen_password($password) {
        $this->set_password(password_hash($password, PASSWORD_DEFAULT));
    }

    /**
     * 2019-01-18 JHI
     * @param $length
     * @param $characters
     * @return string mot de passe
     */
    function gen_random_password($length, $characters) {
        // $length - the length of the generated password
        // $characters - types of characters to be used in the password

        // define variables used within the function
        $symbols = array();
        $used_symbols = '';

        // an array of different character types
        $symbols["lower_case"] = 'abcdefghijklmnopqrstuvwxyz';
        $symbols["upper_case"] = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $symbols["numbers"] = '1234567890';
        $symbols["special_symbols"] = "!?~@#-_+<>[]{}";

        $characters = explode(",",$characters); // get characters types to be used for the passsword
        //print_r($characters);
        foreach ($characters as $key=>$value) {
            $used_symbols .= $symbols[$value]; // build a string with all characters
        }
        $symbols_length = strlen($used_symbols) - 1; //strlen starts from 0 so to get number of characters deduct 1


        $pass = '';
        for ($i = 0; $i < $length; $i++) {
            $n = rand(0, $symbols_length); // get a random character from the string with all characters
            $pass .= $used_symbols[$n]; // add the character to the password string
            //echo $pass;
        }
        return $pass; // return the generated password
    }

    /**
     * @param $password
     */
    public function new_pwd($password){

        $headers = 'MIME-Version: 1.0' . "\r\n";

        $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
        $headers .= 'From: ICT-JB.net <webmaster@ict-jb.net>' . "\r\n";
        $headers .= 'Reply-To: webmaster@ict-jb.net' . "\r\n";
        $headers .= "X-Sender: <www.ict-b.net>" . "\r\n";;
        $headers .= "X-Mailer: PHP" . "\r\n";;
        $headers .= "X-auth-smtp-user: webmaster@ict-jb.net" . "\r\n";;
        $headers .= "X-abuse-contact: webmaster@ict-jb.net" . "\r\n";;
        $expe = "webmaster@ict-jb.net";

        $date_time = date("D, d M Y H:i:s O");
        $headers .= 'DATE: ' . $date_time . "\r\n";


        $sujet_mail = "Nouveau mot de passe pour " . $this->get_prenom() . " " . $this->get_nom();
        $mess_mail = utf8_decode("Votre nouveau mot de passe pour l'accès à vos notes est : " . $password . "\n Prenez en soins !");
        $destinataire_mail = $this->get_email() . ", jacques@hirtzel.ch";
        //$destinataire_mail = "jacques@hirtzel.ch";
        mail($destinataire_mail, $sujet_mail, $mess_mail, $headers);


        //$mail = new mail();
        //$destinataire_mail = array( $this->get_email());

        //Envoie de l'email
        //$mail->send_email($destinataire_mail, $sujet_mail, $mess_mail);
        //$mail->send_email(array('jacques.hirtzel@ceff.ch'), $sujet_mail, $mess_mail);
        //mail($destinataire_mail, $sujet_mail, $mess_mail, $headers);
        //mail("jacques@hirtzel.ch", $sujet_mail, $mess_mail, $headers);
    }

    /**
     * @return bool
     */
    public function get_last_tpi(){
        $query = "SELECT id_tpi FROM t_tpi WHERE ref_can=".$this->get_id()." ORDER BY id_tpi DESC LIMIT 1";

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();

            if($stmt->execute()){
                $tab = array();
                $tab = $stmt->fetch();
                return $tab['id_tpi'];
            }else{
                return false;
            }
        } catch (Exception $ex) {
            return false;
        }
    }

    function has_tpi(){
        $args = array();
        $args["id_can"] = $this->get_id();

        $query = "SELECT * FROM `t_ref_can_exa` RCE 
                  JOIN t_examens EXA on RCE.ref_exa = EXA.id_exa 
                  WHERE RCE.ref_can = :id_can 
                  AND EXA.no_ich_exa = 0 
                  AND YEAR(EXA.date_heure_exa) = YEAR(NOW())";
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $exa = $stmt->fetch();
            if(!empty($exa))
                return true;
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @return bool
     */
    public function update_password() {
        $query = 'UPDATE t_candidats '
            . 'SET password_can=:password '
            . 'WHERE email_can=:email';

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(array(":password"=>$this->get_password(), ":email"=>$this->get_email()));
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

    public function get_lettre_met(){
        $query = "SELECT MET.lettre_met FROM t_metiers MET 
                    JOIN t_groupes GRP ON MET.id_met=GRP.ref_met
                    JOIN t_classes CLA ON GRP.id_grp=CLA.ref_grp
                    JOIN t_candidats CAN ON CLA.id_cla=CAN.ref_classe
                    WHERE CAN.id_can =" .$this->get_id();
        try {
            $stmt = $this->pdo->prepare($query);
            if ($stmt->execute()) {
                $tab = $stmt->fetch();
                return $tab;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }


    /* ----- Getters ----- */

    /**
     * @return mixed
     */
    public function get_id(){
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function get_no_candidat(){
        return $this->no_candidat;
    }

    /**
     * Permet d'enlever le 1 des candidats 1xxx
     * @return bool|string
     */
    function get_no_candidat_minus() {
        $num = $this->no_candidat;
        if (strlen($num) > 3) {
            $numb = substr($num, 1, 3);
            return $numb;
        } else {
            return $num;
        }
    }

    /**
     * @return mixed
     */
    public function get_sexe(){
        return $this->sexe;
    }

    /**
     * @return mixed
     */
    public function get_nom(){
        return $this->nom;
    }

    /**
     * @return mixed
     */
    public function get_prenom(){
        return $this->prenom;
    }

    /**
     * @return mixed
     */
    public function get_email(){
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function get_annee(){
        return $this->annee;
    }

    /**
     * @return mixed
     */
    public function get_systeme(){
        return $this->systeme;
    }

    /**
     * @return mixed
     */
    public function get_employeur(){
        return $this->employeur;
    }

    /**
     * @return mixed
     */
    public function get_classe(){
        return $this->classe;
    }

    /**
     * @return mixed
     */
    function get_nom_employeur() {
        $employeur = new Entreprise($this->employeur);
        return $employeur->get_nom();
    }

    /**
     * @return mixed
     */
    function get_nom_classe() {
        $classe = new Classe($this->classe);
        return $classe->get_nom_cla();
    }

    /**
     * @return mixed
     */
    function get_nom_group(){
        $classe = new Classe($this->classe);
        return $classe->get_nom_grp();
    }

    /**
     * @return mixed
     */
    function get_termine_classe() {
        $classe = new Classe($this->classe);
        return $classe->get_termine_cla();
    }

    /**
     * @return mixed
     */
    function get_stop_form_classe() {
        $classe = new Classe($this->classe);
        return $classe->get_stop_form_cla();
    }

    /**
     * @return mixed
     */
    public function get_adresse(){
        return $this->adresse;
    }

    /**
     * @return mixed
     */
    public function get_password(){
        return $this->password;
    }

    /**
     * @return mixed
     */
    public function get_login(){
        return $this->login;
    }

    /**
     * @return mixed
     */
    public function get_reglement(){
        return $this->reglement;
    }
	
	/**
     * @return mixed
     */
    public function get_dys(){
        return $this->dys;
    }

	 /**
     * @return mixed
     */
    public function get_moyens_supp(){
        return $this->moyens_supp;
    }

    /* ----- Setters ----- */

    /**
     * @param mixed $id
     */
    public function set_id($id){
        $this->id = $id;
    }

    /**
     * @param mixed $no_candidat
     */
    public function set_no_candidat($no_candidat){
        $this->no_candidat = $no_candidat;
    }

    /**
     * @param mixed $sexe
     */
    public function set_sexe($sexe){
        $this->sexe = $sexe;
    }

    /**
     * @param mixed $nom
     */
    public function set_nom($nom){
        $this->nom = $nom;
    }

    /**
     * @param mixed $prenom
     */
    public function set_prenom($prenom){
        $this->prenom = $prenom;
    }

    /**
     * @param mixed $email
     */
    public function set_email($email){
        $this->email = $email;
    }

    /**
     * @param mixed $annee
     */
    public function set_annee($annee){
        $this->annee = $annee;
    }

    /**
     * @param mixed $systeme
     */
    public function set_systeme($systeme){
        $this->systeme = $systeme;
    }

    /**
     * @param mixed $employeur
     */
    public function set_employeur($employeur){
        $this->employeur = $employeur;
    }

    /**
     * @param mixed $classe
     */
    public function set_classe($classe){
        $this->classe = $classe;
    }

    /**
     * @param mixed $adresse
     */
    public function set_adresse($adresse){
        $this->adresse = $adresse;
    }

    /**
     * @param mixed $password
     */
    public function set_password($password){
        $this->password = $password;
    }

    /**
     * @param mixed $login
     */
    public function set_login($login){
        $this->login = $login;
    }

    /**
     * @param mixed $reglement
     */
    public function set_reglement($reglement){
        $this->reglement = $reglement;
    }

	/**
     * @param mixed $dys
     */
    public function set_dys($dys){
        $this->dys = $dys;
    }

	/**
     * @param mixed $moyens_supp
     */
    public function set_moyens_supp($moyens_supp){
        $this->moyens_supp = $moyens_supp;
    }
}

?>

<?php

Class Convocations EXTENDS Projet {

    public function __construct() {
        parent::__construct();
    }

    public function get_all_conf_by_id_exa($id_exa){
        $query =   "SELECT CAN.id_can,CAN.nom_can,CAN.prenom_can,GRP.nom_grp,CLA.nom_cla FROM t_ref_can_exa AS RCE
                    JOIN t_candidats AS CAN on RCE.ref_can=CAN.id_can
                    JOIN t_classes AS CLA on CAN.ref_classe=CLA.id_cla
                    JOIN t_groupes AS GRP on CLA.ref_grp=GRP.id_grp
                    WHERE RCE.conf_convoc = 1 AND RCE.ref_exa =:id_exa";

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(array(":id_exa" => $id_exa));
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $ex) {
            return false;
        }
    }

    public function get_all_conf_0_by_id_exa($id_exa){
        $query =   "SELECT CAN.id_can,CAN.nom_can,CAN.prenom_can,GRP.nom_grp,CLA.nom_cla FROM t_ref_can_exa AS RCE
                    JOIN t_candidats AS CAN on RCE.ref_can=CAN.id_can
                    JOIN t_classes AS CLA on CAN.ref_classe=CLA.id_cla
                    JOIN t_groupes AS GRP on CLA.ref_grp=GRP.id_grp
                    WHERE RCE.conf_convoc = 0 AND RCE.ref_exa =:id_exa";

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(array(":id_exa" => $id_exa));
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $ex) {
            return false;
        }
    }

    public function get_future_exam_grp($groupe) {//Liste des examens pour un groupe
        $query = "SELECT *, COUNT(no_candidat_can) AS nb_exa "
                . "FROM `t_candidats` CAN "
                . "JOIN t_ref_can_exa REF ON REF.ref_can=CAN.id_can "
                . "JOIN t_examens EXA ON EXA.id_exa=REF.ref_exa "
                . "JOIN t_classes CLA ON CLA.id_cla=CAN.ref_classe "
                . "JOIN t_groupes GRP ON GRP.id_grp=CLA.ref_grp "
                . "WHERE GRP.id_grp=:grp AND EXA.convoc_exa=1 AND EXA.definitif_exa=1  AND EXA.date_heure_exa>=NOW() "
                . "GROUP BY no_candidat_can";

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(array(":grp" => $groupe));
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $ex) {
            return false;
        }
    }

    public function get_future_exam_cla($classe) {//Liste des examens pour une classe
        $query = "SELECT *, COUNT(no_candidat_can) AS nb_exa "
                . "FROM `t_candidats` CAN "
                . "JOIN t_ref_can_exa REF ON REF.ref_can=CAN.id_can "
                . "JOIN t_examens EXA ON EXA.id_exa=REF.ref_exa "
                . "JOIN t_classes CLA ON CLA.id_cla=CAN.ref_classe "
                . "JOIN t_groupes GRP ON GRP.id_grp=CLA.ref_grp "
                . "WHERE CLA.id_cla=:cla AND EXA.convoc_exa=1 AND EXA.definitif_exa=1  AND EXA.date_heure_exa>=NOW() "
                . "GROUP BY no_candidat_can";

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(array(":cla" => $classe));
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $ex) {
            return false;
        }
    }

    public function get_future_exam_can($candidat) {//Liste des examens pour un candidat
        $query = "SELECT nom_can, COUNT(no_candidat_can) AS nb_exa "
            . "FROM `t_candidats` CAN "
            . "JOIN t_ref_can_exa REF ON REF.ref_can=CAN.id_can "
            . "JOIN t_examens EXA ON EXA.id_exa=REF.ref_exa "
            . "JOIN t_classes CLA ON CLA.id_cla=CAN.ref_classe "
            . "JOIN t_groupes GRP ON GRP.id_grp=CLA.ref_grp "
            . "WHERE CAN.id_can=:can AND EXA.convoc_exa=1 AND EXA.definitif_exa=1  AND EXA.date_heure_exa>=NOW() "
            . "GROUP BY no_candidat_can";

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(array(":can" => $candidat));
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $ex) {
            return false;
        }
    }

    /************************************************PDF des convocations**************************************************************/
    
    public function get_all_can_in_grp($id_grp) {
        $query = 'SELECT DISTINCT CAN.* '
                . 'FROM t_candidats CAN '
                . 'JOIN t_ref_can_exa RCE ON RCE.ref_can=CAN.id_can '
                . 'JOIN t_examens EXA ON EXA.id_exa=RCE.ref_exa '
                . 'JOIN t_classes CLA ON CLA.id_cla=CAN.ref_classe '
                . 'JOIN t_groupes GRP ON GRP.id_grp=CLA.ref_grp '
                . 'WHERE GRP.id_grp=' . $id_grp . ' AND EXA.definitif_exa=1 AND EXA.convoc_exa=1 AND EXA.date_heure_exa>=NOW()';

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $ex) {
            return false;
        }
    }

    public function get_all_can_in_cla($id_cla) {
        $query = 'SELECT DISTINCT CAN.* '
                . 'FROM t_candidats CAN '
                . 'JOIN t_ref_can_exa RCE ON RCE.ref_can=CAN.id_can '
                . 'JOIN t_examens EXA ON EXA.id_exa=RCE.ref_exa '
                . 'JOIN t_classes CLA ON CLA.id_cla=CAN.ref_classe '
                . 'WHERE CLA.id_cla=' . $id_cla . ' AND EXA.definitif_exa=1 AND EXA.convoc_exa=1 AND EXA.date_heure_exa>=NOW()';

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $ex) {
            return false;
        }
    }

    public function get_all_dual_in_grp($id_grp) {
        $query = 'SELECT DISTINCT CAN.* '
                . 'FROM t_candidats CAN '
                . 'JOIN t_ref_can_exa RCE ON RCE.ref_can=CAN.id_can '
                . 'JOIN t_examens EXA ON EXA.id_exa=RCE.ref_exa '
                . 'JOIN t_classes CLA ON CLA.id_cla=CAN.ref_classe '
                . 'JOIN t_groupes GRP ON GRP.id_grp=CLA.ref_grp '
                . 'WHERE GRP.id_grp=' . $id_grp . ' AND EXA.definitif_exa=1 AND EXA.convoc_exa=1 AND CAN.systeme_can="Dual" AND EXA.date_heure_exa>=NOW()';

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $ex) {
            return false;
        }
    }

    public function get_all_dual_in_cla($id_cla) {
        $query = 'SELECT DISTINCT CAN.* '
                . 'FROM t_candidats CAN '
                . 'JOIN t_ref_can_exa RCE ON RCE.ref_can=CAN.id_can '
                . 'JOIN t_examens EXA ON EXA.id_exa=RCE.ref_exa '
                . 'JOIN t_classes CLA ON CLA.id_cla=CAN.ref_classe '
                . 'WHERE CLA.id_cla=' . $id_cla . ' AND EXA.definitif_exa=1 AND EXA.convoc_exa=1 AND CAN.systeme_can="Dual" AND EXA.date_heure_exa>=NOW()';

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $ex) {
            return false;
        }
    }

    /* A SUPPRIMER */

    public function get_all_nom_cla() {
        $query = "SELECT DISTINCT nom_cla, id_cla "
                . "FROM t_classes "
                . "WHERE termine_cla=0 AND nom_cla <> '' "
                . "ORDER BY nom_cla";

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $ex) {
            return false;
        }
    }

    public function get_all_nom_grp() {
        $query = "SELECT DISTINCT nom_grp, id_grp "
                . "FROM t_groupes GRP "
                . "JOIN t_classes CLA ON CLA.ref_grp=GRP.id_grp "
                . "WHERE termine_cla=0 AND nom_cla <> '' "
                . "ORDER BY nom_grp";

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $ex) {
            return false;
        }
    }

}

?>
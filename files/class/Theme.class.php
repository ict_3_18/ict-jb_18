<?php

class Theme EXTENDS Projet {

    private $id_thm;
    private $nom;
    
    function __construct($id = null) {
        parent::__construct();

        if ($id) {
            $this->set_id_thm($id);
            $this->init();
        }
    }

    public function init() {
        $query = "SELECT * FROM t_themes WHERE id_thm=:id_thm";

        try {
            $stmt = $this->pdo->prepare($query);
            $args['id_thm'] = $this->get_id_thm();
            $stmt->execute($args);
            $tab = $stmt->fetch();

            $this->set_id_thm($tab['id_thm']);
            $this->set_nom($tab['nom_thm']);
            

            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

    public function get_all($order = "id_thm") {
        $query = "SELECT * FROM t_themes ORDER BY ".$order;
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $ex) {
            return false;
        }
    }

    function get_id_thm() {
        return $this->id_thm;
    }

    function get_nom() {
        return $this->nom;
    }

    function set_id_thm($id_thm) {
        $this->id_thm = $id_thm;
    }

    function set_nom($nom) {
        $this->nom = $nom;
    }
}

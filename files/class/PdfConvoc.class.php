<?php

class PdfConvoc extends Mypdf {
    /*********************************************************************************
     * fonction : head_convoc
     *
     * Description :
     * 			Génère l'en-tête du PDF
     *********************************************************************************/

    function head_convoc($candidat, $mention_dual = '') {
        $this->adresse_chef_exp();
        $this->SetFont('arial', 'B', 10);
        $this->SetXY(25, 100);

        if ($candidat->get_systeme() == 'Dual') {
            $this->Cell(10, 4, utf8_decode($candidat->get_nom() . ' ' . $candidat->get_prenom() . ' (' . $candidat->get_no_candidat_minus() . ')' . " / Convocation aux contrôles de comptences ICT"), 0, 2, "J");

            $this->mention_copie($mention_dual);
        } else {
            $this->Cell(10, 4, utf8_decode("Convocation aux contrôles de compétences ICT"), 0, 2, "J");
        }
    }

    function liste_exa($candidat) {
        $this->SetXY(25, 120);
        $this->SetFont('Arial', '', 10);

        $classe = new Classe($candidat->get_classe());
        $groupe = new Groupe($classe->get_ref_grp());

        $tab_infos_met = $groupe->get_all_infos_met_by_id($groupe->get_ref_met());

        if($candidat->get_sexe() == "M"){
            $str = "Dans le cadre de votre formation d'" .  $tab_infos_met['nom_m_met'] . ", vous êtes priés de vous présenter aux contrôles de compétences ci-dessous : ";
        }else{
            $str = "Dans le cadre de votre formation d'" .  $tab_infos_met['nom_f_met'] . ", vous êtes priées de vous présenter aux contrôles de compétences ci-dessous : ";
        }

        $this->MultiCell(160, 4, utf8_decode($str), 0, "J");

        $this->SetXY(35, 135);
        $this->SetFont('Arial', '', 10);

        $tab_convocation = $candidat->get_convoc();
        foreach ($tab_convocation AS $convoc) {
            $examen = new Examen($convoc['id_exa']);

            $this->Cell(10, 4, utf8_decode("ICT " . $convoc['num_mod'] . " - " . $convoc['nom_mod']), 0, 2, "J");

            $this->SetFont('Arial', 'B', 10);

            $date_debut = ucfirst(utf8_encode(strftime("%A %d %B %Y %H:%M", strtotime($convoc['date_heure_exa']))));
            $date = new DateTime($convoc['date_heure_exa']);
            $date->add(new DateInterval('PT' . ($convoc['duree_exa'] * 60) . 'S'));
            $date_fin = date_format($date, "H:i");
            $this->Cell(10, 4, "        " . utf8_decode($date_debut) . " - " . $date_fin, 0, 2, "J");

            $this->SetFont('Arial', '', 10);

            $tab_sle = $examen->get_salles_exa();
            $str_sle = "";
            foreach ($tab_sle As $sle) {
                $str_sle .= $sle['nom_sle'];
                if ($sle != end($tab_sle))
                    $str_sle .= ", ";
            }
            if ($str_sle == "") {
                $str_sle = utf8_decode("Atelier informatique étage D à définir.");
            }
            $this->Cell(10, 4, "          salle : " . $str_sle, 0, 2, "J");
            $this->Cell(10, 4, "", 0, 2, "J");
        }

        $this->SetX(25);
        $this->Cell(10, 4, utf8_decode("Pour rappel, la fréquentation des contrôles de compétences est obligatoire, une absence non justifiée"), 0, 2, "J");
        $this->Cell(10, 4, utf8_decode("(voir au verso) sous 24 heures sera sanctionnée par la note 1."), 0, 2, "J");
        $this->Cell(10, 4, "", 0, 2, "J");
        $this->Cell(10, 4, utf8_decode("La présente convocation est à prendre avec vous à chaque examen."), 0, 2, "J");
        $this->Cell(10, 4, "", 0, 2, "J");
        $this->Cell(10, 4, "", 0, 2, "J");
        $this->SetX(135);
        $this->Cell(10, 4, "Jacques Hirtzel", 0, 2, "J");
    }

    function verso() {

        $this->AddPage();
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(15, 4, utf8_decode("1. Bases légales"), 0, 2, "J");
        $this->SetFont('Arial', '', 8);
        $this->MultiCell(85, 4, utf8_decode("L'organisation des examens est basée sur la loi fédérale sur la formation professionnelle (LFPr), l'ordonnance sur la formation professionnelle (OFPr), la loi cantonale sur la formation et l'orientation professionnelles (LFOP), l'ordonnance sur la formation et l'orientation professionnelles (OFOP), l'ordonnance de direction sur la formation et l'orientation professionnelles (ODFOP) et le règlement actuel d'apprentissage et de fin d'apprentissage de la profession concernée. Les examens ne sont pas publics."), 0, "J");
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(10, 4, utf8_decode("2.	Autorités d'examen"), 0, 2, "J");
        $this->SetFont('Arial', '', 8);
        $this->MultiCell(85, 4, utf8_decode("Les examens sont mis sur pied sous la surveillance de la commission d'examens de fin d'apprentissage compétente. Elle organise les examens en collaboration avec les experts en chef et les expertes en chef qu'elle a nommés et publie les résultats d'examens. "), 0, "J");
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(10, 4, utf8_decode("3.	Obligation de se présenter à l'examen"), 0, 2, "J");
        $this->SetFont('Arial', '', 8);
        $this->MultiCell(85, 4, utf8_decode("Le temps d'examen compte comme temps de travail. Les candidats et candidates qui ne se présentent pas aux examens sans excuse valable ou sans motif important se verront attribuer la note 1 à la branche ou à la position concernée. Les frais qui en résultent sont à leur charge."), 0, "J");
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(10, 4, utf8_decode("4.	Maladie ou accident"), 0, 2, "J");
        $this->SetFont('Arial', '', 8);
        $this->MultiCell(85, 4, utf8_decode("Quiconque ne peut se présenter à l'examen pour des raisons de santé doit le justifier par un certificat médical. Ce dernier doit être envoyé dès que possible à la commission d'examen compétente. Des demandes d'allègement pour cause de maladie ou d'accident faites après coup ne seront pas prises en considération."), 0, "J");
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(10, 4, utf8_decode("5.	Congé militaire"), 0, 2, "J");
        $this->SetFont('Arial', '', 8);
        $this->MultiCell(85, 4, utf8_decode("Les candidats et candidates qui durant la période des examens accomplissent leur service militaire ont droit  un congé. Le candidat ou la candidate doit faire une demande de congé en remettant une copie de la convocation  son supérieur militaire."), 0, "J");
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(10, 4, utf8_decode("6.	Frais d'examen"), 0, 2, "J");
        $this->SetFont('Arial', '', 8);
        $this->MultiCell(85, 4, utf8_decode("Selon l'article 40 LFPr et lart. 131 OFOP, l'entreprise d'apprentissage prend à sa charge les frais inhérents à la préparation des preuves d'examen, à la location des locaux d'examen, à l'outillage et au matériel utilisé aux examens. Aucuns frais d'examen ne peuvent être crédités à l'apprenti-e. Les candidats et candidates selon l'article 32 LFPr ainsi que les répétants et répétantes sans contrat d'apprentissage prennent les frais d'examen à leur charge."), 0, "J");
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(10, 4, utf8_decode("7.	Responsabilité en cas de dommages"), 0, 2, "J");
        $this->SetFont('Arial', '', 8);
        $this->MultiCell(85, 4, utf8_decode("En cas de dégâts aux machines ou aux installations, les fautifs peuvent être rendus responsables s'ils ont été causés volontairement ou par une négligence grave, ou s'ils sont imputables à une formation insuffisante."), 0, "J");
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(10, 4, utf8_decode("8.	Moyens auxiliaires autorisés "), 0, 2, "J");
        $this->SetFont('Arial', '', 8);
        $this->MultiCell(85, 4, utf8_decode("Pour autant qu'aucune liste d'outillage et/ou de matériel spéciale ne soit annexée à la convocation, les moyens auxiliaires suivants sont autorisés :\na)	Travaux pratiques et connaissances professionnelles \nL'outillage usuel doit être apporté. Le journal de travail n'est à présenter que si c'est prévu par le règlement.\nb)	Dessin professionnel\nLes instruments de dessin usuels sont à amener. Pour la réalisation des esquisses, le compas, la règle, les équerres et tout autre moyen auxiliaire de dessin peuvent être utilisés.\nc)	Calcul professionnel\nLes machines  calculer électroniques (sans raccordement) au secteur et les tabelles sans exemples de problèmes sont autorisées. L'usager est seul responsable du bon fonctionnement de l'appareil. Un appareil par candidat-e est autorisé. Il ne pourra pas être prêté de calculatrice à une tierce personne. Le développement complet de la solution ainsi que les opérations à effectuer doivent être portés sur la feuille d'examen.\nL'utilisation de moyens de communication électronique est strictement interdite. Tout échange de données entraîne l'interruption immédiate de l'examen."), 0, "J");

        $this->setXY(100, 10);
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(10, 4, utf8_decode("9.	Irrégularités"), 0, 2, "J");
        $this->SetFont('Arial', '', 8);
        $this->MultiCell(85, 4, utf8_decode("Des irrégularités provoquées par un candidat ou une candidate telles que perturbation du déroulement de l'examen ou emploi de moyens auxiliaires non autorisés doivent être annoncées immédiatement à la commission d'examens compétente. \n	Celle-ci peut :\na)	baisser la note en conséquence dans la position ou la sous-position correspondante,\nb)	exclure le candidat ou la candidate de l'examen ou annuler l'examen et ordonner la répétition de l'examen dans la branche en question ou dans sa totalité,\nc)	lors de la constatation ultérieure d'une irrégularité, requérir auprès de l'Office de la formation professionnelle le retrait du certificat fédéral de capacité.\n	\nLa répétition de l'examen complet compte comme deuxième examen, au sens de l'article 44 LFPr."), 0, "J");
        $this->setXY(100, 80);
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(10, 4, utf8_decode("10.	Communication des résultats"), 0, 2, "J");
        $this->SetFont('Arial', '', 8);
        $this->MultiCell(85, 4, utf8_decode("A  pour les apprentis et apprenties dont le lieu d'apprentissage est situé dans le canton de Berne :\n       - les résultats sont transmis par écrit à l'entreprise\nB  pour les candidats et candidates ayant passé l'examen d'après lart. 41 LFPr et les répétant-e-s sans contrat d'apprentissage :\n       - les résultats sont transmis par écrit aux candidat-e-s\nC  les candidats et candidates dont le lieu d'apprentissage se trouve en dehors du canton de Berne se voient notifier leurs résultats par le canton dans lequel ils ont effectué leur apprentissage."), 0, "J");

        $this->setXY(100, 125);
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(10, 4, "11.	Droit de recours", 0, 2, "J");
        $this->SetFont('Arial', '', 8);
        $this->MultiCell(85, 4, "Le droit de recours se base sur les directives du canton d'apprentissage.", 0, "J");
        $this->setXY(100, 140);
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(10, 4, "12.	Renseignements", 0, 2, "J");
        $this->SetFont('Arial', '', 8);
        $this->MultiCell(85, 4, utf8_decode("Pour toute question, veuillez vous adresser à la commission d'examen ou à l'expert en chef ou experte en chef compétente. Aucun renseignement au sujet des résultats d'examens ne sera donné par téléphone."), 0, "J");

        $this->setXY(140, 180);
        $this->SetFont('Arial', 'B', 8);
        $this->MultiCell(65, 4, " Commission cantonale des\n examens de fin d'apprentissage\n Arrondissement du Jura bernois", 0, "J");

        $this->setXY(140, 200);
        $this->SetFont('Arial', '', 8);
        $this->Cell(10, 4, "Le responsable des examens", 0, 2, "J");
    }

    function accuse_reception($tab_id_can) {
        $this->AddPage();
        $this->SetFont('Arial', 'B', 22);
        $this->setY(20);
        $this->Cell(15, 8, utf8_decode("Accusé de réception"), 0, 2, "J");
        $this->SetFont('Arial', '', 18);
        $this->Cell(15, 6, utf8_decode("Convocation aux contrôles de compétences ICT"), 0, 2, "J");

        $this->setY(40);
        $this->SetFont('Arial', 'B', 12);
        $this->Cell(70, 8, "Candidat-s", 1, 0, "J");
        $this->Cell(80, 8, "Module-s", 1, 0, "J");
        $this->Cell(30, 8, "Signature-s", 1, 1, "J");
        foreach ($tab_id_can AS $id_can) {
            $can = new Candidat($id_can['id_can']);

            if ($can->get_systeme() == "Dual") {
                $tab_dual[] = $can;
            }

            $this->SetFont('Arial', '', 12);
            $this->Cell(150, 10, utf8_decode($can->get_no_candidat_minus() . " - " . $can->get_nom() . " " . $can->get_prenom()), 1, 0, "J");
            $this->Cell(30, 10, "", 1, 1, "J");

            $pos_x = $this->GetX();
            $pos_y = $this->GetY();

            $this->SetY($pos_y - 10);

            $this->Cell(70, 10, "", 1, 0, "J");

            $this->SetFont('Arial', '', 10);
            $mods = '';
            foreach ($can->get_convoc() AS $conv) {
                if ($mods != '') {
                    $mods .= ' - ';
                } else {
                    $mods .= 'ICT: ';
                }
                $exa = new Examen($conv['id_exa']);
                if ($exa->get_date_hrs() > date("Y-m-d H:i:s"))
                    $mods .= $exa->get_num_nom();
            }
            $this->Cell(80, 10, $mods, 1, 0, "J");
            $this->SetX($pos_x);
            $this->SetY($pos_y);
        }
    }

    public function gen_convoc($candidat, $mention = '') {
        //pages
        $this->SetFont('Arial', '', 20);
        $this->SetAutoPageBreak(1, 0);
        $this->SetMargins(10, 10, 10, 10);
        $this->AddPage();
        $this->head_convoc($candidat, $mention);
        $this->adresse_candidat($candidat);
        $this->liste_exa($candidat);
        $this->verso();
    }

}

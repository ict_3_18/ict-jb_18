<?php
/**
 * Created by PhpStorm.
 * User: CP-13JUB
 * Date: 14.03.2019
 * Time: 09:51
 */

class Vacance extends Projet
{
    private $id;
    private $nom_vac;
    private $debut_vac;
    private $fin_vac;

    public function __construct($id = null)
    {
        parent::__construct();

        if ($id) {
            $this->set_id($id);
            $this->init();
        }
    }

    public function init()
    {
        $query = "SELECT * FROM t_vacances WHERE id_vac=:id_vac";

        try {
            $stmt = $this->pdo->prepare($query);
            $args['id_vac'] = $this->get_id();
            $stmt->execute($args);
            $tab = $stmt->fetch();

            $this->set_nom_vac($tab['nom_vac']);
            $this->set_debut_vac($tab['debut_vac']);
            $this->set_fin_vac($tab['fin_vac']);

            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * Permet de retourner toutes les vacances
     * @return array|bool
     */
    public function get_all_vac(){
        $query = "SELECT * FROM t_vacances";

        try {
            $tab_vac = $this->pdo->query($query)->fetchAll(PDO::FETCH_ASSOC);
            return $tab_vac;
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * @return mixed
     */
    public function get_id()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function set_id($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function get_nom_vac()
    {
        return $this->nom_vac;
    }

    /**
     * @param mixed $nom_vac
     */
    public function set_nom_vac($nom_vac)
    {
        $this->nom_vac = $nom_vac;
    }

    /**
     * @return mixed
     */
    public function get_debut_vac()
    {
        return $this->debut_vac;
    }

    /**
     * @param mixed $debut_vac
     */
    public function set_debut_vac($debut_vac)
    {
        $this->debut_vac = $debut_vac;
    }

    /**
     * @return mixed
     */
    public function get_fin_vac()
    {
        return $this->fin_vac;
    }

    /**
     * @param mixed $fin_vac
     */
    public function set_fin_vac($fin_vac)
    {
        $this->fin_vac = $fin_vac;
    }


}
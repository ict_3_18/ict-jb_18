<?php

Class Salle EXTENDS Projet {

    private $id_sle;
    private $nom;
    private $rue;
    private $npa;
    private $localite;
    
    public function __construct($id = null) {

        parent::__construct();

        if ($id) {
            $this->set_id_sle($id);
            $this->init();
        }
    }

    public function init() {
        $query = "SELECT * FROM t_salles WHERE id_sle=:id_sle";
        try {
            $stmt = $this->pdo->prepare($query);
            $args[':id_sle'] = $this->get_id_sle();

            $stmt->execute($args);
            $tab = $stmt->fetch();

            $this->set_nom($tab['nom_sle']);
            $this->set_rue($tab['rue_sle']);
            $this->set_npa($tab['npa_sle']);
            $this->set_localite($tab['localite_sle']);
            
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    public function init_by_name($nom){
        $query = "SELECT * FROM t_salles WHERE nom_sle=:nom_sle";
        try {
            $stmt = $this->pdo->prepare($query);
            $args[':nom_sle'] = $nom;

            $stmt->execute($args);
            $tab = $stmt->fetch();
            $this->set_id_sle($tab['id_sle']);
            $this->init();
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    //renvoie le nom des salles
    public function get_nom_salles(){
        $query = "SELECT id_sle ,nom_sle FROM t_salles order by nom_sle";
        
        $tab = array();
        
        try {
             $stmt = $this->pdo->prepare($query);
             $stmt->execute();
             $tab = $stmt->fetchAll();
             return $tab;
         } catch (Exception $e) {
             return false;
         }
    }
    
    //renvoie id de la salle en fonction du nom
    public function get_id_salle_by_name($name){
        $args[":nom_sle"] = $name;
        $query = "SELECT id_sle FROM t_salles WHERE nom_sle=:nom_sle";
        
        $tab = array();
        
        try {
             $stmt = $this->pdo->prepare($query);
             $stmt->execute($args);
             $tab = $stmt->fetchAll();
             return $tab;
         } catch (Exception $e) {
             return false;
         }
    }
    
    //renvoie id de la salle en fonction du nom
    public function get_name_salle_by_id($id){
        $args[":id_sle"] = $id;
        $query = "SELECT nom_sle FROM t_salles WHERE id_sle=:id_sle";

        try {
             $stmt = $this->pdo->prepare($query);
             $stmt->execute($args);
             $nom_sle = $stmt->fetch();

             return $nom_sle['nom_sle'];
         } catch (Exception $e) {
             return false;
         }
    }
    
    public function __toString() {
        $str = "\n<pre>\n";
        foreach ($this AS $key => $val) {
            if ($key != "pdo") {
                $str .= "\t" . $key;
                $lengh_key = strlen($key);
                for ($i = strlen($key); $i < 20; $i++) {
                    $str .= "&nbsp;";
                }
                $str .= "=>&nbsp;&nbsp;&nbsp;" . $val . "\n";
            }
        }
        $str .= "\n</pre>";
        return $str;
    }

    public function gen_plan($largeur, $hauteur, $place_can = '')
    {
        $ratio_x = $largeur / 1000;
        $ratio_y = $hauteur / 550;
        $tab_tables = $this->get_tables();
        $html = '<div class="col-lg-12 table-container" style="position:relative;height:' . $hauteur . 'px;width:' . $largeur . 'px;">';
        foreach ($tab_tables AS $table) {
            $width = round($table['width_tab'] * $ratio_x);
            $height = round($table['height_tab'] * $ratio_y);
            $pos_x = round($table['posx_tab'] * $ratio_x - 100);
            $pos_y = round($table['posy_tab'] * $ratio_y - 25);
            $pc = substr($table['pcname_tab'], 4, 6);

            if (strtoupper(substr($table['pcname_tab'], 8, 4)) == "PROF") {
                $pc = 'Enseignant';
                $class = 'table table-prof';
            } else if ($place_can == $table['pcname_tab']) {
                $pc = 'Ma place : '.$pc;
                $class = 'table table-ma-place';
            } else {
                $pc = $pc;
                $class = 'table table-candidat';
            }
            $html .= '<div class="' . $class . ($width < $height ? ' vertical' : '') . '" data-id_tab="' . $table['id_tab'] . '" style="position:absolute;width:' . $width . 'px;height:' . $height . 'px;top:' . $pos_y . 'px;left:' . $pos_x . 'px;">';

            if(strlen($table['pcname_tab']) > 3){
                $html .= '<span class="table-titre">' . $pc . '</span>';
            }

          //$html .= '<span class="table-titre">' . $pc . '</span>';

            $html .= '<span class="table-candidats-container"></span>';
            $html .= "</div>";
        }
        $html .= "</div>";
        return $html;
    }
    public function gen_plan_rapport($largeur, $hauteur, $id_pls, $id_exa)
    {
        $exa = new Examen($id_exa);
        $pls = new Plan($id_pls);
        $can_pos = $pls->get_all_can_by_sle($this->get_id_sle(), $id_exa);
        $can = new Candidat();

        $ratio_x = $largeur / 1000;
        $ratio_y = $hauteur / 550;
        $tab_tables = $this->get_tables();
        $html = '<div class="col-lg-12 table-container" style="position:relative;height:' . $hauteur . 'px;width:' . $largeur . 'px;">';
        foreach ($tab_tables AS $table) {
            $width = round($table['width_tab'] * $ratio_x);
            $height = round($table['height_tab'] * $ratio_y);
            $pos_x = round($table['posx_tab'] * $ratio_x - 100);
            $pos_y = round($table['posy_tab'] * $ratio_y - 25);
            $pc = substr($table['pcname_tab'], 4, 6);

            if (strtoupper(substr($table['pcname_tab'], 8, 4)) == "PROF") {
                $pc = 'Enseignant';
                $class = 'table table-prof';
            } else {
                //$pc = $pc;
                $class = 'table table-candidat';
            }

            foreach ($can_pos as $place){
                if($place['id_tab'] == $table['id_tab']){
                    if($can->get_if_dys($place['id_can'])){
                        $class .= " dys";
                    }
                }
            }

            $html .= '<div class="' . $class . ($width < $height ? ' vertical' : '') . '" data-id_tab="' . $table['id_tab'] . '" style="position:absolute;width:' . $width . 'px;height:' . $height . 'px;top:' . $pos_y . 'px;left:' . $pos_x . 'px;">';
			if($pc != ""){
				$html .= '<span class="table-titre">' . $pc . '</span>';
			}

            $html .= '<span class="table-candidats-container">';

            foreach ($can_pos as $place){
                $color = $can->get_color_grp_from_id($place['id_can']);

                if($place['id_tab'] == $table['id_tab']){

                    $num_can = substr($place['no_candidat_can'], "1");

                    $html   .= '<span style="background-color:';

                    if($exa->is_fin_can($num_can)){
                        $html .= 'grey';
                    }else{
                        $html .= '#'.$color;
                    }

                    $html .= '" class="candidat';
                    if(!$exa->is_fin_can($num_can)) {
                        $html .= ' non_fini';
                    }
                    $html .= '" id_tab="' . $place['id_tab'] . '" ' . 'id_can ="'. $place['id_can'] .'" num_can="'. $num_can .'"' . '>' . $num_can . '</span>';



                }
            }

            $html .= '</span>';
            $html .= "</div>";
        }
        $html .= "</div>";
        return $html;
    }

    public function get_tables(){
        $args[":id_sle"] = $this->get_id_sle();
        $query = "SELECT * FROM t_table WHERE ref_sle=:id_sle ORDER BY ordre_tab";

        $tab = array();

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $e) {
            return false;
        }
    }

    function get_id_sle() {
        return $this->id_sle;
    }

    function get_nom() {
        return $this->nom;
    }

    function get_rue() {
        return $this->rue;
    }

    function get_npa() {
        return $this->npa;
    }

    function get_localite() {
        return $this->localite;
    }

    function set_id_sle($id_sle) {
        $this->id_sle = $id_sle;
    }

    function set_nom($nom) {
        $this->nom = $nom;
    }

    function set_rue($rue) {
        $this->rue = $rue;
    }

    function set_npa($npa) {
        $this->npa = $npa;
    }

    function set_localite($localite) {
        $this->localite = $localite;
    }



}

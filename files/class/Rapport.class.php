<?php

Class Rapport {

    private $heure_vnt_exa;
    private $event_exa;
    private $ref_exa;
    private $per_vnt;
    private $ref_sle;

    public function __construct() {
        $this->pdo = new PDO('mysql:dbname=' . BASE_NAME . ';host=' . SQL_HOST, SQL_USER, SQL_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8', PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC));
    }

    public function add($tab) {
        $args["heure_vnt_exa"] = $tab["heure_vnt_exa"];
        $args["event_exa"] = $tab["event_exa"];
        $args["ref_exa"] = $tab["ref_exa"];
        $args["per_vnt"] = $tab["per_vnt"];
        $args["ref_sle"] = $tab["ref_sle"];
        

        $query = "INSERT INTO t_event_exa SET "
                . "heure_vnt_exa = :heure_vnt_exa, "
                . "event_exa = :event_exa, "
                . "ref_exa = :ref_exa, "
                . "per_vnt = :per_vnt, "
                . "ref_sle = :ref_sle";

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
        } catch (Exception $ex) {
            return false;
        }
    }
    
    
    /**
     * 
     * Renvoie les rapports en fonction de l'id de l'examen
     * @Autor Remi Lopes
     */
    public function get_all_rapport_by_id($ref_exa) {
        $args[":ref_exa"] = $ref_exa;
        $query = "SELECT * "
                . "FROM t_event_exa "
                . "WHERE ref_exa = :ref_exa "
                . "ORDER BY heure_vnt_exa DESC ";
        
        $tab = array();
        
        try {
             $stmt = $this->pdo->prepare($query);
             $stmt->execute($args);
             $tab = $stmt->fetchAll();
             return $tab;
         } catch (Exception $e) {
             return false;
         }
    }
    
    /**
     * 
     * Delete un rapport 
     * @Autor Remi Lopes
     */
    public function del_rapport($id_vnt_exa) {
        $args['id_vnt_exa'] = $id_vnt_exa;
        
        $query = "DELETE "
               . "FROM t_event_exa "
               . "WHERE id_vnt_exa = :id_vnt_exa ";
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    function getHeure_vnt_exa() {
        return $this->heure_vnt_exa;
    }

    function getEvent_exa() {
        return $this->event_exa;
    }

    function getRef_exa() {
        return $this->ref_exa;
    }

    function getPer_vnt() {
        return $this->per_vnt;
    }
    
    function getRef_sle() {
        return $this->ref_sle;
    }

    function setHeure_vnt_exa($heure_vnt_exa) {
        $this->heure_vnt_exa = $heure_vnt_exa;
    }

    function setEvent_exa($event_exa) {
        $this->event_exa = $event_exa;
    }

    function setRef_exa($ref_exa) {
        $this->ref_exa = $ref_exa;
    }

    function setPer_vnt($per_vnt) {
        $this->per_vnt = $per_vnt;
    }
    
    function setRef_sle($ref_sle) {
        $this->ref_sle = $ref_sle;
    }

}

?>

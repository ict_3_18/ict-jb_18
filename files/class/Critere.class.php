<?php

class Critere EXTENDS Projet{
    //public $id;
    private $code;
    private $id;
    private $nom;
    private $description;
    private $id_cat;
    private $id_thm;
    private $specifique;
    private $pt_0;
    private $pt_1;
    private $pt_2;
    private $pt_3;
    
    
    //id_crt, code_crt, nom_crt, description_crt, abr_rubrique_crt, rubrique_crt, abr_categorie, categorie_crt, specifique_crt, theme_spec_crt
    
    
    /**
     * 
     * @param type $id
     */
     
    function __construct($id = 0) {
        
        $this->table_name = "t_criteres";
        $this->suffix = "_crt";
        
        if($id != 0){
            $this->set_id($id);
            parent::__construct($id);
                        
        }else{
            parent::__construct();
        }
        
    }

    /**
    * Initialisation de l'objet (l'id doit être setté)
    */
    public function init($id) {
        $query = "SELECT * FROM t_criteres WHERE id_crt=" . $this->get_id();
        $tab = $this->pdo->query($query)->fetch(PDO::FETCH_ASSOC);        
        $this->code = $tab['code_crt'];
        $this->nom = $tab['nom_crt'];
        $this->description = $tab['description_crt'];
        $this->id_cat = $tab['id_cat'];
        $this->id_thm = $tab['id_thm'];
        $this->specifique = $tab['specifique_crt'];
        $this->pt_3 = $tab['pt_3']; 
        $this->pt_2 = $tab['pt_2']; 
        $this->pt_1 = $tab['pt_1']; 
        $this->pt_0 = $tab['pt_0']; 
    }
    
    
    /**
    * Initialisation de l'objet (l'id doit être setté)
    */
    public function init_by_num($num) {
        //echo $num;
        if(strpos($num,".")){
            $tab = explode(".",$num);
        }
        if(strpos($num,",")){
            $tab = explode(",",$num);
        }else{
            if($num > 100){
                $tab[1] = $num;
                $tab[0] = 1;
            }
        }
        
        
        if(isset($tab[0]) && isset($tab[1])){
            //print_r($tab);
        
            $query = "SELECT * FROM t_criteres CRT JOIN t_categories CAT ON CRT.id_cat=CAT.id_cat  WHERE CRT.code_crt=" . $tab[1]." AND CAT.id_rub=".$tab[0];
            $tab_crt = $this->pdo->query($query)->fetch(PDO::FETCH_ASSOC);  
            $this->set_id($tab_crt['id_crt']);
            $this->init($tab_crt['id_crt']);  
            return true;
        }else{
            return false;
        }
        
    }
    
    public function get_all_criteres(){
        $query = "SELECT CONCAT(CAT.id_rub,'.',CRT.code_crt) AS num_crt 
                  FROM t_criteres CRT JOIN t_categories  CAT ON CRT.id_cat=CAT.id_cat WHERE  CRT.id_cat<6 ORDER BY CAT.id_rub, CRT.code_crt";
        $tab_crt = $this->pdo->query($query)->fetchAll(PDO::FETCH_ASSOC); 
        //print_r($tab_crt);
        foreach($tab_crt AS $crt){
            $tab[] = $crt['num_crt'];
        }
        return $tab;
    }
    
    public function get_all(){
        $query = "SELECT * FROM t_criteres ORDER by id_cat, code_crt";
        $tab = $this->pdo->query($query)->fetchAll(PDO::FETCH_ASSOC);
        //print_r($tab_crt);
        return $tab;
    }
    
    public function get_all_with_details(){
        $query = "SELECT * FROM t_criteres CRT JOIN t_rubriques RUB ON CRT.id_cat=RUB.id_rub LEFT JOIN t_themes THM ON CRT.id_thm=THM.id_thm ORDER by RUB.id_rub, CRT.code_crt";
        $tab = $this->pdo->query($query)->fetchAll(PDO::FETCH_ASSOC);
        //print_r($tab_crt);
        return $tab;
    }
    
    public function get_all_specifiques(){
        $query = "SELECT * FROM t_criteres CRT JOIN t_themes THM ON CRT.id_thm=THM.id_thm ORDER by THM.id_thm, CRT.code_crt";
        $tab = $this->pdo->query($query)->fetchAll(PDO::FETCH_ASSOC);
        //print_r($tab_crt);
        return $tab;
    }
    
    
    public function get_all_specifiques_by_thm($id_thm){
        $query = "SELECT * FROM t_criteres CRT JOIN t_themes THM ON CRT.id_thm=THM.id_thm WHERE CRT.id_thm=".$id_thm." ORDER by CRT.code_crt";
        $tab = $this->pdo->query($query)->fetchAll(PDO::FETCH_ASSOC);
        //print_r($tab_crt);
        return $tab;
    }
      
    public function get_next(){
        $query = "SELECT * FROM t_criteres WHERE code_crt=".($this->get_code()+1) ." AND id_cat=". $this->get_id_cat();
        $tab_crt = $this->pdo->query($query)->fetch(PDO::FETCH_ASSOC); 
        if($tab_crt['code_crt'] == $this->get_code()+1){
            return  $this->get_id_cat().".".$tab_crt['code_crt'];
        }else{
            $query = "SELECT * FROM t_criteres WHERE code_crt=1 AND id_cat=".($this->get_id_cat()+1);
            $tab_crt = $this->pdo->query($query)->fetch(PDO::FETCH_ASSOC); 
            if($tab_crt['code_crt'] == 1){
                return  ($this->get_id_cat()+1).".".$tab_crt['code_crt'];
                
            }else{
                return "1.1";
            }
        }
    }
    
    public function get_previous(){
        $query = "SELECT * FROM t_criteres WHERE code_crt=".($this->get_code()-1) ." AND id_cat=". $this->get_id_cat();
        $tab_crt = $this->pdo->query($query)->fetch(PDO::FETCH_ASSOC); 
        if($tab_crt['code_crt'] == $this->get_code()-1){
            $var = ($this->get_id_cat()).".".$tab_crt['code_crt'];
            if( $var  != "1." ){
                    return  $var;
                }else{
                     return "3.4";
                }
           // return  $this->get_id_cat().".".$tab_crt['code_crt'];
        }else{
            $query = "SELECT max(code_crt) AS code_crt FROM t_criteres WHERE  id_cat=".($this->get_id_cat()-1);
            $tab_crt = $this->pdo->query($query)->fetch(PDO::FETCH_ASSOC); 
            if($tab_crt['id_cat'] == $this->get_id_cat()-1){
                $var = ($this->get_id_cat()-1).".".$tab_crt['code_crt'];
                if( $var  != "1." ){
                    return  $var;
                }else{
                     return "3.4";
                }
            }else{
                return "3.4";
            }
        }
    }


    public function set_size($size){
        $query = "update t_criteres SET size_crt=".$size." WHERE id_crt =".$this->get_id();
        $this->pdo->query($query);
    }


    /*Getters*/
    
    function get_id() {
        return $this->id;
    }

    function get_code() {
        return $this->code;
    }

    function get_nom() {
        return $this->nom;
    }

    function get_description() {
        return $this->description;
    }
    
    function get_id_rubrique() {
        return $this->id_rubrique;
    }
    
    function get_nom_rubrique() {
        return $this->nom_rubrique;
    }
    
    function get_abr_categorie() {
        return $this->abr_categorie;
    }
    
    function get_categorie() {
        return $this->categorie;
    }

    function get_specifique() {
        return $this->specifique;
    }

    function get_theme_spec() {
        return $this->theme_spec;
    }
    
    function get_pt_0() {
        return $this->pt_0;
    }

    function get_pt_1() {
        return $this->pt_1;
    }

    function get_pt_2() {
        return $this->pt_2;
    }

    function get_pt_3() {
        return $this->pt_3;
    }
    
    function get_id_cat() {
        return $this->id_cat;
    }
    
    function get_id_thm() {
        return $this->id_thm;
    }
    
    /*Setters*/

    function set_id($id) {
        $this->id = $id;
    }

    function set_code($code) {
        $this->code = $code;
    }

    function set_nom($nom) {
        $this->nom = $nom;
    }

    function set_description($description) {
        $this->description = $description;
    }
    
    function set_id_rubrique($id_rubrique) {
        $this->id_rubrique = $id_rubrique;
    }
    
    function set_nom_rubrique($nom_rubrique) {
        $this->nom_rubrique = $nom_rubrique;
    }
    
    function set_abr_categorie($abr_categorie) {
        $this->abr_categorie = $abr_categorie;
    }
    
    function set_categorie($categorie) {
        $this->categorie = $categorie;
    }

    function set_id_thm($id_thm) {
        $this->id_thm = $id_thm;
    }
    
    function set_specifique($specifique) {
        $this->specifique = $specifique;
    }

    function set_theme_spec($theme_spec) {
        $this->theme_spec = $theme_spec;
    }
    
    function get_tab_crt(){
        $query = "SELECT * from t_criteres ORDER BY nom_crt";
        $tab_crt = $this->pdo->query($query)->fetchAll(PDO::FETCH_ASSOC);
        return $tab_crt;
    }
    
    /*function  get_tab_id_rubrique(){
        $query = "SELECT DISTINCT id_rubrique_crt from t_criteres";
        $tab_id_rubrique = $this->pdo->query($query)->fetch(PDO::FETCH_ASSOC);
        return $tab_id_rubrique;
    }*/
        
    /*function get_tab_crt_by_id_rubrique($id_rubrique){
        $query = "SELECT * FROM t_criteres WHERE id_rubrique_crt = \"" . $id_rubrique . "\" ORDER BY categorie_crt,code_crt, nom_crt";
        $tab_crt = $this->pdo->query($query)->fetchAll(PDO::FETCH_ASSOC);
       
        return $tab_crt;
    }*/
    
    /*function get_tab_crt_by_id_rubrique_by_cat($id_rubrique,$cat){
        $query = "SELECT * FROM t_criteres WHERE id_rubrique_crt = \"" . $id_rubrique . "\" AND categorie_crt = \"" . $cat . "\" ORDER BY categorie_crt,code_crt, nom_crt";
        $tab_crt = $this->pdo->query($query)->fetchAll(PDO::FETCH_ASSOC);
       
        return $tab_crt;
    }*/
    
    /*function get_tab_cat(){
        $query = "SELECT DISTINCT categorie_crt FROM t_criteres ORDER BY categorie_crt";
        $tab_cat = $this->pdo->query($query)->fetch(PDO::FETCH_ASSOC);
        return $tab_cat;  
    }*/
    
    /*function get_tab_theme_spec(){
        $query = "SELECT DISTINCT theme_spec_crt FROM t_criteres ORDER BY theme_spec_crt";
        $tab_theme_spec = $this->pdo->query($query)->fetch(PDO::FETCH_ASSOC);
        return $tab_theme_spec;  
    }*/
    
    /*function get_crt_spec_by_theme($theme){
        $query = "SELECT * FROM `t_criteres` WHERE theme_spec_crt = " . $theme;
        $tab_crt = $this->pdo->query($query)->fetchAll(PDO::FETCH_ASSOC);
        
        return $tab_crt;
        
    }*/
        
    function add_critere($code, $nom, $description, $id_cat, $id_thm, $specifique) {
        //Insertion du critere dans t_criteres 
        $query = 'INSERT INTO t_criteres ' 
               . '(code_crt, nom_crt, description_crt, id_cat, id_thm, specifique_crt) ' 
               . 'VALUES ("'.$code.'", "'.$nom.'", "'.$description.'", "'.$id_cat.'", "'.$id_thm.'" , "'.$specifique.'")'; 
        $stmt = $this->pdo->prepare($query); 
        $stmt->execute(); 
        $id_crt = $this->pdo->lastInsertId(); 
        return $id_crt; 
    } 
 
    function add_critere_tpi($id_crt, $id_tpi) {         
        //Insertion des criteres dans le tpi 
        $query = 'INSERT INTO t_ref_crt_tpi ' 
               . '(ref_crt, ref_tpi) ' 
               . 'VALUES ((SELECT id_crt ' 
                        . 'FROM t_criteres ' 
                        . 'WHERE id_crt='.$id_crt.'), ' 
                        . '(SELECT id_tpi ' 
                        . 'FROM t_tpi ' 
                        . 'WHERE id_tpi='.$id_tpi.'))'; 
        $stmt = $this->pdo->prepare($query); 
        $stmt->execute(); 
    } 
    
    public function __toString() {
        $str = "\n<pre>\n";
        foreach ($this AS $key => $val) {
            if ($key != "pdo") {
                $str .= "\t" . $key;
                $lengh_key = strlen($key);
                for ($i = strlen($key); $i < 20; $i++) {
                    $str .= "&nbsp;";
                }
                $str .= "=>&nbsp;&nbsp;&nbsp;" . $val . "\n";
            }
        }
        $str .= "\n</pre>";
        return $str;
    }

}


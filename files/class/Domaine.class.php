<?php

class Domaine EXTENDS Projet {

    private $id_dom;
    private $nom_dom;
    private $actif_dom;
    private $nb_mod;
    private $id_cla;

    function __construct($id = null) {
        parent::__construct();

        if ($id) {
            $this->set_id_dom($id);
            $this->init();
        }
    }

    public function init() {
        $query = "SELECT * FROM t_domaines WHERE id_dom=:id_dom";

        try {
            $stmt = $this->pdo->prepare($query);
            $args['id_dom'] = $this->get_id_dom();
            $stmt->execute($args);
            $tab = $stmt->fetch();

            $this->set_id_dom($tab['id_dom']);
            $this->set_nom_dom($tab['nom_dom']);
            $this->set_actif_dom($tab['actif_dom']);

            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

    //Vérifie si un lien entre une classe et un domaine existe déjà    
    public function get_nb_mod_by_cla($id_cla) {
        $query = "SELECT * FROM t_cla_dom WHERE id_cla=:id_cla AND id_dom=:id_dom";

        $tab["id_cla"] = $id_cla;
        $tab["id_dom"] = $this->get_id_dom();

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($tab);
            $tab = $stmt->fetch();
            if (sizeof($tab)) {
                return $tab['nb_mod'];
            }else{
                return 0;
            }
        } catch (Exception $ex) {
            return false;
        }
    }

    public function get_all($order = "nom_dom") {
        $query = "SELECT * FROM t_domaines WHERE actif_dom = 1 ORDER BY :order";

        $args[':order'] = $order;

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $ex) {
            return false;
        }
    }

    public function get_select_cla_dom($tab) {
        $query = "SELECT count(*) FROM t_cla_com "
                . "WHERE id_cla=:id_cla AND id_dom=:id_dom";
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($tab);
            $t = $stmt->fetchAll();
            return $t;
        } catch (Exception $ex) {
            return false;
        }
    }

    public function get_all_actif($order = "nom_dom") {
        $query = "SELECT * FROM t_domaines JOIN t_cla_dom ON t_domaine.id_dom = t_cla_dom.id_dom WHERE actif_dom = 1 ORDER BY :order";

        $args[':order'] = $order;

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $ex) {
            return false;
        }
    }

    public function get_all_cla_dom() {
        $query = "SELECT * FROM t_cla_dom ORDER BY id_cla";

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $ex) {
            return false;
        }
    }

    public function get_all_cla_dom_with_id($id) {
        $query = "SELECT * FROM t_cla_dom WHERE id_cla=" . $id;

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $ex) {
            return false;
        }
    }

    public function add_cla_dom($tab) {
        $query = "INSERT INTO t_cla_dom SET "
                . "id_cla = :id_cla, "
                . "id_dom = :id_dom, "
                . "nb_mod = :nb_mod";
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($tab);

            return true;
        } catch (Exception $exc) {
            return false;
        }
    }

    public function update_cla_dom($tab) {
        $query = "UPDATE t_cla_dom SET "
                . "id_cla = :id_cla, "
                . "id_dom = :id_dom, "
                . "nb_mod = :nb_mod "
                . "WHERE id_cla=:id_cla AND id_dom=:id_dom";
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($tab);

            return true;
        } catch (Exception $exc) {
            return false;
        }
    }

    public function delete_cla_dom($id_cla) {
        $query = "DELETE FROM t_cla_dom "
                . "WHERE id_cla=:id_cla AND id_dom=:id_dom";
        $tab['id_cla'] = $id_cla;
        $tab['id_dom'] = $this->get_id_dom();
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($tab);
            return true;
        } catch (Exception $exc) {
            return false;
        }
    }

    function get_id_dom() {
        return $this->id_dom;
    }

    function get_nom_dom() {
        return $this->nom_dom;
    }

    function get_actif_dom() {
        return $this->actif_dom;
    }

    function get_nb_mod() {
        return $this->nb_mod;
    }

    function get_id_cla() {
        return $this->id_cla;
    }

    function set_id_dom($id_dom) {
        $this->id_dom = $id_dom;
    }

    function set_nom_dom($nom_dom) {
        $this->nom_dom = $nom_dom;
    }

    function set_actif_dom($actif_dom) {
        $this->actif_dom = $actif_dom;
    }

    function set_nb_mod($nb_mod) {
        $this->nb_mod = $nb_mod;
    }

    function set_id_cla($id_cla) {
        $this->id_cla = $id_cla;
    }

}

<?php

namespace Barcode\Exceptions;

class InvalidLengthException extends BarcodeException {}
<?php

namespace Barcode\Exceptions;

class UnknownTypeException extends BarcodeException {}
<?php

namespace Barcode\Exceptions;

class InvalidFormatException extends BarcodeException {}
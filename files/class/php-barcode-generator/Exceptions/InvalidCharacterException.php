<?php

namespace Barcode\Exceptions;

class InvalidCharacterException extends BarcodeException {}
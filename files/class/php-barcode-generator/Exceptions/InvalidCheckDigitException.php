<?php

namespace Barcode\Exceptions;

class InvalidCheckDigitException extends BarcodeException {}
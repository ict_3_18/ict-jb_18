<?php

Class Entreprise {

    private $id_emp;
    private $nom_emp;
    private $pers_contact_emp;
    private $adresse_emp;
    private $npa_emp;
    private $localite_emp;
    private $conf_emp;
    private $payement_copies_emp;
    private $payement_envois_emp;

    //Constructeur
    public function __construct($id = null) {
        $this->pdo = new PDO('mysql:dbname=' . BASE_NAME . ';host=' . SQL_HOST, SQL_USER, SQL_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8', PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC));

        if ($id) {
            $this->set_id_emp($id);
            $this->init();
        }
    }

    //Initialisation
    public function init() {
        $query = "SELECT * FROM t_employeurs WHERE id_emp = :id_emp";
        try {
            $stmt = $this->pdo->prepare($query);
            $args['id_emp'] = $this->get_id_emp();
            $stmt->execute($args);
            $tab = $stmt->fetch();

            $this->set_nom_emp($tab['nom_emp']);
            $this->set_pers_contact_emp($tab['pers_contact_emp']);
            $this->set_adresse_emp($tab['adresse_emp']);
            $this->set_npa_emp($tab['npa_emp']);
            $this->set_localite_emp($tab['localite_emp']);
            $this->set_conf_emp($tab['conf_emp']);
            $this->set_payement_copies_emp($tab['payement_copies_emp']);
            $this->set_payement_envois_emp($tab['payement_envois_emp']);

            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function __toString() {
        $str = "\n<pre>\n";
        foreach ($this AS $key => $val) {
            if ($key != "pdo") {
                $str .= "\t" . $key;
                $length_key = strlen($key);
                for ($i = strlen($key); $i < 20; $i++) {
                    $str .= "&nbsp;";
                }
                $str .= "=>&nbsp;&nbsp;&nbsp;" . $val . "\n";
            }
        }
        $str .= "\n</pre>";
        return $str;
    }

    //Ajout d'un employeur dans la base de données
    public function add($tab) {
        //$tab d'arguments
        $args['nom_emp'] = $tab['nom_emp'];
        $args['pers_contact_emp'] = $tab['pers_contact_emp'];
        $args['adresse_emp'] = $tab['adresse_emp'];
        $args['npa_emp'] = $tab['npa_emp'];
        $args['localite_emp'] = $tab['localite_emp'];
        $args['conf_emp'] = $tab['conf_emp'];
        $args['payement_copies_emp'] = $tab['payement_copies_emp'];
        $args['payement_envois_emp'] = $tab['payement_envois_emp'];

        //requête
        $query = "INSERT INTO t_employeurs SET "
                . "nom_emp = :nom_emp, "
                . "pers_contact_emp = :pers_contact_emp, "
                . "adresse_emp = :adresse_emp, "
                . "npa_emp = :npa_emp, "
                . "localite_emp = :localite_emp, "
                . "conf_emp = :conf_emp, "
                . "payement_copies_emp = :payement_copies_emp, "
                . "payement_envois_emp = :payement_envois_emp ";

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
        } catch (Exception $e) {
            return false;
        }
        return $this->pdo->lastInsertId();
    }

    //Modification d'un employeur dans la base de données
    public function update($tab, $id) {
        //$tab d'arguments
        $args['nom_emp'] = $tab['nom_emp'];
        $args['pers_contact_emp'] = $tab['pers_contact_emp'];
        $args['adresse_emp'] = $tab['adresse_emp'];
        $args['npa_emp'] = $tab['npa_emp'];
        $args['localite_emp'] = $tab['localite_emp'];
        $args['conf_emp'] = $tab['conf_emp'];
        $args['payement_copies_emp'] = $tab['payement_copies_emp'];
        $args['payement_envois_emp'] = $tab['payement_envois_emp'];
        $args['id_emp'] = $id;

        //requête
        $query = "UPDATE t_employeurs SET "
                . "nom_emp = :nom_emp, "
                . "pers_contact_emp = :pers_contact_emp, "
                . "adresse_emp = :adresse_emp, "
                . "npa_emp = :npa_emp, "
                . "localite_emp = :localite_emp, "
                . "conf_emp = :conf_emp, "
                . "payement_copies_emp = :payement_copies_emp, "
                . "payement_envois_emp = :payement_envois_emp "
                . "WHERE id_emp = :id_emp";

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
        } catch (Exception $e) {
            //echo $e;
            return false;
        }
        return $this->pdo->lastInsertId();
    }

    //infos des employeurs
    public function get_all($order = "nom_emp") {

        $query = "SELECT * FROM t_employeurs ORDER BY " . $order;

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $e) {
            return false;
        }
    }

    //toutes les infos de l'employeur depuis l'id
    public function get_from_id($id_emp) {
        $args["id"] = $id_emp;

        $query = "SELECT * FROM t_employeurs WHERE id_emp = :id";

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $e) {
            return false;
        }
    }

    //nom de l'employeur, sa localité, mail, téléphone mobile et bureau, prénom et nom de la personne de contact
    public function get_emp_loc_mail_num() {

        $query = "SELECT id_emp, nom_emp, localite_emp, t_personnes.nom_per, t_personnes.prenom_per, t_personnes.email_per, t_personnes.tel_mobile_per, t_personnes.tel_bureau_per FROM t_employeurs 
JOIN t_personnes ON t_employeurs.pers_contact_emp = t_personnes.id_per 
ORDER BY nom_emp ASC";

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $e) {
            return false;
        }
    }

    public function check_no_doublon($nom_emp) {

        $query = "SELECT * FROM t_employeurs WHERE  = :";
        try {
            $stmt = $this->pdo->prepare($query);
            $args[':nom_emp'] = $nom_emp;

            $stmt->execute($args);
            $tab = $stmt->fetch();
            if (is_array($tab)) {
                return false;
            } else {
                return true;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    //Getter et Setter
    function get_id_emp() {
        return $this->id_emp;
    }

    function get_nom() {
        return $this->nom_emp;
    }

    function get_pers_contact_emp() {
        return $this->pers_contact_emp;
    }

    function get_adresse_emp() {
        return $this->adresse_emp;
    }

    function get_npa_emp() {
        return $this->npa_emp;
    }

    function get_localite_emp() {
        return $this->localite_emp;
    }

    function get_conf_emp() {
        return $this->conf_emp;
    }

    function get_payement_copies_emp() {
        return $this->payement_copies_emp;
    }

    function get_payement_envois_emp() {
        return $this->payement_envois_emp;
    }

    function set_id_emp($id_emp) {
        $this->id_emp = $id_emp;
    }

    function set_nom_emp($nom_emp) {
        $this->nom_emp = $nom_emp;
    }

    function set_pers_contact_emp($pers_contact_emp) {
        $this->pers_contact_emp = $pers_contact_emp;
    }

    function set_adresse_emp($adresse_emp) {
        $this->adresse_emp = $adresse_emp;
    }

    function set_npa_emp($npa_emp) {
        $this->npa_emp = $npa_emp;
    }

    function set_localite_emp($localite_emp) {
        $this->localite_emp = $localite_emp;
    }

    function set_conf_emp($conf_emp) {
        $this->conf_emp = $conf_emp;
    }

    function set_payement_copies_emp($payement_copies_emp) {
        $this->payement_copies_emp = $payement_copies_emp;
    }

    function set_payement_envois_emp($payement_envois_emp) {
        $this->payement_envois_emp = $payement_envois_emp;
    }

}

?>
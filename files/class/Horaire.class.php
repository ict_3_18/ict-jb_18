<?php

class Horaire Extends Classe
{

    public function add($tab)
    {
        $query = "INSERT INTO t_h_cadre SET "
            . "jour_cad = :jour_cad, "
            . "debut_cad = :debut_cad, "
            . "fin_cad = :fin_cad, "
            . "pause_cad = :pause_cad, "
            . "ref_cla = :ref_cla, "
            . "ref_bra = :ref_bra ";

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($tab);


            return $this->pdo->lastInsertId();
        } catch (Exception $exc) {
            return false;
        }
    }

    public function update($tab)
    {
        $query = "UPDATE t_h_cadre SET";
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($tab);

            return $this->pdo->lastInsertId();
        } catch (Exception $exc) {
            return false;
        }
    }

    /**
     * Supprime les horaires d'une classe sélectionnée
     */
    public function delete_horaire_by_cla($id)
    {
        $query = "DELETE FROM `t_h_cadre` WHERE ref_cla=" . $id;

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * Chercher tous les horaires d'une classe
     */
    public function get_horaire_cla($id)
    {
        $query = "SELECT * FROM `t_h_cadre` WHERE ref_cla=" . $id;

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $ex) {
            return false;
        }
    }


    /**
     * Trouver si un jour est selectionné par une tranche horaire
     */
    public function get_jour_in_horaire($cla, $jour)
    {
        $query = "SELECT * FROM `t_h_cadre` "
            . "WHERE ref_cla=" . $cla . " && jour_cad=" . $jour;

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            if (!empty($tab)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * Chercher une tranche d'horaire spécifique à une classe
     */
    public function get_tranche_index_cla($id_cla, $jour_cad, $t_hr)
    {
        $query = "SELECT * FROM `t_h_cadre`"
            . " WHERE ref_cla=" . $id_cla
            . " AND jour_cad=" . $jour_cad
            . " AND '" . $t_hr . "' >= debut_cad"
            . " AND '" . $t_hr . "' < fin_cad";
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetch();
            return $tab;
        } catch (Exception $ex) {
            return false;
        }
    }


    /**
     * @author Beruwalage Julian
     * @description Ces fonctions sont utlisées pour l'horaire des TPI
     */

    /**
     * Permet d'ajouter des horaires de TPI dans la base de données
     * @param $tab
     */
    public function add_h_tpi($tab)
    {
        // Check si début TPI
        $query =  "SELECT * FROM t_h_tpi WHERE id_tpi=:id_tpi";
        $args['id_tpi'] = $tab['id_tpi'];
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            if($stmt->rowCount() == 0){
                $tpi = new Tpi($tab['id_tpi']);
                $tab_date['id_tpi'] =  $tab['id_tpi'];
                $tab_date['date_debut_tpi'] =  $tab['date_hor'];
                $tpi->update_date($tab_date);
            }

            $query = "INSERT INTO t_h_tpi SET date_hor=:date_hor, debut_hor=:debut_hor, fin_hor=:fin_hor, total_day_hor=:total_day_hor, pauses_hor=:pauses_hor, ref_can=:ref_can, id_tpi=:id_tpi";
            try {
                $args['date_hor'] = $tab['date_hor'];
                $args['debut_hor'] = $tab['debut_hor'];
                $args['fin_hor'] = $tab['fin_hor'];
                $args['total_day_hor'] = $tab['total_day_hor'];
                $args['pauses_hor'] = $tab['pauses_hor'];
                $args['ref_can'] = $tab['ref_can'];
                $args['id_tpi'] = $tab['id_tpi'];
                $stmt = $this->pdo->prepare($query);
                $stmt->execute($args);
            } catch (PDOExeption $e) {
                echo $e->getMessage(), '<br/><br/>';
                echo _e('Erreur ajout de l\' ajout horaire TPI');
            }
        } catch (PDOExeption $e) {
            echo $e->getMessage(), '<br/><br/>';

            echo _e('Erreur ajout de l\' ajout horaire TPI');
        }


    }

    /**
     * Permet de supprimer l'horaires TPI d'un candidat
     * @param $id_can
     * @return bool
     */
    public function del_h_tpi($id_can){
        $query = "DELETE FROM t_h_tpi WHERE ref_can=:id_can";
        try {
            $args['id_can'] = $id_can;
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            return true;
        } catch (PDOExeption $e) {
            return false;
            echo $e->getMessage(), '<br/><br/>';
            echo _e('Erreur de suppression horaire TPI');
        }
    }


    /**
     * Permet de supprimer une période de TPI
     * @param $tab
     * @return bool
     */
    public function del_periode_h_tpi($tab){
        $query = "DELETE FROM t_h_tpi WHERE id_hor=:id_hor AND id_tpi=:id_tpi";
        try {
            $args['id_hor'] = $tab['id_hor'];
            $args['id_tpi'] = $tab['id_tpi'];
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            return true;
        } catch (PDOExeption $e) {
            return false;
            echo $e->getMessage(), '<br/><br/>';
            echo _e('Erreur de suppression periode horaire TPI');
        }
    }

    /**
     * Permet de récupérer les informations d'une période de TPI
     * @param $id_hor
     * @return array
     */
    public function get_info_h_tpi($id_hor){
        $query = "SELECT * FROM t_h_tpi WHERE id_hor=:id_hor";
        try {
            $args['id_hor'] = $id_hor;
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $result = $stmt->fetchAll();
            return $result;
        } catch (PDOExeption $e) {
            echo $e->getMessage(), '<br/><br/>';
            echo _e('Erreur affichage infos');
        }
    }

    /**
     * Permet de récupérer les informations d'un jour de TPI
     * @param $date_hor
     * @param $id_tpi
     * @return array
     */
    public function get_info_by_date_h_tpi($date_hor,$id_tpi){
        $query = "SELECT * FROM t_h_tpi WHERE date_hor=:date_hor AND id_tpi=:id_tpi";
        try {
            $args['date_hor'] = $date_hor;
            $args['id_tpi'] = $id_tpi;
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $result = $stmt->fetchAll();
            return $result;
        } catch (PDOExeption $e) {
            echo $e->getMessage(), '<br/><br/>';
            echo _e('Erreur affichage infos par date');
        }
    }

    /**
     * Permet de modifier une période de TPI
     * @param $tab
     */
    public function edit_h_tpi($tab){
        $query = "UPDATE t_h_tpi SET debut_hor=:debut_hor, fin_hor=:fin_hor, total_day_hor=:total_day_hor WHERE id_hor=:id_hor";
        try {
            $args['debut_hor'] = $tab['debut_hor'];
            $args['fin_hor'] = $tab['fin_hor'];
            $args['id_hor'] = $tab['id_hor'];
            $total_day = strtotime($args['fin_hor']) - strtotime($args['debut_hor']);
            $args['total_day_hor'] = ((date("G", $total_day) - 1) * 60) + (date("i", $total_day));
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
        } catch (PDOExeption $e) {
            echo $e->getMessage(), '<br/><br/>';
            echo _e('Erreur d\'edition de l\'horaire TPI');
        }
    }

    /**
     * Permet d'ajouter un jour de TPI
     * @param $tab
     */
    public function copy_by_date_h_tpi($tab){
        $query = "INSERT INTO t_h_tpi SET date_hor=:date_hor, debut_hor=:debut_hor, fin_hor=:fin_hor, total_day_hor=:total_day_hor, pauses_hor=:pauses_hor, ref_can=:ref_can, id_tpi=:id_tpi";
        try {
            $args['date_hor'] = $tab['date_hor'];
            $args['debut_hor'] = $tab['debut_hor'];
            $args['fin_hor'] = $tab['fin_hor'];
            $args['total_day_hor'] = $tab['total_day_hor'];
            $args['pauses_hor'] = $tab['pauses_hor'];
            $args['ref_can'] = $tab['ref_can'];
            $args['id_tpi'] = $tab['id_tpi'];
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
        } catch (PDOExeption $e) {
            echo $e->getMessage(), '<br/><br/>';
            echo _e('Erreur ajout horaire TPI');
        }
    }

    /**
     * Permet de récupérer tous l'horaire cadre de la classe du candidat
     * @param $id_cla
     * @return array|bool
     */
    function get_infos_cad_by_classe($id_cla)
    {
        $query = "SELECT * FROM t_h_cadre WHERE ref_cla = :id_cla ORDER BY t_h_cadre.jour_cad, t_h_cadre.debut_cad ASC";
        try {
            $args['id_cla'] = $id_cla;
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $result = $stmt->fetchAll();
            return $result;
        } catch (PDOExeption $e) {
            echo $e->getMessage(), '<br/><br/>';
            return false;
        }
    }

    /**
     * Permet de regrouper l'horaire TPI par date
     * @param $id_tpi
     * @return array|bool
     */
    function get_horaire_tpi($id_tpi)
    {
        $query = "SELECT t_h_tpi.id_hor, t_h_tpi.date_hor, GROUP_CONCAT(t_h_tpi.pauses_hor ORDER BY t_h_tpi.id_hor) AS full_pauses_hor, t_h_tpi.ref_can, t_h_tpi.id_tpi , GROUP_CONCAT(t_h_tpi.debut_hor, ' - ', t_h_tpi.fin_hor ORDER BY t_h_tpi.debut_hor) AS full_day_hor, GROUP_CONCAT(t_h_tpi.id_hor, '.',total_day_hor ORDER BY t_h_tpi.debut_hor) AS total_full_day_hor, SUM(total_day_hor) AS total, t_classes.duree_min_tpi_cla, t_classes.duree_max_tpi_cla
                  FROM t_h_tpi 
                  JOIN t_candidats ON t_h_tpi.ref_can = t_candidats.id_can
                  JOIN t_classes ON t_candidats.ref_classe = t_classes.id_cla
                  WHERE t_h_tpi.id_tpi = :id_tpi
                  GROUP BY date_hor
                  ORDER BY t_h_tpi.date_hor";
        try {
            $args['id_tpi'] = $id_tpi;
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $result = $stmt->fetchAll();
            return $result;
        } catch (PDOExeption $e) {
            echo $e->getMessage(), '<br/><br/>';
            return false;
        }
    }

    //todo : modification aporter !!!!!!!!!!! présentation
    /**
     * Permet de selectionner l'horaire tpi qui se trouve après un certain horaire
     * @param $tab
     * @return array|bool
     */
    function get_info_h_tpi_stop($tab){
        $query = "SELECT * FROM t_h_tpi WHERE id_hor > :id_hor AND id_tpi=:id_tpi AND date_hor >= :date_hor";
        try {
            $args['id_hor'] = $tab['id_hor'];
            $args['id_tpi'] = $tab['id_tpi'];
            $args['date_hor'] = $tab['date_hor'];
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $result = $stmt->fetchAll();
            return $result;
        } catch (PDOExeption $e) {
            echo $e->getMessage(), '<br/><br/>';
            return false;
        }
    }


    function get_info_h_tpi_by_id_tpi_and_date($tab){
        $query = "SELECT * FROM t_h_tpi
                  WHERE date_hor=:date_hor AND id_tpi=:id_tpi";
        try{
            $args['date_hor'] = $tab['date_hor'];
            $args['id_tpi'] = $tab['id_tpi'];
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $result = $stmt->fetchAll();
            return $result;
        }catch (PDOException $e){
            echo $e->getMessage(), '<br/><br/>';
            return false;
        }
    }


}

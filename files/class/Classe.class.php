<?php

class Classe EXTENDS Groupe {
    
    private $id_cla;
    private $nom_cla;
    private $couleur_cla;
    private $ref_grp;
    private $termine_cla;
    private $stop_form_cla;
    private $duree_min_tpi_cla;
    private $duree_max_tpi_cla;
    private $notes_open;

    function __construct($id = null) {
        parent::__construct();

        if ($id) {
            $this->set_id_cla($id);
            $this->init();
            parent::set_id_grp($this->ref_grp);
            parent::init();
        }
    }
    
    public function init() {
        $query = "SELECT * FROM t_classes WHERE id_cla=:id_cla";
        
        try {
            $stmt = $this->pdo->prepare($query);
            $args['id_cla'] = $this->get_id_cla();
            $stmt->execute($args);
            $tab = $stmt->fetch();
            
            $this->set_nom_cla($tab['nom_cla']);
            $this->set_duree_min_tpi_cla($tab['duree_min_tpi_cla']);
            $this->set_duree_max_tpi_cla($tab['duree_max_tpi_cla']);
            $this->set_ref_grp($tab['ref_grp']);
            $this->set_stop_form_cla($tab['stop_form_cla']);
            $this->set_termine_cla($tab['termine_cla']);
            $this->set_notes_open($tab['notes_open']);

            return true;
        } catch (Exception $ex) {
            return false;
        }
    }
    
    public function add($tab) {
        $query = "INSERT INTO t_classes SET "
            . "nom_cla = :nom_cla, "
                . "couleur_cla = :couleur_cla, "
                    . "ref_grp = :ref_grp, "
                        . "termine_cla = :termine_cla, "
                            . "stop_form_cla = :stop_form_cla, "
                                . "duree_min_tpi_cla = :duree_min_tpi_cla, "
                                    . "duree_max_tpi_cla = :duree_max_tpi_cla ";
                                    
                                    try {
                                        $stmt = $this->pdo->prepare($query);
                                        $stmt->execute($tab);
                                        
                                        
                                        return $this->pdo->lastInsertId();
                                    } catch (Exception $exc) {
                                        return false;
                                    }
    }
    
    
    public function __toString(){
        $str = "\n<pre>\n";
        foreach($this AS $key => $val){
            if($key != "pdo"){
                $str .= "\t".$key;
                $lengh_key = strlen($key);
                for($i=strlen($key);$i<20;$i++){
                    $str .= "&nbsp;";
                }
                $str .= "=>&nbsp;&nbsp;&nbsp;".$val."\n";
            }
        }
        $str .= "\n</pre>";
        return $str;
    }
    
    public function update($tab) {
        $query = "UPDATE t_classes SET "
            . "nom_cla = :nom_cla, "
                . "couleur_cla = :couleur_cla, "
                    . "ref_grp = :ref_grp, "
                        . "termine_cla = :termine_cla, "
                            . "stop_form_cla = :stop_form_cla, "
                                . "duree_min_tpi_cla = :duree_min_tpi_cla, "
                                    . "duree_max_tpi_cla = :duree_max_tpi_cla "
                                        . "WHERE id_cla=:id_cla";
                                        try {
                                            $stmt = $this->pdo->prepare($query);
                                            $stmt->execute($tab);
                                            
                                            return $this->pdo->lastInsertId();
                                        } catch (Exception $exc) {
                                            return false;
                                        }
    }
    
    
    /**
     * récupération des classes
     */
    public function get_all($order = "nom_cla") {
        $query = "SELECT * FROM t_classes ORDER BY " . $order;
        
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * Récupération des classes et des groupes triées par archivé
     * @param string $order
     * @author Colin Viatte
     * @date 06.01.2020
     * @return array|bool
     */
    public function get_all_with_grp($order = "archive_grp ASC, nom_grp, nom_cla"){
        $query = "SELECT * FROM t_classes CLA 
                  JOIN t_groupes GRP ON CLA.ref_grp=GRP.id_grp
                  ORDER BY " . $order;

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * Récupération des classes appartenant à un groupe actif (non archivé)
     * @author  Remi Lopes
     * @date    28.08.2019
     * @param   string $order
     * @return  array|bool
     */
    public function get_all_with_grp_actif($order = "nom_grp, nom_cla") {
        $query = "SELECT * FROM t_classes CLA 
                  JOIN t_groupes GRP ON CLA.ref_grp=GRP.id_grp
                  WHERE archive_grp=0
                  ORDER BY " . $order;

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $ex) {
            return false;
        }
    }

    
    /**
     * Récupère les classes actives (classe pas terminée et pas suspendues)
     */
    public function get_all_actifs($order = "nom_cla") {
        $query = "SELECT * FROM t_classes WHERE termine_cla=0 AND nom_cla <> '' ORDER BY " . $order;
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $ex) {
            return false;
        }
    }
    
    /**
     * récupère les données et ...
     */
    public function get_all_order_by_termine() {
        $query = "SELECT t_groupes.color_grp, t_groupes.nom_grp, id_cla, nom_cla, notes_open FROM t_classes "
            . "JOIN t_groupes ON t_classes.ref_grp = t_groupes.id_grp "
                . "ORDER BY termine_cla, id_grp, nom_cla";
                
                try {
                    $stmt = $this->pdo->prepare($query);
                    $stmt->execute();
                    $tab = $stmt->fetchAll();
                    return $tab;
                } catch (Exception $ex) {
                    return false;
                }
    }
    
    /**
     * récupération le nom et l'id d'une classe sélectionnée avec un id
     */
    public function get_cla_with_id($id) {
        $query = "SELECT nom_cla, id_cla FROM t_classes WHERE id_cla = :id";
        
        
        $args[':id'] = $id;
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $ex) {
            return false;
        }
    }
    
    /**
     * renvoie d'une classe sélectionné par un id avec le groupe correspondant
     */
    public function get_select_id_classe($id) {
        $query = "SELECT * FROM t_classes JOIN t_groupes ON t_classes.ref_grp = t_groupes.id_grp "
            . "WHERE id_cla = :id";
            
            $args[':id'] = $id;
            
            try {
                $stmt = $this->pdo->prepare($query);
                $stmt->execute($args);
                $tab = $stmt->fetchAll();
                return $tab;
            } catch (Exception $ex) {
                return false;
            }
    }
    
    /**
     * renvoie une classe sélectionné avec un id avec son groupe et ses candidats
     */
    public function get_select_cla_can_grp($id) {
        $query = "SELECT * FROM t_candidats "
            . "JOIN t_classes ON t_candidats.ref_classe = t_classes.id_cla "
                . "JOIN t_groupes ON t_classes.ref_grp = t_groupes.id_grp "
                    . "WHERE t_classes.id_cla = :id";
                    
                    $args[':id'] = $id;
                    
                    try {
                        $stmt = $this->pdo->prepare($query);
                        $stmt->execute($args);
                        $tab = $stmt->fetchAll();
                        return $tab;
                    } catch (Exception $ex) {
                        return false;
                    }
    }
    
    /**
     * récupère les données et ...
     */
    public function get_select_with_filter() {
        $query = "SELECT t_groupes.color_grp, t_groupes.nom_grp, id_cla, nom_cla FROM t_classes "
            . "JOIN t_groupes ON t_classes.ref_grp = t_groupes.id_grp "
                //. "WHERE termine_cla=0 AND nom_cla <> '' "
        . "ORDER BY nom_cla";
        
        
        
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $ex) {
            return false;
        }
    }
    
    public function get_all_nom_cla() {
        $query = "SELECT DISTINCT nom_cla, id_cla "
            . "FROM t_classes "
                . "WHERE termine_cla=0 AND nom_cla <> '' "
                    . "ORDER BY nom_cla";
                    
                    try {
                        $stmt = $this->pdo->prepare($query);
                        $stmt->execute();
                        $tab = $stmt->fetchAll();
                        return $tab;
                    } catch (Exception $ex) {
                        return false;
                    }
    }
    
    public function get_can_on_cla($id_cla) {
        $query = "SELECT  t_classes.id_cla , t_candidats.id_can FROM t_classes "
            . "LEFT JOIN t_candidats ON t_classes.id_cla = t_candidats.ref_classe "
                . "WHERE id_cla=" . $id_cla;
                try {
                    $stmt = $this->pdo->prepare($query);
                    if ($stmt->execute()) {
                        $tab = $stmt->fetchAll();
                        return $tab_cla_can;
                    } else {
                        return false;
                    }
                } catch (Exception $e) {
                    return false;
                }
    }
    
    
    
    /**
     * Sélectionne les classes actives avec la couleur de leur groupe
     */
    public function get_select_group_color($order = "nom_cla") {
        $query = "SELECT t_groupes.color_grp, t_groupes.nom_grp, id_cla, nom_cla FROM t_classes JOIN t_groupes ON t_classes.ref_grp = t_groupes.id_grp WHERE termine_cla=0 AND nom_cla <> '' ORDER BY nom_cla";
        
        $args[':order'] = $order;
        
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $ex) {
            return false;
        }
    }
    
    /**
     * Redonne un tableau de touts les groupes apartenant au classes
     */
    public function get_tab_grp_all_cla() {
        $query = "SELECT t_classes.id_cla,  t_groupes.id_grp, t_groupes.nom_grp FROM t_classes "
            . "LEFT JOIN t_groupes ON t_classes.ref_grp = t_groupes.id_grp";
            try {
                $stmt = $this->pdo->prepare($query);
                if ($stmt->execute()) {
                    $tab = $stmt->fetchAll();
                    $tab_cla_grp = Array();
                    foreach ($tab as $row) {
                        $tab_grp = array();
                        array_push($tab_grp, $row['id_grp'], $row["nom_grp"]);
                        $tab_cla_grp[$row['id_cla']][] = $tab_grp;
                    }
                    return $tab_cla_grp;
                } else {
                    return false;
                }
            } catch (Exception $e) {
                return false;
            }
    }
    
    /*
     * Redonne un tableau de touts les candidats apartenant à la classe donnée
     * (Perrin)
     */
    
    public function get_tab_can_one_cla($ref_cla) {
        $query = "SELECT t_candidats.id_can , t_candidats.nom_can , t_candidats.prenom_can ,"
            . " t_candidats.no_candidat_can FROM t_classes "
                . " LEFT JOIN t_candidats ON t_classes.id_cla = t_candidats.ref_classe "
                    . " WHERE id_cla = :id_cla ";
                    try {
                        $stmt = $this->pdo->prepare($query);
                        $args[':id_cla'] = $ref_cla;
                        if ($stmt->execute($args)) {
                            $tab = $stmt->fetchAll();
                            $tab_can = Array();
                            foreach ($tab as $row) {
                                array_push($tab_can, $row);
                            }
                            return $tab_can;
                        } else {
                            return false;
                        }
                    } catch (Exception $e) {
                        return false;
                    }
    }
    
    /*
     * Redonne un tableau de touts les candidats apartenant au classes
     * (Perrin)
     */
    
    public function get_tab_can_all_cla() {
        $query = "SELECT  t_classes.id_cla , t_candidats.id_can, t_candidats.no_candidat_can,"
            . " t_candidats.nom_can, t_candidats.prenom_can FROM t_classes "
                . "LEFT JOIN t_candidats ON t_classes.id_cla = t_candidats.ref_classe";
                try {
                    $stmt = $this->pdo->prepare($query);
                    if ($stmt->execute()) {
                        $tab = $stmt->fetchAll();
                        $tab_cla_can = Array();
                        foreach ($tab as $row) {
                            $tab_can = array();
                            array_push($tab_can, $row['id_can'], substr($row["no_candidat_can"], -3) . " - " . $row['nom_can'] . " " . $row['prenom_can']);
                            $tab_cla_can[$row['id_cla']][] = $tab_can;
                        }
                        return $tab_cla_can;
                    } else {
                        return false;
                    }
                    return $tab_return;
                } catch (Exception $ex) {
                    return false;
                }
    }
    
    /**
     * Récupère tous les tableaux avec en index l'id du candidat et dedans le nom et l'id de la classe
     * (Perrin)
     */
    public function get_all_actifs_in_array_with_id_can($order = "nom_cla") {
        $query = "SELECT DISTINCT nom_cla, id_cla , id_can FROM t_classes LEFT JOIN t_candidats ON t_classes.id_cla = t_candidats.ref_classe WHERE termine_cla=0 AND nom_cla <> '' ORDER BY " . $order;
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            $tab_return = array();
            foreach ($tab as $row) {
                if (!isset($tab_return[$row['id_can']])) {
                    $tab_return[$row['id_can']] = array(); //Crée la tableau si il existe pas
                }
                array_push($tab_return[$row['id_can']], $row["id_cla"], $row['nom_cla']); //Push le nom de la classe , l'id de la classe associé au candidat
            }
            return $tab_return;
        } catch (Exception $ex) {
            return false;
        }
    }

    public function lock(){
        $query = "UPDATE t_classes SET notes_open=0 WHERE id_cla=".$this->get_id_cla();
        $stmt = $this->pdo->prepare($query);
        return $stmt->execute();
    }

    public function unlock(){
        $query = "UPDATE t_classes SET notes_open=1 WHERE id_cla=".$this->get_id_cla();
        $stmt = $this->pdo->prepare($query);
        return $stmt->execute();
    }


    public function get_candidat_cla() {
        
    }
    
    function get_id_cla() {
        return $this->id_cla;
    }
    
    function get_nom_cla() {
        return $this->nom_cla;
    }
    
    function get_couleur_cla() {
        return $this->couleur_cla;
    }
    
    function get_ref_grp() {
        return $this->ref_grp;
    }
    
    function get_termine_cla() {
        return $this->termine_cla;
    }
    
    function get_stop_form_cla() {
        return $this->stop_form_cla;
    }
    
    function get_duree_min_tpi_cla() {
        return $this->duree_min_tpi_cla;
    }
    
    function get_duree_max_tpi_cla() {
        return $this->duree_max_tpi_cla;
    }
    
    function set_id_cla($id_cla) {
        $this->id_cla = $id_cla;
    }
    
    function set_nom_cla($nom_cla) {
        $this->nom_cla = $nom_cla;
    }
    
    function set_couleur_cla($couleur_cla) {
        $this->couleur_cla = $couleur_cla;
    }
    
    function set_ref_grp($ref_grp) {
        $this->ref_grp = $ref_grp;
    }
    
    function set_termine_cla($termine_cla) {
        $this->termine_cla = $termine_cla;
    }
    
    function set_stop_form_cla($stop_form_cla) {
        $this->stop_form_cla = $stop_form_cla;
    }
    
    function set_duree_min_tpi_cla($duree_min_tpi_cla) {
        $this->duree_min_tpi_cla = $duree_min_tpi_cla;
    }
    
    function set_duree_max_tpi_cla($duree_max_tpi_cla) {
        $this->duree_max_tpi_cla = $duree_max_tpi_cla;
    }

    /**
     * @return mixed
     */
    public function get_notes_open()
    {
        return $this->notes_open;
    }

    /**
     * @param mixed $notes_open
     */
    public function set_notes_open($notes_open)
    {
        $this->notes_open = $notes_open;
    }
    
}

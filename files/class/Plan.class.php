<?php

Class Plan EXTENDS Projet
{

    private $id_pls;
    private $nom;

    public function __construct($id = null)
    {
        parent::__construct();

        if ($id) {
            echo $this->set_id_pls($id);
            $this->init();
        }
    }

    /**
     * Initialisation de l'objet (l'id doit être setté)
     * @return boolean
     */
    public function init()
    {
        $query = "SELECT * FROM t_plans WHERE id_pls=:id_pls";
        try {
            $stmt = $this->pdo->prepare($query);
            $args = array();
            $args[':id_pls'] = $this->get_id_pls();


            $stmt->execute($args);
            $tab = $stmt->fetch();
            $this->set_nom($tab['nom_pls']);
        } catch (Exception $e) {
            return false;
        }
        return true;
    }


    public function __toString()
    {
        $str = "\n<pre>\n";
        foreach ($this AS $key => $val) {
            if ($key != "pdo") {
                $str .= "\t" . $key;
                $lengh_key = strlen($key);
                for ($i = strlen($key); $i < 20; $i++) {
                    $str .= "&nbsp;";
                }
                $str .= "=>&nbsp;&nbsp;&nbsp;" . $val . "\n";
            }
        }
        $str .= "\n</pre>";
        return $str;
    }


    public function get_tab_id_sle()
    {
        $query = "SELECT * FROM t_places WHERE ref_pls=:id_pls";
        try {
            $stmt = $this->pdo->prepare($query);
            $args = array();
            $args[':id_pls'] = $this->get_id_pls();

            $stmt->execute($args);
            $tab = $stmt->fetchAll();
            foreach ($tab AS $place) {
                $nom_sle = substr($place['num_pc'], 3, 4);
                // $tab_sle[substr($place['num_pc'],3,4)] = substr($place['num_pc'],3,4);
                $sle = new Salle();
                $sle->init_by_name($nom_sle);
                $tab_sle[$sle->get_id_sle()] = $sle->get_id_sle();

            }
            array_unique($tab_sle);
            //  print_r($tab_sle);
            return $tab_sle;
        } catch (Exception $e) {
            return false;
        }
        return false;
    }

    public function get_pos_can($id_can)
    {
        $can = new Candidat($id_can);
        $query = "SELECT * FROM t_places WHERE ref_pls=:id_pls AND num_can=:id_can";
        try {
            $stmt = $this->pdo->prepare($query);
            $args = array();
            $args[':id_pls'] = $this->get_id_pls();
            $args[':id_can'] = $can->get_no_candidat();

            $stmt->execute($args);
            $tab = $stmt->fetch();
            return $tab['num_pc'];
        } catch (Exception $e) {
            return false;
        }
        return false;
    }

    /**
     * Retourne tous les candidat lié à un plan avec son groupe pour la couleur
     * Utilisé dans l'attribution des places
     * @return array|bool
     * @author Alan Pena
     */
    public function get_all_candidats()
    {
        $query = 'SELECT DISTINCT CAN.id_can, CAN.no_candidat_can, GRP.color_grp
                  FROM t_plans PLS 
                  JOIN t_examens EXA ON EXA.ref_pls = PLS.id_pls
                  JOIN t_ref_can_exa REF_CAN ON REF_CAN.ref_exa = EXA.id_exa
                  JOIN t_candidats CAN ON CAN.id_can = REF_CAN.ref_can
                  JOIN t_classes CLA ON CLA.id_cla = CAN.ref_classe
                  JOIN t_groupes GRP ON GRP.id_grp = CLA.ref_grp
                  WHERE PLS.id_pls = :id_pls
                  ORDER BY GRP.nom_grp, CAN.no_candidat_can';
        try {
            $stmt = $this->pdo->prepare($query);
            unset($args);
            $args['id_pls'] = $this->get_id_pls();
            $stmt->execute($args);
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            return false;
        }
    }

    /**
     * Retourne tous les examens lié à un plan
     * Utilisé dans l'attribution des places
     * @return array|bool
     * @author Alan Pena
     */
    public function get_all_examens()
    {
        $query = 'SELECT DISTINCT DSC_MOD.num_mod 
                    FROM t_plans PLS 
                    JOIN t_examens EXA ON EXA.ref_pls = PLS.id_pls 
                    JOIN t_desc_modules DSC_MOD ON DSC_MOD.id_desc_mod = EXA.no_ich_exa 
                    WHERE PLS.id_pls = :id_pls';
        try {
            $stmt = $this->pdo->prepare($query);
            unset($args);
            $args['id_pls'] = $this->get_id_pls();
            $stmt->execute($args);
            return $stmt->fetchAll(PDO::FETCH_COLUMN);
        } catch (PDOException $e) {
            return false;
        }
    }

    /**
     * Retourne toutes les salles qui correspondent au plan,
     * en passant par la table t_ref_examens_salles
     * Utilisé dans l'attribution des places
     * @return array|bool
     * @author Alan Pena
     */
    public function get_all_salles()
    {
        $query = 'SELECT DISTINCT SLE.* 
                  FROM t_plans PLS 
                  JOIN t_examens EXA ON EXA.ref_pls = PLS.id_pls 
                  JOIN t_ref_examens_salles REF_EXA_SLE ON REF_EXA_SLE.ref_exa = EXA.id_exa 
                  JOIN t_salles SLE ON SLE.id_sle = REF_EXA_SLE.ref_sle 
                  WHERE PLS.id_pls = :id_pls';
        try {
            $stmt = $this->pdo->prepare($query);
            unset($args);
            $args['id_pls'] = $this->get_id_pls();
            $stmt->execute($args);
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            return false;
        }
    }

    /**
     * Retourne tout les plans de la base
     * Utilisé dans l'index de l'attribution des places
     * @return array|bool
     * @author Alan Pena
     */
    public function get_all_pls()
    {
        $query = 'SELECT  DISTINCT PLS.* FROM t_plans PLS
                  JOIN t_examens EXA ON PLS.id_pls = EXA.ref_pls
                  WHERE EXA.date_heure_exa >= :now';
        try {
            $stmt = $this->pdo->prepare($query);
            unset($args);
            $args['now'] = date('Y-m-d', strtotime(strval(date('Y-m-d')) . ' -1 year'));
            $stmt->execute($args);
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            return false;
        }
    }

    /**
     * Retourne les années des candidats
     * Exemple : [0] => 2014, [1] => 2015, [2] => 2016
     * @return array|bool
     */
    public function get_all_annees()
    {
        try {
            $query = 'SELECT DISTINCT CAN.annee_can
              FROM t_candidats CAN
              JOIN t_ref_can_exa REF_CAN_EXA ON CAN.id_can = REF_CAN_EXA.ref_can
              JOIN t_examens EXA ON REF_CAN_EXA.ref_exa = EXA.id_exa
              JOIN t_plans PLS ON EXA.ref_pls = PLS.id_pls
              WHERE PLS.id_pls = :ref_pls
              ORDER BY CAN.annee_can DESC';
            $stmt = $this->pdo->prepare($query);
            $stmt->execute([
                'ref_pls' => $this->id_pls
            ]);
            return $stmt->fetchAll(PDO::FETCH_COLUMN);
        } catch (PDOException $e) {
            return false;
        }
    }

    /**
     * Retourne les id_can en fonction de l'annee
     * @param $annee
     * @return array|bool
     */
    public function get_all_can_by_annee($annee)
    {
        try {
            $query = 'SELECT DISTINCT CAN.id_can
                      FROM t_candidats CAN
                      JOIN t_classes CLA ON CAN.ref_classe = CLA.id_cla
                      JOIN t_ref_can_exa REF_CAN_EXA ON CAN.id_can = REF_CAN_EXA.ref_can
                      JOIN t_examens EXA ON REF_CAN_EXA.ref_exa = EXA.id_exa
                      WHERE CAN.annee_can = :annee 
                      AND CLA.termine_cla = 0
                      AND EXA.ref_pls = :ref_pls
                      ORDER BY RAND()';
            $stmt = $this->pdo->prepare($query);
            $stmt->execute([
                'annee' => $annee,
                'ref_pls' => $this->id_pls
            ]);
            return $stmt->fetchAll(PDO::FETCH_COLUMN);
        } catch (PDOException $e) {
            return false;
        }
    }


    /**
     *
     * @param $id_sle
     * @param $id_exa
     * @return array|bool
     */
    public function get_all_can_by_sle($id_sle, $id_exa)
    {
        $query = "SELECT CAN.id_can, CAN.no_candidat_can, SLE.nom_sle, TAB.id_tab, PLC.id_plc, TAB.pcname_tab
                  FROM t_salles SLE
                  JOIN t_table TAB ON SLE.id_sle = TAB.ref_sle
                  JOIN t_places PLC ON TAB.id_tab = PLC.ref_tab
                  JOIN t_candidats CAN ON PLC.id_can = CAN.id_can
                  JOIN t_plans PLS ON PLC.ref_pls = PLS.id_pls
                  JOIN t_examens EXA ON PLS.id_pls = EXA.ref_pls
                  WHERE EXA.id_exa = :id_exa AND SLE.id_sle = :id_sle";
        $args['id_exa'] = $id_exa;
        $args['id_sle'] = $id_sle;

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (PDOException $e) {
            return false;
        }
    }

    /**
     * @param $id_can
     * @return array|bool
     */
    public function get_exa_by_can($id_can)
    {
        try {
            $query = 'SELECT EXA.id_exa
                      FROM t_examens EXA
                      JOIN t_ref_can_exa REF_CAN_EXA ON EXA.id_exa = REF_CAN_EXA.ref_exa
                      JOIN t_candidats CAN ON REF_CAN_EXA.ref_can = CAN.id_can
                      WHERE CAN.id_can = :id_can 
                      AND EXA.ref_pls = :ref_pls
                      AND EXA.id_exa IS NOT NULL';
            $stmt = $this->pdo->prepare($query);
            $stmt->execute([
                'id_can' => $id_can,
                'ref_pls' => $this->id_pls
            ]);
            return $stmt->fetchAll(PDO::FETCH_COLUMN);
        } catch (PDOException $e) {
            return false;
        }
    }

    /**
     * Insert le placement dans la base
     * @param $id_can
     * @param $id_tab
     * @return bool
     */
    public function add_placement($id_can, $id_tab)
    {
        try {
            $query = 'INSERT INTO t_places (ref_pls, id_can, ref_tab)
                      VALUES (:id_pls, :id_can, :ref_tab)';
            $stmt = $this->pdo->prepare($query);
            $stmt->execute([
                'id_pls' => $this->id_pls,
                'id_can' => $id_can,
                'ref_tab' => $id_tab
            ]);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /*
     * Récupère le placement entier d'un plan
     */
    public function get_placements()
    {
        $tab_candidats_par_tables = [];
        try {

            $query = 'SELECT DISTINCT PLC.ref_tab, TAB.ref_sle
                      FROM t_places PLC
                      JOIN t_table TAB ON PLC.ref_tab = TAB.id_tab
                      WHERE PLC.ref_pls = :id_pls';
            $stmt = $this->pdo->prepare($query);
            $stmt->execute([
                'id_pls' => $this->id_pls
            ]);
            $tab_places = $stmt->fetchAll();
        } catch (Exception $e) {
            return false;
        }
        foreach ($tab_places as $key => $place) {
            try {
                $query = 'SELECT PLC.id_can 
                          FROM t_places PLC
                          WHERE PLC.ref_pls = :id_pls
                          AND ref_tab = :id_tab';
                $stmt = $this->pdo->prepare($query);
                $stmt->execute([
                    'id_pls' => $this->id_pls,
                    'id_tab' => $place['ref_tab']
                ]);
                $tab_candidats = $stmt->fetchAll(PDO::FETCH_COLUMN);
                $tab_candidats_with_examens = [];
                foreach ($tab_candidats as $candidat) {
                    $tab_examens = $this->get_exa_by_can($candidat);
                    $tab_candidats_with_examens[] = [
                        'id_can' => $candidat,
                        'examens' => $tab_examens
                    ];
                }
                $tab_candidats_par_tables[$key]['candidats'] = $tab_candidats_with_examens;
            } catch (Exception $e) {
                return false;
            }
            $tab_candidats_par_tables[$key]['table'] = [
                'id_tab' => $place['ref_tab'],
                'id_sle' => $place['ref_sle'],
            ];
        }
        return $tab_candidats_par_tables;
    }

    /**
     * Crée le plan avec le nom en argument et associe les examens qui sont passer en argument
     * @param $nom_pls
     * @param $tab_exa
     * @return bool
     */
    public function create($nom_pls, $tab_exa)
    {
        if (empty($nom_pls) || empty($tab_exa)) {
            return false;
        }
        try {
            $query = 'INSERT INTO t_plans (nom_pls) VALUES (:nom_pls);';
            $stmt = $this->pdo->prepare($query);
            $stmt->execute([
                'nom_pls' => $nom_pls
            ]);
            $id_pls = $this->pdo->lastInsertId();
            foreach ($tab_exa as $exa) {
                $query = 'UPDATE t_examens SET ref_pls = :id_pls WHERE id_exa = :id_exa;';
                $stmt = $this->pdo->prepare($query);
                $stmt->execute([
                    'id_pls' => $id_pls,
                    'id_exa' => $exa
                ]);
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Supprime un plan et vide les placements qui lui appartenait
     * @return bool
     */
    public function delete()
    {
        try {
            $query = 'DELETE FROM t_plans WHERE id_pls = :id_pls';
            $stmt = $this->pdo->prepare($query);
            $stmt->execute([
                'id_pls' => $this->id_pls
            ]);
            $query = 'UPDATE t_examens EXA SET ref_pls = 0 WHERE EXA.ref_pls = :id_pls';
            $stmt = $this->pdo->prepare($query);
            $stmt->execute([
                'id_pls' => $this->id_pls
            ]);
            $this->reset();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Efface tous les placements d'un plan
     * @return bool
     */
    public function reset()
    {
        try {
            $query = 'DELETE FROM t_places WHERE ref_pls = :ref_pls';
            $stmt = $this->pdo->prepare($query);
            $stmt->execute([
                'ref_pls' => $this->id_pls
            ]);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Supprime la place de l'elève donné dans le plan
     * @Autor CP-16RDM
     */
    public function del_plc_can($id_can){
        try {
            $query = 'DELETE FROM t_places WHERE ref_pls = :ref_pls AND id_can = :id_can';
            $stmt = $this->pdo->prepare($query);
            $stmt->execute([
                'ref_pls' => $this->id_pls,
                'id_can' => $id_can
            ]);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @return mixed
     */
    public function get_id_pls()
    {
        return $this->id_pls;
    }

    /**
     * @param mixed $id_pls
     */
    public function set_id_pls($id_pls)
    {
        $this->id_pls = $id_pls;
    }

    /**
     * @return mixed
     */
    public function get_nom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function set_nom($nom)
    {
        $this->nom = $nom;
    }

}
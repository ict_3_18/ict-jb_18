<?php

class Notes extends Projet {

    private $id_can;

    public function __construct($id_can = null) {

        parent::__construct();

        if ($id_can) {
            $this->set_id_can($id_can);
        }
    }
    
    function get_notes_to_dom($id_dom) {
        $query = "SELECT DMO.num_mod, DMO.nom_mod, RCE.note_can, RCE.remedie_note "
                . "FROM t_examens EXA "
//                . "JOIN t_domaines DOM ON DOM.id_dom=EXA.id_dom "
                . "JOIN t_ref_can_exa RCE ON RCE.ref_exa=EXA.id_exa "
                . "JOIN t_candidats CAN ON CAN.id_can=RCE.ref_can "
                . "JOIN t_desc_modules DMO ON DMO.id_desc_mod=EXA.no_ich_exa "
                . "WHERE " //DOM.actif_dom=1 AND "
                . "EXA.date_heure_exa<NOW() AND RCE.note_can<>\"\" AND CAN.id_can=" . $this->get_id_can() . " AND EXA.id_dom=" . $id_dom . " "
                . "ORDER BY DMO.num_mod";

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            return($tab);
        } catch (Exception $e) {
            return false;
        }
    }
    
    //Pour le PDF du bulletin de notes
    function get_notes_valides_to_dom($id_dom) {
        $query = "SELECT DMO.num_mod, DMO.nom_mod, RCE.note_can, RCE.remedie_note, EXA.ltsi_exa, EXA.sans_note_exa
                  FROM t_examens EXA 
                  JOIN t_ref_can_exa RCE ON RCE.ref_exa=EXA.id_exa 
                  JOIN t_candidats CAN ON CAN.id_can=RCE.ref_can 
                  JOIN t_desc_modules DMO ON DMO.id_desc_mod=EXA.no_ich_exa 
                  WHERE RCE.note_can<>\"\" 
                    AND CAN.id_can=" . $this->get_id_can() . " 
                    AND EXA.id_dom=" . $id_dom . " 
                    AND RCE.remedie_note=0 
                    AND EXA.saisi_note_exa = 1
                ORDER BY DMO.num_mod";

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            return($tab);
        } catch (Exception $e) {
            return false;
        }
    }
    
    function get_avg_to_dom() {
        $query = "SELECT AVG(note_can) AS avg, DOM.nom_dom "
                . "FROM t_examens EXA "
                . "JOIN t_domaines DOM ON DOM.id_dom=EXA.id_dom "
                . "JOIN t_ref_can_exa RCE ON RCE.ref_exa=EXA.id_exa "
                . "JOIN t_candidats CAN ON CAN.id_can=RCE.ref_can "
                . "JOIN t_desc_modules DMO ON DMO.id_desc_mod=EXA.no_ich_exa "
                . "WHERE DOM.actif_dom=1 AND EXA.date_heure_exa<NOW() AND RCE.note_can<>\"\" AND RCE.remedie_note=0 AND CAN.id_can=".$this->get_id_can()." "
                . "GROUP BY DOM.nom_dom "
                . "ORDER BY DMO.nom_mod";

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            
            $retour = array();
            foreach ($tab AS $dom){
                $retour[$dom['nom_dom']] = $dom['avg'];
            }
            return($retour);
        } catch (Exception $e) {
            return false;
        }
    }
    
    

    function get_domaines() {
        $query = "SELECT DOM.* "
                . "FROM t_examens EXA "
                . "JOIN t_domaines DOM ON DOM.id_dom=EXA.id_dom "
                . "JOIN t_ref_can_exa RCE ON RCE.ref_exa=EXA.id_exa "
                . "JOIN t_candidats CAN ON CAN.id_can=RCE.ref_can "
//                . "WHERE DOM.actif_dom=1 AND EXA.date_heure_exa<NOW() "
                . "AND RCE.note_can<>\"\" AND RCE.remedie_note=0 AND CAN.id_can=" . $this->get_id_can()." " 
                . "GROUP BY DOM.nom_dom "
                . "ORDER BY DOM.nom_dom DESC";

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            return($tab);
        } catch (Exception $e) {
            return false;
        }
    }

    function get_color_avg_from_avg($id_dom) {
        $query = "SELECT AVG(RCE.note_can) AS avg 
                  FROM t_examens EXA 
                  JOIN t_domaines DOM ON DOM.id_dom=EXA.id_dom 
                  JOIN t_ref_can_exa RCE ON RCE.ref_exa=EXA.id_exa 
                  JOIN t_candidats CAN ON CAN.id_can=RCE.ref_can 
                  WHERE DOM.actif_dom=1 
                    AND EXA.date_heure_exa<NOW() 
                    AND RCE.note_can<>\"\" 
                    AND RCE.remedie_note=0 
                    AND EXA.saisi_note_exa = 1
                    AND CAN.id_can=" . $this->get_id_can() . " 
                    AND DOM.id_dom=" . $id_dom;

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetch();

            if (round($tab['avg'] * 2) / 2 > 4.0) {
                $tab["color"] = "success";
            } else if (round($tab['avg'] * 2) / 2 == 4.0) {
                $tab["color"] =  "warning";
            } else if (round($tab['avg'] * 2) / 2 < 4.0) {
                $tab["color"] =  "danger";
            }
            return $tab;
        } catch (Exception $e) {
            
        }
        return false;
    }


    function get_color_avg_valide_from_avg($id_dom) {
        $query = "SELECT AVG(RCE.note_can) AS avg 
                  FROM t_examens EXA 
                  JOIN t_domaines DOM ON DOM.id_dom=EXA.id_dom 
                  JOIN t_ref_can_exa RCE ON RCE.ref_exa=EXA.id_exa 
                  JOIN t_candidats CAN ON CAN.id_can=RCE.ref_can 
                  WHERE DOM.actif_dom=1 
                    AND EXA.date_heure_exa<NOW() 
                    AND RCE.note_can<>\"\" 
                    AND RCE.remedie_note=0 
                    AND EXA.saisi_note_exa = 1
                    AND CAN.id_can=" . $this->get_id_can() . " 
                    AND DOM.id_dom=" . $id_dom;

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetch();

            if (round($tab['avg'] * 2) / 2 > 4.0) {
                $tab["color"] = "success";
            } else if (round($tab['avg'] * 2) / 2 == 4.0) {
                $tab["color"] =  "warning";
            } else if (round($tab['avg'] * 2) / 2 < 4.0) {
                $tab["color"] =  "danger";
            }
            return $tab;
        } catch (Exception $e) {

        }
        return false;
    }

    function get_id_can() {
        return $this->id_can;
    }

    function set_id_can($id_can) {
        $this->id_can = $id_can;
    }

}

<?php

class Groupe EXTENDS Projet {

    private $id_grp;
    private $nom_grp;
    private $color_grp;
    private $ref_met;

    function __construct($id_grp = null) {
        parent::__construct();

        if ($id_grp) {
            $this->set_id_grp($id_grp);
            $this->init();
        }
    }

    public function init() {
        $query = "SELECT * FROM t_groupes WHERE id_grp=:id_grp";

        try {
            $stmt = $this->pdo->prepare($query);
            $args['id_grp'] = $this->get_id_grp();
            $stmt->execute($args);
            $tab = $stmt->fetch();

            $this->set_color_grp($tab["color_grp"]);
            $this->set_nom_grp($tab["nom_grp"]);
            $this->set_ref_met($tab["ref_met"]);

            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

    public function add($tab) {
        $query = "INSERT INTO t_groupes SET "
                . "nom_grp= :nom_grp, "
                . "color_grp = :color_grp, "
                . "archive_grp = :archive_grp";

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($tab);

            return $this->pdo->lastInsertId();
        } catch (Exception $exc) {
            return false;
        }
    }
    

    public function update($tab) {
        $query = "UPDATE t_groupes SET "
                . "nom_grp= :nom_grp, "
                . "color_grp = :color_grp, "
                . "archive_grp = :archive_grp "
                . "WHERE id_grp=:id_grp";

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($tab);

            return $tab;
        } catch (Exception $exc) {
            return false;
        }
    }
    
    /**
     * Modifie dans la base le groupe sélectionné 
     */
    public function update_actif($id_grp, $status) {
        $query = "UPDATE t_groupes "
                . "SET archive_grp = :status "
                . "WHERE id_grp=:id_grp";

        try {
            $stmt = $this->pdo->prepare($query);
            $args[':id_grp'] = $id_grp;
            $args[':status'] = $status;
            if ($stmt->execute($args)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }
    
    public function __toString() {
        $str = "\n<pre>\n";
        foreach($this AS $key => $val){
            if($key != "pdo"){
                $str .= "\t".$key;
                $lengh_key = strlen($key);
                for($i=strlen($key);$i<20;$i++){
                    $str .= "&nbsp;";
                }
                $str .= "=>&nbsp;&nbsp;&nbsp;".$val."\n";
            }
        }
        $str .= "\n</pre>";
        return $str;
    }

    /**
     * Récupération de tout les groupes
     */
    public function get_all($order = "nom_grp") {
        $query = "SELECT * FROM t_groupes ORDER BY " . $order;

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * Récupération de tout les groupes
     */
    public function get_all_order_by_archive() {
        $query = "SELECT * FROM t_groupes ORDER BY archive_grp ASC, id_grp DESC";


        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * Récupération de tout les groupes non archivé
     */
    public function get_all_no_archive() {
        $query = "SELECT * FROM t_groupes "
                . "WHERE archive_grp=0 "
                . "ORDER BY nom_grp";
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * récupération le nom et l'id d'un groupe sélectionnée avec un id
     */
    public function get_grp_with_id($id) {
        $query = "SELECT * FROM t_groupes WHERE id_grp = :id";


        $args[':id'] = $id;
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * Redonne un tableau de toutes les classes apartenant au groupe 
     */
    public function get_tab_all_cla_by_grp($id_grp) {
        $query = "SELECT * FROM t_groupes "
                . "LEFT JOIN t_classes ON t_classes.ref_grp = t_groupes.id_grp "
                . "WHERE t_groupes.id_grp=" . $id_grp;
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $e) {
            return false;
        }
    }


    /**
     * Récupération des groupes dont la classe n'a pas terminé sa formation
     */
    public function get_all_no_termine() {
        $query = "SELECT DISTINCT GRP.* "
                . "FROM t_groupes GRP "
                . "JOIN t_classes CLA ON CLA.ref_grp=GRP.id_grp "
                . "WHERE termine_cla=0 AND nom_cla <> '' "
                . "ORDER BY nom_grp";

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * Récupération des groupes dont la classe n'a pas terminé sa formation
     * Ne sélectionne qu'une seule fois les groupes
     */
    public function get_all_nom_groupe() {
        $query = "SELECT DISTINCT nom_grp, id_grp "
                . "FROM t_groupes GRP "
                . "JOIN t_classes CLA ON CLA.ref_grp=GRP.id_grp "
                . "WHERE termine_cla=0 AND nom_cla <> '' "
                . "ORDER BY nom_grp";

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * Renvoie toutes les classes d'un groupe
     */
    public function get_classes($id_grp) {
        $query = "SELECT * FROM t_classes CLA "
                . "JOIN t_groupes GRP ON GRP.id_grp=CLA.ref_grp"
                . "WHERE GRP.id_grp = :id_grp";

        $args[':id_grp'] = $id_grp;
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $ex) {
            return false;
        }
    }

    //Methodes Perrin

    /**
     * Redonne un tableau de toutes les classes apartenant au groupe 
     * (Perrin)
     */
    public function get_tab_cla_all_grp() {
        $query = "SELECT t_groupes.id_grp , t_classes.id_cla, t_classes.nom_cla FROM t_groupes "
                . "LEFT JOIN t_classes ON t_classes.ref_grp = t_groupes.id_grp";
        try {
            $stmt = $this->pdo->prepare($query);
            if ($stmt->execute()) {
                $tab = $stmt->fetchAll();
                $tab_grp_cla = Array();
                foreach ($tab as $row) {
                    $tab_cla = array();
                    array_push($tab_cla, $row['id_cla'], $row["nom_cla"]);
                    $tab_grp_cla[$row['id_grp']][] = $tab_cla;
                }
                return $tab_grp_cla;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Redonne un tableau de tous les candidats apartenant au groupe 
     * (Perrin)
     */
    public function get_tab_can_all_grp() {
        $query = "SELECT t_groupes.id_grp , t_candidats.id_can, t_candidats.no_candidat_can,"
                . " t_candidats.nom_can, t_candidats.prenom_can FROM t_groupes "
                . "LEFT JOIN t_classes ON t_classes.ref_grp = t_groupes.id_grp "
                . "LEFT JOIN t_candidats ON t_classes.id_cla = t_candidats.ref_classe";
        try {
            $stmt = $this->pdo->prepare($query);
            if ($stmt->execute()) {
                $tab = $stmt->fetchAll();
                $tab_grp_can = Array();
                foreach ($tab as $row) {
                    $tab_can = array();
                    array_push($tab_can, $row['id_can'], substr($row["no_candidat_can"], -3) . " - " . $row['nom_can'] . " " . $row['prenom_can']);
                    $tab_grp_can[$row['id_grp']][] = $tab_can;
                }
                return $tab_grp_can;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * récupération le nom d'un groupe sélectionnée avec un id
     * (Perrin)
     */
    public function get_nom_grp_with_id($id) {
        $query = "SELECT nom_grp FROM t_groupes WHERE id_grp = :id";

        $args[':id'] = $id;
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $tab = $stmt->fetch();
            return $tab['nom_grp'];
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * Redonne un tableau de tous les candidats apartenant a un groupe 
     * (Perrin)
     */
    public function get_tab_can_one_grp($ref_grp,$order="no_candidat_can ASC") {
        $query = "SELECT t_candidats.id_can , t_candidats.nom_can , t_candidats.prenom_can ,"
                . " t_candidats.no_candidat_can FROM t_groupes "
                . "LEFT JOIN t_classes ON t_classes.ref_grp = t_groupes.id_grp "
                . "LEFT JOIN t_candidats ON t_classes.id_cla = t_candidats.ref_classe "
                . "WHERE id_grp = :id_grp "
                . "ORDER BY ".$order."";
        try {
            $args[':id_grp'] = $ref_grp;
            $stmt = $this->pdo->prepare($query);
            if ($stmt->execute($args)) {
                $tab = $stmt->fetchAll();
                $tab_can = Array();
                foreach ($tab as $row) {
                    array_push($tab_can, $row);
                }
                return $tab_can;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }
    /**
     * redonne un tableau de tous les canditats dual apartenant a un groupe
     *
    */
    public function get_all_dual($ref_grp) {
        $query = "SELECT t_candidats.id_can , t_candidats.nom_can , t_candidats.prenom_can , t_candidats.systeme_can , t_candidats.no_candidat_can FROM t_groupes
                  LEFT JOIN t_classes ON t_classes.ref_grp = t_groupes.id_grp 
                  LEFT JOIN t_candidats ON t_classes.id_cla = t_candidats.ref_classe 
                  WHERE t_candidats.systeme_can != \"\" and id_grp = :id_grp";
        try {
            $args[':id_grp'] = $ref_grp;
            $stmt = $this->pdo->prepare($query);
            if ($stmt->execute($args)) {
                $tab = $stmt->fetchAll();
                $tab_can = Array();
                foreach ($tab as $row) {
                    array_push($tab_can, $row);
                }
                return $tab_can;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }
    /**
     * redonne un tableau de tous les canditats classic apartenant a un groupe
     *
     */
    public function get_all_calssic($ref_grp) {
        $query = "SELECT t_candidats.id_can , t_candidats.nom_can , t_candidats.prenom_can , t_candidats.systeme_can , t_candidats.no_candidat_can FROM t_groupes
                  LEFT JOIN t_classes ON t_classes.ref_grp = t_groupes.id_grp 
                  LEFT JOIN t_candidats ON t_classes.id_cla = t_candidats.ref_classe 
                  WHERE t_candidats.systeme_can = \"\" and id_grp = :id_grp";
        try {
            $args[':id_grp'] = $ref_grp;
            $stmt = $this->pdo->prepare($query);
            if ($stmt->execute($args)) {
                $tab = $stmt->fetchAll();
                $tab_can = Array();
                foreach ($tab as $row) {
                    array_push($tab_can, $row);
                }
                return $tab_can;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Redonne un tableau de tous les candidats n'ayant pas terminé apartenant a un groupe
     * (R.Lopes)
     */
    public function get_tab_can_one_grp_no_termine($ref_grp) {
        $query = "SELECT t_candidats.id_can , t_candidats.nom_can , t_candidats.prenom_can ,"
            . " t_candidats.no_candidat_can FROM t_groupes "
            . "LEFT JOIN t_classes ON t_classes.ref_grp = t_groupes.id_grp "
            . "LEFT JOIN t_candidats ON t_classes.id_cla = t_candidats.ref_classe "
            . "WHERE id_grp = :id_grp AND t_classes.termine_cla = 0";
        try {
            $args[':id_grp'] = $ref_grp;
            $stmt = $this->pdo->prepare($query);
            if ($stmt->execute($args)) {
                $tab = $stmt->fetchAll();
                $tab_can = Array();
                foreach ($tab as $row) {
                    array_push($tab_can, $row);
                }
                return $tab_can;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Récupère tout les tableaux avec en index l'id de la classe dedans le nom du groupe et l'id du groupe associé à cette classe 
     * (Perrin)
     */
    public function get_all_actifs_in_array_with_id_cla($order = "nom_grp") {
        $query = "SELECT DISTINCT nom_grp, id_grp , id_cla "
                . "FROM t_groupes GRP LEFT JOIN t_classes"
                . " CLA ON CLA.ref_grp=GRP.id_grp WHERE "
                . "termine_cla=0 AND nom_cla <> '' ORDER BY " . $order;
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            $tab_return = array();
            foreach ($tab as $row) {
                if (!isset($tab_return[$row['id_cla']])) {
                    $tab_return[$row['id_cla']] = array(); //Crée la tableau si il existe pas 
                }
                array_push($tab_return[$row['id_cla']], $row["id_grp"], $row['nom_grp']); //Push le nom du groupe et l'id du groupe associé a l'id de la classe 
            }
            return $tab_return;
        } catch (Exception $ex) {
            return false;
        }
    }
    
    
    /**
     * Récupère tout les tableaux avec en index l'id du candidat dedans le nom du groupe et l'id du groupe associé à ce candidat 
     * (Perrin)
     */
    public function get_all_actifs_in_array_with_id_can($order = "nom_grp"){
        $query = "SELECT DISTINCT nom_grp, id_grp , id_can FROM t_groupes"
                . " GRP LEFT JOIN t_classes CLA ON CLA.ref_grp=GRP.id_grp "
                . "LEFT JOIN t_candidats ON CLA.id_cla = t_candidats.ref_classe"
                . " WHERE termine_cla=0 AND nom_cla <> '' ORDER BY ".$order; 
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            $tab_return = array(); 
            foreach($tab as $row){
                if(!isset($tab_return[$row['id_can']])){
                    $tab_return[$row['id_can']] = array() ; //Crée la tableau si il existe pas 
                }
                array_push($tab_return[$row['id_can']], $row["id_grp"] , $row['nom_grp']); //Push le nom du groupe et l'id du groupe associé a l'id du candidat
            }
            return $tab_return;
        } catch (Exception $ex) {
            return false;
        }
    }
    
    /**
     * Tous les candidats des groupes non archivé avec des classes pas terminé
     * (Perrin)
     */
    public function get_tab_can_by_grp_no_archive_no_termine($id) {
        $query = "SELECT * FROM t_groupes "
                . "JOIN t_classes ON t_classes.ref_grp = t_groupes.id_grp "
                . "JOIN t_candidats ON t_classes.id_cla = t_candidats.ref_classe "
                . "WHERE t_classes.termine_cla=0 AND t_groupes.archive_grp=0 AND t_classes.nom_cla <> '' AND id_grp=" . $id. "
                ORDER BY t_candidats.no_candidat_can ASC";
        try {
            $stmt = $this->pdo->prepare($query);
            if ($stmt->execute()) {
                $tab = $stmt->fetchAll();
                return $tab;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    public function get_all_infos_met_by_id($id){
        $query = "SELECT DISTINCT MET.* FROM t_groupes GRP 
                  JOIN t_metiers MET ON GRP.ref_met = MET.id_met
                  WHERE MET.id_met =" . $id;
        try {
            $stmt = $this->pdo->prepare($query);
            if ($stmt->execute()) {
                $tab = $stmt->fetch();
                return $tab;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    function get_id_grp() {
        return $this->id_grp;
    }

    function get_nom_grp() {
        return $this->nom_grp;
    }

    function get_color_grp() {
        return $this->color_grp;
    }

    function set_id_grp($id_grp) {
        $this->id_grp = $id_grp;
    }

    function set_nom_grp($nom_grp) {
        $this->nom_grp = $nom_grp;
    }

    function set_color_grp($color_grp) {
        $this->color_grp = $color_grp;
    }

    public function set_ref_met($ref_met)
    {
        $this->ref_met = $ref_met;
    }

    public function get_ref_met()
    {
        return $this->ref_met;
    }
}

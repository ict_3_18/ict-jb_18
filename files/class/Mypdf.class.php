<?php

require WAY . 'class/fpdf/fpdf.php';
setlocale(LC_TIME, 'fr', 'fr_FR', 'fr_FR.ISO8859-1');

class Mypdf extends FPDF {
    /*     * *******************************************************************************
     * fonction : adresse_chef_exp
     *
     * Description :
     * 			Génère l'en-tête du PDF
     * ******************************************************************************* */

    function adresse_chef_exp() {
        $this->SetFont('arial', 'B', 10);
        $this->SetXY(25, 20);
        $this->Cell(10, 4, utf8_decode("Collège d'experts "), 0, 2, "J");
        $this->Cell(10, 4, "Jura bernois ", 0, 2, "J");
        $this->Cell(10, 4, " ", 0, 2, "J");
        $this->SetFont('arial', '', 10);
        $this->Cell(10, 4, "Jacques Hirtzel", 0, 2, "J");
        $this->Cell(10, 4, "Expert en chef ", 0, 2, "J");
        $this->Cell(10, 4, "Les Frasses 26", 0, 2, "J");
        $this->Cell(10, 4, "2612 Cormoret", 0, 2, "J");
        $this->Cell(10, 4, "Prof. : 032 942 43 15", 0, 2, "J");
        $this->Cell(10, 4, "Mobile : 078 755 02 83", 0, 2, "J");
        $this->image(WAY . "images/pdf/ours.png", 10, 100, 10);
    }

    function mention_copie($mention) {
        $this->SetFont('arial', 'B', 14);
        $this->SetTextColor(255, 0, 0);
        $this->SetXY(130, 30);
        $this->Cell(10, 4, $mention, 0, 2, "J");
        $this->SetTextColor(0, 0, 0);
    }

    function adresse_candidat($candidat) {
        $entreprise = new Entreprise($candidat->get_employeur());

        $this->SetFont('arial', 'B', 10);


        if ($candidat->get_systeme() == 'Dual') {
            // Affichage de l'adresse de l'employeur
            $this->SetXY(130, 40);
            $this->SetFont('arial', 'B', 12);
            if (!$entreprise->get_conf_emp()) {
                $this->Cell(30, 10, 'LETTRE SIGNATURE', 0, 2, 'J');
            } else {
                $this->Cell(30, 10, '', 0, 2, 'J');
            }
            $this->SetFont('arial', 'B', 10);
            $nom_emp = utf8_decode($entreprise->get_nom());
            if (strlen($nom_emp) > 30) {
                $nom_emp_1 = substr($nom_emp, 0, 30);
                $pos_separator = strrpos($nom_emp_1, " ");
                $nom_emp_1 = substr($nom_emp, 0, $pos_separator);
                $nom_emp_2 = substr($nom_emp, $pos_separator + 1);
                $this->Cell(30, 5, $nom_emp_1, 0, 2, 'J');
                $this->Cell(30, 5, $nom_emp_2, 0, 2, 'J');
            } else {
                $this->Cell(30, 5, $nom_emp, 0, 2, 'J');
            }
            $this->SetFont('arial', '', 10);
            $this->SetX(130);
            $this->Cell(30, 5, 'A l\'attention de :', 0, 2, 'J');
            $this->SetX(130);
            $this->SetFont('arial', 'B', 10);
            $emp = new personne($entreprise->get_pers_contact_emp());
            $this->Cell(30, 5, utf8_decode($emp->get_nom() . " " . $emp->get_prenom()), 0, 2, 'J');

            $this->SetX(130);
            $this->Cell(30, 5, utf8_decode($entreprise->get_adresse_emp()), 0, 2, 'J');

            $this->SetX(130);
            $this->Cell(30, 5, $entreprise->get_npa_emp() . ' ' . utf8_decode($entreprise->get_localite_emp()), 0, 2, 'J');
        } else {
            $this->SetXY(130, 50);
            // Affichage du nom, prnom et du numro de candidat
            $this->Cell(30, 10, utf8_decode($candidat->get_nom() . " " . $candidat->get_prenom() . " (" . $candidat->get_no_candidat_minus() . ")"), 0, 2, "J");
        }
    }

    function convert_hexa_to_RGB($hexadecimal){
        $RGB = array();
        $hexa = str_split($hexadecimal);
        $hexad = $hexa[0].
                $hexa[1].
                $hexa[1].
                $hexa[2].
                $hexa[2].
                $hexa[3].
                $hexa[3]
        ;
        list($r, $g, $b) = sscanf($hexad, "#%02X%02X%02X");
        $RGB['red'] = $r;
        $RGB['green'] = $g;
        $RGB['blue'] = $b;

        return $RGB;
    }

}

?>
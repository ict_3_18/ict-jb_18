<?php

class PdfRapport extends Mypdf {
    /*********************************************************************************
     * fonction : head_convoc
     *
     * Description : 
     * 			Génère l'en-tête du PDF
     *********************************************************************************/

    function head_rapport() {

    }



    public function gen_rapport($exa)
    {
        //pages
        $exa = new Examen($exa->get_id());
        $this->SetFont('Arial', '', 20);
        $this->SetAutoPageBreak(1, 15);
        $this->SetMargins(10, 10, 10, 10);
        $this->AddPage();
        $this->Cell(190, 12, "Rapport d'examen ICT-".$exa->get_num_nom(),0,1,"C");

        $this->Cell(190, 10, $exa->get_date_hrs_jour_date()." ".$exa->get_heure_db_exa(),0,1,"C");


        $this->SetFont('Arial', 'b', 12);
        $this->Cell(40, 10, "Salles : ",0,0,"L");
		$this->SetFont('Arial', '', 12);
        $tab_salles = $exa->get_salles_exa();
        foreach($tab_salles AS $salle) {
            $this->Cell(30, 10, $salle['nom_sle'], 0, 0, "L");
        }
		
		
		$this->Ln();
		$this->SetFont('Arial', 'b', 12);
        $this->Cell(22,6,utf8_decode("Moyens autorisés : "),0,0,"L");
		$this->SetFont('Arial', '', 12);
        $this->Ln();
        $this->MultiCell(186,6,utf8_decode($exa->get_materiel_exa()),1);
        $this->Ln();

		$this->Ln();	
		$this->SetFont('Arial', 'b', 12);
        $this->Cell(22,6,utf8_decode("Consignes de début d'examen : "),0,0,"L");
		$this->SetFont('Arial', '', 12);
        $this->Ln();
        $this->MultiCell(186,6,utf8_decode($exa->get_infos_begin_exa()),1);
        $this->Ln();

		
        
        //$this->Ln();
        $this->SetFont('Arial', '', 12);
        $evnt = new Evenement();
        $can = new Candidat();
        $sle = new Salle();
        $tab_evenement = $evnt->get_vnt_exa($exa->get_id());
        /*echo '<pre>';
        print_r($tab_evenement);
        echo '</pre>';*/
        $this->Ln();
		$this->SetFont('Arial', 'b', 12);
        $this->Cell(22,6,utf8_decode("Activités : "),0,0,"L");
		$this->SetFont('Arial', '', 12);
        $this->Ln();

        foreach(array_reverse($tab_evenement) AS $evt) {
            $this->Cell(17, 6, substr($evt['heure_vnt_exa'],0,5), 0, 0, "L");
            $this->Cell(14, 6, $sle->get_name_salle_by_id($evt['ref_sle']), 0, 0, "L");
            $pos_x = $this->GetX();
            /*if (utf8_decode(substr($evt['event_exa'],-3)) != $evt['fin_exa']) {
                $this->MultiCell(150, 6, utf8_decode($evt['event_exa']), 0);
            }*/
            $pos_y = $this->GetY();

            $this->SetXY($pos_x,$pos_y);
            $this->MultiCell(150, 6, utf8_decode($evt['event_exa']), 0);
//            $this->MultiCell(150, 6, utf8_decode($evt['event_exa'])." ".$evt['id_vnt_can'], 0);
//            $this->MultiCell(150, 6, utf8_decode($evt['event_exa'])." ".$can->"deleted"("1".substr($evt['event_exa'],-3)), 0);


        }
		$this->Ln();
		$this->SetFont('Arial', 'b', 12);
        $this->Cell(22,6,utf8_decode("Procès verbal de séance finale de correction : "),0,0,"L");
		$this->SetFont('Arial', '', 12);
        $this->Ln();
        $this->MultiCell(186,6,utf8_decode($exa->get_com_rapport()),1);
        $this->Ln();
		
		$this->Cell(40, 10, "St-Imier le : ___________________",0,0,"L");
		$this->Ln();
		$this->SetFont('Arial', 'b', 12);
        $this->Cell(40, 10, "Enseignant(s) : ",0,0,"L");
		$this->SetFont('Arial', '', 12);
        $tab_ens = $exa->get_profs_exam($exa->get_id());
        foreach($tab_ens AS $ens) {
            $this->Cell(70, 10, utf8_decode($ens['nom_per']." ".$ens['prenom_per']), 0, 0, "L");
        }

        $this->Ln();
		$this->SetFont('Arial', 'b', 12);
        $this->Cell(100, 10, "Experts : ",0,0,"L");
        $this->Cell(40, 10, "Signatures :",0,1,"L");
		$this->SetFont('Arial', '', 12);
        $tab_exp = $exa->get_expert_exam($exa->get_id());
        foreach($tab_exp AS $exp) {
			$this->Cell(20, 20, "",0,0,"L");
            $this->Cell(70, 20, utf8_decode($exp['nom_per']." ".$exp['prenom_per']), 0, 1, "L");
        }

		
    }

}

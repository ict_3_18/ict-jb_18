<?php
Class PDFRecap_note extends Mypdf{

    function Footer()
    {
        $this->SetY(285);
        $this->SetFont('Arial','',10);
        $this->Cell(60,10,utf8_decode("Contrôlé : __________________"),0,0,'L');

        $this->SetY(285);
        $this->SetX(155);
        $this->SetFont('Arial','',10);
        $this->Cell(60,10,"".date("Y-m-d H:i:s")."",0,0,'L');
    }

    public function gen_recap($id_exa)
    {
        $this->SetTextColor(0,0,0);
        $exa = new Examen($id_exa);

        require_once 'php-barcode-generator/BarcodeGeneratorPNG.php';
        $generatorPNG = new Barcode\BarcodeGeneratorPNG();

        $this->SetAutoPageBreak(1, 15);
        $this->SetMargins(15, 10, 10, 0);
        $this->AddPage();
        $fp = fopen('temp/data.png', 'w');
        fwrite($fp, $generatorPNG->getBarcode($exa->calcul_checksum(), $generatorPNG::TYPE_EAN_13)."");
        fclose($fp);

        $this->Image('temp/data.png',152,10,40,12,'PNG');
        $this->SetFont('Arial', '', 8);
        $this->SetXY(152,25);
        $this->Cell(40,0,$exa->calcul_checksum(),0,0,'C');

        $this->SetFont('Arial', 'B', 14);
        $this->SetXY(15,10);
        $tab_can = $exa->get_grp_cla_can();
        $count_candidats = count($tab_can);

        $this->Cell(101, 12, utf8_decode("Récapitulatif résultats CCO ICT ".$exa->get_num_nom()),0,1,"L");
        $this->SetFont('Arial', 'B', 11);
        $this->Cell(101, 1, utf8_decode("".$exa->get_nom_mod($exa->get_id()).""),0,1,"");
        $this->SetFont('Arial', '', 10);
        $this->Cell(101, 10, utf8_decode("".$exa->get_date_hrs_jour_date()." ".$exa->get_date_hrs_heure()." / ".$count_candidats." Candidats"),0,1,"L");

        $this->SetFont('Arial', 'B', 10);
        $this->Cell(10, 5,utf8_decode("N°"),1,0,'C');
        $this->Cell(80, 5,utf8_decode("Nom, prénom"),1,0,'L');
        $this->Cell(20, 5,"Groupe",1,0,'L');
        $this->Cell(20, 5,"Classe",1,0,'L');
        $this->Cell(18, 5,"Note 1/2",1,0,'L');
        $this->Cell(18, 5,utf8_decode("Échec(s)"),1,0,'C');
        $this->Cell(12, 5,"Check",1,1,'C');
        $this->SetFont('Arial', '', 10);

        $somme_notes = 0;
        $nb_echecs = 0;
        foreach($tab_can AS $candidat){
            $tab_couleur_classe = $this->convert_hexa_to_RGB($candidat['color_grp']);
            $this->SetFillColor($tab_couleur_classe['red'],$tab_couleur_classe['green'],$tab_couleur_classe['blue']);
            $num_can = str_split($candidat['no_candidat_can']);
            $this->Cell(10, 5, "" . $num_can[1] . $num_can[2] . $num_can[3]."", 1, 0, 'C', true);
            $this->Cell(80,5,utf8_decode("".$candidat['nom_can'].", ".$candidat['prenom_can'].""),1,0,'L');
            $this->Cell(20,5,"".$candidat['nom_grp']."",1,0,'L');
            $this->Cell(20,5,"".$candidat['nom_cla']."",1,0,'L');
            $this->Cell(18, 5,"".sprintf("%.1f",$candidat['note_can'])."",1,0,'C');
            if($candidat['note_can'] < 4) {
                $this->Cell(18, 5, "X", 1, 0, 'C');
                $nb_echecs = $nb_echecs + 1;
            }
            else
                $this->Cell(18, 5,"",1,0,'C');
            $this->Cell(12, 5,"",1,1,'C');
            $somme_notes += $candidat['note_can'];
        }
        $this->Cell(148,5,utf8_decode("Nombre d'échecs"),1,0,'L');
        $this->Cell(18,5,"".$nb_echecs." / ".$count_candidats."",1,1,'C');
        $this->Cell(130,5,"Moyenne",1,0,'L');
        $this->Cell(18,5,"".sprintf("%.2f",$somme_notes / $count_candidats)."",1,0,'C');
        $this->Cell(18,5,"".sprintf("%.0f",($nb_echecs / $count_candidats * 100))."%",1,1,'C');
        $this->SetFillColor(0,0,0);
        $this->SetTextColor(255,255,255);
        $this->Cell(166,5,"Dossiers de destination : Archives",1,0,'C',true);


        $this->SetTextColor(0,0,0);

        foreach($tab_can AS $candidat){
            $tab_grp[] = $candidat['nom_grp'];
        }
        $tab_grp_unique = array_unique($tab_grp);

        foreach($tab_grp_unique AS $groupe) {
            foreach ($tab_can AS $value) {
                if($value['nom_grp'] == $groupe) {
                    $tab_value[$groupe][] = $value['nom_grp'];
                    $count_candidat_by_groupe[$groupe] = count($tab_value[$groupe]);
                }
            }
        }

        foreach($tab_grp_unique AS $groupe) {
            $this->SetFont('Arial', 'B', 14);
            $this->SetAutoPageBreak(1, 15);
            $this->SetMargins(15, 10, 10, 0);
            $this->AddPage();
            $this->Cell(110, 12, utf8_decode("Récapitulatif résultats CCO ICT " . $exa->get_num_nom()), 0, 1, "L");
            $this->SetFont('Arial', 'B', 11);
            $this->Cell(101, 1, utf8_decode("" . $exa->get_nom_mod($exa->get_id()) . ""), 0, 1, "");
            $this->SetFont('Arial', '', 10);
            $this->Cell(101, 10, utf8_decode("" . $exa->get_date_hrs_jour_date() . " " . $exa->get_date_hrs_heure() . " / " . $count_candidat_by_groupe[$groupe] . " Candidats"), 0, 1, "L");

            $this->SetFont('Arial', 'B', 10);
            $this->Cell(10, 5, utf8_decode("N°"), 1, 0, 'C');
            $this->Cell(100, 5, utf8_decode("Nom, prénom"), 1, 0, 'L');
            $this->Cell(20, 5, "Classe", 1, 0, 'L');
            $this->Cell(18, 5, "Note 1/2", 1, 0, 'L');
            $this->Cell(18, 5, utf8_decode("Échec(s)"), 1, 0, 'C');
            $this->Cell(12, 5, "Check", 1, 1, 'C');
            $this->SetFont('Arial', '', 10);

            $somme_notes_grp[$groupe] = 0;
            $nb_echecs_grp[$groupe] = 0;
            foreach ($tab_can AS $candidat) {
                if($candidat['nom_grp'] == $groupe) {
                    $tab_couleur_classe = $this->convert_hexa_to_RGB($candidat['color_grp']);
                    $this->SetFillColor($tab_couleur_classe['red'], $tab_couleur_classe['green'], $tab_couleur_classe['blue']);
                    $num_can = str_split($candidat['no_candidat_can']);
                    $this->Cell(10, 5, "" . $num_can[1] . $num_can[2] . $num_can[3] . "", 1, 0, 'C', true);
                    $this->Cell(100, 5, utf8_decode("" . $candidat['nom_can'] . ", " . $candidat['prenom_can'] . ""), 1, 0, 'L');
                    $this->Cell(20, 5, "" . $candidat['nom_cla'] . "", 1, 0, 'L');
                    $this->Cell(18, 5, "" . sprintf("%.1f", $candidat['note_can']) . "", 1, 0, 'C');
                    if ($candidat['note_can'] < 4) {
                        $this->Cell(18, 5, "X", 1, 0, 'C');
                        $nb_echecs_grp[$groupe] = $nb_echecs_grp[$groupe] + 1;
                    } else
                        $this->Cell(18, 5, "", 1, 0, 'C');
                    $this->Cell(12, 5, "", 1, 1, 'C');
                    $somme_notes_grp[$groupe] += $candidat['note_can'];
                }
            }
            $this->Cell(148, 5, utf8_decode("Nombre d'échecs"), 1, 0, 'L');
            $this->Cell(18, 5, "" . $nb_echecs_grp[$groupe] . " / " . $count_candidat_by_groupe[$groupe] . "", 1, 1, 'C');
            $this->Cell(130, 5, "Moyenne", 1, 0, 'L');
            $this->Cell(18, 5, "" . sprintf("%.2f", $somme_notes_grp[$groupe] / $count_candidat_by_groupe[$groupe]) . "", 1, 0, 'C');
            $this->Cell(18, 5, "" . sprintf("%.0f", ($nb_echecs_grp[$groupe] / $count_candidat_by_groupe[$groupe] * 100)) . "%", 1, 1, 'C');
            $this->Cell(166, 5, "Dossiers de destination : ".$groupe, 1, 0, 'C', true);

            $this->Image('temp/data.png',152,10,40,12,'PNG');
            $this->SetFont('Arial', '', 8);
            $this->SetXY(152,25);
            $this->Cell(40,0,$exa->calcul_checksum(),0,0,'C');

            $this->SetFont('Arial', 'B', 14);
            $this->SetXY(15,10);
        }
    }
}

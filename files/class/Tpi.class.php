<?php

class Tpi extends Projet
{


    private $id;
    private $id_exa;
    private $id_can;
    private $nom;
    private $id_exp_1;
    private $id_exp_2;
    private $id_m_app;
    private $date_db;
    private $lieu;
    private $classe;
    private $nom_can;
    private $prenom_can;
    private $numero_can;
    private $crt_def;

    public function __construct($id = null)
    {
        parent::__construct();

        if ($id) { //Si on a initialisé l'id en argument
            $this->set_id($id);
            $this->init();
        }
    }

    /*
    * Initialisation de l'objet
    * @return boolean
    */
    public function init($id = 0)
    {
        $query = "SELECT * FROM t_tpi TPI 
                    JOIN t_candidats CAN ON TPI.ref_can=CAN.id_can 
                    JOIN t_classes CLA ON CAN.ref_classe=CLA.id_cla
                    WHERE TPI.id_tpi=:id_tpi";//
        try {
            $stmt = $this->pdo->prepare($query);
            $args[':id_tpi'] = $this->get_id();

            $stmt->execute($args);
            $tab = $stmt->fetch();

            $this->set_nom($tab['nom_tpi']);
            $this->set_nom_can($tab['nom_can']);
            $this->set_prenom_can($tab['prenom_can']);
            $this->set_numero_can($tab['no_candidat_can']);
            $this->set_id_exa($tab['ref_exa']);
            $this->set_id_can($tab['ref_can']);
            $this->set_classe($tab['nom_cla']);
            $this->set_date_db($tab['date_debut_tpi']);
            //$this->set_id_exp_1($tab['ref_exp_1']);
            //$this->set_id_exp_2($tab['ref_exp_2']);
            //$this->set_id_m_app($tab['ref_maitre_app']);
            $this->set_lieu($tab['lieu_tpi']);
            $this->set_crt_def($tab['crt_def_tpi']);

            //TODO Ajouter les objectifs
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    function update($data)
    {

        $query = "UPDATE t_tpi SET ";
        foreach ($data AS $field => $value) {
            $query .= $field . "=:" . $field . ", ";
            $args[$field] = $value;
        }
        $query = substr($query, 0, -2);
        $query .= " WHERE id_tpi = " . $this->get_id();

        //echo $query;
        $stmt = $this->pdo->prepare($query);
        $stmt->execute($args);
    }

    /**
     * renvoie tous les TPI d'une année
     * @param $id_exa
     * @return array|bool
     */
    public function get_all_tpi_by_id_exa($id_exa)
    {
        $query = "SELECT * FROM t_tpi TPI JOIN t_candidats CAN ON TPI.ref_can=CAN.id_can WHERE ref_exa=:id_exa ORDER BY no_candidat_can";
        try {
            $stmt = $this->pdo->prepare($query);

            $args[':id_exa'] = $id_exa;

            $stmt->execute($args);
            $tab = $stmt->fetchAll();

            return $tab;

        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    /**
     * renvoie tous les TPI d'une année
     * @param $id_exa
     * @return array|bool
     */
    public function get_all_tpi_by_id_exa_and_id_per($id_exa,$id_per){
//        $query = "SELECT * FROM t_tpi TPI
//					JOIN t_candidats CAN ON TPI.ref_can=CAN.id_can
//					WHERE ref_exa=:id_exa
//					AND ( ref_exp_1 = :id_per
//						OR ref_exp_2 = :id_per
//						OR  ref_maitre_app = :id_per)
//					ORDER BY no_candidat_can";

        $query = "SELECT * FROM t_tpi TPI 
					JOIN t_candidats CAN ON TPI.ref_can=CAN.id_can
					JOIN t_tpi_per TPI_PER ON TPI.id_tpi = TPI_PER.id_tpi
					JOIN t_fonctions_tpi FNT ON TPI_PER.id_fnt = FNT.id_fnt 
					WHERE ref_exa=:id_exa AND (TPI_PER.id_per = :id_per)";
        try {
            $stmt = $this->pdo->prepare($query);

            $args[':id_exa'] = $id_exa;
            $args[':id_per'] = $id_per;

            $stmt->execute($args);
            $tab = $stmt->fetchAll();

            return $tab;

        } catch (Exception $e) {
            return false;
        }
        return true;
    }


    public function __toString()
    {
        $str = "\n<pre>\n";
        foreach ($this AS $key => $val) {
            if ($key != "pdo") {
                $str .= "\t" . $key;
                $length_key = strlen($key);
                for ($i = strlen($key); $i < 20; $i++) {
                    $str .= "&nbsp;";
                }
                $str .= "=>&nbsp;&nbsp;&nbsp;" . $val . "\n";
            }
        }
        $str .= "\n</pre>";
        return $str;
    }


    public function get_horaire()
    {
        /* mise à jour de l'ancienne version  */
        /*$query = "UPDATE `t_h_tpi` SET `id_tpi` = '" . $this->get_id() . "' WHERE `t_h_tpi`.`ref_can` = " . $this->get_id_can();
        $stmt = $this->pdo->prepare($query);
        $stmt->execute();
*/
        $query = "SELECT * FROM t_h_tpi WHERE id_tpi=" . $this->get_id() . " ORDER BY date_hor, debut_hor";
        $stmt = $this->pdo->prepare($query);
        $stmt->execute();
        $tab_horaire = $stmt->fetchAll();

        $new_tab = array();
        foreach ($tab_horaire AS $horaire) {
            $tmp['db'] = $horaire['debut_hor'];
            $tmp['fin'] = $horaire['fin_hor'];
            if (isset($new_tab[$horaire['date_hor']])) {
                $index = sizeof($new_tab[$horaire['date_hor']]) - 1;
                $time_fin_prev = strtotime($new_tab[$horaire['date_hor']][$index]['fin']);
                $time_db_new = strtotime($tmp['db']);
                if ($time_fin_prev + 1200 < $time_db_new) {
                    $new_tab[$horaire['date_hor']][] = $tmp;
                } else {
                    $new_tab[$horaire['date_hor']][$index]['fin'] = $horaire['fin_hor'];
                }
            } else {
                $new_tab[$horaire['date_hor']][] = $tmp;
            }


        }
        /*echo "<pre>";
        print_r($new_tab);
        echo "</pre>";*/
        return $new_tab;

    }


    public function gen_horaire_tpi()
    {
        $str = "<h3>Horaire de travail</h3>";
        $tab_horaire = $this->get_horaire();

        $str .= "<table class=\"table table-striped\">
                <tr>
                    <th>Dates</th>
                    <th>Heures</th>
                </tr>
                ";
        $date = "";
        foreach ($tab_horaire AS $date => $horaire) {
            $str .= "<tr>";
            $str .= "<td>" . strftime("%A %d %B", strtotime($date)) . "</td>";
            $str .= "<td>";
            foreach ($horaire AS $periode) {
                $str .= $periode['db'] . " - " . $periode['fin'] . " et ";
            }
            $str = substr($str, 0, -4);
            $str .= "</td>";
            $str .= "</tr>";
        }
        $str .= "</table>";

        return $str;
    }

    public function gen_message_attrib_tpi($can)
    {

        $emp = new Entreprise($can->get_employeur());
        //setlocale (LC_TIME, 'fr_FR.utf8','fra');

        //$message = (include "../TPI/includes/mail.include.php");
       $message = $this->mail_info_gen($can);
       // $message .= "test";
        return ($message);
    }

    public function mail_info_gen(){

        $ma_app = new Personne($this->get_id_m_app());
        $exp_1 = new Personne($this->get_id_exp_1());
        $exp_2 = new Personne($this->get_id_exp_2());
        $can = new Candidat($this->get_id_can());
        $emp = new Entreprise($can->get_employeur());
        $str = "<div class=\"container\">
        <div class=\"panel panel-primary\">
            <div class=\"panel-heading\">
                <h3>TPI - ". $this->get_nom() ."</h3>
            </div>
            <div class=\"panel-body\">
            <div class=\"row\">
                <label for=\"receiver\" class=\"col col-md-2\">Destinataires :</label>
                <input id=\"receiver\" name=\"receiver\" type=\"text\" class=\"col col-md-10\" value=\"".$ma_app->get_email()."\"><br>
            </div>
            <div class=\"row\">
                <label for=\"cc\" class=\"col col-md-2\">CC :</label>
                <input id=\"cc\" name=\"cc\" type=\"text\" class=\"col col-md-10\" value=\"".$exp_1->get_email()."; ".$exp_2->get_email()."; "."\"><br>
            </div><br>
            <textarea id='ex_tpi' name='ex_tpi' rows='4' cols='100' form='mail_form'>Le TPI ci-dessus vous est attribué en tant que supérieur ou expert, vous trouvez ci-dessous les coordonnées des autres personnes impliquées.
L'horaire de travail ainsi que le cahier des charges vous seront remis par le supérieur du candidat.</textarea><br>";

        $str .= $this->gen_tab_info_per_tpi();
        $str .= "<label for='lieu_tpi'><b>Lieu du TPI : </b></label>
                 <input type=\"text\" id=\"lieu_tpi\" name=\"lieu_tpi\" value=\"". $emp->get_nom() .", ".$emp->get_adresse_emp().", ".$emp->get_localite_emp() ."\" size=\"30\">";
         $str .= $this->gen_horaire_tpi();
         $str .= "<br>
              <textarea id='footer_tpi' name='footer_tpi' cols='100' rows='8' form='mail_form'>
Merci d'avance de me signaler les éventuelles erreurs ou changements dans l'horaire.
Tout en vous remerciant pour votre engagement je vous adressse mes meilleures salutations.
Jacques Hirtzel
Expert en chef informatique Berne francophone
Les Frasses 26
2612 Cormoret
078 755 02 83
                </textarea><br>
    <input type=\"hidden\"  id=\"id_tpi\" name=\"id_tpi\" value=\"".$_POST["id_tpi"]."\">
    <input type=\"button\" class=\"btn btn-primary\" value=\"Send\" id=\"send_mail\" name=\"send\">
</div>
</div>
</div>";
return $str;
    }


    public function gen_tab_info_per_tpi(){
                $ma_app = new Personne($this->get_id_m_app());
                $exp_1 = new Personne($this->get_id_exp_1());
                $exp_2 = new Personne($this->get_id_exp_2());
                $can = new Candidat($this->get_id_can());

                $str = "<table class=\"table table-sm\">
                     <tr>
                        <th>Fonction</th>
                        <th>Nom, prénom</th>
                        <th>E-mail</th>
                        <th>Tél. prof.</th>
                        <th>Tél. mobile</th>
                    </tr>
                    <tbody>
                    <tr>
                        <td>Candidat : </td>
                        <td>".$can->get_nom()." ".$can->get_prenom()."</td>
                        <td>".$can->get_email()."</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Supérieur : </td>
                        <td>".$ma_app->get_nom()." ".$ma_app->get_prenom()."</td>
                        <td>".$ma_app->get_email()."</td>
                        <td>".$ma_app->get_tel_bureau()."</td>
                        <td>". $ma_app->get_tel_mobile()."</td>
                    </tr>
                    <tr>
                        <td>Expert responsable : </td>
                         <td>".$exp_1->get_nom()." ".$exp_1->get_prenom()."</td>
                        <td>".$exp_1->get_email()."</td>
                        <td>".$exp_1->get_tel_bureau()."</td>
                        <td>". $exp_1->get_tel_mobile()."</td>
                    </tr>
                    <tr>
                        <td>Expert accompagnant : </td>
                        <td>".$exp_2->get_nom()." ".$exp_2->get_prenom()."</td>
                        <td>".$exp_2->get_email()."</td>
                        <td>".$exp_2->get_tel_bureau()."</td>
                        <td>". $exp_2->get_tel_mobile()."</td>
                    </tr>
                    </tbody>
                </table>";
        return $str;
    }


    public function mail_info()
    {
        $ma_app = new Personne($this->get_id_m_app());
        $exp_1 = new Personne($this->get_id_exp_1());
        $exp_2 = new Personne($this->get_id_exp_2());
        $can = new Candidat($this->get_id_can());
        // $to = array("jacques.hirtzel@ceff.ch", "jacques@hirtzel.ch");
        $emp = new Entreprise($can->get_employeur());
        $to = $ma_app->get_email() . ", " . $exp_1->get_email() . ", " . $exp_2->get_email();
        $subject = "TPI - " . $can->get_nom() . " - " . $this->get_nom();

        $message = $this->gen_message_attrib_tpi($can, $ma_app, $exp_1, $exp_2);

        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // More headers
        $headers .= 'From: <chef-expert@ict-jb.net>' . "\r\n";
        $headers .= 'Reply-To: <chef-expert@ict-jb.net>' . "\r\n";
        $headers .= 'Cc: jacques@hirtzel.ch' . "\r\n";
        echo $message;
        if ($can->get_nom()) {
            echo mail($to, $subject, $message, $headers);
        }
    }

    public function get_criteres()
    {
        $query = "SELECT CTP.id_crt 
                    FROM t_ref_crt_tpi CTP
                    WHERE id_tpi = " . $this->get_id();
        $stmt = $this->pdo->prepare($query);
        $stmt->execute();

        $tab = $stmt->fetchAll();
        $new_tab = array();
        foreach ($tab AS $crt) {
            $new_tab[] = $crt['id_crt'];
        }
        return $new_tab;
    }

    public function get_criteres_details()
    {
        $query = "SELECT * 
                    FROM t_ref_crt_tpi CTP
                    JOIN t_criteres CRT ON CRT.id_crt=CTP.id_crt 
                    JOIN t_categories CAT ON CRT.id_cat=CAT.id_CAT 
                    WHERE id_tpi = " . $this->get_id(). " ORDER BY CRT.id_cat, CRT.code_crt";
        $stmt = $this->pdo->prepare($query);
        $stmt->execute();

        $tab = $stmt->fetchAll();
        return $tab;
    }


    /**
     * Retourne vrai si le critère n'est pas encore assigné au TPI ou faux dans le cas contraire.
     * Author CP-16NIS
     * Date Février 2020
     * @param $id_crt : id du critère
     * @return bool
     */
    public function crt_existe_pas($id_crt) {
        $query = "SELECT * FROM t_ref_crt_tpi WHERE id_crt = \"" . $id_crt . "\" AND id_tpi = \"". $this->get_id() . "\"";

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();

            $tab = $stmt->fetch();
            if (is_array($tab)) {
                return false;
            } else {
                return true;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    public function assoc_all_crt_tpi()
    {
        $query = "SELECT * FROM t_criteres WHERE id_thm is null";
        $stmt = $this->pdo->prepare($query);
        $stmt->execute();
        $tab_crt = $stmt->fetchAll();
        foreach ($tab_crt AS $crt) {
            $query = "INSERT INTO t_ref_crt_tpi SET id_tpi=" . $this->get_id() . ", id_crt =" . $crt['id_crt'];
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
        }
        return $this->get_criteres();
    }

    public function del_crt($id_crt)
    {
        $query = "DELETE FROM t_ref_crt_tpi WHERE id_crt=" . $id_crt . " AND id_tpi = " . $this->get_id();
        $stmt = $this->pdo->prepare($query);
        $stmt->execute();
    }

    public function add_crt($id_crt)
    {
        $query = "INSERT INTO t_ref_crt_tpi SET id_crt=" . $id_crt . ", id_tpi = " . $this->get_id();
        $stmt = $this->pdo->prepare($query);
        $stmt->execute();
    }

    public function def_crt($def = 1)
    {
        $query = "UPDATE t_tpi SET crt_def_tpi=:def WHERE id_tpi = " . $this->get_id();
        $stmt = $this->pdo->prepare($query);
        $args = array(':def' => $def);
        $stmt->execute($args);
    }

    /**
     * @param $ref_exa
     * @param $ref_can
     * @return bool
     * Autor CP-16RDM
     * Date Janvier 2020
     */
    public function add_tpi($ref_exa,$ref_can){
        $query = "INSERT INTO `t_tpi` (`id_tpi`, `ref_exa`, `ref_can`, `nom_tpi`, `ref_exp_1`, `ref_exp_2`, 
                    `ref_maitre_app`, `date_debut_tpi`, `date_fin_tpi`, `lieu_tpi`, `rue_tpi`, 
                    `npa_tpi`, `localite_tpi`, `visite_1_tpi`, `visite_2_tpi`, 
                    `date_presentation_tpi`, `lieu_presentation_tpi`, `cdc_recu_tpi`, `aut_exec_tpi`, `crt_def_tpi`, `ref_pro`) 
                    VALUES (NULL, :ref_exa, :ref_can, '', '0', '0', '0', '0000-00-00', '0000-00-00', '', '', '', '', '0000-00-00', '0000-00-00', '0000-00-00', '', '0', '0', '0', '0')";
        try {
            $stmt = $this->pdo->prepare($query);
            $args[':ref_exa'] = $ref_exa;
            $args[':ref_can'] = $ref_can;

            $stmt->execute($args);
            $tab = $stmt->fetch();
            if (is_array($tab)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param $ref_exa
     * @param $ref_can
     * @return bool
     * Autor CP-16RDM
     * Date Janvier 2020
     */
    public function remove_tpi($ref_exa,$ref_can){
        $query = "DELETE FROM t_tpi WHERE ref_exa = :ref_exa 
                  AND ref_can = :ref_can";

        try {
            $stmt = $this->pdo->prepare($query);
            $args[':ref_exa'] = $ref_exa;
            $args[':ref_can'] = $ref_can;
            $stmt->execute($args);
            return true;

        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param $ref_exa
     * @param $ref_can
     * @return bool
     * Autor CP-16RDM
     * Date Janvier 2020
     */
    public function check_doublon_tpi($ref_exa,$ref_can){
        $query = "SELECT * FROM t_tpi WHERE ref_exa = :ref_exa "
            . " AND ref_can = :ref_can ";
        try {
            $stmt = $this->pdo->prepare($query);
            $args[':ref_exa'] = $ref_exa;
            $args[':ref_can'] = $ref_can;

            $stmt->execute($args);
            $tab = $stmt->fetch();
            if (is_array($tab)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param $ref_exa
     * @param $ref_can
     * @return bool
     * Autor CP-16RDM
     * Date Janvier 2020
     */
    public function get_id_tpi_by_ref_exa_and_ref_can($ref_exa,$ref_can){
        $query = "SELECT id_tpi FROM t_tpi WHERE ref_exa = :ref_exa AND ref_can = :ref_can";
        try {
            $stmt = $this->pdo->prepare($query);
            $args[':ref_exa'] = $ref_exa;
            $args[':ref_can'] = $ref_can;

            $stmt->execute($args);
            $tab = $stmt->fetch();

            return $tab['id_tpi'];

        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param $id_tpi
     * @return bool
     * Autor CP-16RDM
     * Date Janvier 2020
     */
    public function remove_crt_tpi($id_tpi){
            $query = "DELETE FROM t_ref_crt_tpi WHERE id_tpi = :id_tpi";
            try {
                $stmt = $this->pdo->prepare($query);
                $args[':id_tpi'] = $id_tpi;
                $stmt->execute($args);
                return true;

            } catch (Exception $e) {
                return false;
            }
    }

    /**
     * @param $id_tpi
     * @return bool
     * Autor CP-16RDM
     * Date Janvier 2020
     */
    public function has_per_in_tpi($id_tpi){
        $query = "SELECT * FROM t_tpi_per WHERE id_tpi = :id_tpi";
        $args[':id_tpi'] = $id_tpi;

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            if (!empty($stmt->fetchAll())) {
                return true;
            }
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * @author Beruwalage Julian
     * @description Ces fonctions sont utlisées pour l'horaire des TPI
     */

    /**
     * Permet de modifier la date de début du TPI
     * @param $tab
     */
    function update_date($tab)
    {
        $query = "UPDATE t_tpi SET date_debut_tpi=:date_debut_tpi WHERE id_tpi=:id_tpi";
        try {
            $args['date_debut_tpi'] = $tab['date_debut_tpi'];
            $args['id_tpi'] = $tab['id_tpi'];
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
        } catch (PDOExeption $e) {
            echo $e->getMessage(), '<br/><br/>';
            echo _e('Erreur ajout de vacances');
        }
    }

    /**
     * @return mixed
     */
    public function get_id()
    {
        return $this->id;
    }

    /**
     * @param mixed $id_exa
     */
    public function set_id($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function get_id_exa()
    {
        return $this->id_exa;
    }

    /**
     * @param mixed $id_exa
     */
    public function set_id_exa($id_exa)
    {
        $this->id_exa = $id_exa;
    }

    /**
     * @return mixed
     */
    public function get_id_can()
    {
        return $this->id_can;
    }

    /**
     * @param mixed $id_can
     */
    public function set_id_can($id_can)
    {
        $this->id_can = $id_can;
    }

    /**
     * @return mixed
     */
    public function get_nom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function set_nom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function get_id_exp_1()
    {
        //return $this->id_exp_1;
        $query = "SELECT id_per FROM t_tpi TPI
        JOIN t_tpi_per TPI_PER ON TPI.id_tpi = TPI_PER.id_tpi
        JOIN t_fonctions_tpi FNT ON TPI_PER.id_fnt = FNT.id_fnt
        WHERE FNT.id_fnt = 2 AND TPI.id_tpi = :tpi";
        $stmt = $this->pdo->prepare($query);
        $args = array('tpi' => $this->id);
        $stmt->execute($args);
        $id_per = $stmt->fetch();
        $id_per = $id_per['id_per'];
        return $id_per;
    }

    /**
     * @param mixed $id_exp_1
     */
    public function set_id_exp_1($id_exp_1)
    {
        //$this->id_exp_1 = $id_exp_1;
        $query = "SELECT id_per FROM t_tpi TPI
        JOIN t_tpi_per TPI_PER ON TPI.id_tpi = TPI_PER.id_tpi
        WHERE TPI_PER.id_fnt = 2 AND TPI.id_tpi = :tpi";
        $stmt = $this->pdo->prepare($query);
        $args = array('tpi' => $this->id);
        $stmt->execute($args);
        $id_per = $stmt->fetch();


        if(empty($id_per) && $id_exp_1 != 0){
            //INSERT
            $query = "INSERT INTO t_tpi_per (id_tpi, id_per, id_fnt) VALUES (:tpi,:per,2)";
            $stmt = $this->pdo->prepare($query);
            $args = array('tpi' => $this->id,'per' => $id_exp_1);
            $stmt->execute($args);
        }else{
            //UPDATE
            if($id_exp_1 == 0){
                $query = "DELETE FROM t_tpi_per WHERE id_tpi = :tpi AND id_fnt = 2";
                $stmt = $this->pdo->prepare($query);
                $args = array('tpi' => $this->id);
            }else{
                $query = "UPDATE t_tpi_per SET id_per = :per WHERE id_tpi = :tpi AND id_fnt = 2";
                $stmt = $this->pdo->prepare($query);
                $args = array('tpi' => $this->id,'per' => $id_exp_1);
            }

            $stmt->execute($args);
        }
    }

    /**
     * @return mixed
     */
    public function get_id_exp_2()
    {
        //return $this->id_exp_2;
        $query = "SELECT id_per FROM t_tpi TPI
        JOIN t_tpi_per TPI_PER ON TPI.id_tpi = TPI_PER.id_tpi
        JOIN t_fonctions_tpi FNT ON TPI_PER.id_fnt = FNT.id_fnt
        WHERE FNT.id_fnt = 3 AND TPI.id_tpi = :tpi";
        $stmt = $this->pdo->prepare($query);
        $args = array('tpi' => $this->id);
        $stmt->execute($args);
        $id_per = $stmt->fetch();
        $id_per = $id_per['id_per'];
        return $id_per;
    }

    /**
     * @param mixed $id_exp_2
     */
    public function set_id_exp_2($id_exp_2)
    {
        //$this->id_exp_2 = $id_exp_2;
        $query = "SELECT id_per FROM t_tpi TPI
        JOIN t_tpi_per TPI_PER ON TPI.id_tpi = TPI_PER.id_tpi
        WHERE TPI_PER.id_fnt = 3 AND TPI.id_tpi = :tpi";
        $stmt = $this->pdo->prepare($query);
        $args = array('tpi' => $this->id);
        $stmt->execute($args);
        $id_per = $stmt->fetch();


        if(empty($id_per) && $id_exp_2 != 0){
            //INSERT
            $query = "INSERT INTO t_tpi_per (id_tpi, id_per, id_fnt) VALUES (:tpi,:per,3)";
            $stmt = $this->pdo->prepare($query);
            $args = array('tpi' => $this->id,'per' => $id_exp_2);
            $stmt->execute($args);
        }else {
            //UPDATE

            if($id_exp_2 == 0){
                $query = "DELETE FROM t_tpi_per WHERE id_tpi = :tpi AND id_fnt = 3";
                $stmt = $this->pdo->prepare($query);
                $args = array('tpi' => $this->id);
            }else{
                $query = "UPDATE t_tpi_per SET id_per = :per WHERE id_tpi = :tpi AND id_fnt = 3";
                $stmt = $this->pdo->prepare($query);
                $args = array('tpi' => $this->id, 'per' => $id_exp_2);
            }

            $stmt->execute($args);
        }

    }

    /**
     * @return mixed
     */
    public function get_id_m_app()
    {
        //return $this->id_m_app;
        $query = "SELECT id_per FROM t_tpi TPI
        JOIN t_tpi_per TPI_PER ON TPI.id_tpi = TPI_PER.id_tpi
        JOIN t_fonctions_tpi FNT ON TPI_PER.id_fnt = FNT.id_fnt
        WHERE FNT.id_fnt = 1 AND TPI.id_tpi = :tpi";
        $stmt = $this->pdo->prepare($query);
        $args = array('tpi' => $this->id);
        $stmt->execute($args);
        $id_per = $stmt->fetch();
        $id_per = $id_per['id_per'];
        return $id_per;
    }

    /**
     * @param mixed $id_m_app
     */
    public function set_id_m_app($id_m_app)
    {
        //$this->id_m_app = $id_m_app;
        $query = "SELECT id_per FROM t_tpi TPI
        JOIN t_tpi_per TPI_PER ON TPI.id_tpi = TPI_PER.id_tpi
        WHERE TPI_PER.id_fnt = 1 AND TPI.id_tpi = :tpi";
        $stmt = $this->pdo->prepare($query);
        $args = array('tpi' => $this->id);
        $stmt->execute($args);
        $id_per = $stmt->fetch()['id_per'];


        if(empty($id_per) && $id_m_app != 0){
            //INSERT
            $query = "INSERT INTO t_tpi_per (id_tpi, id_per, id_fnt) VALUES (:tpi,:per,1)";
            $stmt = $this->pdo->prepare($query);
            $args = array('tpi' => $this->id,'per' => $id_m_app);
            $stmt->execute($args);
        }else {
            //UPDATE

            if($id_m_app == 0){
                $query = "DELETE FROM t_tpi_per WHERE id_tpi = :tpi AND id_fnt = 1";
                $stmt = $this->pdo->prepare($query);
                $args = array('tpi' => $this->id);
            }else{
                $query = "UPDATE t_tpi_per SET id_per = :per WHERE id_tpi = :tpi AND id_fnt = 1";
                $stmt = $this->pdo->prepare($query);
                $args = array('tpi' => $this->id, 'per' => $id_m_app);
            }



            $stmt->execute($args);
        }
    }

    /**
     * @return mixed
     */
    public function get_date_db()
    {
        return $this->date_db;
    }

    /**
     * @param mixed $date_db
     */
    public function set_date_db($date_db)
    {
        $this->date_db = $date_db;
    }

    /**
     * @return mixed
     */
    public function get_lieu()
    {
        return $this->lieu;
    }

    /**
     * @param mixed $lieu
     */
    public function set_lieu($lieu)
    {
        $this->lieu = $lieu;
    }

    /**
     * @return mixed
     */
    public function get_nom_can()
    {
        return $this->nom_can;
    }

    /**
     * @param mixed $nom_can
     */
    public function set_nom_can($nom_can)
    {
        $this->nom_can = $nom_can;
    }

    /**
     * @return mixed
     */
    public function get_prenom_can()
    {
        return $this->prenom_can;
    }

    /**
     * @param mixed $prenom_can
     */
    public function set_prenom_can($prenom_can)
    {
        $this->prenom_can = $prenom_can;
    }

    /**
     * @return mixed
     */
    public function get_numero_can()
    {
        return $this->numero_can;
    }

    /**
     * @param mixed $numero_can
     */
    public function set_numero_can($numero_can)
    {
        $this->numero_can = $numero_can;
    }


    /**
     * @return mixed
     */
    public function get_classe()
    {
        return $this->classe;
    }

    /**
     * @param mixed $numero_can
     */
    public function set_classe($classe)
    {
        $this->classe = $classe;
    }

    /**
     * @return mixed
     */
    public function get_crt_def()
    {
        return $this->crt_def;
    }

    /**
     * @param mixed $crt_def
     */
    public function set_crt_def($crt_def)
    {
        $this->crt_def = $crt_def;
    }

    public function get_all_tpi_act_by_id_per($id_per)
    {
        /*$query = "SELECT TPI.* FROM t_tpi TPI
                        JOIN t_tpi_per TPI_PER ON TPI.id_tpi = TPI_PER.id_tpi
                        WHERE TPI_PER.id_per = :id_per
                        AND TPI.date_fin_tpi > NOW()";*/

        // TODO Pour évaluation
        $query = "SELECT TPI.* FROM t_tpi TPI
                        JOIN t_tpi_per TPI_PER ON TPI.id_tpi = TPI_PER.id_tpi 
                        WHERE TPI_PER.id_per = :id_per 
                        AND TPI.date_fin_tpi > '2019-01-01'";

        $stmt = $this->pdo->prepare($query);
        $args = array('id_per' => $id_per);
        $stmt->execute($args);
        $tab = $stmt->fetchALL();
        return $tab;
    }
    public function get_tpi_days_by_per($id_per){
        $query = "SELECT DISTINCT date_hor fROM t_h_tpi WHERE id_tpi IN (SELECT DISTINCT TPI.id_tpi FROM `t_tpi_per` TPP JOIN t_tpi TPI ON TPP.id_tpi=TPI.id_tpi WHERE `id_per` = :id_per AND date_fin_tpi>= now()) ORDER BY date_hor";
        $stmt = $this->pdo->prepare($query);
        $args = array('id_per' => $id_per);
        $stmt->execute($args);
        $tab = $stmt->fetchALL();
        $tab_final = array();
        foreach ($tab as $date){
            $tab_final[$date['date_hor']] = strftime("%A",strtotime($date['date_hor']));
        }
        return $tab_final;
    }

    public function get_all_email(){
        $query = "SELECT DISTINCT email_per FROM t_tpi TPI
        JOIN t_tpi_per TPI_PER ON TPI.id_tpi = TPI_PER.id_tpi
        JOIN t_personnes PER ON TPI_PER.id_per = PER.id_per WHERE TPI.id_tpi = ".$this->id;
        $stmt = $this->pdo->prepare($query);
        $stmt->execute();
        $tab = $stmt->fetchALL(PDO::FETCH_COLUMN);
        return $tab;
    }
}

?>
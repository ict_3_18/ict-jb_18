<?php
/**
 * Created by PhpStorm.
 * User: CP-15ALP
 * Date: 06.03.2019
 * Time: 15:13
 */

class Table extends Projet
{

    private $id_tab;
    private $pcname_tab;
    private $mac_tab;
    private $resolutionX_tab;
    private $resolutionY_tab;
    private $width_tab;
    private $height_tab;
    private $posx_tab;
    private $posy_tab;
    private $ref_sle;
    private $ordre_tab;

    public function __construct($id = null)
    {
        parent::__construct($id);

        if ($id) {
            $this->set_id_tab($id);
            $this->init();
        }
    }

    /**
     * Initialise la table via le constructeur avec l'id
     * @return bool
     */
    public function init()
    {
        $query = "SELECT * FROM t_table WHERE id_tab = :id_tab";
        try {
            $stmt = $this->pdo->prepare($query);
            unset($args);
            $args['id_tab'] = $this->get_id_tab();
            $stmt->execute($args);
            $results = $stmt->fetch(PDO::FETCH_ASSOC);

            $this->set_pcname_tab($results['pcname_tab']);
            $this->set_mac_tab($results['mac_tab']);
            $this->set_resolutionX_tab($results['resolutionX_tab']);
            $this->set_resolutionY_tab($results['resolutionY_tab']);
            $this->set_width_tab($results['width_tab']);
            $this->set_height_tab($results['height_tab']);
            $this->set_posx_tab($results['posx_tab']);
            $this->set_posy_tab($results['posy_tab']);
            $this->set_ref_sle($results['ref_sle']);
            $this->set_ordre_tab($results['ordre_tab']);
            return true;
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function change_order($ordre, $id_tab)
    {
        $query = "UPDATE t_table SET ordre_tab = :ordre_tab WHERE id_tab = :id_tab;";
        try {
            $stmt = $this->pdo->prepare($query);
            unset($args);
            $args['ordre_tab'] = $ordre;
            $args['id_tab'] = $id_tab;
            $stmt->execute($args);
            return true;
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    /**
     * @return mixed
     */
    public function get_id_tab()
    {
        return $this->id_tab;
    }

    /**
     * @param mixed $id_tab
     */
    public function set_id_tab($id_tab)
    {
        $this->id_tab = $id_tab;
    }

    /**
     * @return mixed
     */
    public function get_pcname_tab()
    {
        return $this->pcname_tab;
    }

    /**
     * @param mixed $pcname_tab
     */
    public function set_pcname_tab($pcname_tab)
    {
        $this->pcname_tab = $pcname_tab;
    }

    /**
     * @return mixed
     */
    public function get_mac_tab()
    {
        return $this->mac_tab;
    }

    /**
     * @param mixed $mac_tab
     */
    public function set_mac_tab($mac_tab)
    {
        $this->mac_tab = $mac_tab;
    }

    /**
     * @return mixed
     */
    public function get_resolutionX_tab()
    {
        return $this->resolutionX_tab;
    }

    /**
     * @param mixed $resolutionX_tab
     */
    public function set_resolutionX_tab($resolutionX_tab)
    {
        $this->resolutionX_tab = $resolutionX_tab;
    }

    /**
     * @return mixed
     */
    public function get_resolutionY_tab()
    {
        return $this->resolutionY_tab;
    }

    /**
     * @param mixed $resolutionY_tab
     */
    public function set_resolutionY_tab($resolutionY_tab)
    {
        $this->resolutionY_tab = $resolutionY_tab;
    }

    /**
     * @return mixed
     */
    public function get_width_tab()
    {
        return $this->width_tab;
    }

    /**
     * @param mixed $width_tab
     */
    public function set_width_tab($width_tab)
    {
        $this->width_tab = $width_tab;
    }

    /**
     * @return mixed
     */
    public function get_height_tab()
    {
        return $this->height_tab;
    }

    /**
     * @param mixed $height_tab
     */
    public function set_height_tab($height_tab)
    {
        $this->height_tab = $height_tab;
    }

    /**
     * @return mixed
     */
    public function get_posx_tab()
    {
        return $this->posx_tab;
    }

    /**
     * @param mixed $posx_tab
     */
    public function set_posx_tab($posx_tab)
    {
        $this->posx_tab = $posx_tab;
    }

    /**
     * @return mixed
     */
    public function get_posy_tab()
    {
        return $this->posy_tab;
    }

    /**
     * @param mixed $posy_tab
     */
    public function set_posy_tab($posy_tab)
    {
        $this->posy_tab = $posy_tab;
    }

    /**
     * @return mixed
     */
    public function get_ref_sle()
    {
        return $this->ref_sle;
    }

    /**
     * @param mixed $ref_sle
     */
    public function set_ref_sle($ref_sle)
    {
        $this->ref_sle = $ref_sle;
    }

    /**
     * @return mixed
     */
    public function get_ordre_tab()
    {
        return $this->ordre_tab;
    }

    /**
     * @param mixed $ordre_tab
     */
    public function set_ordre_tab($ordre_tab)
    {
        $this->ordre_tab = $ordre_tab;
    }

    public function __toString()
    {
        $str = "\n<pre>\n";
        foreach ($this AS $key => $val) {
            if ($key != "pdo") {
                $str .= "\t" . $key;
                $lengh_key = strlen($key);
                for ($i = strlen($key); $i < 20; $i++) {
                    $str .= "&nbsp;";
                }
                $str .= "=>&nbsp;&nbsp;&nbsp;" . $val . "\n";
            }
        }
        $str .= "\n</pre>";
        return $str;
    }

}
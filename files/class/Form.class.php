<?php

class Form {

    static function label($id, $text = "", $for = "", $class = "") {
        $str = "";
        $str .= "<label "
                . "name=\"" . $id . "\" "
                . "id=\"" . $id . "\" "
                . "class=\"" . $class . "\" "
                . "for=\"" . $for . "\">";
        $str .= $text . "</label>";
        return $str;
    }

    static function hidden($id, $value = "", $other = "") {
        $str = "";
        $str .= "<input type=\"hidden\" "
                . "value=\"" . $value . "\" "
                . "name=\"" . $id . "\" "
                . "id=\"" . $id . "\" "
                . $other . ">";
        return $str;
    }

    static function text($id, $value = "", $class = "", $placeholder = "", $other = "") {
        $str = "";
        $str .= "<input type=\"text\" "
                . "class=\"" . $class . "\" "
                . "value=\"" . $value . "\" "
                . "placeholder=\"" . $placeholder . "\" "
                . "name=\"" . $id . "\" "
                . "id=\"" . $id . "\" "
                . $other . " >";
        return $str;
    }

    static function select($id, $tab, $key_value, $key_option, $attr_sel = "", $default = "", $class = "") {
        $str = "";
        $str = "<select id=\"" . $id . "\" "
                . "name=\"" . $id . "\" "
                . "class=\"" . $class . "\" "
                . "" . $attr_sel . ">";
        foreach ($tab as $key => $line) {
            $select = "";
            if ($default == $line[$key_value]) {
                $select = "Selected";
            }
            $str .= "<option value=\"" . $line[$key_value] . "\" "
                    . "" . $select . ">"
                    . $line[$key_option]
                    . "</option>";
        }
        $str .= "</select>";
        return $str;
    }

    static function number($id, $value = "", $min = "", $max = "", $class = "", $placeholder = "", $step = "", $other = "") {
        $str = "";
        $str .= "<input type=\"number\" "
                . "class=\"" . $class . "\" "
                . "value=\"" . $value . "\" "
                . "placeholder=\"" . $placeholder . "\" "
                . "name=\"" . $id . "\" "
                . "id=\"" . $id . "\" "
                . "min=\"" . $min . "\" "
                . "max=\"" . $max . "\" "
                . "step=\"" . $step . "\" "
                . $other . " >";
        return $str;
    }

    static function checkbox($id, $class = "", $checked = "", $other = "") {
        if ($checked == 1 || $checked) {
            $checked = "checked";
        } else if ($checked == 0 || !$checked) {
            $checked = "";
        }
        $str = "";
        $str .= "<input type=\"checkbox\" "
                . "class=\"" . $class . "\" "
                . "name=\"" . $id . "\" "
                . "id=\"" . $id . "\" "
                . $checked . " " . $other . ">";
        return $str;
    }

    static function button($id, $value = "", $text = "", $class = "", $form = "", $other = "") {
        $str = "";
        $str .= "<button "
                . "class=\"" . $class . "\" "
                . "value=\"" . $value . "\" "
                . "name=\"" . $id . "\" "
                . "id=\"" . $id . "\" "
                . "form=\"" . $form . "\" "
                . $other . " >";
        $str .= $text;
        $str .= "</button>";
        return $str;
    }

    static function radio($id, $name, $value = "", $class = "", $other = "") {
        $str = "";
        $str .= "<input type=\"radio\" "
                . "class=\"" . $class . "\" "
                . "value=\"" . $value . "\" "
                . "name=\"" . $name . "\" "
                . "id=\"" . $id . "\" "
                . $other . " >";
        return $str;
    }

    // submit
    static function submit($id, $value = "", $class = "", $other = "") {
        $str = "";
        $str .= "<input type=\"submit\" "
                . "class=\"" . $class . "\" "
                . "value=\"" . $value . "\" "
                . "name=\"" . $id . "\" "
                . "id=\"" . $id . "\" "
                . $other . " >";
        return $str;
    }

}

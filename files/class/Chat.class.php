<?php

Class Chat EXTENDS Projet {

    protected $id;
    protected $msg;
    protected $hrs;
    protected $id_sle;
    protected $id_exa;

    /**
     * Chat constructor.
     * @param null $id
     */
    public function __construct($id = null) {

        parent::__construct();

        if ($id){
            $this->set_id($id);
            $this->init();
        }
    }

    /**
     * @return bool
     * Initialization
     */
    public function init(){
        $query = "SELECT * FROM `t_chat` WHERE `id_chat` = :id";

        $args[':id'] = $this->get_id();

        try{
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);

            return true;
        }catch (Exception $e){
            return false;
        }
    }

    /**
     * @param $msg
     * @param $hrs
     * @param $id_exa
     * @param $id_sle
     * @return bool|string
     * Ajout un message au chat pour
     */
    function add_chat($msg, $hrs, $id_exa, $id_sle) {
        $query = "INSERT INTO `t_chat` 
                  (`message_chat`, `heure_chat`, `id_sle`, `id_exa`) 
                  VALUES ( :msg, :hrs, :id_sle, :id_exa)";

        $args[':msg'] = $msg;
        $args[':hrs'] = $hrs;
        $args[':id_exa'] = $id_exa;
        $args[':id_sle'] = $id_sle;

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);

            return $this->pdo->lastInsertId();
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * @param $id_exa
     * @param $hrs
     * @return array|bool|string
     * Retourne les messages de chat dont l'heure est suppérieur a celle préciser pour un examen
     */
    function get_messages($id_exa,$hrs) {
        $query = "SELECT CHT.*, SLE.nom_sle FROM `t_chat` CHT
                  JOIN t_salles SLE ON SLE.id_sle=CHT.id_sle
                  WHERE CHT.`id_exa` = :id_exa
                  AND heure_chat > :hrs";

        $args[':id_exa'] = $id_exa;
        $args[':hrs'] = $hrs;

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $msgs = $stmt->fetchAll();

            if($msgs == null){
                $msgs = "0";
            }
            return $msgs;
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * @param $id_exa
     * @return array|bool
     * Retourne la liste des messages de chat pour l'examen
     */
    function get_all_messages($id_exa) {
        $query = "SELECT CHT.*, SLE.nom_sle FROM `t_chat` CHT
                  JOIN t_salles SLE ON SLE.id_sle=CHT.id_sle
                  WHERE CHT.`id_exa` = :id_exa
                  ORDER BY CHT.heure_chat  DESC";

        $args[':id_exa'] = $id_exa;

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $msgs = $stmt->fetchAll();

            return $msgs;
        } catch (Exception $ex) {
            return false;
        }
    }

    /** ------------ Getter & Setter ------------ */

    /**
     * @return mixed
     */
    public function get_id()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function set_id($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function get_msg()
    {
        return $this->msg;
    }

    /**
     * @param mixed $msg
     */
    public function set_msg($msg)
    {
        $this->msg = $msg;
    }

    /**
     * @return mixed
     */
    public function get_hrs()
    {
        return $this->hrs;
    }

    /**
     * @param mixed $hrs
     */
    public function set_hrs($hrs)
    {
        $this->hrs = $hrs;
    }

    /**
     * @return mixed
     */
    public function get_id_sle()
    {
        return $this->id_sle;
    }

    /**
     * @param mixed $id_sle
     */
    public function set_id_sle($id_sle)
    {
        $this->id_sle = $id_sle;
    }

    /**
     * @return mixed
     */
    public function get_id_exa()
    {
        return $this->id_exa;
    }

    /**
     * @param mixed $id_exa
     */
    public function set_id_exa($id_exa)
    {
        $this->id_exa = $id_exa;
    }
}
?>
<?php
class Rubrique extends Projet {
    private $id;
    private $num;
    private $nom;
    
    function __construct($id = 0) {
        
        $this->table_name = "t_rubriques";
        $this->suffix = "_rub";
        parent::__construct($id);
        if($id != 0){
            $this->set_id($id);
        }
        
    }
    
    
    public function init() {
        $query = "SELECT * FROM t_rubriques WHERE id_rub=:id_rub";
        try {
            $stmt = $this->pdo->prepare($query);
            $args[':id_rub'] = $this->get_id();

            $stmt->execute($args);
            $tab = $stmt->fetch();
            
            $this->set_id($tab['id_rub']);
            $this->set_num($tab['num_rub']);
            $this->set_nom($tab['nom_rub']);        
        } catch (Exception $e) {
            return false;
        }
        return true;
    }
    
    public function get_all(){
         $query = "SELECT * FROM t_rubriques ORDER BY num_rub";
        $tab = $this->pdo->query($query)->fetchAll(PDO::FETCH_ASSOC);
        //print_r($tab_crt);
        return $tab;
    }
    
    public function get_all_cat($id_rub = 0){
        if(!$id_rub) {
            $id_rub = $this->get_id();
        }
        $query = "SELECT * FROM t_categories CAT WHERE CAT.id_rub = ".$id_rub." ORDER BY id_cat ";
        $tab = $this->pdo->query($query)->fetchAll(PDO::FETCH_ASSOC);
        //print_r($tab_crt);
        return $tab;
    }
    
    public function get_nom_with_cat_and_crt() {
        $query = "SELECT RUB.num_rub, RUB.nom_rub, nom_cat, CRT.nom_crt, CRT.id_cat, CRT.code_crt FROM `t_rubriques` RUB "
                . "JOIN t_categories CAT ON RUB.id_rub = CAT.id_rub "
                . "JOIN t_criteres CRT ON CAT.id_cat = CRT.id_cat "
                . "ORDER BY CRT.id_cat ASC, CRT.code_crt ASC";
        $tab = $this->pdo->query($query)->fetchAll(PDO::FETCH_ASSOC);
        //print_r($tab_crt);
        return $tab;
    }
    
    public function __toString() {
        $str = "\n<pre>\n";
        foreach ($this AS $key => $val) {
            if ($key != "pdo") {
                $str .= "\t" . $key;
                $lengh_key = strlen($key);
                for ($i = strlen($key); $i < 20; $i++) {
                    $str .= "&nbsp;";
                }
                $str .= "=>&nbsp;&nbsp;&nbsp;" . $val . "\n";
            }
        }
        $str .= "\n</pre>";
        return $str;
    }
    
    function get_id() {
        return $this->id;
    }

    function get_num() {
        return $this->num;
    }

    function get_nom() {
        return $this->nom;
    }

    function set_id($id) {
        $this->id = $id;
    }

    function set_num($num) {
        $this->num = $num;
    }

    function set_nom($nom) {
        $this->nom = $nom;
    }

    
}

?>


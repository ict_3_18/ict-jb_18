<?php

Class Evenement EXTENDS Projet {

    function __construct($id = null) {
        parent::__construct($id);
    }

    public function init($id = '') {
        parent::init($id);
    }

    function add_evenement($id_exa, $hrs_vnt, $text_vnt,  $fin_exa, $id_sle) {

        $query = "INSERT INTO `t_event_exa` 
                  (`ref_exa`, `heure_vnt_exa`, `event_exa`, `fin_exa`, `ref_sle`) 
                  VALUES (:id_exa, :hrs_vnt, :text_vnt, :fin_exa, :id_sle)";

        $args[':id_exa'] = $id_exa;
        $args[':hrs_vnt'] = $hrs_vnt;
        $args[':text_vnt'] = $text_vnt;
        $args[':fin_exa'] = $fin_exa;
        $args[':id_sle'] = $id_sle;

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);

            return $this->pdo->lastInsertId();
        } catch (Exception $ex) {
            return false;
        }
    }

    function add_candidat_event($id_can, $id_evnt) {
        try {
            //Insertion des candidats dans l'événement
            $query = 'INSERT INTO t_ref_can_vnt '
                   . '(ref_can, ref_vnt_exa) '
                   . 'VALUES ((SELECT id_can '
                            . 'FROM t_candidats '
                            . 'WHERE id_can='.$id_can.'), '
                            . '(SELECT id_vnt_exa '
                            . 'FROM t_event_exa '
                            . 'WHERE id_vnt_exa='.$id_evnt.'))';
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
        } catch (Exception $ex) {
            
        }
    }

    /**
     * Date : 14.01.2020
     * @param $id_exa
     * @return array|bool
     * Recherche les événements d'un examen
     */
    function get_vnt_exa($id_exa) {
        $query = 'SELECT * FROM `t_event_exa` 
                  WHERE `ref_exa` = :id_exa 
                  ORDER BY heure_vnt_exa DESC';

        $args[':id_exa'] = $id_exa;

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $tab_vnt = $stmt->fetchAll();

            return $tab_vnt;
        } catch (Exception $ex) {
            return false;
        }
    }

    function get_last_evenement($id_evnt) {
        try {
            //Recherche de l'id de l'événement que l'on vient de créer
            $query = 'SELECT EVNT.*, GROUP_CONCAT(SUBSTR(CAN.no_candidat_can,2,3)) AS no_candidat_can, SLE.nom_sle '
                   . 'FROM t_event_exa AS EVNT '
                   . 'LEFT JOIN t_ref_can_vnt AS CAN_EVNT ON EVNT.id_vnt_exa=CAN_EVNT.ref_vnt_exa '
                   . 'LEFT JOIN t_candidats AS CAN ON CAN.id_can=CAN_EVNT.ref_can '
                   . 'LEFT JOIN t_salles AS SLE ON SLE.id_sle=EVNT.ref_sle '
                   . 'WHERE EVNT.id_vnt_exa = ' .$id_evnt. ' '
                   . 'ORDER BY EVNT.heure_vnt_exa DESC';
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $last_evnt = $stmt->fetch();
        } catch (Exception $ex) {
            
        }
        return $last_evnt;
    }

    /**
     * Date : 09.01.2020
     * @param $id
     * @param $texte
     * @return bool
     * Modification d'un événement d'examen.
     */
    function modif_evenement($id, $texte) {
        $query = "UPDATE `t_event_exa` 
                  SET `event_exa` = :texte 
                  WHERE `t_event_exa`.`id_vnt_exa` = :id";

        $args[':id'] = $id;
        $args[':texte'] = $texte;

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);

            return true;
        } catch (Exception $ex) {
            return false;
        }
    }
    
    function get_id_event($id_exa, $remarque_evnt, $heure_evnt) {
        try {
            //Recherche de l'id de la remarque que l'on veut supprimer
            $query = 'SELECT id_vnt_exa '
                   . 'FROM t_event_exa '
                   . 'WHERE ref_exa = '.$id_exa.' '
                   . 'AND event_exa = "'.$remarque_evnt.'" '
                   . 'AND heure_vnt_exa = "'.$heure_evnt.'"';
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $id_evnt = $stmt->fetch();
        } catch (Exception $ex) {
            
        }
        return $id_evnt;
    }
    
    function suppr_candidat($id_evnt) {
        try {
            //Suppression de la relation entre la remarque et le candidat
            $query = 'DELETE FROM t_ref_can_vnt WHERE ref_vnt_exa = '.$id_evnt;
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
        } catch (Exception $ex) {
            
        }
    }
    
    function suppr_evenement($id_evnt) {
        try {
            //Suppression de la remarque
            $query = 'DELETE FROM t_event_exa '
                   . 'WHERE id_vnt_exa = '.$id_evnt;
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();

            return true;
        } catch (Exception $ex) {
            return false;
        }
    }
}

?>
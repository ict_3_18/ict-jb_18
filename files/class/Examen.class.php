<?php

Class Examen EXTENDS Projet {

    private $id;
    private $num_nom;
    private $ann;
    private $ver;
    private $id_dom;
    private $date_hrs;
    private $dur;
    private $sal;
    private $materiel_exa;
    private $soft_exa;
    private $infos_begin_exa;
    private $data_exa;
    private $lec_exa;
    private $salle_inf_exa;
    private $reseau_exa;
    private $nom;
    private $description;
    private $id_pls;
    private $definitif;


    private $archive;
    private $notes;
    private $cd_can;
    private $cd_exa;
    private $epr_can;
    private $epr_cla;
    private $epr_archive;
    private $rapport;
    private $validation;
    private $com_rapport;


    private $data;

    /**
     * Examen constructor.
     * @param null $id
     */
    public function __construct($id = null) {

        parent::__construct();

        if ($id) {
            $this->set_id($id);
            $this->init();
        }
    }

    /**
     * Initialisation de l'objet
     * @return boolean
     */
    public function init() {
        $query = "SELECT * FROM t_examens EXA JOIN t_desc_modules DEM ON EXA.no_ich_exa	 = DEM.id_desc_mod WHERE id_exa=:id_exa";
        try {
            $stmt = $this->pdo->prepare($query);
            $args[':id_exa'] = $this->get_id();

            $stmt->execute($args);
            $tab = $stmt->fetch();

            $this->set_num_nom($tab['no_ich_exa']);
            $this->set_ann($tab['version_ich_exa']);
            $this->set_ver($tab['v_exam_ich_exa']);
            $this->set_id_dom($tab['id_dom']);
            $this->set_date_hrs($tab['date_heure_exa']);
            $this->set_dur($tab['duree_exa']);
            $this->set_sal($tab['salle_exa']);
            $this->set_materiel_exa($tab['moyens_aut_exa']);
            $this->set_soft_exa($tab['soft_exa']);
            $this->set_infos_begin_exa($tab['infos_begin_exa']);
            $this->set_lec_exa($tab['lec_exa']);
            $this->set_dep_exa($tab['dep_exa']);
////            $this->set_lec_exa($tab['dep_exa']);
            $this->set_data_exa($tab['data_aut_exa']);
            $this->set_salle_inf_exa($tab['type_salle_exa']);
            $this->set_reseau_exa($tab['reseau_exa']);
            $this->set_nom($tab['nom_mod']);
            $this->set_description($tab['description_mod']);
            $this->set_id_pls($tab['ref_pls']);

            // Examen définitif
            $this->set_definitif($tab['definitif_exa']);

            // Examen archivé
            $this->set_archive($tab['active_archivage_exa']);

            // Notes saisies
            $this->set_notes($tab['saisi_note_exa']);

            // CD avec travaux des candidats
            $this->set_cd_can($tab['archiver_cd_ep_exa']);

            // CD avec données d'exa
            $this->set_cd_exa($tab['archiver_cd_exa']);

            // Archivage des travaux des candidats
            $this->set_epr_can($tab['archiver_dos_exa']);

            //Archiver l'épreuve dans le classeur des candidats
            $this->set_epr_cla($tab['archiver_cla_exa']);

            //Archiver l'épreuve sous forme de dossier
            $this->set_epr_archive($tab['archiver_ep_exa']);

            $this->set_rapport($tab['ref_pls']);

            $this->set_validation($tab['valider_exa']);

            $this->set_com_rapport($tab['com_rapport']);


            //TODO Ajouter les objectifs
        } catch (Exception $e) {
            return false;
        }
        return true;
    }
    /*
     * Ajout par défaut pour l'ajout de base d'un examen 
     * (sans la salle et autres données plus complexe)
     * @return boolean
     */
    function add($tab) {
        // Tableau d'arguments
        $args['num_nom_exa'] = $tab['num_nom_exa'];
        $args['ann_exa'] = $tab['ann_exa'];
        $args['ver_exa'] = $tab['ver_exa'];
        $args['id_dom'] = $tab['id_dom'];
        $args['date_hrs_exa'] = $tab['date_hrs_exa'];
        $args['dur_exa'] = $tab['dur_exa'];

        $query = "INSERT INTO t_examens SET "
                . "no_ich_exa = :num_nom_exa , "
                . "version_ich_exa = :ann_exa , "
                . "v_exam_ich_exa = :ver_exa , "
                . "id_dom = :id_dom , "
                . "date_heure_exa = :date_hrs_exa , "
                . "duree_exa = :dur_exa ";
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
        } catch (Exception $e) {
            //echo $e;
            return false;
        }
        return $this->pdo->lastInsertId();
    }

    /*
     * Ajoute les différentes salles à la table de référence pour les salles
     * @return boolean
     */
    function add_salle($salles, $id_exa) {
        foreach ($salles as $sal) {
            $args['id_exa'] = $id_exa;
            $args['id_sle'] = $sal;

            $query = "INSERT INTO `t_ref_examens_salles` SET "
                    . "ref_exa = :id_exa, "
                    . "ref_sle = :id_sle ";
            try {
                $stmt = $this->pdo->prepare($query);
                $stmt->execute($args);
            } catch (Exception $e) {
                //echo $e;
                return false;
                break;
            }
            //return ?
        }
        return true;
    }

    /*
     * Supprime les salles d'un examen
     * @return boolean
     */
    function del_salle($id_exa) {
        $args['id_exa'] = $id_exa;

        $query = "DELETE FROM `t_ref_examens_salles` "
                . "WHERE ref_exa = :id_exa";
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
        } catch (Exception $e) {
            //echo $e;
            return false;
        }
        return true;
    }

    /*
     * Cherche un examen a partir de son ID et retourn toutes les données de cette examen
     * @return Examen (array)
     */
    function find_exam($id) {

        $args['id_exa'] = $id;

        $query = "SELECT no_ich_exa, "
                . "version_ich_exa, "
                . "v_exam_ich_exa, "
                . "id_dom, date_heure_exa, "
                . "duree_exa, "
                . "temps_supp_dys_exa, "
                . "definitif_exa "
                . "FROM `t_examens` "
                . "WHERE id_exa = :id_exa";
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $tab = $stmt->fetchAll();
            $exa = array();
            foreach ($tab as $exam) {
                $exa['no_ich_exa'] = $exam['no_ich_exa'];
                $exa['version_ich_exa'] = $exam['version_ich_exa'];
                $exa['v_exam_ich_exa'] = $exam['v_exam_ich_exa'];
                $exa['id_dom'] = $exam['id_dom'];
                $exa['date_heure_exa'] = $exam['date_heure_exa'];
                $exa['duree_exa'] = $exam['duree_exa'];
                $exa['temps_supp_dys_exa'] = $exam['temps_supp_dys_exa'];
                $exa['definitif_exa'] = $exam['definitif_exa'];
            }
            return $exa;
        } catch (Exception $e) {
            return false;
        }
    }

    /*
     * Liste de tout les examens actifs (pour la page list_examen.php)
     * @return array
     */
    function list_exam() {
        $query = "SELECT EXA.*, DESCM.*, "
                . "COUNT(CAN_EXA.ref_can) AS Candidats "
                . "FROM `t_examens` EXA "
                . "JOIN t_desc_modules DESCM ON EXA.no_ich_exa=DESCM.id_desc_mod "
                . "LEFT JOIN t_ref_can_exa CAN_EXA ON EXA.id_exa = CAN_EXA.ref_exa "
                . "WHERE EXA.active_archivage_exa = 0  AND YEAR(EXA.date_heure_exa) >= YEAR(UTC_DATE())-5 "
                . "GROUP BY EXA.id_exa, EXA.no_ich_exa, EXA.version_ich_exa "
                . "ORDER BY EXA.date_heure_exa ASC";

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * 2019-01-18 / JHI
     */
    function list_exa_by_date(){
        $tab_exa = $this->list_exam();
        foreach($tab_exa AS $exa){
            $tab_day[substr($exa['date_heure_exa'],0,10)][] = $exa;
        }
        return $tab_day;
    }


    /*
     * Récupère tout les experts pour un examen
     * @return array experts
     */
    function get_expert_exam($id_exa){
        $query = "SELECT PER.id_per, EXA.id_exa, EXA.no_ich_exa, EXA.date_heure_exa, PER.prenom_per, PER.nom_per, PER.email_per, REF.ref_fnc FROM `t_examens` EXA "
                ."LEFT JOIN t_ref_per_exa REF ON EXA.id_exa = REF.ref_exa "
                ."LEFT JOIN t_personnes PER on REF.ref_per = PER.id_per "
                ."WHERE EXA.active_archivage_exa = 0  AND YEAR(EXA.date_heure_exa) >= YEAR(UTC_DATE())-3 AND REF.ref_fnc = 1 "
                . "AND id_exa = :id_exa";
        
        $args['id_exa'] = $id_exa;
        
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $e) {
            return false;
        }
    }
    
    /*
     * Récupère tout les enseignants pour un examen
     * @return array enseignants
     */
    function get_profs_exam($id_exa){
        $query = "SELECT PER.id_per, EXA.id_exa, EXA.no_ich_exa, EXA.date_heure_exa, PER.prenom_per, PER.nom_per, PER.email_per, REF.ref_fnc FROM `t_examens` EXA "
                . "LEFT JOIN t_ref_per_exa REF ON EXA.id_exa = REF.ref_exa "
                . "LEFT JOIN t_personnes PER on REF.ref_per = PER.id_per "
                . "WHERE EXA.active_archivage_exa = 0  AND YEAR(EXA.date_heure_exa) >= YEAR(UTC_DATE())-3 AND REF.ref_fnc = 3 "
                . "AND id_exa = :id_exa";
        
        $args['id_exa'] = $id_exa;
        
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Redonne une liste de tous les candidats pr�sents dans un examen
     * @return array
     */
    public function get_can() {

        $query = "SELECT CAN.id_can, CAN.no_candidat_can, CAN.nom_can, CAN.prenom_can, REF.note_can, REF.remedie_note FROM t_examens AS EXA "
            . "JOIN t_ref_can_exa AS REF ON EXA.id_exa = REF.ref_exa "
            . "JOIN t_candidats AS CAN ON REF.ref_can = CAN.id_can "
            . "WHERE EXA.id_exa = :ref_exa ORDER BY `CAN`.`no_candidat_can` ASC";

        try {
            $stmt = $this->pdo->prepare($query);
            $args[':ref_exa'] = $this->get_id();
            $stmt->execute($args);
            return $stmt->fetchAll();
        } catch (Exception $e) {
            return false;
        }
    }

    /*
        * Methode de modification des notes des candidats
        * @return true or false
        * @author cp-16sdu
        * @version 11.14.18
        */
    public function modify_note_can($id_can, $note) {

        $args['note_can'] = $note;
        $args['id_can'] = $id_can;
        $args['id_exa'] = $this->get_id();

        $query = "UPDATE t_ref_can_exa SET note_can=:note_can WHERE ref_can=:id_can AND ref_exa=:id_exa";

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            return true;
        } catch (Exception $exc) {
            return false;
        }
    }

    /*
     * Methode de modification de la remédiation des notes des candidats
     * @return true or false
     * @author cp-16sdu
     * @version 11.14.18
     */
    public function remedie_can($id_can, $remedie) {

        $args['id_can'] = $id_can;
        $args['id_exa'] = $this->get_id();
        $args['remedie'] = $remedie;

        $query = "UPDATE t_ref_can_exa SET remedie_note=:remedie WHERE ref_can=:id_can AND ref_exa=:id_exa";

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            return true;
        } catch (Exception $exc) {
            return false;
        }
    }


    /**
     * renvoie l'id du prochain EXA TPI
     * @return bool|mixed
     */
    public function get_next_tpi(){
        $query = "SELECT DISTINCT ref_exa FROM t_tpi ORDER BY ref_exa DESC Limit 1";

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetch();
            return $tab;
        } catch (Exception $exc) {
            return false;
        }
    }

    /*
     * Transforme les minutes en heures minutes et secondes
     * @return Heure en 00:00:00
     */
    function min_to_hrs_min_sec($time, $format = '%02d:%02d:%02d') {
        if ($time < 1) {
            return;
        }
        $hours = floor($time / 60);
        $minutes = ($time % 60);
        $seconds = 00;
        return sprintf($format, $hours, $minutes, $seconds);
    }

    /*
     * Transforme les heures minutes secondes en minutes
     * @return int (minutes)
     */
    function hrs_min_sec_to_min($time) {
        $exp = explode(':', $time);
        $hours = $exp[0] * 60;
        $minutes = $exp[1];
        /*$seconds = floor($exp[2] / 60);*/
        $minutes_res = $hours + $minutes /*+ $seconds*/;
        return $minutes_res;
    }

    /*
     * Update de l'examen
     * @return boolean
     */
    function modifier_exam($id, $tab) {
        $args['id_exa'] = $id;

        $args['num_nom_exa'] = $tab['num_nom_exa'];
        $args['ann_exa'] = $tab['ann_exa'];
        $args['ver_exa'] = $tab['ver_exa'];
        $args['id_dom'] = $tab['id_dom'];
        $args['date_hrs_exa'] = $tab['date_hrs_exa'];
        $args['dur_exa'] = $tab['dur_exa'];
        $args['tmp_dys'] = $tab['tmp_dys'];
        $args['definitif_exa'] = $tab['definitif_exa'];

        $query = "UPDATE `t_examens` "
                . "SET `no_ich_exa` = :num_nom_exa, `version_ich_exa` = :ann_exa, "
                . "`v_exam_ich_exa` = :ver_exa, `id_dom` = :id_dom, "
                . "`date_heure_exa` = :date_hrs_exa, `duree_exa` = :dur_exa, "
                . "`temps_supp_dys_exa` = :tmp_dys, "
                . "`definitif_exa` = :definitif_exa "
                . "WHERE `t_examens`.`id_exa` = :id_exa";
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    /**
     * Retourne tout les dommaines (utiliser pour lister les options)
     * @return array
     */
    function get_all_domaines() {

        $query = "SELECT * FROM t_domaines";

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            return($tab);
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Renvoie : liste examen a venir (après date actuel)
     * @return array|bool
     */
    public function get_future_exam() {
        $query = "SELECT EXA.*, MD.nom_mod, COUNT(ref_can) AS nb_can FROM `t_examens` EXA "
                . "JOIN t_desc_modules MD ON EXA.no_ich_exa=MD.id_desc_mod "
                . "JOIN t_ref_can_exa CAN ON EXA.id_exa=CAN.ref_exa "
                . "WHERE YEAR(date_heure_exa) >= YEAR(NOW()) AND MONTH(date_heure_exa) >= MONTH(NOW()) "
                . "GROUP BY id_exa "
                . "ORDER BY date_heure_exa, no_ich_exa";

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $ex) {
            return false;
        }
    }
    
    /**
     * Renvoie la couleur pour chaque examen
     * @return array
     */
    public function get_couleur_examen($exa) {

        $query = "SELECT DISTINCT GRP.color_grp, GRP.nom_grp, count(CAN.id_can) AS nb_can FROM t_examens EXA
                JOIN t_ref_can_exa REF ON REF.ref_exa=EXA.id_exa
                JOIN t_candidats CAN ON CAN.id_can=REF.ref_can
                JOIN t_classes CLA ON CLA.id_cla=CAN.ref_classe
                JOIN t_groupes GRP ON GRP.id_grp=CLA.ref_grp 
                WHERE id_exa=:examen 
                GROUP BY GRP.id_grp";

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(array(':examen' => $exa));
            $tab = $stmt->fetchAll();

            return $tab;
        } catch (Exception $ex) {

            return false;
        }
    }

    /**
     * Set le champ convoc_exa
     * @return boolean
     */

    public function change_convocation($val) {
        $query = "UPDATE t_examens SET convoc_exa = :val WHERE t_examens.id_exa = :id_exa;";

        $args[':id_exa'] = $this->get_id();
        $args[':val'] = $val;

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            //$tab = $stmt->fetch();

            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * Cherche Cherche le nom du module pour un examen
     * @return String (mom du module)
     */

    public function get_nom_mod($id_exa){


        $query = 'SELECT MDL.nom_mod '
                . 'FROM `t_examens` AS EXA '
                . 'LEFT JOIN t_desc_modules MDL '
                . 'ON EXA.no_ich_exa=MDL.num_mod '
                . 'WHERE id_exa ='.$id_exa;
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetch();
            return $tab['nom_mod'];
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * Cherche la/les salle/s pour un examen 
     * (utiliser dans la partie Convocation et la liste des examens)
     * @return array
     */
    public function get_salles_exa(){
        $query = 'SELECT DISTINCT nom_sle,id_sle '
                . 'FROM t_salles SLE '
                . 'JOIN t_ref_examens_salles RES ON RES.ref_sle=SLE.id_sle '
                . 'JOIN t_examens EXA ON EXA.id_exa=RES.ref_exa '
                . 'WHERE EXA.id_exa=' . $this->get_id() . ' '
                . 'ORDER BY SLE.nom_sle ';

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $ex) {
            return false;
        }
    }

    public function get_salles_and_exa(){
        $query = 'SELECT DISTINCT * '
                . 'FROM t_salles SLE '
                . 'JOIN t_ref_examens_salles RES ON RES.ref_sle=SLE.id_sle '
                . 'JOIN t_examens EXA ON EXA.id_exa=RES.ref_exa '
                . 'WHERE EXA.id_exa=' . $this->get_id() . ' '
                . 'ORDER BY SLE.nom_sle ';

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            return $tab;
        } catch (Exception $ex) {
            return false;
        }
    }

    /*
     * Regarde si l'examen est déjà présent
     * @return boolean
     * $date_hrs_exa,
     */

    public function check_no_doublon($num_nom_exa, $ann_exa, $ver_exa) {

        $query = "SELECT * FROM t_examens WHERE no_ich_exa = :num_nom_exa "
                //. " AND date_heure_exa = :date_hrs_exa "
                . " AND version_ich_exa = :ann_exa "
                . " AND v_exam_ich_exa = :ver_exa ";
        try {
            $stmt = $this->pdo->prepare($query);
            $args[':num_nom_exa'] = $num_nom_exa;
            //$args[':date_hrs_exa'] = $date_hrs_exa;
            $args[':ann_exa'] = $ann_exa;
            $args[':ver_exa'] = $ver_exa;

            $stmt->execute($args);
            $tab = $stmt->fetch();
            if (is_array($tab)) {
                return false;
            } else {
                return true;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param $com_rapport
     * @return bool
     * Autor CP-16RDM
     * Date 14.01.2020
     */
    public function add_com_rapport_exa($com_rapport){
        $query = "UPDATE t_examens SET com_rapport = :com_rapport WHERE id_exa = :id_exa";
        try {
            $stmt = $this->pdo->prepare($query);
            $args[':com_rapport'] = $com_rapport;
            $args[':id_exa'] = $this->get_id();
            return $stmt->execute($args);
        } catch (Exception $e) {

        }
    }

    /**
     * Ajoute un candidat à un examen 
     * @return boolean Retourne true si tout est ok
     */
    public function add_candidat_examen($ref_can, $ref_exa) {
        $query = "INSERT INTO t_ref_can_exa SET ref_can = :ref_can, ref_exa = :ref_exa";
        try {
            $stmt = $this->pdo->prepare($query);
            $args[':ref_can'] = $ref_can;
            $args[':ref_exa'] = $ref_exa;
            return $stmt->execute($args);
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Ajoute des candidats à un examen 
     * @return boolean true si les candidats ont été ajoutés à l'examen 
     * @param type $tab_candidats $ref_exa
     */
    public function add_candidats_examen($tab_candidats, $ref_exa){
        foreach($tab_candidats as $can){
            //Ajoute le candidat à l'examen si il n'est pas encore dans la base 
            $query = "INSERT INTO t_ref_can_exa (ref_can, ref_exa) SELECT :ref_can, :ref_exa FROM t_ref_can_exa "
                    . "WHERE NOT EXISTS (SELECT ref_can, ref_exa FROM t_ref_can_exa WHERE"
                    . " ref_can = :ref_can AND ref_exa = :ref_exa ) limit 1";
            try {
                $stmt = $this->pdo->prepare($query);
                $args[':ref_can'] = $can['id_can'];
                $args[':ref_exa'] = $ref_exa;
                if(!$stmt->execute($args)){
                    //Retourne false seulement si il y a un problème lors de l'insert
                    return false; 
                }
            } catch (Exception $e) {
                return false;
            }
        }
        return true ; 
    }

    function set_type_archive($action,$status){
        try {
            $query = "UPDATE t_examens SET ".$action."=:value WHERE id_exa=:id_exa";
            $args = array();
            $args['id_exa'] = $this->get_id();
            if($status == 1) {
                $args['value'] = 1;
            }else{
                $args['value'] = 0;
            }
            //print_r($args);
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            return true;
        } catch (Exception $e) {
            return false;
            echo 'Exception reçue : ',  $e->getMessage(), "\n";
        }
    }



    function tables_candidats_salle($id_sle){
        try {
            //Recherche des tables dans une salle donnée
            $query = 'SELECT PLC.*, CAN.*, TAB.*, GRP.color_grp 
                      FROM t_examens EXA 
                      JOIN t_ref_can_exa CEX ON CEX.ref_exa=EXA.id_exa 
                      JOIN t_candidats CAN ON CAN.id_can=CEX.ref_can 
                      LEFT JOIN t_classes AS CLA ON CLA.id_cla=CAN.ref_classe
                      LEFT JOIN t_groupes AS GRP ON CLA.ref_grp=GRP.id_grp
                      JOIN `t_places` PLC ON PLC.ref_pls = EXA.ref_pls AND PLC.id_can=CAN.id_can 
                      JOIN t_table TAB ON PLC.ref_tab = TAB.id_tab 
                      WHERE EXA.id_exa='.$this->get_id().' AND TAB.ref_sle=' . $id_sle;

            /*
            echo $query = 'SELECT DISTINCT CLA.couleur_cla, CAN.no_candidat_can, CAN.id_can, CAN.dys_can, TAB.* '
                . 'FROM t_table AS TAB '
                . 'LEFT JOIN t_places AS PLS ON PLS.num_pc = TAB.pcname_tab '
                . 'LEFT JOIN t_candidats AS CAN ON CAN.no_candidat_can=PLS.num_can '
                . 'LEFT JOIN t_ref_can_exa AS EXA ON EXA.ref_can = CAN.id_can '
                . 'LEFT JOIN t_classes AS CLA ON CLA.id_cla=CAN.ref_classe '
                . 'WHERE TAB.ref_sle =' . $id_sle
                . ' AND (EXA.ref_exa IS NULL OR EXA.ref_exa ='.$this->get_id().')';*/
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tables_candidants = $stmt->fetchAll();
        } catch (Exception $ex) {

        }
        return $tables_candidants;
    }

    function is_fin_can($num_can){
        try {
            $query = "SELECT * FROM t_event_exa WHERE ref_exa=".$this->get_id()." 
            AND event_exa =\"Fin de l'examen pour le candidat ".$num_can."\"";

            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetch();
            //print_r($tab);
            if(is_array($tab)){
                return true;
            }else{
                return false;
            }
        } catch (Exception $ex) {
            return false;
        }
    }

    function get_can_fin($id_exa, $id_sle){
        $query = "SELECT DISTINCT fin_exa FROM `t_event_exa` 
                  WHERE `ref_exa` = :id_exa AND `ref_sle` = :id_sle 
                  AND fin_exa > 0 ORDER BY fin_exa";

        $args[':id_exa'] = $id_exa;
        $args[':id_sle'] = $id_sle;

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $tab = $stmt->fetchAll();

            $tab_fin = array();
            foreach ($tab as $can){
                $tab_fin[] = $can['fin_exa'];
            }

            return $tab_fin;
        } catch (Exception $ex) {
            return false;
        }
    }


    /**
     * Regarde si un candidat n'as pas déja été ajouté à un examen 
     * @return boolean Retourne true si le candidat n'est pas associé à cet examen 
     */
    public function check_doublon_can_exa($ref_can, $ref_exa) {
        $query = "SELECT * from t_ref_can_exa WHERE ref_exa = :ref_exa AND ref_can = :ref_can";
        try {
            $stmt = $this->pdo->prepare($query);
            $args[':ref_can'] = $ref_can;
            $args[':ref_exa'] = $ref_exa;
            $stmt->execute($args);
            if(empty($stmt->fetch())){
                return true;
            }
            return false;
        } catch (Exception $e) {
            return false;
        }
    }

    public function __toString() {
        $str = "\n<pre>\n";
        foreach ($this AS $key => $val) {
            if ($key != "pdo") {
                $str .= "\t" . $key;
                $lengh_key = strlen($key);
                for ($i = strlen($key); $i < 20; $i++) {
                    $str .= "&nbsp;";
                }
                $str .= "=>&nbsp;&nbsp;&nbsp;" . $val . "\n";
            }
        }
        $str .= "\n</pre>";
        return $str;
    }

    public function add_expert_exam($tab){
        $args['id_exa'] = $this->get_id();
        $args['exp_exa'] = $tab['exp_exa'];

        $query = "INSERT INTO t_ref_per_exa "
            . "SET ref_per = :exp_exa, "
            . "ref_fnc = 1, "
            . "ref_exa = :id_exa";
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    public function add_enseignant_exam($tab) {
        $args['id_exa'] = $this->get_id();
        $args['ens_exa'] = $tab['ens_exa'];

        $query = "INSERT INTO t_ref_per_exa "
            . "SET ref_per = :ens_exa, "
            . "ref_fnc = 3, "
            . "ref_exa = :id_exa";
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    function del_enseignant_expert_exam($tab) {
        $args['ref_exa'] = $tab['id_exa'];
        $args['ref_per'] = $tab['id_per'];
        $args['ref_fnc'] = $tab['ref_fnc'];

        $query = "DELETE FROM t_ref_per_exa WHERE ref_exa = :ref_exa AND ref_per =:ref_per AND ref_fnc = :ref_fnc";
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
        } catch (Exception $e) {
            return false;
        }
        return true;
    }


    public function add_materiel_exam($tab) {
        $args = array();
        $args['id_exa'] = $this->get_id();
        $args['lec_exa'] = $tab['lec_exa'];
        $args['salle_inf_exa'] = $tab['salle_inf_exa'];
        $args['reseau_exa'] = $tab['reseau_exa'];
        $args['moyens_aut_exa'] = $tab['materiel_exa'];
        $args['soft_exa'] = $tab['soft_exa'];
        $args['data_aut_exa'] = $tab['data_aut_exa'];
        $args['infos_begin_exa'] = $tab['infos_begin_exa'];

        $query = "UPDATE t_examens SET 
                    lec_exa = :lec_exa, 
                    type_salle_exa = :salle_inf_exa, 
                    reseau_exa =:reseau_exa,
                    moyens_aut_exa = :moyens_aut_exa, 
                    soft_exa =:soft_exa,
                    data_aut_exa=:data_aut_exa,
                    infos_begin_exa =:infos_begin_exa
                    WHERE t_examens.id_exa = :id_exa";
        try {
            $stmt = $this->pdo->prepare($query);
            if($stmt->execute($args)){
                return true;
            }else{
                return false;
            }
        } catch (Exception $e) {
            //echo 'Exception reçue : ',  $e->getMessage(), "\n";
            return false;
        }
    }


    public function get_materiel_exam($id) {
        $args['id_exa'] = $id;

        $query = "SELECT "
            . "moyens_aut_exa, "
            . "lec_exa, "
            . "type_salle_exa, "
            . "reseau_exa "
            . "FROM t_examen "
            . "WHERE t_examens.id_exa = :id_exa";
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $tab = $stmt->fetchAll();
            $exa = array();
            foreach ($tab as $exam) {
                $exa['aut_exa'] = $exam['moyens_aut_exa'];
                $exa['lec_exa'] = $exam['lec_exa'];
                $exa['salle_inf_exa'] = $exam['type_salle_exa'];
                $exa['reseau_exa'] = $exam['reseau_exa'];
            }
            return $exa;
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    function start_exa($id_sle,$duree_exa, $duree_dys, $heure_db){
        try {
            //Insertion des candidats dans l'événement
            $query = 'UPDATE t_ref_examens_salles 
                      SET debut_exa=:debut_exa, duree_exa=:duree_exa, duree_dys_exa=:duree_dys_exa 
                      WHERE ref_sle = :id_sle AND ref_exa=:id_exa';

            $args = array();
            $args[':debut_exa'] = $heure_db;
            $args[':duree_exa'] = $duree_exa;
            $args[':duree_dys_exa'] = $duree_dys;
            $args[':id_sle'] = $id_sle;
            $args[':id_exa'] = $this->get_id();
            //print_r($args);
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
        } catch (Exception $ex) {
            echo "Erreur";
        }
    }


    function get_heure_db_exa_by_sle($id_sle){
        try {
            //Insertion des candidats dans l'événement
            $query = 'SELECT * FROM t_ref_examens_salles 
                      WHERE ref_sle = :id_sle AND ref_exa=:id_exa';

            $args = array();

            $args[':id_sle'] = $id_sle;
            $args[':id_exa'] = $this->get_id();
            //print_r($args);
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            return $stmt->fetch();
        } catch (Exception $ex) {
            echo "Erreur";
        }
    }


    function get_pos_can($id_can){
        $id_pls = $this->get_id_pls();
        $pls = new Plan($id_pls);
        $pls->get_tab_id_sle();
        //echo $pls;
        return  $pls->get_pos_can($id_can);

    }

    /**
     * Renvoie tous les examens qui sont disponnible pour un plan
     */
    function get_exam_for_plan()
    {
        try {
        $query = 'SELECT DESC_MOD.num_mod, EXA.id_exa, EXA.v_exam_ich_exa, EXA.date_heure_exa, SLE.nom_sle
                    FROM t_examens EXA
                    JOIN t_desc_modules DESC_MOD ON DESC_MOD.id_desc_mod = EXA.no_ich_exa
                    JOIN t_ref_examens_salles EXA_SLE ON EXA_SLE.ref_exa = EXA.id_exa
                    JOIN t_salles SLE ON SLE.id_sle = EXA_SLE.ref_sle
                    WHERE EXA.ref_pls = 0
                    AND EXA.date_heure_exa >= NOW()';
        $stmt = $this->pdo->prepare($query);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            return false;
        }
    }

    public function get_grp_cla_can() {

        $query = "SELECT CAN.id_can, CAN.no_candidat_can, CAN.nom_can, CAN.prenom_can, REF.note_can, REF.remedie_note, CLA.nom_cla, GRP.color_grp, GRP.nom_grp FROM t_examens AS EXA "
            . "JOIN t_ref_can_exa AS REF ON EXA.id_exa = REF.ref_exa "
            . "JOIN t_candidats AS CAN ON REF.ref_can = CAN.id_can "
            . "JOIN t_classes AS CLA ON CLA.id_cla = CAN.ref_classe "
            . "JOIN t_groupes AS GRP ON CLA.ref_grp = GRP.id_grp "
            . "WHERE EXA.id_exa = :ref_exa ORDER BY `CAN`.`no_candidat_can` ASC";

        try {
            $stmt = $this->pdo->prepare($query);
            $args[':ref_exa'] = $this->get_id();
            $stmt->execute($args);
            return $stmt->fetchAll();
        } catch (Exception $e) {
            return false;
        }
    }



    function get_note_can($id_can){
        $query = "SELECT CAN_EXA.note_can 
                    FROM t_examens EXA
                    JOIN t_ref_can_exa CAN_EXA ON CAN_EXA.ref_exa = EXA.id_exa
                    JOIN t_candidats CAN ON CAN.id_can = CAN_EXA.ref_can
                    WHERE CAN.id_can = :id_can
                    AND EXA.id_exa = :id_exa";
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute([
                'id_can' => $id_can,
                'id_exa' => $this->get_id()
            ]);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function add_suivis($tab) {
        $args = array();
        $args['id_exa'] = $this->get_id();
        $args['archiver_cla_exa'] = $tab['archiver_cla_exa'];
        $args['saisi_note_exa'] = $tab['saisi_note_exa'];
        $args['archiver_dos_exa'] = $tab['archiver_dos_exa'];
        $args['valider_exa'] = $tab['valider_exa'];
        $args['archiver_cd_ep_exa'] = $tab['archiver_cd_ep_exa'];
        $args['archiver_cd_exa'] = $tab['archiver_cd_exa'];
        //print_r($args);
        $query = "UPDATE t_examens SET 
                    archiver_cla_exa = :archiver_cla_exa, 
                    saisi_note_exa = :saisi_note_exa, 
                    archiver_dos_exa =:archiver_dos_exa,
                    valider_exa = :valider_exa, 
                    archiver_cd_ep_exa =:archiver_cd_ep_exa,
                    archiver_cd_exa=:archiver_cd_exa
                    WHERE t_examens.id_exa = :id_exa";
        try {
            $stmt = $this->pdo->prepare($query);
            if($stmt->execute($args)){
                return true;
            }else{
                return false;
            }
        } catch (Exception $e) {
            //echo 'Exception reçue : ',  $e->getMessage(), "\n";
            return false;
        }
        return true;
    }

    public function add_active_archivage_exa($active_archivage_exa) {
        $args= array();
        $args['id_exa'] = $this->get_id();
        $args['active_archivage_exa'] = $active_archivage_exa;
        $query = "UPDATE t_examens SET 
                    active_archivage_exa = :active_archivage_exa
                    WHERE t_examens.id_exa = :id_exa";
        try {
            $stmt = $this->pdo->prepare($query);
            if($stmt->execute($args)){
                return true;
            }else{
                return false;
            }
        } catch (Exception $e) {
            //echo 'Exception reçue : ',  $e->getMessage(), "\n";
            return false;
        }
        return true;
    }

    public function add_archiver_cla_exa($archiver_cla_exa) {
        $args= array();
        $args['id_exa'] = $this->get_id();
        $args['archiver_cla_exa'] = $archiver_cla_exa;
        $query = "UPDATE t_examens SET 
                    archiver_cla_exa = :archiver_cla_exa
                    WHERE t_examens.id_exa = :id_exa";
        try {
            $stmt = $this->pdo->prepare($query);
            if($stmt->execute($args)){
                return true;
            }else{
                return false;
            }
        } catch (Exception $e) {
            //echo 'Exception reçue : ',  $e->getMessage(), "\n";
            return false;
        }
        return true;
    }

    public function add_saisi_note_exa($saisi_note_exa) {
        $args= array();
        $args['id_exa'] = $this->get_id();
        $args['saisi_note_exa'] = $saisi_note_exa;
        $query = "UPDATE t_examens SET  
                    saisi_note_exa =:saisi_note_exa
                    WHERE t_examens.id_exa = :id_exa";
        try {
            $stmt = $this->pdo->prepare($query);
            if($stmt->execute($args)){
                return true;
            }else{
                return false;
            }
        } catch (Exception $e) {
            //echo 'Exception reçue : ',  $e->getMessage(), "\n";
            return false;
        }
        return true;
    }

    public function add_archiver_dos_exa($archiver_dos_exa) {
        $args= array();
        $args['id_exa'] = $this->get_id();
        $args['archiver_dos_exa'] = $archiver_dos_exa;
        $query = "UPDATE t_examens SET  
                    archiver_dos_exa =:archiver_dos_exa
                    WHERE t_examens.id_exa = :id_exa";
        try {
            $stmt = $this->pdo->prepare($query);
            if($stmt->execute($args)){
                return true;
            }else{
                return false;
            }
        } catch (Exception $e) {
            //echo 'Exception reçue : ',  $e->getMessage(), "\n";
            return false;
        }
        return true;
    }

    public function add_valider_exa($valider_exa) {
        $args= array();
        $args['id_exa'] = $this->get_id();
        $args['valider_exa'] = $valider_exa;
        $query = "UPDATE t_examens SET  
                    valider_exa =:valider_exa
                    WHERE t_examens.id_exa = :id_exa";
        try {
            $stmt = $this->pdo->prepare($query);
            if($stmt->execute($args)){
                return true;
            }else{
                return false;
            }
        } catch (Exception $e) {
            //echo 'Exception reçue : ',  $e->getMessage(), "\n";
            return false;
        }
        return true;
    }

    public function add_archiver_cd_ep_exa($archiver_cd_ep_exa) {
        $args= array();
        $args['id_exa'] = $this->get_id();
        $args['archiver_cd_ep_exa'] = $archiver_cd_ep_exa;
        $query = "UPDATE t_examens SET  
                    archiver_cd_ep_exa =:archiver_cd_ep_exa
                    WHERE t_examens.id_exa = :id_exa";
        try {
            $stmt = $this->pdo->prepare($query);
            if($stmt->execute($args)){
                return true;
            }else{
                return false;
            }
        } catch (Exception $e) {
            //echo 'Exception reçue : ',  $e->getMessage(), "\n";
            return false;
        }
        return true;
    }

    public function add_archiver_cd_exa($archiver_cd_exa)
    {
        $args= array();
        $args['id_exa'] = $this->get_id();
        $args['archiver_cd_exa'] = $archiver_cd_exa;
        $query = "UPDATE t_examens SET  
                    archiver_cd_exa =:archiver_cd_exa
                    WHERE t_examens.id_exa = :id_exa";
        try {
            $stmt = $this->pdo->prepare($query);
            if($stmt->execute($args)){
                return true;
            }else{
                return false;
            }
        } catch (Exception $e) {
            //echo 'Exception reçue : ',  $e->getMessage(), "\n";
            return false;
        }
        return true;
    }

    function calcul_checksum(){

//        $tab = $this->sql->select("t_ref_can_exa,t_candidats","*","WHERE ref_can=id_can AND ref_exa = ".$this->get_id()." Order by id_can",0);
        $tab = $this->get_can();

//        for($i=0;$i<$this->nombre_candidats();$i++){
        for($i=0;$i<sizeof($tab);$i++){
//            $can = new candidat($this->sql,$tab[$i]['id_can']);
//            $can = new candidat($tab[$i]['id_can']);
//            $tab_notes[] = $can->note_exa($this->get_id());
            $tab_notes[] = $this->get_note_can($tab[$i]['id_can']);
        }
        // Moyenne de l'exa
        $somme = 0;
        $echec = 1;
        for($i=0;$i<sizeof($tab_notes);$i++){
            $somme += $tab_notes[$i];
            if($tab_notes[$i]<4){
                $echec++;
            }
        }
        if($somme ==0){
            return 0;
        }
        //$check_1 = substr(floor(($somme/sizeof($tab_notes))*1000),0,-1);

        // Moyenne des candidats paires de l'exa
        // 3 derniers chiffres de la moyenne arrondie � 2 d�cimales * 1000
        $somme = 0;
        for($i=0;$i<sizeof($tab_notes);$i=$i+2){
            $somme += $tab_notes[$i];
        }
        $check_1 = substr(floor(($somme/sizeof($tab_notes))*1000),1);

        // Moyenne des candidats impaires de l'exa
        // 3 premiers chiffres de la moyenne arrondie � 2 d�cimales * 1000
        $somme = 0;
        for($i=1;$i<sizeof($tab_notes);$i=$i+2){
            $somme += $tab_notes[$i];
        }
        $check_2 = substr(floor(($somme/sizeof($tab_notes))*1000),0,-1);

        // Moyenne des candidats 0,3,6,9... de l'exa
        // 3 premiers chiffres de la moyenne arrondie � 2 d�cimales * 1000
        $somme = 0;
        for($i=0;$i<sizeof($tab_notes);$i=$i+3){
            $somme += $tab_notes[$i];
        }
        $check_3 = substr(floor(($somme/sizeof($tab_notes))*1000),0,-1);

        // Moyenne des candidats 1,4,7... de l'exa
        // 3 premiers chiffres de la moyenne arrondie � 2 d�cimales * 1000
        $somme = 0;
        for($i=1;$i<sizeof($tab_notes);$i=$i+3){
            $somme += $tab_notes[$i];
        }
        $check_4 = substr(floor(($somme/sizeof($tab_notes))*1000),0,-1);


        // Moyenne des candidats 2,5,8... de l'exa
        // 3 premiers chiffres de la moyenne arrondie � 2 d�cimales * 1000
        $somme = 0;
        for($i=2;$i<sizeof($tab_notes);$i=$i+3){
            $somme += $tab_notes[$i];
        }
        $check_5 = substr(floor(($somme/sizeof($tab_notes))*1000),0,-1);

        // md5 sur la cha�ne
        $str = md5((99-$echec).$check_1.$check_2.$check_3.$check_4.$check_5);

        //Suppression des caract�res non num�riques
        $str = preg_replace('/[^[:digit:]]/',"",$str);

        // limitation � 12 caract�res
        $str = substr($str,0,12);
        $this->checksum = $str;
        return($this->checksum);
    }

    function get_id() {
        return $this->id;
    }

    function get_num_nom() {
        return $this->num_nom;
    }

    function get_soft_exa() {
        return $this->soft_exa;
    }

    function get_infos_begin_exa() {
        return $this->infos_begin_exa;
    }

    function get_ann() {
        return $this->ann;
    }

    function get_ver() {
        return $this->ver;
    }

    function get_id_dom() {
        return $this->id_dom;
    }

    function get_date_hrs() {
        return $this->date_hrs;
    }

    function get_heure_db_exa(){
       return date("H:i",strtotime($this->date_hrs));
    }

    function get_date_hrs_jour_date(){
        return strftime("%A %e %B", strtotime($this->date_hrs));
    }

    function get_date_hrs_heure(){
        return strftime("%Hh%M", strtotime($this->date_hrs));
    }

    function get_dur() {
        return $this->dur;
    }

    function get_sal() {
        return $this->sal;
    }

    function get_nom(){
        return $this->nom;
    }

    function set_id($id) {
        $this->id = $id;
    }

    function set_num_nom($num_nom) {
        $this->num_nom = $num_nom;
    }

    function set_ann($ann) {
        $this->ann = $ann;
    }

    function set_ver($ver) {
        $this->ver = $ver;
    }

    function set_id_dom($id_dom) {
        $this->id_dom = $id_dom;
    }

    function set_date_hrs($date_hrs) {
        $this->date_hrs = $date_hrs;
    }

    function set_dur($dur) {
        $this->dur = $dur;
    }

    function set_sal($sal) {
        $this->sal = $sal;
    }

    function set_validation($validation) {
        $this->validation = $validation;
    }

    function set_com_rapport($com_rapport) {
        $this->com_rapport = $com_rapport;
    }

    function get_com_rapport() {
        return $this->com_rapport;
    }

    function get_validation() {
        return $this->validation;
    }

    function get_materiel_exa() {
        return $this->materiel_exa;
    }

    function get_lec_exa() {
        return $this->lec_exa;
    }

    function get_id_pls() {
        return $this->id_pls;
    }

    function get_salle_inf_exa() {
        return $this->salle_inf_exa;
    }

    function get_reseau_exa() {
        return $this->reseau_exa;
    }

    function set_materiel_exa($materiel_exa) {
        $this->materiel_exa = $materiel_exa;
    }

    function set_soft_exa($soft_exa) {
        $this->soft_exa = $soft_exa;
    }

    function set_infos_begin_exa($infos_begin_exa) {
        $this->infos_begin_exa = $infos_begin_exa;
    }

    function set_lec_exa($lec_exa) {
        $this->lec_exa = $lec_exa;
    }

    function set_dep_exa($dep_exa) {
        $this->dep_exa = $dep_exa;
    }

    function set_nom($nom) {
        $this->nom = $nom;
    }

    function set_id_pls($id_pls) {
        $this->id_pls = $id_pls;
    }

    function set_description($description) {
        $this->description = $description;
    }


    function set_salle_inf_exa($salle_inf_exa) {
        $this->salle_inf_exa = $salle_inf_exa;
    }

    function set_reseau_exa($reseau_exa) {
        $this->reseau_exa = $reseau_exa;
    }

    /**
     * @return mixed
     */
    public function get_data_exa()
    {
        return $this->data_exa;
    }

    /**
     * @param mixed $data_exa
     */
    public function set_data_exa($data_exa)
    {
        $this->data_exa = $data_exa;
    }


    /**
     * @return mixed
     */
    public function get_definitif()
    {
        return $this->definitif;
    }

    /**
     * @param mixed $definitif
     */
    public function set_definitif($definitif)
    {
        $this->definitif = $definitif;
    }

    /**
     * @return mixed
     */
    public function get_archive()
    {
        return $this->archive;
    }

    /**
     * @param mixed $archive
     */
    public function set_archive($archive)
    {
        $this->archive = $archive;
    }

    /**
     * @return mixed
     */
    public function get_notes()
    {
        return $this->notes;
    }

    /**
     * @param mixed $notes
     */
    public function set_notes($notes)
    {
        $this->notes = $notes;
    }

    /**
     * @return mixed
     */
    public function get_cd_can()
    {
        return $this->cd_can;
    }

    /**
     * @param mixed $cd_can
     */
    public function set_cd_can($cd_can)
    {
        $this->cd_can = $cd_can;
    }

    /**
     * @return mixed
     */
    public function get_cd_exa()
    {
        return $this->cd_exa;
    }

    /**
     * @param mixed $cd_exa
     */
    public function set_cd_exa($cd_exa)
    {
        $this->cd_exa = $cd_exa;
    }

    /**
     * @return mixed
     */
    public function get_epr_can()
    {
        return $this->epr_can;
    }

    /**
     * @param mixed $epr_can
     */
    public function set_epr_can($epr_can)
    {
        $this->epr_can = $epr_can;
    }

    /**
     * @return mixed
     */
    public function get_epr_cla()
    {
        return $this->epr_cla;
    }

    /**
     * @param mixed $epr_cla
     */
    public function set_epr_cla($epr_cla)
    {
        $this->epr_cla = $epr_cla;
    }

    /**
     * @return mixed
     */
    public function get_epr_archive()
    {
        return $this->epr_archive;
    }

    /**
     * @param mixed $epr_archive
     */
    public function set_epr_archive($epr_archive)
    {
        $this->epr_archive = $epr_archive;
    }

    /**
     * @return mixed
     */
    public function get_rapport()
    {
        return $this->rapport;
    }

    /**
     * @param mixed $rapport
     */
    public function set_rapport($rapport)
    {
        $this->rapport = $rapport;
    }

}

<?php
session_start();
require("./config/config.inc.php");
require(WAY . "/includes/autoload.inc.php");
require_once(WAY . "/Public/includes/head.inc.php");
ini_set('display_errors',1);
?>

<div class="row">
    <div class="header">
        <h3>Mot de passe perdu</h3>
    </div>
</div>

<div class="panel panel-primary">
    <div class="panel-heading">
        Demande d'un nouveau mot de passe
    </div>
    <div class="panel-body">
        <?php
        if(isset($_POST['generer'])){

            $per = new Personne();
            $per->init_by_mail($_POST['email']);

            $password = $per->gen_random_password(8,"lower_case,upper_case,numbers,special_symbols");
//            echo $password;
            $per->gen_password($password);
            $per->update_password();
            $per->new_pwd($password);
            echo "<h1>un nouveau mot de passe vous a été envoyé !!</h1>";
            echo "Vérifiez votre dossier spam également....";
            echo "<a href=\"login.php\" >Retour à la page de login</a>";

        }else {
            ?>
            <div class="col-md-4">
                <form name=form method="Post" action="<?= $_SERVER['PHP_SELF'] ?>">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input type="email" class="form-control" placeholder="Votre adresse e-mail" autofocus id="email_can" name="email">
                    </div>
                    <br>
                    <div class="input-group">
                        <button class="btn btn-primary btn-block" type="submit" id="generer" name="generer">Générer un nouveau mot de passe</button>
                    </div>
                </form>
            </div>
        <?php
        }

        ?>

    </div>

    <div class="panel-footer">
        ICT-JB.net
    </div>
</div>
</div>
</body>
</html>

<!doctype html>
<html lang="fr">
    <head>
        <!--meta http-equiv="Content-Security-Policy" content="default-src 'self' ;style-src https://* 'unsafe-inline'; img-src https://* ;  child-src 'none'; "--> 
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <title>ICT-JB</title>

        <!-- css utilisée dans tout le site -->
        <link rel="stylesheet" href="<?php echo URL; ?>css/global.css">

        <!-- css spécifique à chaque module (dossier) -->
        <link rel="stylesheet" href="./css/module.css">


        <!--- css de bootatrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!--- date picker -->
<!--        <link rel="stylesheet" href="../plugins/datepicker/css/datepicker3.css" media="screen">-->
        <!--- JQuery -->
        <script src="https://code.jquery.com/jquery-3.2.1.js"></script>
        
        <!-- JQuery UI -->
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        
        <!--- Bootstrap -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

        <!--- JQuery validate -->
        <script src="<?php echo URL; ?>plugins/jquery.validate.js"></script>
        
        <!--- Librairie js personnelle -->
        <script src="<?php echo URL; ?>js/functions.js"></script>
        
        <!--- Js pour la barre de recherche -->
        <script src="<?php echo URL; ?>js/search_candidat.js"></script>

        <!--<link rel="manifest" href="/manifest.json" />
		<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
		<script>
		  var OneSignal = window.OneSignal || [];
		  OneSignal.push(function() {
			OneSignal.init({
			  appId: "2e4ef0a0-4615-4d31-81fe-5e1da08e45ee",
			});
		  });
		</script>-->
    </head>
    <body>
        <div class="container-fluid">
            <div id="loading">
                <img id="loading_img" src="<?php echo URL; ?>icones/loading.gif">
            </div>

            <?php
            if (isset($_SESSION['id'])) {
                ?>
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="<?php echo URL; ?>">ICT-JB</a>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                <?php

                                //Autorisation pour la gestion des utilisateurs
                                if($per->check_aut("ADM_USR")) {
                                ?>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Personnes<span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo URL; ?>Personnes/index.php?type=exp">Experts</a></li>
                                        <li><a href="<?php echo URL; ?>Personnes/index.php?type=ens">Enseignants</a></li>
                                        <li><a href="<?php echo URL; ?>Personnes/index.php?type=sup">Supp&eacute;rieurs professionnels</a></li>
                                        <li><a href="<?php echo URL; ?>entreprises/index.php">Employeurs</a></li>
                                        <li><a href="<?php echo URL; ?>Personnes/index.php?type=ina">Inactifs</a></li>
                                    </ul>
                                </li>
                                <?php
                                }

                                //Autorisation pour la vue des groupes et des classes
                                if($per->check_aut("ADM_CAN;USR_GRP")) {
                                ?>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Candidats<span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <?php
                                        if($per->check_aut("USR_GRP")) {
                                        ?>
                                        <li><a href="<?php echo URL; ?>groupes/classes.php">Classes</a></li>
                                        <li><a href="<?php echo URL; ?>groupes/groupes.php">Groupes</a></li>
                                        <?php
                                        }
                                        if($per->check_aut("ADM_CAN")) {
                                        ?>
                                        <form id='liste_form' action='<?php echo URL ?>Candidats/liste.php' method='post'>
                                            <?php
                                                $grp = new Groupe();
                                                $List_grp = $grp->get_all_no_termine();
                                                foreach($List_grp AS $group){
                                                    echo "<li><label class='dropdown_label' for='modifier_".$group['id_grp']."'>".$group['nom_grp']."</label></li>"
                                                    ."<input type=submit id=modifier_".$group['id_grp']." class='btn-primary' value='".$group['id_grp']."' name='liste' hidden='true'>";
                                                }
                                            ?>
                                        </form>
                                        <li><a href="<?php echo URL; ?>Candidats/ajouter.php">Ajouter</a></li>
                                        <?php
                                        }
                                        ?>
                                    </ul>
                                </li>
                                <?php
                                }

                                //Autorisation pour visualiser les examens
                                if($per->check_aut("USR_EXA")) {
                                ?>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-haspopup="true" aria-expanded="false">Examens<span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo URL; ?>examens/liste_examen.php">Liste des examens</a></li>
                                        <?php
                                        if ($per->check_aut("ADM_USR")) {
                                            ?>
                                            <li><a href="<?php echo URL; ?>examens/suivi_exa.php">Suivi</a></li>
                                            <li><a href="<?php echo URL; ?>examens/add_examen.php">Ajouter un examen</a></li>
                                            <li><a href="<?php echo URL; ?>Convocations/index.php">Convocations</a>
                                            </li>
                                            <li><a href="<?php echo URL; ?>examens/suivi.php">Suivis</a></li>
                                            <li><a href="<?php echo URL; ?>plans/">Plans des tables</a></li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                </li>
                                <?php
                                }

                                //Autorisation pour visualiser les examens
                                if($per->check_aut("ADM_EXA")) {
                                    ?>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                           aria-haspopup="true" aria-expanded="false">Administration<span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="<?= URL ?>examens/exa_futurs.php">Examens à venir</a></li>
                                        </ul>
                                    </li>
                                    <?php
                                }




                                //Autorisation de droits d'accès .. Partie module ?
                                if($per->check_aut("ADM_AUT")){
                                ?>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Gestion<span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <!-- Partie module non existante !-->
                                        <li><a href="<?php echo URL; ?>modules/index.php">Modules ICT</a></li>
                                        <li><a href="<?php echo URL; ?>droits/autorisations.php">Autorisations</a></li>
                                        <li><a href="<?php echo URL; ?>droits/attribution_fnc_per.php">Attribution fonctions au personnes</a></li>
                                        <li><a href="<?php echo URL; ?>droits/attribution_aut_fnc.php">Attribution fonctions au autorisations</a></li>
                                        <li><a href="<?php echo URL; ?>droits/fonctions.php">Fonctions</a></li>
                                        <li><a href="<?php echo URL; ?>Modules/add_module.php">Modules</a></li>
                                        
                                    </ul>
                                </li>
                                <?php
                                }
//&& $exa->has_tpi($can->get_id())
                                //Autorisation
                                if($per->check_aut("USR_TPI")){
                                ?>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">TPI<span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo URL; ?>TPI/liste.php">Liste</a></li>
                                        <?php if($per->check_aut("ADM_TPI")){ ?>
                                        <li><a href="<?php echo URL; ?>TPI/index.php">R&eacute;partition</a></li>
                                        <li><a href="<?php echo URL; ?>TPI/seances.php">S&eacute;ances</a></li>
                                        <?php } ?>
                                    </ul>
                                </li>
                                <?php
                                }
                                ?>
                            </ul>
                            <form id="form_choice_can" class="navbar-form navbar-left" name="form_choice_can" action="<?php echo URL; ?>Candidats/profil.php" method="post">
                                <div class="ui-widget">
                                    <!--Barre de recherche-->
                                    <input id="search" name="id_can" class="form-control" placeholder="Recherche de candidats">
                                    <input type="hidden" name="id_choice_can" id="id_choice_can">
                                    <input type="hidden" name="url" id="url" value='<?php echo URL; ?>'>
                                </div>
                            </form>
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="<?php echo URL; ?>Personnes/profil.php">Profil</a></li>
                                <li><a href="<?php echo URL; ?>logout.php">D&eacute;connexion</a></li>
                            </ul>
                        </div>
                    </div>                    
                </nav>
                <?php
            }
            ?>
            <!-- Zone de notification -->
            <div class="alert" id="alert">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong class="bold"></strong><span class="message"></span>
            </div>   
<!doctype html>
<html lang="fr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <title>ICT-JB</title>

        <!-- css utilisée dans tout le site -->
        <link rel="stylesheet" href="<?php echo URL; ?>/css/global.css">

        <!-- css spécifique à chaque module (dossier) -->
        <link rel="stylesheet" href="css/module.css">

        <!--- css de bootatrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!--- JQuery -->
        <script src="https://code.jquery.com/jquery-3.2.1.js"></script>

        <!--- Bootstrap -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

        <!--- JQuery validate -->
        <script src="<?php echo URL; ?>plugins/jquery.validate.js"></script>

        <!--- Librairie js personnelle -->
        <script src="<?php echo URL; ?>js/functions.js"></script>


        <link rel="manifest" href="/manifest.json" />
        <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
        <script>
            var OneSignal = window.OneSignal || [];
            OneSignal.push(function() {
                OneSignal.init({
                    appId: "2e4ef0a0-4615-4d31-81fe-5e1da08e45ee",
                });
            });
        </script>

    </head>
    <body>
        <div class="container">

            <?php
            if (isset($_SESSION['id'])) {

                $can = new Candidat($_SESSION['id']);
                ?>
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="<?php echo URL; ?>Public/index.php"><?php echo $can->get_nom() . " " . $can->get_prenom() . " (" . $can->get_no_candidat_minus() . ")"; ?></a>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                <li><a href="<?php echo URL; ?>Public/Consignes/">Consignes</a></li>
                            </ul>
                            <ul class="nav navbar-nav">
                                <li><a href="<?php echo URL; ?>Public/notes/">Notes</a></li>
                            </ul>
                            <ul class="nav navbar-nav">
                                <li><a href="<?php echo URL; ?>Public/Calendar/">Agenda</a></li>
                            </ul>

                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="<?php echo URL; ?>Public/logout.php">Déconnexion</a></li>
                            </ul>
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="<?php echo URL; ?>Public/profile/profile.php">Mon profil</a></li>
                            </ul>
                            <ul class="nav navbar-nav">
                                <li><a href="<?php echo URL; ?>Convocations/pdf/convoc.pdf.php" target="_blank">Convocation</a></li>
                            </ul>
                            <?php
                            if($can->has_tpi()){
                                ?>
                                <ul class="nav navbar-nav">
                                    <li><a href="<?php echo URL; ?>Public/tpi/index.php" >TPI</a></li>
                                </ul>

                            <?php
                            }
                            ?>
                        </div>
                    </div>                    
                </nav>
                <?php
            }
            ?>

            <!-- Zone de notification -->
            <div class="alert" id="alert">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong class="bold"></strong><span class="message"></span>
            </div>
<?php

require_once(WAY . "/includes/autoload.inc.php");

if (isset($_SESSION['id'])) {
    $can = new Candidat($_SESSION['id']);

    if (!$can->check_connect()) {
        session_destroy();
        header('Location: ' . URL . 'Public/login.php');
        exit;
    }
} else {
    session_destroy();
    header('Location: ' . URL . 'Public/login.php');
    exit;
}
?>
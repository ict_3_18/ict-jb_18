<?php

require("../../config/config.inc.php");
require_once WAY . 'includes/autoload.inc.php';

$can = new Candidat(Projet::decrypt($_GET['code']));

echo "BEGIN:VCALENDAR\n"
 . "X-WR-CALNAME:Contrôles de compétences\n"
 . "VERSION:1.0\n";

foreach ($can->get_convoc() AS $conv) {
    /* Calcule des dates */
    $date_debut = date("Ymd\THis", strtotime($conv['date_heure_exa']));
    $date_fin = date("Ymd\THis", strtotime($conv['date_heure_exa']) + ($conv['duree_exa'] * 60));

    /* Affichage des salles */
    $examen = new Examen($conv['id_exa']);
    $tab_sle = $examen->get_salles_exa();
    $str_sle = "";
    foreach ($tab_sle As $sle) {
        $str_sle .= $sle['nom_sle'];
        if ($sle != end($tab_sle))
            $str_sle .= ", ";
    }
    if ($str_sle == "")
        $str_sle = "Atelier informatique étage D à définir.";

    /* Affichage du calendar */
    echo "BEGIN:VEVENT\n"
    . "SUMMARY:ICT " . $conv['num_mod'] . " - " . $conv['nom_mod'] . "\n"
    . "UID:" . date('Y-m-d-H-i-s') . "\n"
    . "DTSTART;TZID=Europe/Zurich:" . $date_debut . "\n"
    . "DTEND;TZID=Europe/Zurich:" . $date_fin . "\n"
    . "Description:Description: " . $conv['lien_desc'] . "\n"
    . "LOCATION:" . $str_sle . "\n"
    . "BEGIN:VALARM\n"
    . "TRIGGER:-PT7D\n"
    . "REPEAT:1\n"
    . "DURATION:PT1M\n"
    . "ACTION:DISPLAY\n"
    . "DESCRIPTION:CCO\n"
    . "END:VALARM\n"
    . "BEGIN:VALARM\n"
    . "TRIGGER:-PT1D\n"
    . "REPEAT:1\n"
    . "DURATION:PT1M\n"
    . "ACTION:DISPLAY\n"
    . "DESCRIPTION:CCO\n"
    . "END:VALARM\n"
    . "END:VEVENT\n";
}
echo "END:VCALENDAR";
?>
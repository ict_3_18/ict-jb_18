<?php
session_start();

require("../../config/config.inc.php");
require(WAY . "/includes/autoload.inc.php");
require WAY . '/Public/includes/secure.inc.php';
require_once(WAY . "/Public/includes/head.inc.php");

error_reporting(E_ALL);
ini_set('display_errors', 1);
$can = new Candidat($_SESSION['id']);

$id_exa = 504;
$exa = new Examen($id_exa);
?>



<div class="panel panel-primary">
    <div class="panel-heading">
        Ma position à l'examen ICT <?= $exa->get_num_nom() ?>
    </div>
    <div class="panel-body">
                <?php
                    //    print_r($exam);
                    $tab_sle = $exa->get_salles_exa();
                    foreach($tab_sle AS $sle){
                        echo " ".$sle['nom_sle'];
                    }
                    //echo $can->get_id();
                    //echo $exa->get_nom_mod();
                    echo $exa;

                $place = $exa->get_pos_can($_SESSION['id']);
                echo $nom_salle = substr($place,3,4);
                $sle = new salle();
                $sle->init_by_name($nom_salle);
               // echo $sle;
                echo $sle->gen_plan(800,600,$place);

                ?>

    </div>

    <div class="panel-footer">
        Ceff industrie
    </div>
</div>

<div class="panel panel-primary">
    <div class="panel-heading">
        Lien pour ajoter son agenda à Outlook, google agenda, ...
    </div>
    <div class="panel-body">
        <div class="col-md-offset-2 col-md-8">

            <div class="input-group url">
                <input type="text" class="form-control text" disabled value="<?php echo URL; ?>Calendar/cal_can.php?code=<?php echo Projet::encrypt($_SESSION['id']); ?>">
                <span class="input-group-btn">
                    <button class="btn btn-primary" type="button"><span class="glyphicon glyphicon-copy" aria-hidden="true"></span></button>
                </span>
            </div>

        </div>
    </div>

    <div class="panel-footer">
        Ceff industrie
    </div>
</div>
</div>
<script>
    $('.url').click(function () {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val($('.text').val()).select();
        document.execCommand("copy");
        $temp.remove();
        message("Le lien a été copié", "success");
    });
</script>
</body>
</html>
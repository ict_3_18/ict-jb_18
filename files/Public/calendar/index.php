<?php
session_start();

require("../../config/config.inc.php");
require(WAY . "/includes/autoload.inc.php");
require WAY . '/Public/includes/secure.inc.php';
require_once(WAY . "/Public/includes/head.inc.php");
$exa = new Examen($can->get_id());
?>
<div class="col-md-6">
    <div class="panel panel-primary">
        <div class="panel-heading">
            Convocation aux prochains examens
        </div>
        <div class="panel-body">
            <?php
            $can = new Candidat($_SESSION['id']);
            $tab_exa = $can->get_convoc("new");

            if($tab_exa){
                echo "<h3>Nouveau(x) examen(s) annoncé(s)</h3>";


                foreach($tab_exa AS $exam) {
                    $exa = new Examen($exam['id_exa']);
                    echo "<hr>";
                    echo $exa->get_num_nom() . " - " . $exa->get_nom() . "<br>";
                    echo $exa->get_heure_db_exa() . " - " . $exa->get_date_hrs_jour_date() . "<br>";
                    echo "<hr>";
                }
                $nb_exa = count($tab_exa);


                ?>

                <button id="submit" id_can="<?=$_SESSION['id'];?>" class="btn btn-danger "><b>J'ai pris connaissance de mes examens à venir !</b></button><br>
                <?php
            }
            $tab_exa = $can->get_convoc("old");

            if($tab_exa){
                echo "<h3>Examen(s) à venir</h3>";
            }

            $test = 0;
            foreach($tab_exa AS $exam) {
                $exa = new Examen($exam['id_exa']);
                echo "<hr>";
                echo $exa->get_num_nom() . " - " . $exa->get_nom() . "<br>";
                echo $exa->get_heure_db_exa() . " - " . $exa->get_date_hrs_jour_date() . "<br>";
                echo "<hr>";
            }

            ?>
            <button class="btn btn-warning"><a class="col-md-12" href="<?php echo URL; ?>Convocations/pdf/convoc.pdf.php" target="_blank">Convocation</a></button>


        </div>
    </div>
</div>

<div class="col-md-6">
    <div class="panel panel-primary">
        <div class="panel-heading">
            Lien pour ajouter les examens à son agenda à Outlook, Google agenda, ...
        </div>
        <div class="panel-body">
            <div class="col-md-offset-2 col-md-8">

                <div class="input-group url">
                    <input type="text" class="form-control text" disabled value="<?php echo URL; ?>Public/calendar/cal_can.php?code=<?php echo Projet::encrypt($_SESSION['id']); ?>">
                    <span class="input-group-btn">
                    <button class="btn btn-primary" type="button"><span class="glyphicon glyphicon-copy" aria-hidden="true"></span></button>
                </span>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="col-md-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            Mes prochains examens
        </div>
        <div class="panel-body">


            <?php
            /*
            $can = new Candidat($_SESSION['id']);
            $tab_exa = $can->get_convoc();*/
            // print_r($tab_exa);
            foreach($tab_exa AS $exam){
                //    print_r($exam);
                $exa = new Examen($exam['id_exa']);
                $tab_sle = $exa->get_salles_exa();
                $place = $exa->get_pos_can($_SESSION['id']);
                $tab_plc = $can->get_sle_exa($exa->get_id());
                $sle = new salle($tab_plc['id_sle']);
                // echo $sle->get_nom();

                ?>
                <div class="col-md-4">
                    <h2>ICT <?= $exa->get_num_nom() ?></h2>
                    <p><?= $exa->get_nom() ?></p>
                    <h4><?= $exa->get_date_hrs_jour_date()." ".$exa->get_date_hrs_heure() ?> / <?= $sle->get_nom() ?></h4>

                    <h4>Moyens autorisés</h4>
                    <p>
                        <b>Physiquement : </b>
                        <br>
                        <?= ($exa->get_materiel_exa() == null) ? "Aucun" : str_replace(chr(10),"<br>",$exa->get_materiel_exa()) ?>
                    </p>
                    <p>
                        <b>Sur R:\\cco_ich_<?= $exa->get_num_nom() ?> : </b>
                        <br>
                        <?= ($exa->get_lec_exa() && $exa->get_data_exa() != null) ? str_replace(chr(10),"<br>",$exa->get_data_exa()) :"Aucun" ?>
                    </p>
                </div>
                <?php
                $place = $tab_plc['pcname_tab'];
                echo $sle->gen_plan(700, 350, $place);
            }
            ?>

        </div>
    </div>
</div>





</div>

<script>
    $('.url').click(function () {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val($('.text').val()).select();
        document.execCommand("copy");
        $temp.remove();
        message("Le lien a été copié", "success");
    });
</script>
<script src="<?php echo URL; ?>Public/calendar/js/index.js"></script>
</body>
</html>
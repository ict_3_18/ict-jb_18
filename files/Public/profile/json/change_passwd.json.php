<?php
header('Content-Type: application/json');
session_start();
require("../../../config/config.inc.php");
require_once(WAY."/includes/autoload.inc.php");

$can = new Candidat();

if($can->check_email($_SESSION['email'])) {
    if ($can->check_login($_SESSION['email'], $_POST['old_passwd'])) {
        $encoded_passwd = password_hash($_POST['new_passwd'], PASSWORD_DEFAULT);
        $candidat = new Candidat($_SESSION["id"]);
        $candidat->set_password($encoded_passwd);
        $candidat->update_password();

        $tab['reponse'] = true;
        $tab['message']['texte'] = "Mot de passe modifié !";
        $tab['message']['type'] = "success";

        $user_browser_ip = $_SERVER['HTTP_USER_AGENT'] . $_SERVER['REMOTE_ADDR'];
        $_SESSION["login_string"] = password_hash($encoded_passwd . $user_browser_ip, PASSWORD_DEFAULT);

    } else {
        $tab['message']['texte'] = "Mot de passe incorrect !";
        $tab['message']['type'] = "danger";
    }
} else {
    $tab['message']['texte'] = "Une erreur est survenue, veuillez vous reconnecter";
    $tab['message']['type'] = "danger";
}
echo json_encode($tab);
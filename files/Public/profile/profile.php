<?php
session_start();
require("../../config/config.inc.php");
require_once(WAY . "/includes/autoload.inc.php");
require WAY . '/Public/includes/secure.inc.php';
require_once(WAY . "/Public/includes/head.inc.php");

if(isset($_SESSION["id"])) {
    $can = new Candidat($_SESSION['id']);
    ?>

    <div class="panel panel-primary">
        <div class="panel-heading">
            Mon profil
        </div>
        <div class="panel-body">
            <h3><?= $can->get_prenom()." ".$can->get_nom()." - ".$can->get_no_candidat_minus() ?></h3>
            <a href="<?= URL."Public/profile/change_passwd.php" ?>">
                <button id="change-passwd" class="btn btn-primary" data-toggle="tooltip" title="Changer de mot de passe">
                    Changer de mot de passe
                </button>
            </a>
        </div>
    </div>

    <?php
}
?>
</div>
</body>
</html>
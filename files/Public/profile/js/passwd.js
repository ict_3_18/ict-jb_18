/**
 * A l'initialisation de la page :
 * 1. Générer un nouveau mot de passe dans le champ prévu pour ça
 * 2. Initialiser le popover du presse-papier
 */
$(function () {
    genAndInsertPassword();

    $('.popover-copy').popover({
        container: 'body',
        delay: {
            "show": 0,
            "hide": 1500
        },
        trigger: 'focus'
    })
});

/**
 * Visibilité des mots de passe
 */
$(".passw-visibility-btn").click(function () {
    var input = $(this);
    var forElement = input.attr("for");
    var element = $("#"+forElement);
    if(element.attr("type") === "password") {
        element.attr("type", "text");
        input.children("span.glyphicon").toggleClass("glyphicon-eye-open");
        input.children("span.glyphicon").toggleClass("glyphicon-eye-close");
    } else {
        element.attr("type", "password");
        input.children("span.glyphicon").toggleClass("glyphicon-eye-open");
        input.children("span.glyphicon").toggleClass("glyphicon-eye-close");
    }
});

/**
 * Shuffles array in place.
 * @param {Array} a items An array containing the items.
 */
function shuffle(a) {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
}

/**
 * Génère un mot de passe. TODO : Shuffle de manière correcte le mot de passe à la fin
 * @param length : length
 * @param characters : char_types
 * @returns {string} : password
 */
function generatePassword(length, characters) {
    var symbols = ['abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', '1234567890', '$@$!%*?&'];
    var chars = characters.split(',');
    var usedChars = '';
    var password = '';

    for(let i = 0; i < chars.length; i++) {
        switch (chars[i]) {
            case 'lower_case':
                usedChars += symbols[0];
                password += symbols[0].charAt(Math.floor(Math.random() * symbols[0].length));
                break;
            case 'upper_case':
                usedChars += symbols[1];
                password += symbols[1].charAt(Math.floor(Math.random() * symbols[1].length));
                break;
            case 'numbers':
                usedChars += symbols[2];
                password += symbols[2].charAt(Math.floor(Math.random() * symbols[2].length));
                break;
            case 'special_symbols':
                usedChars += symbols[3];
                password += symbols[3].charAt(Math.floor(Math.random() * symbols[3].length));
                break;
        }
    }

    for (let i = password.length-1; i < length; i++) {
        password += usedChars.charAt(Math.floor(Math.random() * usedChars.length));
    }

    var passwordSplit = password.split();
    passwordSplit = shuffle(passwordSplit);

    password = passwordSplit.toString();

    return password;
}

/**
 * Génère un mot de passe à 8 caractères et l'insère dans le label dédié
 */
function genAndInsertPassword() {
    var password = generatePassword(8, 'lower_case,upper_case,numbers,special_symbols');
    $("#password_gen").val(password);
}

/**
 * Copie le mot de passe dans le presse-papier.
 */
function copyPassword() {
    var copyTextFrom = $("#password_gen");

    copyTextFrom.focus();
    copyTextFrom.select();

    document.execCommand('copy');
}
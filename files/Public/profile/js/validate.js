$(function(){

    $.validator.addMethod("PWCHECK",
        function(value, element) {
            if(/^(?=.*?[A-Z]{1,})(?=(.*[a-z]){1,})(?=(.*[0-9]){1,})(?=(.*[$@$!%*?&]){1,}).{8,}$/.test(value)){
                return true;
            }else{
                return false;
            }
        }
    );

    $("#change_passwd_form").validate(
        {
            rules: {
                old_passwd: {
                    required: true
                },
                new_passwd: {
                    required: true,
                    PWCHECK: true
                },
                new_passwd_conf: {
                    required: true,
                    equalTo: "#new_passwd"
                }
            },
            messages:{
                old_passwd:{
                    required: "Veuillez saisir votre mot de passe"
                },
                new_passwd:{
                    required: "Veuillez saisir un mot de passe.",
                    PWCHECK: "Le mot de passe doit comporter au minimum 8 caractères, dont une minuscule, une majuscule, un chiffre et un caractère spécial."
                },
                new_passwd_conf:{
                    required: "Veuillez saisir une deuxième fois votre mot de passe.",
                    equalTo: "Les mots de passe ne sont pas identiques."
                },
            },
            submitHandler: function(form) {
                var old_passwd = $("#old_passwd").val();
                var new_passwd = $("#new_passwd").val();
                $.post(
                    "./json/change_passwd.json.php?_="+Date.now(),
                    {
                        old_passwd: old_passwd,
                        new_passwd: new_passwd
                    },
                    function(data, status){
                        if(data.message){
                            message(data.message.texte,data.message.type);
                        }
                        if(data.reponse === true){
                            window.location.assign("../index.php");
                        }
                    },
                    'json'
                )
            }
        }
    );
});
<?php
session_start();
require("../../config/config.inc.php");
require_once(WAY . "/includes/autoload.inc.php");
require WAY . '/Public/includes/secure.inc.php';
require_once(WAY . "/Public/includes/head.inc.php");

if(isset($_SESSION["id"])) {
    $can = new Candidat($_SESSION['id']);
    ?>
    <div class="panel panel-primary">
        <div class="panel-heading">
            Changer de mot de passe
        </div>
        <div class="panel-body">
            <h3>Changer de mot de passe</h3>
            <form id="change_passwd_form">
                <div class="form-group row">
                    <div class="col-sm-4 col-form-label">
                        <label for="old_passwd">Ancien mot de passe</label>
                    </div>
                    <div class="col-sm-8">
                        <div class="input-group mb-2">
                            <input type="password" class="form-control" autocomplete="on" id="old_passwd" name="old_passwd">
                            <div class="input-group-addon passw-visibility-btn" for="old_passwd">
                               <span class="glyphicon glyphicon-eye-open"></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-4 col-form-label">
                        <label for="new_passwd">Nouveau mot de passe</label>
                    </div>
                    <div class="col-sm-8">
                        <div class="input-group mb-2">
                            <input type="password" class="form-control" autocomplete="new-password" id="new_passwd" name="new_passwd">
                            <div class="input-group-addon passw-visibility-btn" for="new_passwd">
                               <span class="glyphicon glyphicon-eye-open"></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-4 col-form-label">
                        <label for="new_passwd_conf">Confirmation du nouveau mot de passe</label>
                    </div>
                    <div class="col-sm-8">
                        <div class="input-group mb-2">
                            <input type="password" class="form-control" autocomplete="off" id="new_passwd_conf" name="new_passwd_conf">
                            <div class="input-group-addon passw-visibility-btn" for="new_passwd_conf">
                               <span class="glyphicon glyphicon-eye-open"></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-4">
                        <div class="input-group">
                            <div class="input-group-btn">
                                <button class="btn" type="button" onclick="genAndInsertPassword()">
                                    <span class="glyphicon glyphicon-refresh"></span>
                                </button>
                            </div>
                            <input type="text" class="form-control" id="password_gen">
                            <div class="input-group-btn">
                                <button class="btn popover-copy" type="button" onclick="copyPassword()" data-toggle="popover" data-placement="right" data-content="Copié dans le presse-papier !">
                                    <span class="glyphicon glyphicon-copy"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-2">
                        <button type="submit" class="btn btn-primary btn-md center-block" value="Confirmer" title="Confirmer" id="submit">Confirmer</button>
                    </div>
                    <div class="col-sm-2">
                        <button type="reset" class="btn btn-warning btn-md center-block" value="Annuler" title="Annnuler" id="cancel">Annuler</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<?php
}
?>

</div>
</body>
<script src="./js/passwd.js"></script>
<script src="./js/validate.js"></script>
</html>
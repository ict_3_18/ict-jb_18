<?php
session_start();
require("../config/config.inc.php");
require(WAY . "/includes/autoload.inc.php");

//require WAY . '/Public/includes/secure.inc.php';
require_once(WAY . "/Public/includes/head.inc.php");
ini_set('display_errors',0);
?>



<div class="row">
    <div class="header">
        <h3>Mot de passe perdu</h3>
    </div>
</div>

<div class="panel panel-primary">
    <div class="panel-heading">
        Demande d'un nouveau mot de passe
    </div>
    <div class="panel-body">
        <?php
        if(isset($_POST['generer'])){

            $can = new Candidat();
            $can->init_by_mail($_POST['email']);

            if(!$can->get_termine_classe()) {

                $password = $can->gen_random_password(8, "lower_case,upper_case,numbers,special_symbols");
                $can->gen_password($password);
                $can->update_password();

                //   $can->new_pwd();
                /*
                    $sql = new mysql(BASE_NAME);
                    $sql->connect();
                    $tab = $sql->select("t_candidats","*","WHERE email_can like \"".strtolower($_POST['email'])."\"",0);
                    if(sizeof($tab[0])){*/
                //$can = new candidat($sql,$tab[0]['id_can']);
                $can->new_pwd($password);
                echo "<h1>un nouveau mot de passe vous a été envoyé !!</h1>";
                echo "Vérifiez votre dossier spam également....";
                echo "<a href=\"login.php\" >Retour à la page de login</a>";
                /*}else{
                    echo "cet e-mail n'est pas enregistré !!!";
                }*/
            }else{
                ?>
                <div class="col-md-4">
                    <h1>Votre compte n'est plus actif.</h1>
                    <p>Si vous n'avez pas terminé votre formation au ceff vous pouvez prendre contact avec le chef expert. Dans le cas contraire je vous souhaite le meilleur pour votre avenir.</p>
                </div>
                <?php
            }

        }else {

            ?>
            <div class="col-md-4">
                <form name=form method="Post" action="<?= $_SERVER['PHP_SELF'] ?>">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input type="email" class="form-control" placeholder="Votre adresse e-mail" autofocus id="email_can" name="email">
                    </div>
                    <br>
                    <div class="input-group">
                        <button class="btn btn-primary btn-block" type="submit" id="generer" name="generer">Générer un nouveau mot de passe</button>
                    </div>
                </form>
            </div>
        <?php
        }

        ?>

    </div>

    <div class="panel-footer">
        ICT-JB.net
    </div>
</div>
</div>
</body>
</html>

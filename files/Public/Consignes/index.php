<?php
session_start();

require("../../config/config.inc.php");
require(WAY . "/includes/autoload.inc.php");
require WAY . '/Public/includes/secure.inc.php';
require_once(WAY . "/Public/includes/head.inc.php");
?>

<div class="panel panel-primary">
    <div class="panel-heading">
        Consignes pour les contrôles de compétences (CCO) ICT au ceff INDUSTRIE
    </div>
    <div class="panel-body">
        <dl>
            <dt>Moyens pédagogiques autorisés</dt>
            <dd>Votre enseignant vous a communiqué les moyens autorisés durant l'examen (support de cours, documentation, exercices), veuillez vous y conformer</dd>

            <dt>Matériel autorisé</dt>
            <dd>Pour les CCO vous n'avez droit qu'à du matériel pour écrire, soit :
            <ul>
                <li>Crayons</li>
                <li>Stylos</li>
                <li>Gomme</li>
                <li>Règle</li>
            </ul>
            Les trousses sont interdites</dd>
            <dt>Appareils électroniques</dt>
            <dd>Tous les appareils électroniques sont interdits, entre autres :
                <ul>
                    <li>Téléphone</li>
                    <li>Tablette</li>
                    <li>PC portable</li>
                    <li>Montre connectée</li>
                    <li>...</li>
                </ul>
            </dd>
            <dt>Vestes, sacs, etc.</dt>
            <dd>Les vestes et sacs, casquettes, bonnets, etc. doivent êtres rangés dans votre casier. Pour les candidats DUAL une zone sera mise à disposition par les enseignants pour déposer votre matériel, vos appareils électroniques doivent êtres éteints et rangés.</dd>
            <dt>Données personnelles numériques</dt>
            <dd>Si votre enseignant vous a autorisé à déposer des données électroniques sur votre disque R: (R:\cco_ich_<i>numéro_de_l'examen</i>) vous devez déposer ces données au plus tard le jour précédant l'examen à 20h00. Il est inutile de vous présenter le jour de l'examen avec une clef USB ou un autre support.</dd>


        </dl>

    </div>
</div>


</body>
</html>
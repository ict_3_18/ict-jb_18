<?php
session_start();
require("../../config/config.inc.php");
require(WAY . "/includes/autoload.inc.php");
require WAY . '/Public/includes/secure.inc.php';
require_once(WAY . "/Public/includes/head.inc.php");

$note_exa = new Notes($can->get_id());
$id_tpi = $can->get_last_tpi();
$tpi = new Tpi($id_tpi);
$criteres = $tpi->get_criteres();

?>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4>Experts</h4>
        </div>
        <div class="panel-body">
            <?php
                $exp_1 = new Personne($tpi->get_id_exp_1());
                $exp_2 = new Personne($tpi->get_id_exp_2());
            ?>
            <table class="table table-striped">
                <tr>
                    <th>Fonction</th>
                    <th>Nom, prénom</th>
                    <th>Téléphone</th>
                    <th>E-mail</th>
                </tr>
                <tr>
                    <td>Expert responsable</td>
                    <td><?=$exp_1->get_nom()." ".$exp_1->get_prenom() ?></td>
                    <td><?=$exp_1->get_tel_bureau()?></td>
                    <td><?=$exp_1->get_email()?></td>
                </tr>
                <tr>
                    <td>Expert accompagnant</td>
                    <td><?=$exp_2->get_nom()." ".$exp_2->get_prenom() ?></td>
                    <td><?=$exp_2->get_tel_bureau()?></td>
                    <td><?=$exp_2->get_email()?></td>
                </tr>
            </table>
        </div>
    </div>
<?php
if(strtotime($tpi->get_date_db()) <= time() && time()> 25200){
    ?>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4>Fichiers</h4>
        </div>
        <div class="panel-body">
            <table class="table table-striped table-bordered">
            <?php
               // echo "toto";
                $chemin = WAY ."/TPI/files/".$tpi->get_id();

                $tab_files = scandir($chemin);
                foreach ($tab_files AS $key => $file){
                    $url =  URL ."/TPI/files/".$tpi->get_id()."/".$file;
                    if($file != "." && $file != ".." ) {
                        $ext = substr($file,strrpos($file,".")+1);
                        ?>
                        <tr id="file_<?=$key?>">
                            <td><img src="<?= URL."/images/ico/".$ext.".png"?>"</td>
                            <td><a href="<?=$url?>" download="<?=$url?>"><?=$file?><a></a></td>


                        </tr>
                        <?php
                    }
                }

            ?>
            </table>
        </div>
    </div>
   <?php
 }
            ?>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4>Horaire</h4>
        </div>
        <div class="panel-body">
            <div>
                <h1>Attention !</h1>
                <p>L'horaire ci-dessous peut comporter des erreurs, merci de vous fier à l'horaire distribué par votre supérieur.<br>
                    Merci</p>

            </div>
            <?php
            echo $tpi->gen_horaire_tpi();
            ?>

        </div>
    </div>
<?php

$tab_crt = $tpi->get_criteres_details();

if(!$tab_crt){
 //   $tab_crt = $tpi->assoc_all_crt_tpi();
}


$rubrique = new Rubrique();
$categorie = new Categorie();
$tab_rub = $rubrique->get_all();;



if($tpi->get_crt_def()) {
    ?>
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4>Critères d'évaluation du TPI</h4>
        </div>
        <div class="panel-body">
            <table class="table_crt table table-bordered table-hover col-md-12">
            <?php
            $id_cat = 0;
            foreach ($tab_crt AS $crit){
                if($crit['id_cat'] != 6) {
                    $crt = new Critere($crit['id_crt']);

                    if ($id_cat != $crt->get_id_cat()){
                        $id_cat = $crt->get_id_cat();
                        $cat =new Categorie($id_cat);
                        echo "<tr>";
                        echo "<th colspan='2'><h2>" . $cat->get_nom() . "</h2></th>";
                        echo "</tr>";
                    }
                    echo "<tr>";
                    echo "<th colspan='2'>" . (($crt->get_id_cat() != 5) ? $crt->get_id_cat()."." : "" ). $crt->get_code() . " " . $crt->get_nom() . "</b></th>";
                    echo "</tr>";
                    echo "<tr>";
                    echo "<td colspan='2'>" . str_replace(chr(10) . chr(13), "<br>", $crt->get_description()) . "</td>";
                    echo "</td>";
                    echo "</tr>";
                    echo "<tr class='pt3'>";
                    echo "<td>3 points</td><td>" . $crt->get_pt_3() . "</td>";
                    echo "</tr>";
                    echo "<tr class='pt2'>";
                    echo "<td>2 points</td><td>" . $crt->get_pt_2() . "</td>";
                    echo "</tr>";
                    echo "<tr class='pt1'>";
                    echo "<td>1 points</td><td>" . $crt->get_pt_1() . "</td>";
                    echo "</tr>";
                    echo "<tr class='pt0'>";
                    echo "<td>0 points</td><td>" . $crt->get_pt_0() . "</td>";
                    echo "</tr>";
                }
            }
            ?>
            </table>
        </div>
    </div>
    <?php
}
?>
</body>
</html>
<?php

header('Content-Type: application/json');
session_start();
require("../../config/config.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

$can = new Candidat();

if ($can->check_email($_POST['email_can'])) { //Vérifie si un email existe dans la base de données
    $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    $pw = "";
    for ($i = 0; $i < 6; $i++) {
        $index = rand(0, strlen($chars) - 1);
        $pw .= substr($chars, $index, 1);
    }

    $can->set_email($_POST['email_can']);
    $can->gen_password($pw);
    $can->update_password();

    $tab['reponse'] = true;
  //  echo $pw;
} else {
    $tab['message']['texte'] = "Email invalide !";
    $tab['message']['type'] = "danger";
}

echo json_encode($tab);
?>
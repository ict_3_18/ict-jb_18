<?php
header('Content-Type: application/json');
session_start();
require("./../../config/config.inc.php");
require_once(WAY."/includes/autoload.inc.php");

$can = new Candidat();

//echo $per;

if($can->check_email($_POST['email_can'])){ //Vérifie si un email existe dans la base de données
    if($can->check_login($_POST['email_can'],$_POST['password_can'])){ //Vérifie si la combinaison email-password existe bien dans la base de données
        $can->init_by_mail($_POST['email_can']);
        $can->log_log("Login de ".$can->get_email());
        $tab['reponse'] = true;
    }else{
        $tab['message']['texte'] = "Combinaison invalide !";
        $tab['message']['type'] = "danger";
    }
}else{
   $tab['message']['texte'] = "Combinaison invalide !";
   $tab['message']['type'] = "danger";
}

echo json_encode($tab);
?>
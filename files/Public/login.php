<?php
session_start();
require("../config/config.inc.php");
require_once(WAY . "/includes/autoload.inc.php");
require_once(WAY . "/Public/includes/head.inc.php");
?>


<div class="row">
    <div class="header">
        <h3>Bienvenue sur ICT-JB</h3>
    </div>
</div>

<div class="panel panel-primary">
    <div class="panel-heading">
        Connexion au candidat
    </div>
    <div class="panel-body">
        <div class="col-md-4">
            <form id="connexion_form" style="margin-top: 20px;">
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                    <input type="email" class="form-control" placeholder="Votre adresse e-mail" autofocus id="email_can" name="email_can">
                </div>
                <div class="input-group" style="margin-top: 5px;">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                    <input type="password" class="form-control" placeholder="Mot de passe" required="" id="password" name="password">
                </div>
                <br>
                <button class="btn btn-lg btn-primary btn-block" type="submit" id="login" name="login">Se connecter</button>
                <a href="./lost_passwd.php">Mot de passe perdu</a>
            </form>
        </div>
        <div class="col-md-8">
            <h3>L'utilisation de Twitter est abandonnée pour les notifications</h3>
            <p>Un nouveau système de notification multi-plateformes a été mis en place, l'activation se fait simplement via la petite cloche rouge en bas de page.</p>
            <p>Il est possible de s'enregistrer via un navigateur tant sur PC que sur mobile.</p>
            <p>L'accès aux notes est désormais activable par classe.</p>
            <p>L'accès n'est plus possible pour les candidats terminaux. Le site <a href="https://www.osp-jb.ch/?Informaticien_CFC">osp-jb.ch</a> vous renseigne sur l'état de votre résultat</p>
            <table class="table table-bordered">
                <tr>
                    <th>Classe avec accès au site</th>
                    <th>Visibilité des notes</th>
                </tr>
            <?php
            $cla = new Classe();
            $tab_cla = $cla->get_all_actifs();

            //print_r($tab_cla);
            foreach($tab_cla AS $cla){
                $grp = new Groupe($cla['ref_grp']);
                ?>
                <tr>
                    <td><?=$grp->get_nom_grp()." - ".$cla['nom_cla']?></td>
                    <td>
                        <?php
                            if($cla['notes_open']){
                                echo '<button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-eye-open"></span>';
                            }else{
                                echo '<button type="button" class="btn btn-danger"><span class="glyphicon glyphicon-eye-close"></span>';
                            }

                        ?>
                        </button>
                    </td>
                </tr>
            <?php
            }
            ?>
            </table>
        </div>
    </div>

    <div class="panel-footer">
        ICT-JB.net
    </div>
</div>
</div>
<script src="./js/login.js"></script>
</body>
</html>
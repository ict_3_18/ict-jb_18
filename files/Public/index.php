<?php
session_start();
require("../config/config.inc.php");
header('Location: ' . URL."/Public/calendar/index.php");
require(WAY . "/includes/autoload.inc.php");
require WAY . '/Public/includes/secure.inc.php';
require_once(WAY . "/Public/includes/head.inc.php");
?>
<div class="panel panel-primary">
    <div class="panel-heading">
        Sites des candidats ICT de la Berne francophone
    </div>
    <div class="panel-body">
        Merci de vous conformer aux consignes de ce site en ce qui concerne vos examens.
    </div>
    <div class="panel-footer">
        ICT-JB.net
    </div>
</div>
</div>
</body>
</html>
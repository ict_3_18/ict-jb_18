$(function () {

    $("#connexion_form").validate(
            {
                rules: {
                    email_can: {
                        required: true,
                        email: true
                    },
                    password: "required"
                },

                messages: {
                    email_can: {
                        required: "Votre adresse e-mail est indispensable à la connexion",
                        email: "Votre adresse e-mail doit avoir le format suivant : name@domain.com"
                    },
                    password: "Veuillez saisir votre mot de passe"
                },
                submitHandler: function (form) {
                    $.post(
                            "./json/login.json.php?_=" + Date(),
                            {
                                email_can: $("#email_can").val(),
                                password_can: $("#password").val()
                            },
                            function (data, status) {
                                if (data.message) {
                                    message(data.message.texte, data.message.type);
                                }
                                if (data.reponse === true) {
                                    message("logué", "success");
                                    window.location.assign("index.php");
                                }
                            }
                    );
                }
            }
    );
});
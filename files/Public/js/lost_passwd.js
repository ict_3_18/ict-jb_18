$(function () {

    $(".form-mdp-perdu").validate(
            {
                rules: {
                    email_can: {
                        required: true,
                        email: true
                    }
                },

                messages: {
                    email_can: {
                        required: "Votre adresse e-mail est indispensable à la connexion",
                        email: "Votre adresse e-mail doit avoir le format suivant : name@domain.com"
                    }
                },
                submitHandler: function (form) {
                    $.post(
                            "./json/lost_passwd.json.php?_=" + Date(),
                            {
                                email_can: $("#email_can").val()
                            },
                            function (data, status) {
                                if (data.message) {
                                    message(data.message.texte, data.message.type);
                                }
                                if (data.reponse === true) {
                                    message("mot de passe modifier", "success");
                                    window.location.assign("login.php");
                                }
                            }
                    );
                }
            }
    );
});
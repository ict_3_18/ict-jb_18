<?php

function mail_seances($file_name,$mail_to){
    $from_mail = "chef-expert@ict-jb.net";
    $from_name = "ICT-JB";
    $subject = "VISITE EXPERTS TPI";
    $message_body = "Vos experts ont planifié une visite, les informations sont disponible dans le fichier calendrier ci-joint";
    $reply_to = "chef-expert@ict-jb.net";

    $headers = 'MIME-Version: 1.0' . "\r\n";

    $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
    $headers .= 'From: ICT-JB.net <webmaster@ict-jb.net>' . "\r\n";
    $headers .= 'Reply-To: webmaster@ict-jb.net' . "\r\n";
    $headers .= "X-Sender: <www.ict-b.net>" . "\r\n";;
    $headers .= "X-Mailer: PHP" . "\r\n";;
    $headers .= "X-auth-smtp-user: webmaster@ict-jb.net" . "\r\n";;
    $headers .= "X-abuse-contact: webmaster@ict-jb.net" . "\r\n";;
    $expe = "webmaster@ict-jb.net";

    $date_time = date("D, d M Y H:i:s O");
    $headers .= 'DATE: ' . $date_time . "\r\n";

    $message = "";

    /* Attachment File */
    // Attachment location

    $path = WAY."TPI/ical/temp/";

    // Read the file content
    $file = $path.$file_name;
    $file_size = filesize($file);
    $handle = fopen($file, "r");
    $content = fread($handle, $file_size);
    fclose($handle);
    $content = chunk_split(base64_encode($content));

    /* Set the email header */
    // Generate a boundary
    $boundary = md5(uniqid(time()));

    // Email header
  /*  $header = "From: ".$from_name." \r\n";
    $header .= "Reply-To: ".$reply_to."\r\n";
    $header .= "MIME-Version: 1.0\r\n";
*/
    // Multipart wraps the Email Content and Attachment
    $header .= "Content-Type: multipart/mixed;\r\n";
    $header .= " boundary=\"".$boundary."\"";

    $message .= "This is a multi-part message in MIME format.\r\n\r\n";
    $message .= "--".$boundary."\r\n";

    // Email content
    // Content-type can be text/plain or text/html
    $message .= "Content-Type: text/html; charset=\"UTF-8\"\r\n";
    $message .= "Content-Transfer-Encoding: 7bit\r\n";
    $message .= "\r\n";
    $message .= "$message_body\r\n";
    $message .= "--".$boundary."\r\n";

    // Attachment
    // Edit content type for different file extensions
    $message .= "Content-Type: application/xml;\r\n";
    $message .= " name=\"".$file_name."\"\r\n";
    $message .= "Content-Transfer-Encoding: base64\r\n";
    $message .= "Content-Disposition: attachment;\r\n";
    $message .= " filename=\"".$file_name."\"\r\n";
    $message .= "\r\n".$content."\r\n";
    $message .= "--".$boundary."--\r\n";

    // Send email
    if (mail($mail_to, $subject, $message, $header)) {
        echo "Sent";
    } else {
        echo "Error";
    }
}
?>
<?php
header("Content-Type: application/json");
require("../../config/config.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

session_start();
$aut = 'USR_TPI';

require(WAY . "./includes/secure.inc.php");

$tpi = new Tpi($_POST['id_tpi']);
if($tpi->crt_existe_pas($_POST["id_crt"])) {
    $tpi->add_crt($_POST['id_crt']);
}
echo json_encode($_POST);
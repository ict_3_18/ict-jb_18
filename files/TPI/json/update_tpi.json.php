<?php
header("Content-Type: application/json");
require("../../config/config.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

session_start();
$aut = 'USR_TPI';

require(WAY . "./includes/secure.inc.php");
/*
try {
    $tab = ['date_debut_tpi' => date_format(date_create($_POST['upd_date_debut_tpi']), 'Y-m-d')];
    $tpi = new Tpi();
    $tab['id_tpi'] = $_POST['upd_id_tpi'];
    $tpi->update_date($tab);
}catch (PDOExeption $e){
    echo $e->getMessage(),'<br/><br/>';
    echo _e("Erreur d'ajout");
}

echo json_encode($tab);
*/

$tpi =  new Tpi($_POST['id_tpi']);
$tpi->set_id_exp_2($_POST['ref_exp_2']);
$tpi->set_id_exp_1($_POST['ref_exp_1']);
$tpi->set_id_m_app($_POST['ref_maitre_app']);
$tab_data = $_POST;
unset($tab_data['id_tpi']);
$tpi->update($tab_data);

//echo $tpi;
echo json_encode($_POST);
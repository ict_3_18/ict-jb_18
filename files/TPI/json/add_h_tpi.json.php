<?php
header("Content-Type: application/json");
require("../../config/config.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

session_start();
$aut = 'USR_TPI';

require(WAY . "./includes/secure.inc.php");

try {
    $tab = ['date_hor' => $_POST['add_date_hor'], 'debut_hor' => $_POST['add_debut_hor'], 'fin_hor' => $_POST['add_fin_hor'], 'total_day_hor' => $_POST['add_total_day_hor'], 'pauses_hor' => $_POST['add_pauses_hor'], 'ref_can' => $_POST['add_ref_can'], 'id_tpi' => $_POST['add_id_tpi']];
    $hor_tpi = new Horaire();
    $tab['id_hor'] = $_POST['add_id_hor'];
    $tab['last_insert_id'] = $hor_tpi->add_h_tpi($tab);



}catch (PDOExeption $e){
    echo $e->getMessage(),'<br/><br/>';
    echo _e("Erreur d'ajout");
}

echo json_encode($tab);
<?php
header("Content-Type: application/json");
require("../../config/config.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

session_start();
$aut = 'USR_TPI';

require(WAY . "./includes/secure.inc.php");
//print_r($_POST);


$date = $_POST['date'];
$heure = $_POST['heure'];
$id_can = $_POST['id_can'];
$id_tpi = $_POST['id_tpi'];
$id = $_SESSION['id'];


$can = new Candidat($id_can);
$tpi = new Tpi($id_tpi);
$emails = $tpi->get_all_email();
$emails_string = "";
foreach ($emails AS $email){
    $emails_string .= $email.",";
}
$emails_string .= $can->get_email();

$heure_nom = date("H-i-s",strtotime($heure));

$file_name = "ICAL-".$date."-".$heure_nom."-".$can->get_no_can_with_id($id_can).".ics";

require("./../ical/seances.ical.php");
ical_creator($date,$heure,$file_name);

require("./../mail_seances.php");
// TODO pour évaluation
$emails_string = "jacques@hirtzel.ch,jacques.hirtzel@ceff.ch";
mail_seances($file_name, $emails_string);


unlink(WAY."TPI/ical/temp/".$file_name);
<table class="table table-sm">
    <tr>
        <th>Fonction</th>
        <th>Nom, prénom</th>
        <th>E-mail</th>
        <th>Tél. prof.</th>
        <th>Tél. mobile</th>
    </tr>
    <tbody>
    <tr>
        <td>Candidat : </td>
        <td><?= $can->get_nom() ?> <?= $can->get_prenom() ?></td>
        <td><?= $can->get_email() ?></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>Supérieur : </td>
        <td><?= $ma_app->get_nom() ?> <?= $ma_app->get_prenom() ?></td>
        <td><?= $ma_app->get_email() ?></td>
        <td><?= $ma_app->get_tel_bureau() ?></td>
        <td><?= $ma_app->get_tel_mobile() ?></td>
    </tr>
    <tr>
        <td>Expert responsable : </td>
        <td><?= $exp_1->get_nom() ?> <?= $exp_1->get_prenom() ?></td>
        <td><?= $exp_1->get_email() ?></td>
        <td><?= $exp_1->get_tel_bureau() ?></td>
        <td><?= $exp_1->get_tel_mobile() ?></td>
    </tr>
    <tr>
        <td>Expert accompagnant : </td>
        <td><?= $exp_2->get_nom() ?> <?= $exp_2->get_prenom() ?></td>
        <td><?= $exp_2->get_email() ?></td>
        <td><?= $exp_2->get_tel_bureau() ?></td>
        <td><?= $exp_2->get_tel_mobile() ?></td>
    </tr>
    </tbody>
</table><br>
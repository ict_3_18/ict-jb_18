<div class="container">
    <form method="post" id="mail_form" class="mail_form">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3>TPI - <?= $this->get_nom() ?></h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <label for="sender" class="col col-md-2">Envoyeur :</label>
                    <input id="sender" name="sender" type="text" class="col col-md-2"><br>
                </div>
                <div class="row">
                    <label for="receiver" class="col col-md-2">Receveur :</label>
                    <input id="receiver" name="receiver" type="text" class="col col-md-2"><br>
                </div>
                <div class="row">
                    <label for="cc" class="col col-md-2">CC :</label>
                    <input id="cc" name="cc" type="text" class="col col-md-2"><br>
                </div><br>
                <textarea id='ex_tpi' name="ex_tpi" rows='4' cols='100' form="mail_form">
Le TPI ci-dessus vous est attribué en tant que supérieur ou expert, vous trouvez ci-dessous les coordonnées des autres personnes impliquées.
L'horaire de travail ainsi que le cahier des charges vous seront remis par le supérieur du candidat.
                </textarea><br>
                <?php
                    include "personnes_table.include.php";
                ?>
                <label for="lieu_tpi"><b>Lieu du TPI : </b></label>
                <input type="text" id="lieu_tpi" name="lieu_tpi" value="<?= $emp->get_nom() ?>, <?=$emp->get_adresse_emp() ?>, <?= $emp->get_localite_emp() ?>" size="30">
                <?= $this->gen_horaire_tpi() ?><br>
                <textarea id='footer_tpi' name="footer_tpi" cols='100' rows='8' form="mail_form">
Merci d'avance de me signaler les éventuelles erreurs ou changements dans l'horaire.
Tout en vous remerciant pour votre engagement je vous adressse mes meilleures salutations.
Jacques Hirtzel
Expert en chef informatique Berne francophone
Les Frasses 26
2612 Cormoret
078 755 02 83
                </textarea><br>
                <input type="hidden" id="id_tpi" name="id_tpi" value="<?= $_POST["id_tpi"] ?>">
                <input type="submit" class="btn" value="Send" id="send" name="send">
            </div>
        </div>
    </form>
</div>
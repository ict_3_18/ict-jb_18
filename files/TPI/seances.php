<?php
session_start();
require("./../config/config.inc.php");
$aut = "ADM_TPI";
require(WAY . "/includes/secure.inc.php");
require_once(WAY . "/includes/autoload.inc.php");
require_once(WAY . "/includes/head.inc.php");


//require("./includes/navbar.inc.php");

//define("DISPLAY_ERROR", 1);

// Type d'erreur à afficher
error_reporting(E_ALL);
$tpi = new Tpi();
$can = new Candidat();
$tab_tpi =  $tpi->get_all_tpi_act_by_id_per($_SESSION['id']);


//$monday = strtotime('monday this week'); // Date du lundi de la semaine en cours
//$weekdays = array(); // Variable contenant les jours de la semaine (de lundi à vendredi) et leurs dates.


//echo strftime("%A %d %B",time());


$days = $tpi->get_tpi_days_by_per($_SESSION['id']);
//print_r($days);

$tab_horaire_by_tpi = array();
$start = ""; // L'heure de commencement la plus tôt pour un tpi
$end = ""; // L'heure de fin la plus tardive pour un tpi

//Trier les horraire par jours
foreach ($days as $date => $weekday) {
    //Trier le horaires par TPI
    foreach ($tab_tpi as $infos_tpi) {
        $tpi = new Tpi($infos_tpi['id_tpi']);
        $tab_horaire = $tpi->get_horaire();
        //Remplir le tableaux des horaire
        foreach ($tab_horaire as $day => $horaire){
            if($day == $date) {
                $tab_horaire_by_tpi[$date][$infos_tpi['ref_can']] = $horaire;
                $tab_horaire_by_tpi[$date][$infos_tpi['ref_can']]['id_tpi'] = $infos_tpi['id_tpi'];

                // Recherche de l'heure de début la plus tôt et de l'heure de lin la plus tardive
                foreach ($horaire as $hours) {
                    if ($start == "" || strtotime($start) > strtotime($hours['db'])) {
                        $start = $hours['db'];
                    }
                    if ($end == "" || strtotime($end) < strtotime($hours['fin'])) {
                        $end = $hours['fin'];
                    }
                }
            }
        }


    }
}
//création des prériode de 30 minutes
$perdiods = array();
array_push($perdiods,$start);
while(strtotime(end($perdiods)) < strtotime($end)){
    $last_period = strtotime(end($perdiods));
    $period = strtotime("+ 30 minute",$last_period);
    $str_period = date('H:i:s',$period);
    array_push($perdiods,$str_period);
}

echo "<table class='table table-bordered table-hover' id='table-seances'>";
echo "<thead><tr id='hours-row'><th colspan='2' class='border-right'></th>";
foreach ($perdiods as $period){
    echo "<th scope='col'>".substr($period,0,-3)."</th>";
}
echo "</tr></thead>";
echo "<tbody>";
foreach ($tab_horaire_by_tpi as $day => $horaire_by_per){
    echo "<tr><th rowspan='".(count($horaire_by_per)+1)."'>".$day." (".strftime("%A",strtotime($day)).")</th>";
    foreach ($horaire_by_per as $id_per => $horaire) {
        echo "<tr>";
        echo "<th class='border-right'>".$can->get_nom_prenom_can_with_id($id_per)."</th>";
        foreach ($perdiods as $period) {
            $is_here = FALSE;
            foreach ($horaire as $key => $heures) {

                if(strcasecmp($key,'id_tpi') != 0) {
                    if (strtotime($period) >= strtotime($heures['db']) && strtotime($period) < strtotime($heures['fin'])) {
                        $is_here = TRUE;
                    }
                }
            }
            if($is_here){
                echo "<td class='is-here' date='".$date."' id_tpi='".$horaire['id_tpi']."' id_can='".$id_per."' period='".$period."' name_can='".$can->get_nom_prenom_can_with_id($id_per)."'></td>";
            }else{
                echo "<td></td>";
            }

        }
        echo "</tr>";
    }
}
echo "</tbody></table>";




?>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">Ajouter une scéance</h3>
<!--                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>-->
                </button>
            </div>
            <div class="modal-body">
                Vous Souhaitez planifier une visite de TPI pour <span id="name_seance"></span> le <span id="date_seance"></span> à
                <?php
                echo '<br>Heures : ';
                echo '<select id="select-h" class="form-control">';
                for($h = 0; $h <= 24; $h++){
                    $i = str_pad($h, 2, '0', STR_PAD_LEFT);
                    echo '<option value="'.$i.'">'.$i.'</option>';
                }
                echo '</select>';

                echo '<br>Minutes :';
                echo '<select id="select-m" class="form-control">';
                for($m = 0; $m <= 60; $m++){
                    $i = str_pad($m, 2, '0', STR_PAD_LEFT);
                    echo '<option value="'.$i.'">'.$i.'</option>';
                }
                echo '</select>';
                ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Retour</button>
                <button type="button" class="btn btn-primary" id="send">Envoyer</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(".is-here").click(function(){
        $("#myModal").modal();
        period = $(this).attr("period");
        heure = period.substring(0,2);
        minute = period.substring(3,5);
        id_can = $(this).attr("id_can");
        id_tpi = $(this).attr("id_tpi");
        name_can = $(this).attr("name_can");
        date = $(this).attr("date");

        $('#select-h').val(heure);
        $('#select-m').val(minute);

        console.log(heure);
        console.log(minute);

        $('#name_seance').html('<b>'+name_can+'</b>');
        $('#date_seance').html('<b>'+date+'</b>');
        //$('#hour_seance').html('<b>'+period+'</b>');
    });

    $("#send").click(function(){
        heure_final = $('#select-h').val();
        minute_final = $('#select-m').val();
    $.post("./json/send_ical.json.php", {
        id_can: id_can,
        id_tpi: id_tpi,
        date: date,
        heure: heure_final+":"+minute_final+":00"
    });
    });


</script>

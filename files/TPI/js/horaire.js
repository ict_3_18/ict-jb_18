$(function () {
    /**
     * Initialisation du Datepicker principal
     */
    $('#datepicker_h_tpi').datepicker({
        format: 'dd/mm/yyyy',
        weekStart: 1,
        language: 'fr',
        forceParse: false,
        daysOfWeekDisabled: '0,6',
        autoclose: true,
        todayHighlight: true
    });


    $('#btn_gen_h_tpi').on('click', function () {

        $.post(
            '../tpi/json/update_tpi_info_cla.json.php', {
                upd_date_debut_tpi: $('#datepicker_h_tpi').val(),
                upd_id_tpi: $('#tpi_id').val(),
            },
            function (data) {
                console.log(data);
                $.post(
                    '../bus/json/del_h_tpi.json.php', {
                        id_tpi: $('#tpi_id').val()
                    },
                    function (data) {
                        console.log(data);
                        $.post(
                            '../bus/json/add_h_tpi.json.php', {
                                add_date_hor: $('#tpi_id').val()
                            });
                    });
            });
    });


});
$(function(){
    $(".set_crt_def").click(function(){
        var id_tpi = $(this).attr("id_tpi");
        var val = $(this).attr("val");

        $.post(
            "./json/set_crt_def.json.php",
            {
                id_tpi:id_tpi,
                val:val

            },
            function(data){

                $(".set_crt_def").toggleClass("btn-danger").toggleClass("btn-primary");
                console.log(data.val);
                if(data.val == "1"){
                    $(".set_crt_def").attr("val","0");
                    $(".set_crt_def").html("Indiquer les critères comme indéfinis");
                }else{
                    $(".set_crt_def").attr("val","1");
                    $(".set_crt_def").html("Indiquer les critères comme définis");
                }
            }
        );
    });

    $(".table_crt").on("click",".del_crt", function(){
        var id_tpi = $(this).attr("id_tpi");
        var id_crt = $(this).attr("id_crt");

        $.post(
            "./json/del_crt_tpi.json.php",
            {
                id_tpi:id_tpi,
                id_crt:id_crt
            },
            function(data){
                $("#crt_"+data.id_crt).removeClass("del_crt").addClass("add_crt").html("Ajouter").removeClass("btn-danger").addClass("btn-primary");
            }
        );
    });

    $(".table_crt").on("click",".add_crt", function(){
        var id_tpi = $(this).attr("id_tpi");
        var id_crt = $(this).attr("id_crt");

        $.post(
            "./json/add_crt_tpi.json.php",
            {
                id_tpi:id_tpi,
                id_crt:id_crt
            },
            function(data){
                $("#crt_"+data.id_crt).removeClass("add_crt").addClass("del_crt").removeClass("btn-primary").addClass("btn-danger").html("Retirer");
            }
        );
    });

    $("#tab_crit_spec").click(function(){
        var id_tpi = $(this).attr("id_tpi");
        $("#content_load").load(
            "./load/criteres_spec.load.php",
            {
                id_tpi: id_tpi
            }
        );
    });

});
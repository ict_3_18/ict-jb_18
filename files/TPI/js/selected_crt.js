$(function() {
    $(".remove_item").click(function () {
        $.post(
            "./json/del_crt_tpi.json.php",
            {
                id_crt: $(this).attr("id_crt"),
                id_tpi: $(this).attr("id_tpi")
            },
            function del_crt(data, status) {
                $("#content_load").load(
                    "./load/criteres_spec.load.php",
                    {
                        id_tpi: data.id_tpi
                    }
                );
                $("#selected_crt").load(
                    "./load/selected_crt.load.php",
                    {
                        id_tpi: data.id_tpi
                    }
                );
            }
        );
    });
});
$(function(){
    $(".field").change(function(){
        var id_tpi = $(this).attr("id_tpi");
        console.log($("#nom_tpi_"+id_tpi).val());
        console.log($("#ma_app_"+id_tpi).val());
        console.log($("#exp_1_"+id_tpi).val());
        console.log($("#exp_2_"+id_tpi).val());

        $.post(
            "./json/update_tpi.json.php",
            {
                id_tpi:id_tpi,
                nom_tpi:$("#nom_tpi_"+id_tpi).val(),
                ref_maitre_app:$("#ma_app_"+id_tpi).val(),
                ref_exp_1:$("#exp_1_"+id_tpi).val(),
                ref_exp_2:$("#exp_2_"+id_tpi).val()
            }
        );


    });

    $(".btn_choice_crt").click(function(){
        $("#content_load").load(
            "./load/criteres.load.php",
            {
                id_tpi: $(this).attr("id_tpi")
            }
        );
    });

    $(".btn_pdf_eval").click(function(){
        var id_tpi = $(this).attr("id_tpi");
        $.post(
            "./pdf/tpi.pdf.php",
            {
                id_tpi:id_tpi,
                nom_tpi:$("#nom_tpi_"+id_tpi).val(),
                ref_maitre_app:$("#ma_app_"+id_tpi).val(),
                ref_exp_1:$("#exp_1_"+id_tpi).val(),
                ref_exp_2:$("#exp_2_"+id_tpi).val()
            }
        );
    });
});
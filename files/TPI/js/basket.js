$(function(){
	
	$("#id_thm").change(function(){
		if($("#id_thm").val() != 0){
			$("table.theme").css("display","none");
			$("table.thm_"+$("#id_thm").val()).css("display","block");
		}else{
			$("table.theme").css("display","block");
		}
	});

	function refresh_basket(data, status){
		$("#selected_crt").load("./load/selected_crt.load.php",
			{
				id_crt: data.id_crt,
				id_tpi: data.id_tpi
			}
		);
	}
	
	$(".btn_basket").click(function(){
		if($(this).hasClass("btn-danger")){
			$.post(
					"./json/del_crt_tpi.json.php",
					{
						id_crt: $(this).attr("id_crt"),
						id_tpi: $(this).attr("id_tpi")
					},
					function del_crt(data,status){
						$(".btn_crt_"+data.id_crt).removeClass("btn-danger");
						$(".btn_crt_"+data.id_crt).children("span.glyphicon").removeClass("glyphicon-minus").addClass("glyphicon-plus");
						refresh_basket(data,status);	
					}
					
			);
		} else {
			$.post(	
					"./json/add_crt_tpi.json.php",
					{
						id_crt: $(this).attr("id_crt"),
						id_tpi: $(this).attr("id_tpi")
					},
					function add_crt(data,status) {
						$(".btn_crt_"+data.id_crt).addClass("btn-danger");
						$(".btn_crt_"+data.id_crt).children("span.glyphicon").removeClass("glyphicon-plus").addClass("glyphicon-minus");
						refresh_basket(data,status);	
					}
			);
		}
	});
	
	$(".del_crt").click(function(){
		$.post(
				"./json/del_crt_to_basket.json.php",
				{
					id_crt: $(this).attr("id_crt")
				},
				function del_crt(data,status){
					$("#tr_crt_"+data.del_crt).remove();
					$(".btn_crt_"+data.add_crt).removeClass("btn-success");
					refresh_basket(data,status);	
				}
				
		);
	});

	$("#tab_crit_gen").click(function(){
		var id_tpi = $(this).attr("id_tpi");
		$("#content_load").load(
			"./load/criteres.load.php",
			{
				id_tpi: id_tpi
			}
		);
	});
});
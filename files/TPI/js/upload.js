$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var id_tpi = $("#id_tpi").val()
    var way = $("#way").val()
    var url = $("#url").val()
    var url = window.location.hostname === 'new.ict-jb.net' ? url+'TPI/json/import.json.php?folder='+id_tpi : WAY+'TPI/files/'+id_tpi;
    $('#fileupload').fileupload({
        url: url,
        folder : id_tpi,
        dataType: 'json',
        done: function (e, data) {
        $.each(data.result, function (index, file) {
            //console.log(file);
            $('<p/>').text(file.name).appendTo('#files');
        });
    },
    progressall: function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#progress .progress-bar').css(
            'width',
            progress + '%'
        );
    }
}).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');

    $(".delete_file").click(function(){
        //console.log("del");
        var num_line = $(this).parent().parent().attr("id");
        var file = $(this).attr("file");
        $.post(
            "./json/del_file.json.php",
            {
                file:file,
                num_line:num_line
            },
            function(data){
                $("#"+data.num_line).remove();
            }
        );
    });

});
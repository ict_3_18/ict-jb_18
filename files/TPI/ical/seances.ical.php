<?php
require_once("./../../plugins/icalendar-master/zapcallib.php");

function ical_creator($date,$heure,$file_name){
$title = "Visite experts";
// date/time is in SQL datetime format
$event_start = $date." ".$heure;
$event_end = $date." ".$heure;

// create the ical object
$icalobj = new ZCiCal();

// create the event within the ical object
$eventobj = new ZCiCalNode("VEVENT", $icalobj->curnode);

// add title
$eventobj->addNode(new ZCiCalDataNode("SUMMARY:" . $title));

// add start date
$eventobj->addNode(new ZCiCalDataNode("DTSTART:" . ZCiCal::fromSqlDateTime($event_start)));

// add end date
$eventobj->addNode(new ZCiCalDataNode("DTEND:" . ZCiCal::fromSqlDateTime($event_end)));

// UID is a required item in VEVENT, create unique string for this event
// Adding your domain to the end is a good way of creating uniqueness
$uid = date('Y-m-d-H-i-s') . "@demo.icalendar.org";
$eventobj->addNode(new ZCiCalDataNode("UID:" . $uid));

// DTSTAMP is a required item in VEVENT
$eventobj->addNode(new ZCiCalDataNode("DTSTAMP:" . ZCiCal::fromSqlDateTime()));

// Add description
$eventobj->addNode(new ZCiCalDataNode("Description:" . ZCiCal::formatContent(
        "Visite des experts")));

// write iCalendar feed to stdout
$ical = $icalobj->export();
//echo $ical;

//Save File
$myfile = fopen(WAY."TPI/ical/temp/".$file_name, "w+") or die("Unable to open file!");
$txt = $ical;
fwrite($myfile, $txt);
fclose($myfile);
}

<?php
session_start();
require("./../config/config.inc.php");
$aut = "ADM_TPI";
require(WAY . "/includes/secure.inc.php");
require_once(WAY . "/includes/autoload.inc.php");
require_once(WAY . "/includes/head.inc.php");
//require("./includes/navbar.inc.php");

//define("DISPLAY_ERROR", 1);

// Type d'erreur à afficher
error_reporting(E_ALL);

$exa = new Examen();
$id_exa = $exa->get_next_tpi()['ref_exa'];

$tpi = new Tpi();

$tab_tpi =  $tpi->get_all_tpi_by_id_exa($id_exa);

?>
<table class="table table-striped table-hover table-condensed">
    <tr>
        <th class="col-md-2">Candidat</th>
        <th class="col-md-4">Sujet</th>
        <th class="col-md-2">Suppérieur</th>
        <th class="col-md-2">Exepert resp.</th>
        <th class="col-md-2">Exepert acc.</th>
    </tr>
<?php

foreach($tab_tpi AS $tp ){
    $tpi = new Tpi($tp['id_tpi']);
    $tab_ma_app = $per->get_all_supp_prof_actif();
    //print_r($tab_ma_app);
    ?>
    <tr>
        <td>
            <?= $tpi->get_nom_can()." ".$tpi->get_prenom_can()." (".$tpi->get_classe().")"." (".$tpi->get_lieu().")";?>

        </td>
        <td>
            <input type="text" class="col-md-12 form-control field" id_tpi="<?= $tpi->get_id();?>" id="nom_tpi_<?= $tpi->get_id();?>" value="<?= $tpi->get_nom();?>";
        </td>
        <td>
            <select class="field  form-control" id_tpi="<?= $tpi->get_id();?>" id="ma_app_<?= $tpi->get_id();?>">
                <?php


                echo "<option value=\"0\">---</option>";
                foreach($tab_ma_app AS $ma_app){
                    echo "<option value=\"".$ma_app['id_per']."\" ";
                    if($tpi->get_id_m_app() == $ma_app['id_per']){
                        echo " selected ";
                    }
                    //echo ($tpi->get_ma_app() == $ma_app['id_per']) ? " SELECTED " : "";
                    echo ">".$ma_app['nom_per']." ".$ma_app['prenom_per']."</option>";
                }
                ?>
            </select>
        </td>
        <td>
            <select class="field  form-control" id_tpi="<?= $tpi->get_id();?>" id="exp_1_<?= $tpi->get_id();?>">
                <?php
                $tab_exp = $per->get_all_expert_actif();
                echo "<option value=\"0\">---</option>";
                foreach($tab_exp AS $exp){
                    echo "<option value=\"".$exp['id_per']."\" ";
                    if($tpi->get_id_exp_1() == $exp['id_per']){
                        echo " selected ";
                    }
                    echo ">".$exp['nom_per']." ".$exp['prenom_per']."</option>";
                }
                ?>
            </select>
        </td>
        <td>
            <select class="field  form-control" id_tpi="<?= $tpi->get_id();?>" id="exp_2_<?= $tpi->get_id();?>">
                <?php
                echo "<option value=\"0\">---</option>";
                foreach($tab_exp AS $exp){
                    echo "<option value=\"".$exp['id_per']."\" ";
                    if($tpi->get_id_exp_2() == $exp['id_per']){
                        echo " selected ";
                    }
                    echo ">".$exp['nom_per']." ".$exp['prenom_per']."</option>";
                }
                ?>
            </select>
        </td>
		
        <td>
		<?php
		if($per->check_aut("ADM_TPI")){
		?>
            <form method="post" action="mail_info.php">
                <input type="hidden" name="id_tpi" value="<?= $tpi->get_id();?>" >
                <input type="submit" class="btn btn_mail_info" id_tpi="<?= $tpi->get_id();?>" value="mail">
            </form>
        </td>
		<?php
		}
		?>
    </tr>

    <?php

}


//print_r($tab);

?>

</table>
<script src="./js/tpi.js"></script>
</body>
</html>

<?php
session_start();
require("./../../config/config.inc.php");
$aut = "ADM_TPI;USR_TPI";
require(WAY . "/includes/secure.inc.php");
require_once(WAY . "/includes/autoload.inc.php");
$thm = new Theme();
$crt = new Critere();
$tpi = new Tpi($_POST['id_tpi']);

$tab_thm = $thm->get_all();

?>

<div class="container col-md-8">
<h3>
Critères d'évaluation du TPI de <?= $tpi->get_nom_can()." ".$tpi->get_prenom_can() ?>
</h3>

<nav>
    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item pointer" id="tab_crit_gen" id_tpi="<?= $tpi->get_id() ?>"><a class="nav-link" id="crit_gen">Critères généraux</a></li>
        <li class="nav-item active"><a class="nav-link" >Critères spécifiques</a></li>
        <li class="nav-item pull-right">
            <form class="form-inline">
                <label class="control-label" for="id_thm">Filtrer par thème</label>
                <select class="form-control" id="id_thm">
                    <?php
                    foreach($tab_thm AS $theme){
                        echo "<option value=\"".$theme['id_thm']."\">".$theme['nom_thm']."</option>";
                    }
                    ?>
                    <option value="<?= $theme['id_thm'] ?>"><?= $theme['nom_thm'] ?></option>
                </select>
            </form>
        </li>
    </ul>
</nav>

<?php
foreach($tab_thm AS $theme){
    echo "<table class=\"table table-bordered table-stripped theme thm_".$theme['id_thm']."\">";
    $tab_crt_by_thm = $crt->get_all_specifiques_by_thm($theme['id_thm']);
    foreach($tab_crt_by_thm AS $crit){
        $classBtn = "";
        $icon = "glyphicon-plus";
        if(!$tpi->crt_existe_pas($crit['id_crt'])){
                $classBtn = "btn-danger";
                $icon = "glyphicon-minus";
        }
?>
<tr class="titre critere_spec critere thm_<?= $crit['id_thm'] ?>">
    <th class="text-center"><?= $crit['code_crt'] ?></th>
    <th><?= $crit['nom_crt'] ?></th>
    <th class="basket_theme nom_theme text-center">
        <button class="btn btn-sm btn-default align-middle btn_basket btn_crt_<?= $crit['id_crt'] ?> <?= $classBtn ?>" id_crt="<?= $crit['id_crt'] ?>" id_tpi=" <?= $tpi->get_id() ?>">
            <span class="glyphicon <?= $icon ?> text-default"></span>
        </button>
    </th>
    <th class="nom_theme text-center"><?= $crit['nom_thm'] ?></th>
<tr>
    <tbody class="critere_spec">
    <tr class="description critere_spec thm_<?= $crit['id_thm'] ?>">
        <td colspan="4"><?= str_replace(chr(10),"<br>",$crit['description_crt'])?></td>
    </tr>
    <tr class="pt3 critere_spec thm_<?= $crit['id_thm'] ?>">
        <td>3&nbsp;points</td>
        <td colspan="4"><?= str_replace(chr(10),"<br>",$crit['pt_3'])?></td>
    </tr>
    <tr class="pt2 critere_spec thm_<?= $crit['id_thm'] ?>">
        <td>2&nbsp;points</td>
        <td colspan="4"><?= str_replace(chr(10),"<br>",$crit['pt_2'])?></td>
    </tr>
    <tr class="pt1 critere_spec thm_<?= $crit['id_thm'] ?>">
        <td>1&nbsp;points</td>
        <td colspan="4"><?= str_replace(chr(10),"<br>",$crit['pt_1'])?></td>
    </tr>
    <tr class="pt0 critere_spec thm_<?= $crit['id_thm'] ?>">
        <td>0&nbsp;point</td>
        <td colspan="4"><?= str_replace(chr(10),"<br>",$crit['pt_0'])?></td>
    </tr>
    </tbody>
    <?php
    }
    echo "</table>";
    }
    ?>
        <div id="selected_crt" class="container" id_tpi="<?= $tpi->get_id() ?>">
            <?php
                if(!isset($_POST["no_reload"])) {
                    ?>
                    <script>
                        $("#selected_crt").load(
                            "./load/selected_crt.load.php",
                            {
                                id_tpi: <?= $tpi->get_id() ?>
                            }
                        );
                    </script>
                    <?php
                }
            ?>
        </div>
    </div>
    <script src="./js/basket.js"></script>
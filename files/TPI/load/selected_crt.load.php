<?php
session_start();
require("./../../config/config.inc.php");
$aut = "ADM_TPI;USR_TPI";
require(WAY . "/includes/secure.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

$tpi = new Tpi($_POST['id_tpi']);
$criteres = $tpi->get_criteres();

define("ID_CAT_CRT_SPEC", 5);

/** Filtre les critères selon s'ils sont des critères spéciaux.
 * @param $var
 * @return bool
 */
function filter_array($var) {
    $critere = new Critere($var);
    if ($critere->get_id_cat() == ID_CAT_CRT_SPEC) {
        return true;
    }
    return false;
}

$fl_criteres = array_filter($criteres, "filter_array");

$needPlural = count($fl_criteres) > 1;
?>

<table id="table_sel_crt" class="table">
    <thead>
        <th colspan="4"><?= count($fl_criteres) ?> critère<?= $needPlural ? "s" : "" ?> sélectionné<?= $needPlural ? "s" : "" ?></th>
    </thead>
    <tbody>
    <?php
foreach ($fl_criteres as $crt) {
    $crt = new Critere($crt);
    $thm = new Theme($crt->get_id_thm());
    ?>
    <tr>
        <td><?= $crt->get_code() ?></td>
        <td><?= $thm->get_nom() ?></td>
        <td><?= $crt->get_nom() ?></td>
        <td>
            <button class="btn btn-sm btn-default align-middle btn_crt_<?= $crt->get_id() ?> btn-danger remove_item" id_crt="<?= $crt->get_id() ?>" id_tpi=" <?= $tpi->get_id() ?>">
                <span class="glyphicon glyphicon-minus text-default"></span>
            </button>
        </td>
    </tr>
    <?php
}
?>
    </tbody>
</table>
<script src="./js/selected_crt.js"></script>
<?php
session_start();
require("./../../config/config.inc.php");
$aut = "ADM_TPI;USR_TPI";
require(WAY . "/includes/secure.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

$tpi = new Tpi($_POST['id_tpi']);

$tab_crt = $tpi->get_criteres();

if(!$tab_crt){
    $tab_crt = $tpi->assoc_all_crt_tpi();
}

$rubrique = new Rubrique();
$categorie = new Categorie();
$tab_rub = $rubrique->get_all();
echo "<div class=\"container col-md-8\">";
if ($tpi->get_id_m_app() == $per->get_id() || $per->check_aut("ADM_TPI")) {

    if (!$tpi->get_crt_def()) {
        $class = "primary";
        $val = 1;
        $text = "Indiquer les critères comme définis";
    } else {
        $class = "danger";
        $val = 0;
        $text = "Indiquer les critères comme indéfinis";
    }
}
?>

<h3>
    Critères d'évaluation du TPI de <?= $tpi->get_nom_can()." ".$tpi->get_prenom_can() ?>
</h3>

<nav>
    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item active"><a class="nav-link" id="crit_gen">Critères généraux</a></li>
        <li class="nav-item pointer" id="tab_crit_spec" id_tpi="<?= $tpi->get_id() ?>"><a class="nav-link" >Critères spécifiques</a></li>
        <li class="nav-item pull-right"><button class="btn btn-<?= $class ?> set_crt_def" id_tpi="<?= $tpi->get_id() ?>" val="<?= $val ?>"><?= $text ?></button></li>
    </ul>
</nav>

<table class="table_crt table table-striped table-bordered table-hover">
<?php
$rub = $tab_rub[0];
//foreach($tab_rub AS $rub){
    echo "<tr>";
    echo "<th colspan='2'><h2>" . $rub['id_rub'] . ".".$rub['nom_rub']."</h2></th>";
    echo "</tr>";
    $tab_cat = $rubrique->get_all_cat($rub['id_rub']);
    foreach($tab_cat as $cat) {
        if($cat['id_cat'] != 5) {
            echo "<tr>";
            echo "<th colspan='2'>" . $cat['nom_cat'] . "</th>";
            echo "</tr>";
            $tab_all_crt = $categorie->get_all_criteres($cat['id_cat']);
            foreach ($tab_all_crt as $crt) {
                echo "<tr>";
                if (in_array($crt['id_crt'], $tab_crt)) {
                    echo "<td><b>" . $rub['id_rub'] . "." . $crt['code_crt'] . " " . $crt['nom_crt'] . "</b>";
                    echo "<br>" . $crt['description_crt'];
                    echo "</td>";
                    if ($tpi->get_id_m_app() == $per->get_id() || $per->check_aut("ADM_TPI")) {
                        echo "<td><button class=\"btn btn-danger del_crt\" id=\"crt_" . $crt['id_crt'] . "\" id_crt=\"" . $crt['id_crt'] . "\" id_tpi=\"" . $tpi->get_id() . "\">Retirer</button></td>";
                    }
                } else {
                    echo "<td>" . $rub['id_rub'] . "." . $crt['code_crt'] . " " . $crt['nom_crt'] . "</b>";
                    echo "<br>" . $crt['description_crt'];
                    echo "</td>";
                    if ($tpi->get_id_m_app() == $per->get_id() || $per->check_aut("ADM_TPI")) {
                        echo "<td><button class=\"btn btn-primary add_crt\" id=\"crt_" . $crt['id_crt'] . "\" id_crt=\"" . $crt['id_crt'] . "\" id_tpi=\"" . $tpi->get_id() . "\">Ajouter</button></td>";
                    }
                }
                echo "</tr>";
            }
        }
    }
//}
echo "</div>";
?>
</table>
<script src="./js/criteres.js"></script>
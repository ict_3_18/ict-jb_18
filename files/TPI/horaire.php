<?php
session_start();
require("./../config/config.inc.php");
$aut = "USR_TPI";
require(WAY . "/includes/secure.inc.php");
require_once(WAY . "/includes/autoload.inc.php");
require_once(WAY . "/includes/head.inc.php");
//require("./includes/navbar.inc.php");
//print_r($_POST);
$tpi_id = $_POST['id_tpi'];
$tpi = new Tpi($tpi_id);
$can = new Candidat($tpi->get_id_can());
$hor = new Horaire();

echo "<pre>";
print_r($can->get_classe());
print_r($tpi->get_id_can());
print_r($hor->get_horaire_cla($can->get_classe()));

echo "</pre>";
//print_r($tpi->get_horaire());
?>
<!--    <div>-->
<!--        <h1>Attention !</h1>-->
<!--        <p>L'horaire ci-dessous peut comporter des erreurs, merci de vous fier à l'horaire distribué par le-->
<!--            supérieur.<br> Merci</p>-->
<!--    </div>-->


    <div class="col h_general">
        <div class="row">
            <div class="col-md-6">
                <div class="col">
                    <h2>Candidat : <?= $tpi->get_prenom_can() . " " . $tpi->get_nom_can(); ?></h2>
                </div>
                <div class="col">
                    <h4>Classe : <?= $tpi->get_classe(); ?></h4>
                </div>
                <div class="col" id="date">
                    <div class="input-group input-daterange">
                        <div class="col-md-6"><label for="db_tpi">Début du TPI</label></div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <span id="label_datepicker" class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                <input class="form-control" id="datepicker_h_tpi" type="text" value="<?php echo date("d/m/Y"); ?>">
                                <input type="hidden" id="tpi_id" value="<?= $tpi_id; ?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-md-offset-6">
                    <button class="btn btn-primary" id="btn_gen_h_tpi">Générer</button>
                </div>
            </div>
        </div>
    </div>

    <script src="../plugins/datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="../plugins/datepicker/js/locales/bootstrap-datepicker.fr.js" type="text/javascript"></script>
    <script src="js/horaire.js"></script>
<?php
//echo $tpi->gen_horaire_tpi();
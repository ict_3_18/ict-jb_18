<?php
session_start();
require("./../config/config.inc.php");
$aut = "ADM_TPI;USR_TPI;";
require(WAY . "/includes/secure.inc.php");
require_once(WAY . "/includes/autoload.inc.php");
require_once(WAY . "/includes/head.inc.php");
//require("./includes/navbar.inc.php");

//define("DISPLAY_ERROR", 0);

// Type d'erreur à afficher
error_reporting(E_ALL);

$exa = new Examen();
$id_exa = $exa->get_next_tpi()['ref_exa'];

$tpi = new Tpi();

if($per->check_aut("ADM_TPI")){
	$tab_tpi =  $tpi->get_all_tpi_by_id_exa($id_exa);
}elseif($per->check_aut("USR_TPI")){	
	$tab_tpi = $tpi->get_all_tpi_by_id_exa_and_id_per($id_exa,$per->get_id());
}

?>
<div id="content_load">
<table class="table table-striped table-hover table-condensed">
    <tr>
        <th>Candidat</th>
        <th>Classe</th>
        <th>Lieu</th>
        <th>Début</th>
        <th>Sujet</th>
        <th>Supérieur</th>
        <th>Expert resp.</th>
        <th>Expert acc.</th>

        <th></th>
        <th></th>
        <th></th>
        <th></th>
    </tr>
<?php

foreach($tab_tpi AS $tp ) {
    $tpi = new Tpi($tp['id_tpi']);
    $can = new Candidat($tpi->get_id_can());
    $ma_app = new Personne($tpi->get_id_m_app());
    $exp_1 = new Personne($tpi->get_id_exp_1());
    $exp_2 = new Personne($tpi->get_id_exp_2());
    ?>
    <tr>
        <td><a href="mailto:<?= $can->get_email() ?>"><?= $tpi->get_nom_can() . " " . $tpi->get_prenom_can() ?></a></td>
        <td><?= $tpi->get_classe() ?></td>
        <td><?= $tpi->get_lieu() ?></td>
        <td>
            <?php
            if (strtotime($tpi->get_date_db()) <= time()) {
                echo "<b>" . strftime("%A %d %B", strtotime($tpi->get_date_db())) . "</b>";
            } else {
                echo strftime("%A %d %B", strtotime($tpi->get_date_db()));
            }
            ?>
        </td>
        <td><?= $tpi->get_nom() ?></td>

        <td><a href="mailto:<?= $ma_app->get_email() ?>"><?= $ma_app->get_nom() . " " . $ma_app->get_prenom() ?></a>
        </td>
        <td><a href="mailto:<?= $exp_1->get_email() ?>"><?= $exp_1->get_nom() . " " . $exp_1->get_prenom(); ?></a></td>
        <td><a href="mailto:<?= $exp_2->get_email() ?>"><?= $exp_2->get_nom() . " " . $exp_2->get_prenom(); ?></a></td>
        <td>
            <form method="post" action="../horaires/index.php">
                <input type="hidden" name="id_tpi" value="<?= $tpi->get_id(); ?>">
                <button class="btn btn_pdf_eval " id_tpi="<?= $tpi->get_id(); ?>" id="exp_2_<?= $tpi->get_id(); ?>">
                    Horaire
                </button>
            </form>
        </td>
        <td>
            <form method="post" action="../horaires/pdf/horaire_tpi.pdf.php" target="_blank">
                <input type="hidden" name="id_tpi" value="<?= $tpi->get_id(); ?>">
                <button class="btn btn_print_pdf" id_tpi="<?= $tpi->get_id(); ?>" id="exp_2_<?= $tpi->get_id(); ?>">
                    <img src="image/print_icon_20x20.png" alt="Print icon">
                </button>
            </form>
        </td>
        <td>
            <form method="post" action="upload.php">
                <input type="hidden" name="id_tpi" value="<?= $tpi->get_id(); ?>">
                <button class="btn btn_pdf_eval " id_tpi="<?= $tpi->get_id(); ?>" id="exp_2_<?= $tpi->get_id(); ?>">
                    Fichiers
                </button>
            </form>
        </td>
        <td>
            <?php
            if ($tpi->get_id_m_app() == $per->get_id() || $per->check_aut("ADM_TPI")) {
                $class = (!$tpi->get_crt_def() ? "warning" : "primary")
                ?>
                <button class="btn btn_choice_crt btn-<?= $class ?>" id_tpi="<?= $tpi->get_id(); ?>"
                        id="exp_2_<?= $tpi->get_id(); ?>">Critères
                </button>
                <!--<form method="post" action="load/criteres.php">
                <input type="hidden" name="id_tpi" value="<?= $tpi->get_id(); ?>">


            </form>-->
                <?php
            }
            ?>
        </td>
        <td>
            <form method="post" action="pdf/tpi.pdf.php">
                <input type="hidden" name="id_tpi" value="<?= $tpi->get_id(); ?>">
                <button class="btn btn_pdf_eval " id_tpi="<?= $tpi->get_id(); ?>" id="exp_2_<?= $tpi->get_id(); ?>">
                    Grille
                </button>
            </form>
        </td>
    </tr>
    <?php
}
?>
</table>
</div>
<script src="./js/tpi.js"></script>
</body>
</html>

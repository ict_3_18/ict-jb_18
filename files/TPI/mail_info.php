<?php
session_start();
require("./../config/config.inc.php");
$aut = "ADM_TPI";
require(WAY . "/includes/secure.inc.php");
require_once(WAY . "/includes/autoload.inc.php");
require_once(WAY . "/includes/head.inc.php");
//require("./includes/navbar.inc.php");
//print_r($_POST);

$tpi = new Tpi($_POST['id_tpi']);
//$tpi->mail_info();
    $ma_app = new Personne($tpi->get_id_m_app());
    $exp_1 = new Personne($tpi->get_id_exp_1());
    $exp_2 = new Personne($tpi->get_id_exp_2());
    $can = new Candidat($tpi->get_id_can());

    $emp = new Entreprise($can->get_employeur());
    $to = $ma_app->get_email() . ", " . $exp_1->get_email() . ", " . $exp_2->get_email();
    $subject = "TPI - " . $can->get_nom() . " - " . $tpi->get_nom();

    $message = $tpi->gen_message_attrib_tpi($can, $ma_app, $exp_1, $exp_2);

    if(isset($_POST["send"])) {
        //print_r($_POST);

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= "X-Mailer: PHP" . "\r\n";;

        if(isset($_POST["sender"])){
            $headers .= 'From: <' . $_POST["sender"] . '>' . "\r\n";
            $headers .= 'Reply-To: <' . $_POST["receiver"] . '>' . "\r\n";
            $headers .= 'Cc: ' . $_POST["cc"] . "\r\n";
        } else {
            $headers .= 'From: <adrienmatthieu.rossier@ceff.ch>' . "\r\n";
            $headers .= 'Reply-To: <adrienmatthieu.rossier@ceff.ch>' . "\r\n";
            $headers .= 'Cc: adrienmatthieu.rossier@ceff.ch' . "\r\n";
        }

        $date_time = date("D, d M Y H:i:s O");
        $headers .= 'DATE: ' . $date_time . "\r\n";

        $table = (include "./includes/personnes_table.include.php");

        $mail_body =
            $_POST["ex_tpi"] . "<br>" .
            $table . "<br>" .
            $_POST["lieu_tpi"] .
            $tpi->gen_horaire_tpi() .
            $_POST["footer_tpi"];
        if ($can->get_nom()) {
            mail($to, $subject, $message, $headers);
        }
    }
?>

<?php

//session_start();
require ('../../class//fpdf/fpdf.php');
require ('../../config/config.inc.php');
require_once (WAY . "./includes/autoload.inc.php");

$pdf = new FPDF;
$tpi = new Tpi($_POST['id_tpi']);
$can = new Candidat($tpi->get_id_can());
$exp_1 = new Personne($tpi->get_id_exp_1());
$exp_2 = new Personne($tpi->get_id_exp_2());
$m_app = new Personne($tpi->get_id_m_app());
$categorie = new Categorie();
$rubrique = new Rubrique();
$critere = new Critere();

$tab_crt_tpi = $tpi->get_criteres();

/////////////////////////////////////////////////PAGE 1/////////////////////////////////////////////////
$pdf->AddPage();
$pdf->SetFont('Arial', 'B', 12);
$pdf->SetTitle("Formulaire TPI", true);

//En-tête
$pdf->SetFont('Arial', '', 10);
$pdf->Cell(0, 10, utf8_decode("Formulaire d'évaluation des TPI pour informaticien-ne CFC"), 'B', 1);

//Titre
$pdf->SetFont('Arial', 'B', 14);
$pdf->MultiCell(0, 7, utf8_decode("Formulaire d'évaluation des TPI pour informaticien-ne CFC ordonnance 2014 à l'usage des experts et supérieurs professionnels"), 0, 1);

$pdf->SetFont('Arial', 'B', 10);
$pdf->Cell(10, 12, utf8_decode("TOUS LES TERMES UTILISES DOIVENT ETRE COMPRIS DANS LEUR SENS EPICENE."), 0, 1);

$pdf->SetFont('Arial', 'B', 10);
$pdf->Cell(0, 10, utf8_decode("Titre du TPI : ".$tpi->get_nom()), 'B', 1);
$pdf->Cell(0, 10, utf8_decode("Entreprise : "), 'B', 1);

//Tableau 1
//$header = array('','Nom', 'Prénom', 'Date', 'Signature');
$colgauche = array('Candidat', 'Prénom', 'Date', 'Signature');

//$pdf->Cell(30,5,"R",1);
$pdf->Cell(45, 5, utf8_decode(""), 1);
$pdf->Cell(65, 5, utf8_decode("Nom, prénom"), 1);
$pdf->Cell(40, 5, utf8_decode("Date"), 1);
$pdf->Cell(40, 5, utf8_decode("Signature"), 1, 1);

$pdf->Cell(45, 10, utf8_decode("Candidat"), 1);
$pdf->Cell(65, 10, utf8_decode($can->get_nom()." ".$can->get_prenom()), 1);
$pdf->Cell(40, 10, utf8_decode(""), 1);
$pdf->Cell(40, 10, utf8_decode(""), 1, 1);

$pdf->Cell(45, 10, utf8_decode("Supérieur"), 1);
$pdf->Cell(65, 10, utf8_decode($m_app->get_nom()." ".$m_app->get_prenom()), 1);
$pdf->Cell(40, 10, utf8_decode(""), 1);
$pdf->Cell(40, 10, utf8_decode(""), 1, 1);

$pdf->Cell(45, 10, utf8_decode("Expert responsable"), 1);
$pdf->Cell(65, 10, utf8_decode($exp_1->get_nom()." ".$exp_1->get_prenom()), 1);
$pdf->Cell(40, 10, utf8_decode(""), 1);
$pdf->Cell(40, 10, utf8_decode(""), 1, 1);

$pdf->Cell(45, 10, utf8_decode("Expert accompagnant"), 1);
$pdf->Cell(65, 10, utf8_decode($exp_2->get_nom()." ".$exp_2->get_prenom()), 1);
$pdf->Cell(40, 10, utf8_decode(""), 1);
$pdf->Cell(40, 10, utf8_decode(""), 1, 1);

$pdf->Cell(45, 10, utf8_decode("Expert en chef"), 1);
$pdf->Cell(65, 10, utf8_decode("Hirtzel Jacques"), 1);
$pdf->Cell(40, 10, utf8_decode(""), 1);
$pdf->Cell(40, 10, utf8_decode(""), 1, 1);

$liste2 = array('Appréciation :' => "Le niveau de compétence lié au critère (0-3 points par critère).",
    'Critères de détail : ' => "Selon l'énoncé du travail, ces critères peuvent être complétés ou non-évalués (biffer le critère non-retenu)",
    'Attribution des notes : ' => "Selon le barème fédéral, seules les notes entières et les demi-notes sont admises.",
    'Confidentialité : ' => "La communication aux candidats ou à des tiers d'informations concernant le résultat de toutes ou parties de la procédure"
    . " de qualification est strictement interdite. Seul le service cantonal responsable des procédures de qualification est habilité à transmettre les résultats.",
    'Attribution des points : ' => "Les points sont attribués en fonction de la distribution ci-dessous. L'interprétation de chaque critère est expliquée dans le Guide à l'usage des évaluateurs",
    'Calcul de la moyenne : ' => "La moyenne est calculée sur la base des points obtenus par rapport à la somme des points possibles.");

foreach ($liste2 as $key => $texte) {
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Cell(40, 6, utf8_decode($key), 0);
    $pdf->SetFont('Arial', '', 10);
    $pdf->MultiCell(0, 6, utf8_decode($texte), 0, 'J');
   // $pdf->Ln();
}

$pdf->SetFont('Arial', 'B', 10);
$pdf->Ln();
$pdf->MultiCell(0, 5, utf8_decode("Le site tpi.ict-jb.net fournit les explications nécessaires sur la manière d'évaluer les critères."), 0);
$pdf->Ln();

$pdf->MultiCell(0, 5, utf8_decode("Toutes les annotations, remarques et supports divers permettant de justifier précisément le résultat doivent être remis à l'expert en chef avec le présent document."), 0);


$tab = array(
    array('Très bon, quantitativement et qualitativement', 'Complètement rempli(s)', 'Acquise', '3'),
    array('Suffisant - Bon', 'Majoritairement rempli(s)', 'Acquise', '2'),
    array('Insuffisant', 'Partiellement rempli(s)', 'Non-acquise', '1'),
    array('Inutilisable ou non exécuté', 'Pas ou très peu rempli(s)', 'Non-acquise', '0')
);

$pdf->Ln(10);

$pdf->Cell(90, 5, utf8_decode("Appréciation"), 1);
$pdf->Cell(50, 5, utf8_decode("Critère(s)"), 1);
$pdf->Cell(40, 5, utf8_decode("Compétence"), 1);
$pdf->Cell(10, 5, utf8_decode("Pt(s)"), 1, 1);

$pdf->SetFont('Arial', '', 10);

foreach ($tab as $ligne) {
    $pdf->Cell(90, 8, utf8_decode($ligne[0]), 1, 0);
    $pdf->Cell(50, 8, utf8_decode($ligne[1]), 1, 0);
    $pdf->Cell(40, 8, utf8_decode($ligne[2]), 1, 0);
    $pdf->Cell(10, 8, utf8_decode($ligne[3]), 1, 1);
}

$tab_evaluateurs[0] = "Appréciation du supérieur professionnel, validation par les experts";
$tab_evaluateurs[1] = "Appréciation du supérieur professionnel et des experts";
$tab_evaluateurs[2] = "Appréciation des experts";

$tab_size[0] = 210;
$tab_size[1] = 210;
$tab_size[2] = 165;

for($i=0;$i<3;$i++){

    ///////////////////////////////////////////////// PAGES /////////////////////////////////////////////////
    $tab_rub = $rubrique->get_all();
    $tab_cat = $rubrique->get_all_cat($tab_rub[$i]['id_rub']);


    //Nom de la rubrique
    $nom_rub = $tab_rub[$i]['num_rub'] . ". " . $tab_rub[$i]['nom_rub'];

    /////Début de la page/////



    $pdf->AddPage();
    $pdf->SetFont('Arial', 'B', 12);

    //En-tête
    $pdf->SetFont('Arial', '', 10);
    $pdf->Cell(0, 10, utf8_decode("Formulaire d'évaluation des TPI pour informaticien-ne CFC"), 'B', 1);

    $pdf->Cell(100, 12, utf8_decode($tab_evaluateurs[$i]), 0, 1);

    $pdf->SetFont('Arial', 'B', 14);
    $pdf->Cell(0, 10, utf8_decode($nom_rub), 0, 1);


    $pdf->SetFont('Arial', '', 10);
    $pdf->Cell(89, 6, utf8_decode("Critères évalués"), 1, 0);
    $pdf->Cell(12, 6, utf8_decode("Points"), 1, 0, 'C');
    $pdf->Cell(89, 6, utf8_decode("Motif de la déduction de point(s)"), 1, 1);

    $size=0;
    $nb=0;
    foreach ($tab_cat as $cat) {
        $size += 6;
        $nb++;
        $tab_crt = $categorie->get_all_criteres($cat['id_cat']);
        foreach($tab_crt AS $crt) {
            if(in_array($crt['id_crt'],$tab_crt_tpi)) {
                $size += $crt['size_crt'];
                $nb++;
            }
        }
    }
    $offset =  round(($tab_size[$i]-$size)/$nb);


    $pdf->SetFont('Arial', '', 10);
    $posXmulti = $pdf->GetX();
    $posYmulti = $pdf->GetY();
    $pos_ori_x = $posXmulti;
    $pos_ori_y = $posYmulti;
    $nb = 0;
    foreach ($tab_cat as $cat) {
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->MultiCell(101, 6, utf8_decode($cat['nom_cat']), 1, 'L');
        $pdf->SetFont('Arial', '', 10);
        $tab_crt = $categorie->get_all_criteres($cat['id_cat']);
        foreach($tab_crt AS $crt){
            if(in_array($crt['id_crt'],$tab_crt_tpi)) {
                $nb++;
                //$pdf->SetXY($posXmulti, $posYmulti);
                $posY = $pdf->GetY();
                $pdf->MultiCell(89, 6, $tab_rub[$i]['num_rub'] . "." . $crt['code_crt'] . " " . utf8_decode($crt['nom_crt']), 0, 'L');

                if ($i == 2 && $crt['code_crt'] == 5) {
                    $texte = utf8_decode("Utiliser le formulaire d'entretien en annexe pour tenir le  PV (questions-réponses) de l'entretien professionnel. Puis noter les critères liés aux questions posées ainsi que le nombre de points attribuées en fonction de la précision des réponses fournies dans le tableau ci-contre");
                    $pdf->SetXY($posXmulti + 101, $posY);
                    $pdf->MultiCell(89, 6, $texte, 0, 'L');
                }
                $pdf->SetXY($posXmulti, $posY);
                $pdf->Cell(89, $crt['size_crt'] + $offset, '', 1, 1);
                $posXmulti = $pdf->GetX();
                $posYmulti = $pdf->GetY();
                $pdf->SetXY($posXmulti + 89, $posY);
                //$pdf->Cell(12, 6, utf8_decode(""), 1, 1);
                $pdf->Cell(12, $crt['size_crt'] + $offset, '', 1, 1);
                // $critere = new Critere($crt['id_crt']);
                // $critere->set_size(($posYmulti-$posY));
                $pdf->SetXY($posXmulti, $posYmulti);
            }
        }
    }
    $pos_final_x  =$pdf->GetX();
    $pos_final_y  =$pdf->GetY();
    $pdf->SetXY($pos_ori_x+101, $pos_ori_y);
    $size_y = $pos_final_y-$pos_ori_y;
    $texte = "";

    $pdf->cell(89,$size_y,'',1,1);

    //Tableau calcul de points
  //  $pdf->SetY(250);

    /*$tab = array(
        array(, ' ', $nb*3),
        array(, 'B', ''),
        array("Note attribuée aux compétences professionnelles", '(5/'.($nb*3).'*B)+1', '')
    );*/
  //  $pdf->Cell(170, 8, utf8_decode("Nombre de points totaux possibles (3 X nombre de critères utilisés)"), 1, 0);
//    $pdf->Cell(20, 8, $nb*3, 1, 1, 'C');
    $pdf->Cell(120, 8, utf8_decode("Nombre de points obtenus"), 1, 0);
    $pdf->Cell(40, 8, utf8_decode("A =>"), 1, 0, 'C');
    $pdf->Cell(30, 8, utf8_decode("      / ".($nb*3)), 1, 1, 'C');
    $pdf->Cell(120, 8, utf8_decode("Note attribuée aux compétences professionnelles"), 1, 0);
    $pdf->Cell(40, 8, utf8_decode("(5 / ".($nb*3)." * A) + 1"), 1, 0, 'C');
    $pdf->Cell(30, 8, utf8_decode(""), 1, 1, 'C');
    $pdf->Cell(160, 8, utf8_decode("Note arrondie au demi point => "), 1, 0, 'R');
    $pdf->Cell(30, 8, utf8_decode(""), 1, 1, 'C');
    $pdf->SetFont('Arial', '', 12);
    /*foreach ($tab as $ligne) {
        $pdf->Cell(150, 8, utf8_decode($ligne[0]), 1, 0);
        $pdf->Cell(20, 8, utf8_decode($ligne[1]), 1, 0, 'C');
        $pdf->Cell(20, 8, utf8_decode($ligne[2]), 1, 1,'C');
    }*/
}


/////////////////////////////////////////////////PAGE 4/////////////////////////////////////////////////
//$pdf->AddPage();
  //Tableau calcul de la note finale
  $tab = array(
  array('1. Compétences professionnelles (50%, inscrire 2x)', ''),
  array('1. Compétences professionnelles (50%, inscrire 2x)', ''),
  array('2. Documentation (25%)', ''),
  array('3. Entretien et présentation (25%)', '')
  );


  $pdf->SetY(225);

  $pdf->SetFont('Arial', 'B', 14);
  $pdf->Cell(00, 8, utf8_decode('Calcul de la note finale'), 0, 1, 'C');

  $pdf->SetFont('Arial', '', 12);

  foreach ($tab as $ligne) {
  $pdf->Cell(150, 8, utf8_decode($ligne[0]), 1, 0);
  $pdf->Cell(40, 8, utf8_decode($ligne[1]), 1, 1);
  //$pdf->Cell(20, 8, utf8_decode($ligne[2]), 1, 1);
  }

    $pdf->SetFont('Arial', 'B', 12);


  $pdf->Cell(150, 8, utf8_decode('Note finale du TPI (moyenne au dixième)'), 1, 0, 'R');
  $pdf->Cell(40, 8, utf8_decode(''), 1, 1);


$pdf->Output();
?>